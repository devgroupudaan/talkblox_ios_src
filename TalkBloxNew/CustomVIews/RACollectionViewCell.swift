//
//  RACollectionViewCell.swift
//  TalkBlox
//
//  Created by Mac on 04/03/16.
//  Copyright © 2016 MobiWebTech. All rights reserved.
//

import Foundation
import UIKit

class RACollectionViewCell: UICollectionViewCell
{
    var imageView: UIImageView!
    var btnBlox : UIButton!
//    var delBlox : UIButton!
    var gradientLayer: CAGradientLayer?
    var hilightedCover: UIView!
    override var isHighlighted: Bool {
        didSet {
            self.hilightedCover.isHidden = !self.isHighlighted
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configure()
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.btnBlox.frame = self.bounds
        self.imageView.frame = self.bounds
        self.hilightedCover.frame = self.bounds
        
//        self.delBlox.frame = CGRect(x: self.frame.width - 30, y: 0, width: 30, height: 30)
        //self.applyGradation(self.imageView)
    }
    
    fileprivate func configure()
    {
        self.imageView = UIImageView()
        self.imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.imageView.contentMode = UIView.ContentMode.scaleAspectFill
        self.addSubview(self.imageView)
        
        self.btnBlox = UIButton()
        self.btnBlox.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.btnBlox.contentMode = UIView.ContentMode.scaleAspectFill
        self.addSubview(self.btnBlox)
        
        self.hilightedCover = UIView()
        self.hilightedCover.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.hilightedCover.backgroundColor = UIColor.clear
        self.hilightedCover.isHidden = true
        self.addSubview(self.hilightedCover)
        
//        self.delBlox = UIButton()
//        self.delBlox.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
//        self.delBlox.setTitle("", for: UIControl.State())
//        self.addSubview(self.delBlox)
    }
    
    fileprivate func applyGradation(_ gradientView: UIView!) {
        self.gradientLayer?.removeFromSuperlayer()
        self.gradientLayer = nil
        
        self.gradientLayer = CAGradientLayer()
        self.gradientLayer!.frame = gradientView.bounds
        
        let mainColor = UIColor(white: 0, alpha: 0.3).cgColor
        let subColor = UIColor.clear.cgColor
        self.gradientLayer!.colors = [subColor, mainColor]
        self.gradientLayer!.locations = [0, 1]
        
        gradientView.layer.addSublayer(self.gradientLayer!)
    }
}
