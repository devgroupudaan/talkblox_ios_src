//
//  GalleryCell.swift
//  TalkBlox
//
//  Created by Mac on 15/12/15.
//  Copyright © 2015 MobiWebTech. All rights reserved.
//

import Foundation
import UIKit

class GalleryCell: UICollectionViewCell
{
    @IBOutlet var imgItem : UIImageView!
    @IBOutlet var lblText : UILabel!
    @IBOutlet var lblTittleText : UILabel!
    
    
    
    convenience init()
    {
        self.init()
        print("Default init")
    }
}
