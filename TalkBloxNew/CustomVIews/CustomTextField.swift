//
//  CustomTextField.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/3/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
   
    @IBInspectable public var 	placeholderColor: UIColor = .white
    override func layoutSubviews() {
        self.attributedPlaceholder =  NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: UIColor.white])
    }
}
