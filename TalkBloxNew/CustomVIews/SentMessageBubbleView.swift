//
//  MessageBubbleView.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 28/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit

class SentMessageBubbleView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func draw(_ rect: CGRect) {
        let bezierPath = UIBezierPath()
        //Draw main body
//        bezierPath.move(to: CGPoint(x: rect.minX, y: rect.minY))
//        bezierPath.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
//        bezierPath.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 10.0))
//        bezierPath.addLine(to: CGPoint(x: rect.minX, y: rect.maxY - 10.0))
//        bezierPath.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
//        //Draw the tail
//        bezierPath.move(to: CGPoint(x: rect.maxX - 25.0, y: rect.maxY - 10.0))
//        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY))
//        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY - 10.0))
//        UIColor.lightGray.setFill()
//        bezierPath.fill()
//        bezierPath.close()
        
        bezierPath.move(to: CGPoint(x: rect.minX, y: rect.minY))
        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.minY))
        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY))
        bezierPath.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        bezierPath.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        //Draw the tail
        bezierPath.move(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY - 25.0))
        bezierPath.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 10.0))
        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY - 10.0))
        UIColor.darkGray.setFill()
        bezierPath.fill()
        bezierPath.close()
    }
}
