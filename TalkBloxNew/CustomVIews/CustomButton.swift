////
////  CustomButton.swift
////  TalkBloxNew
////
////  Created by Codezilla-7 on 5/3/19.
////  Copyright © 2019 iOSDeveloper. All rights reserved.
////
//
//import UIKit
//
//class CustomButton: UIButton {
//    @IBInspectable public var shadowColor: UIColor = .black
//    @IBInspectable public var shadowRadius: CGFloat = 3.0
//    @IBInspectable public var opacity: Float = 3.0
//    @IBInspectable public var shadowOffsetWidth: Int = 4
//    @IBInspectable public var shadowOffsetHieght: Int = 4
//    @IBInspectable public var borderColor: UIColor = .black
//    @IBInspectable public var borderWidth: CGFloat = 0.0
//    @IBInspectable public var CornerRadius: CGFloat = 0.0
//
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        self.layer.shadowColor = shadowColor.cgColor
//        self.layer.shadowRadius = shadowRadius
//        self.layer.shadowOpacity = opacity
//        self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHieght)
//        self.layer.borderColor = borderColor.cgColor
//        self.layer.borderWidth = borderWidth
//        self.layer.cornerRadius = CornerRadius
//        self.clipsToBounds = true
//        self.layer.masksToBounds = false
//    }
//
//}
