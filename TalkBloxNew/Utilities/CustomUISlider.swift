//
//  CustomUISlider.swift
//  TalkBlox
//
//  Created by mac on 7/20/16.
//  Copyright © 2016 MobiWebTech. All rights reserved.
//

import UIKit

class CustomUISlider: UISlider {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        //keeps original origin and width, changes height, you get the idea
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 2.0))
        super.trackRect(forBounds: customBounds)
        
        var newBounds = super.trackRect(forBounds: bounds)
        newBounds.size.height = 10
        
        return customBounds
    }
    
    //while we are here, why not change the image here as well? (bonus material)
    override func awakeFromNib() {
        self.setThumbImage(UIImage(named: "sliderCircle"), for: UIControl.State())
        super.awakeFromNib()
    }
    

    
    
}
