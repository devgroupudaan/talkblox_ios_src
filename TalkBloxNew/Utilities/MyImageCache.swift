//
//  MyImageCache.swift
//  TalkBlox
//
//  Created by Mac on 04/02/16.
//  Copyright © 2016 MobiWebTech. All rights reserved.
//

import Foundation
class MyImageCache {
    
    static let sharedCache: NSCache = { () -> NSCache<AnyObject, AnyObject> in 
        let cache = NSCache<AnyObject, AnyObject>()
        cache.name = "MyImageCache"
        cache.countLimit = 20 // Max 20 images in memory.
        cache.totalCostLimit = 10*1024*1024 // Max 10MB used.
        return cache
    }()
    
}
