//
//  AppTheme.swift
//  TalkBox
//
//  Created by mac on 11/20/15.
//  Copyright © 2015 MobiWebTech. All rights reserved.
//

//define


import Foundation
import UIKit
import Alamofire
import JGProgressHUD
import AVKit
 
let M_PI:Float = 180

class AppTheme: NSObject
{
//    enum Method: String
//    {
//        case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
//    }
    
    var AppUrl : String = "http://talkblox.com/api/"
    //var AppUrl : String = "http://45.79.153.165:81/api/"
    
    var themeColorBlue : UIColor = UIColor(red: 0.0/255.0, green: 161.0/255.0, blue: 228.0/255.0, alpha: 1)
    var themeColorGreen : UIColor = UIColor(red: 46.0/255.0, green: 236.0/255.0, blue: 16.0/255.0, alpha: 1)
    
    
    func getImageFromUrl(_ urlString : String) -> UIImage {
        var urlStringValue = urlString
        urlStringValue = urlStringValue.replacingOccurrences(of: " ", with: "%20")
        var image : UIImage = #imageLiteral(resourceName: "signup_background") //UIImage(named: "preview_layer")!
        if(urlStringValue.contains(".mp4"))
        {
            var completeURL = WEB_URL.BaseURL + urlString
            if urlString.hasPrefix("https://api.talkblox.info/") {
                completeURL = urlString
            }
            let completeURLStr = completeURL.replacingOccurrences(of: " ", with: "%20")
            do {
                let asset = AVAsset(url: URL(string: completeURLStr)!)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let timestamp = CMTimeMakeWithSeconds(0.1, preferredTimescale: 600)
                print("Timestemp:   \(timestamp)")
                let cgImage = try imgGenerator.copyCGImage(at: timestamp, actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                return thumbnail
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
                return image
            }
        }
        let urlNew:String = urlStringValue//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if let imageData = try? Data(contentsOf: URL(string: urlNew)!)
        {
            let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
            print("isAnimated: \(isImageAnimated)")
            if isImageAnimated {
                image = UIImage.gifImageWithData(imageData)!
            } else {
                image = UIImage(data: imageData)!
            }
            print(imageData.count)
            return image
        }
        else
        {
            return image
        }
    }
    
    func showAlert(_ errorMessage : String , errorTitle : String) -> UIAlertController
    {
        let errorAlert : UIAlertController = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        errorAlert.view.backgroundColor = UIColor.white
        drawBorder(errorAlert.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
        let OKAction = UIAlertAction(title: "OK", style: .default)
            {
                (action) in
                // ...
            }
        errorAlert.addAction(OKAction)
        /*
        self.presentViewController(errorAlert, animated: true)
            {
                // ...
        }
        */
        return errorAlert
    }
    
    func drawBorder(_ view: UIView, color: UIColor, borderWidth:CGFloat, cornerRadius:CGFloat)
    {
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
    }
    
//    func drawShadow(_ viewToAddLayer : AnyObject)
//    {
//        if #available(iOS 13.0, *) {
//            let layer = viewToAddLayer.layer
//            layer?.shadowColor = UIColor.black.cgColor
//            layer?.shadowOffset = CGSize(width: 10, height: 10)
//            layer?.shadowOpacity = 0.7
//            layer?.shadowRadius = 5
//        } else {
//            // Fallback on earlier versions
//        }
//        
//       
//    }
    
    func setNavigationBarTitleImage() -> UIImageView
    {
        var titleView : UIImageView
        // set the dimensions you want here
        titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 80, height: 22))
        // Set how do you want to maintain the aspect
        titleView.contentMode = .scaleAspectFill
        titleView.image = UIImage(named: "TalkbloxLogo")
        return titleView
        
    }
    
    /*
    func setNavigationBarTitleImageButton() -> UIButton
    {
        let titleView : UIButton = UIButton()
        
        // set the dimensions you want here
        titleView.setImage(UIImage(named: "Talkblox"), forState: UIControlState.normal)
        // Set how do you want to maintain the aspect
        titleView.contentMode = .ScaleAspectFit
        titleView.frame = CGRectMake(0, 0, 80, 25)
        
        titleView.addTarget(self, action: "showEliteHome:", forControlEvents: UIControlEvents.TouchUpInside)
        
        return titleView
    }
    */
    
    func setTableProperties(_ table: UITableView) -> UITableView
    {
        table.autoresizingMask = UIView.AutoresizingMask.flexibleWidth
        
        table.layer.borderColor = themeColorBlue.cgColor
        table.layer.borderWidth = 1
        table.separatorColor = themeColorBlue
        table.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 15);
        table.tableFooterView = UIView()
        
        return table
    }
    
    func callGetServiceForDownload(_ strFileURL:String, fileNameToBeSaved:String,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        print("download service called")
        let strFillStatus = killFileOnPath(fileNameToBeSaved)
        if(strFillStatus == "File Killed" || strFillStatus == "No file"){
            
            let destination: DownloadRequest.Destination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(fileNameToBeSaved)
                return (documentsURL, [.removePreviousFile])
            }
//             Alamofire.download(
//                strFileURL,
//                method: .get,
//                encoding: JSONEncoding.default,
//                headers: nil,
//                to: destination).downloadProgress(closure: { (progress) in
//                    //progress closure
//                }).response(completionHandler: { (DefaultDownloadResponse) in
//                    //here you able to access the DefaultDownloadResponse
//                    //result closure
//
//                    if (DefaultDownloadResponse.error != nil) {
//                        completion("error", DefaultDownloadResponse.error.debugDescription as AnyObject)
//                    }else{
//                        completion("success", fileNameToBeSaved as AnyObject)
//                    }
//                })
            
            AF.download(
                strFileURL,
                method: .get,
                encoding: JSONEncoding.default,
                headers: nil,
                to: destination).downloadProgress(closure: { (progress) in
                    //progress closure
                }).response(completionHandler: { (DefaultDownloadResponse) in
                    //here you able to access the DefaultDownloadResponse
                    //result closure
                    
                    if (DefaultDownloadResponse.error != nil) {
                        completion("error", DefaultDownloadResponse.error.debugDescription as AnyObject)
                    }else{
                        completion("success", fileNameToBeSaved as AnyObject)
                    }
                })
        
            
        }else {
            print("No File Found,....")
        }
    }
    
    func callPostService(_ urlString: String, param : [String: AnyObject]?,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
//        Alamofire.request(urlString, method: .post, parameters: param, headers: nil).responseJSON {
//                response in
//                print(response);
//                let JSON = response.result.value
//                if let dict: NSDictionary = JSON as? NSDictionary
//                {
//                    if(dict["status"] as! Bool == true) {
//                        completion("success", JSON as AnyObject)
//                    } else if (dict["success"] as! Bool == true) {
//                        completion("success", JSON as AnyObject)
//                    } else {
//                        if let errorMessage : String = dict["message"] as? String {
//                            completion("error", errorMessage.capitalized as AnyObject)
//                        } else {
//                            completion("error", "Getting Error, Please try again." as AnyObject)
//                        }
//                    }
//                } else {
//                    completion("error", "Getting Error, Please try again." as AnyObject)
//                }
//        }
        AF.request(urlString, method: .post, parameters: param, headers: nil).responseJSON { (response) in
                switch response.result {
                              case .success(let value):
                                  if let JSON = value as? [String: Any] {
                                    if let dict: NSDictionary = JSON as? NSDictionary
                                    {
                                        if(dict["status"] as! Bool == true) {
                                            completion("success", JSON as AnyObject)
                                        } else if (dict["success"] as! Bool == true) {
                                            completion("success", JSON as AnyObject)
                                        } else {
                                            if let errorMessage : String = dict["message"] as? String {
                                                completion("error", errorMessage.capitalized as AnyObject)
                                            } else {
                                                completion("error", "Getting Error, Please try again." as AnyObject)
                                            }
                                        }
                                    } else {
                                        completion("error", "Getting Error, Please try again." as AnyObject)
                                    }
                                  }
                              case .failure(let error): break
                                  // error handling
                                print(error)
                                completion("error", "Getting Error, Please try again." as AnyObject)
                              }
        }
    }
    
    func callGetService(_ urlString: String, param : [String: AnyObject]?,  completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
//        Alamofire.request(urlString, method: .get, parameters:param).responseJSON {
//                response in
//                //******
//                let JSON = response.result.value
//                    if let dict: NSDictionary = JSON as? NSDictionary {
//                        if (dict.allKeys as NSArray).contains("status") {
//                            if let status : Bool = dict.object(forKey: "status") as? Bool {
//                                if(status) {
//                                completion("success", dict)
//                                } else {
//                                   completion("error", dict)
//                                }
//                            }
//                        } else if (dict.allKeys as NSArray).contains("success") {
//                            if let status : Bool = dict.object(forKey: "success") as? Bool {
//                                if(status) {
//                                    completion("success", dict)
//                                } else {
//                                    completion("error", dict)
//                                }
//                            }
//                        }
//                    }
//                    else
//                    {
//                       /* if let errorMessage : String = JSON["message"] as? String
//                        {
//                            completion("error", errorMessage.capitalized as AnyObject)
//                        }
//                        else
//                        {
//                            completion("error", "Error in fetching data" as AnyObject)
//                        }*/
//                    }
//
//
////                else
////                {
////                    completion("error", "Error in fetching data" as AnyObject)
////
////                }
//
//
//                /*
//                if let JSON: AnyObject = response.result.value
//                {
//                    if let dict: NSDictionary = JSON as? NSDictionary
//                    {
//                        if(dict.count > 1)
//                        {
//                            completion(result: "success", data: dict)
//                        }
//                            else if(JSON.count > 1)
//                        {
//                                completion(result: "success", data: JSON)
//                        }
//                        else
//                        {
//                            completion(result: "error", data: "Getting Error, Please try again.")
//                        }
//                    }
//                    else
//                    {
//                        completion(result: "error", data: "Getting Error, Please try again.")
//                    }
//
//                }
//                else
//                {
//                    completion(result: "error", data: "Getting Error, Please try again.")
//                }
//                */
//        }
        
        
        
        AF.request(urlString, method: .get, parameters:param).responseJSON { (response) in
                switch response.result {
                              case .success(let value):
                                  if let JSON = value as? [String: Any] {
                                    if let dict: NSDictionary = JSON as? NSDictionary {
                                        if (dict.allKeys as NSArray).contains("status") {
                                            if let status : Bool = dict.object(forKey: "status") as? Bool {
                                                if(status) {
                                                completion("success", dict)
                                                } else {
                                                   completion("error", dict)
                                                }
                                            }
                                        } else if (dict.allKeys as NSArray).contains("success") {
                                            if let status : Bool = dict.object(forKey: "success") as? Bool {
                                                if(status) {
                                                    completion("success", dict)
                                                } else {
                                                    completion("error", dict)
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                       /* if let errorMessage : String = JSON["message"] as? String
                                        {
                                            completion("error", errorMessage.capitalized as AnyObject)
                                        }
                                        else
                                        {
                                            completion("error", "Error in fetching data" as AnyObject)
                                        }*/
                                    }
                                  }
                              case .failure(let error): break
                                  // error handling
                                print(error)
                                completion("error", "Getting Error, Please try again." as AnyObject)
                              }
        }
    }
    
    
    //MARK: Upload Image Data
    func uploadImageAndData(_ strURL : String, parameters : [String:AnyObject], uploadDataArray : NSMutableArray, dataType : String, serviceType : String , contactIds:Dictionary<String, AnyObject>,completion: @escaping (_ result: String, _ data : AnyObject) -> Void){
        
        //let URL = strURL
        /*
        Alamofire.upload(.POST, URL, multipartFormData:
            {
            multipartFormData in
            
            if(uploadDataArray.count > 0)
            {
                print("uploadDataArray count :- %@", uploadDataArray.count)
                //var i : Int = 0
      
                for var i : Int = 0; i < uploadDataArray.count; i++
                {
                    //i++
                    print("in for condition for image")
                    if  let imageData : NSData = uploadDataArray.objectAtIndex(i) as? NSData
                    {
                        print("have image data to send")
                        /*original*/
                       // multipartFormData.appendBodyPart(data: imageData, name: "media[]", fileName: String(format: "file%d.png", i) , mimeType: "image/png")
                        
                        // multipartFormData.appendBodyPart(data: imageData, name:String(format: "media[%d]", i) , fileName:"file.png", mimeType: "image/png")
                        
                         multipartFormData.appendBodyPart(data: imageData, name: "media[]", fileName: String(format: "file%d.png", i) , mimeType: "image/png")
                        

                        
                        
//[formData appendPartWithFileData:imgData name:[NSString stringWithFormat:@"candidates_image%d",count] fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                    }
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
            }
            
            }, encodingCompletion: {
                encodingResult in
                
                switch encodingResult {
                case .Success(let upload, _, _):
                    print("s")
                    upload.responseJSON { response in
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON:NSDictionary = response.result.value as NSDictionary {
                            print("JSON: \(JSON)")
                            if let dict: NSDictionary = JSON as? NSDictionary
                            {
                                print(dict)
                                if(dict["success"] as! Bool == true)
                                {
                                    completion(result: "success", data: JSON)
                                }
                                else
                                {
                                    completion(result: "error", data: JSON)
                                }
                            }
                            
                        }
                    }
                    
                case .Failure(let encodingError):
                    print(encodingError)
                }
        })
        */
        
        /*let urlRequest = urlRequestWithComponents(strURL, parameters: parameters, aryImageData: uploadDataArray, serviceType: serviceType,contactIds: contactIds)
        
        //print(urlRequest)
       // print(urlRequest.0)
        //print(urlRequest.1)
        
        upload(urlRequest.0, data: urlRequest.1).responseJSON
            {
                response in
                
                if let JSON:NSDictionary = response.result.value as NSDictionary {
                    print("JSON: \(JSON)")
                    if let dict: NSDictionary = JSON as? NSDictionary
                    {
                        //print(dict)
                        if let str : String = dict["error"] as? String
                        {
                            completion(result: "error", data: str)
                        }
                        else if let status : Bool = dict["success"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else if let status : Bool = dict["status"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else
                        {
                            completion(result: "error", data: dict.valueForKey("message")!)
                        }
                    }
                    
                }
                else
                {
                    completion(result: "error", data: "Getting Error, Please try again.")
                }
            }*/
        
            /*
            .responseString{ response in
            switch response.result {
            case .Success:
                print(response.response?.statusCode)
                
            case .Failure(let error):
                print(error)
            }
        }
        */
        
        /*
        Alamofire.upload(<#T##method: Method##Method#>, <#T##URLString: URLStringConvertible##URLStringConvertible#>, multipartFormData: <#T##MultipartFormData -> Void#>, encodingCompletion: <#T##(Manager.MultipartFormDataEncodingResult -> Void)?##(Manager.MultipartFormDataEncodingResult -> Void)?##Manager.MultipartFormDataEncodingResult -> Void#>)
        
        upload(<#T##URLRequest: URLRequestConvertible##URLRequestConvertible#>, multipartFormData: <#T##MultipartFormData -> Void#>, encodingCompletion: <#T##(Manager.MultipartFormDataEncodingResult -> Void)?##(Manager.MultipartFormDataEncodingResult -> Void)?##Manager.MultipartFormDataEncodingResult -> Void#>)
        
        */
        
        
           
    }
    
    //MARK: Sending image in multipart
    //TODO:
   /*func urlRequestWithComponents(_ urlString:String, parameters:Dictionary<String, AnyObject>, aryImageData:NSMutableArray, serviceType : String , contactIds:Dictionary<String, AnyObject>) -> (URLRequestConvertible, NSData) {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        if(serviceType == "POST")
        {
            mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        }
        else
        {
             mutableURLRequest.httpMethod = Alamofire.HTTPMethod.put.rawValue
        }
        
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        if(aryImageData.count > 0)
        {
            if(serviceType == "POST")
            {
            
            for imageData in aryImageData
            {
                uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"media[]\"; filename=\"file.png\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append((imageData as! NSData) as Data)
            }
            }
            else
            {
                for imageData in aryImageData
                {
                    uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"file.png\"\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append((imageData as! NSData) as Data)
                }
            }
        }
        
        // add parameters
        for (key, value) in parameters
        {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        //uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        //add contacts 
        if(serviceType == "POST")
        {
        for (key, value) in contactIds
        {
            print(key)
            if let aryContactData:Dictionary<String, AnyObject> = value as? Dictionary<String, AnyObject>
            {
                for (key, value) in aryContactData
                {
                    uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                }
            }
        }
       
        }
        
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.encode(mutableURLRequest as! ParameterEncoding) as! URLRequestConvertible, uploadData)
    }*/
    
    //MARK: Uploading Video Data
    func uploadVideoAndData(_ strURL : String, parameters : [String:AnyObject], uploadDataArray : NSMutableArray, dataType : String, serviceType : String , contactIds:Dictionary<String, AnyObject>,completion: @escaping (_ result: String, _ data : AnyObject) -> Void){
        
        /*//let URL = strURL
        let urlRequest = urlRequestWithComponentsVideo(strURL, parameters: parameters, aryImageData: uploadDataArray, serviceType: serviceType,contactIds: contactIds)
        //print(urlRequest)
        print(urlRequest.0)
        //print(urlRequest.1)
        
        upload(urlRequest.0, data: urlRequest.1).responseJSON
            {
                response in
                
                if let JSON:NSDictionary = response.result.value as NSDictionary {
                    print("JSON: \(JSON)")
                    if let dict: NSDictionary = JSON as? NSDictionary
                    {
                        print(dict)
                        
                        //print(dict)
                        if let str : String = dict["error"] as? String
                        {
                            completion(result: "error", data: str)
                        }
                        else if let status : Bool = dict["success"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else if let status : Bool = dict["status"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else
                        {
                            completion(result: "error", data: dict.valueForKey("message")!)
                        }
                    }
                    
                }
        }*/
    }
    
    //MARK: Sending video in multipart
    
    //TODO:
    /*func urlRequestWithComponentsVideo(_ urlString:String, parameters:Dictionary<String, AnyObject>, aryImageData:NSMutableArray, serviceType : String, contactIds:Dictionary<String, AnyObject>) -> (URLRequestConvertible, NSData) {
        
//        // create url request to send
//        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
//        mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        if(serviceType == "POST")
        {
            mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        }
        else
        {
            mutableURLRequest.httpMethod = Alamofire.HTTPMethod.put.rawValue
        }
        
        
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add Video
        if(aryImageData.count > 0)
        {
            if(serviceType == "POST")
            {
            for imageData in aryImageData
            {
                uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"media[]\"; filename=\"media.mp4\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: video/mp4\r\n\r\n".data(using: String.Encoding.utf8)!)//video/quicktime
                uploadData.append((imageData as! NSData) as Data)
            }
            }
            else
            {
                for imageData in aryImageData
                {
                uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"media.mp4\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: video/mp4\r\n\r\n".data(using: String.Encoding.utf8)!)//video/quicktime
                uploadData.append((imageData as! NSData) as Data)
                }
            }
        }
        
        // add parameters
        for (key, value) in parameters {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        //uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        //add contacts
        if(serviceType == "POST")
        {
            for (key, value) in contactIds
            {
                print(key)
                if let aryContactData:Dictionary<String, AnyObject> = value as? Dictionary<String, AnyObject>
                {
                    for (key, value) in aryContactData
                    {
                        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                        uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                    }
                }
            }
            
        }
        
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.encode(mutableURLRequest as! ParameterEncoding) as! URLRequestConvertible, uploadData)
    }*/
    
        
    //MARK: Sending message in multipart
    
    func uploadMessageData(_ strURL : String, parameters : [String:AnyObject],uploadData : [String:AnyObject], contactIds:Dictionary<String, AnyObject>, bgAudio : [String:AnyObject], completion: @escaping (_ result: String, _ data : AnyObject) -> Void){
        
        /*//let URL = strURL
        print("\n\n\n in upload method array count \(uploadData.count)")
        let urlRequest = urlRequestWithComponentsMessage(strURL, parameters: parameters,aryImageData: uploadData, contactIds: contactIds, bgAudio:bgAudio)
        //print(urlRequest)
        print(urlRequest.0)
        //print(urlRequest.1)
        
        upload(urlRequest.0, data: urlRequest.1).responseJSON
            {
                response in
                print(response)
                if let JSON:NSDictionary = response.result.value as NSDictionary
                {
                    print("JSON: \(JSON)")
                    if let dict: NSDictionary = JSON as? NSDictionary
                    {
                        print(dict)
                        
                        //print(dict)
                        if let str : String = dict["error"] as? String
                        {
                            completion(result: "error", data: str)
                        }
                        else if let status : Bool = dict["success"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else if let status : Bool = dict["status"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else
                        {
                            completion(result: "error", data: dict.valueForKey("message")!)
                        }
                    }
                }
                else
                {
                    print("In else condition of message sending service")
                    completion(result: "error", data: "Error in sending message. Please try again")
                }
        }*/
    }
    
    
    /*func urlRequestWithComponentsMessage(_ urlString:String, parameters:Dictionary<String, AnyObject>,aryImageData:Dictionary<String, AnyObject>, contactIds:Dictionary<String, AnyObject>, bgAudio : [String:AnyObject]) -> (URLRequestConvertible, NSData)
    {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)

            mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue

        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        
        if(aryImageData.count > 0)
        {
                //for imageData in aryImageData
                //{
            var aryIndex : NSInteger = 0
            
            for i in 0..<aryImageData.count
           //for (key, value) in aryImageData
           {
           //print(key)
            let str = String("AllData\(i)")
            if let aryData:Dictionary<String, AnyObject> = aryImageData[str!] as? Dictionary<String, AnyObject>
            {
                var index : NSInteger = 0
            for (key, value) in aryData
            {
                var indexString : String = ""
                print(key)
                //print(value)
                if(key == "medias[][frame_number]")
                {
                    print("\t \(value)")
                    if let str : String = value as? String
                    {
//                        indexString = str.stringByReplacingOccurrencesOfString("AllData", withString: "", options: NSString.CompareOptions.LiteralSearch, range: nil)
                        
                        indexString = (str as NSString).replacingOccurrences(of: "AllData", with: "")

                    }
                }
                
                print("\t\t \(indexString)")
                uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                
                if(key == "medias[][media]IMAGE")
                {
                    if let imageData : NSData = value as? NSData
                    {
                        //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                        
                    uploadData.append("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"media\(i).png\"\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                    
                    uploadData.append(imageData as Data)
                    }
                }
                else if(key == "medias[][media]VIDEOIMAGE")
                {
                    if let imageData : NSData = value as? NSData
                    {
                        //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                        uploadData.append("Content-Disposition: form-data; name=\"video_default_img\"; filename=\"media\(i).png\"\r\n".data(using: String.Encoding.utf8)!)
                        uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                        
                        uploadData.append(imageData as Data)
                    }
                }
                else if(key == "medias[][media]VIDEO")
                {
                    if let dict : NSDictionary = value as? NSDictionary
                    {
                        if((dict.allKeys as NSArray).contains("medias[][media]VIDEO"))
                        {
                            if let imageData : NSData = dict.value(forKey: "medias[][media]VIDEO") as? NSData
                            {
                                //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                                uploadData.append("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"media\(i).mp4\"\r\n".data(using: String.Encoding.utf8)!)
                                uploadData.append("Content-Type: video/mp4\r\n\r\n".data(using: String.Encoding.utf8)!)//video/quicktime
                                uploadData.append(imageData as Data)
                            }
                        }
                        if((dict.allKeys as NSArray).contains("medias[][media]VIDEOIMAGE"))
                        {
                            if let imageData : NSData = dict.value(forKey: "medias[][media]VIDEOIMAGE") as? NSData
                            {
                                //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                                uploadData.append("Content-Disposition: form-data; name=\"video_default_img\"; filename=\"media\(i).png\"\r\n".data(using: String.Encoding.utf8)!)
                                uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                                
                                uploadData.append(imageData as Data)
                            }
                        }
                    }
                    
                    
                    //old code changed due to accuracy
                    /*
                    if let imageData : NSData = value as? NSData
                    {
                        //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                    uploadData.appendData("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"media\(i).mp4\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                    uploadData.appendData("Content-Type: video/mp4\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)//video/quicktime
                    uploadData.appendData(imageData)
                    }
                    */
                    
                }
                else if(key == "medias[][media]BACK")
                {
                    if let imageData : NSData = value as? NSData
                    {
                        //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                        uploadData.append("Content-Disposition: form-data; name=\"bubble_bg[image_data]\"; filename=\"media\(i).png\"\r\n".data(using: String.Encoding.utf8)!)
                        uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                        
                        uploadData.append(imageData as Data)
                    }
                }
                else
                {
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                }
                index += 1
            }
            }
            aryIndex += 1
           }
                //}
            
        }
        if(bgAudio.count > 0)
        {
        for (key, value) in bgAudio
        {
            /*
             "message[media_type]"
             @"Sound"
             "message[bg_media]"
             @"media.caf"
             */
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            if(key == "message[bg_media]")
            {                
                if let imageData : NSData = value as? NSData
                {
                    uploadData.append("Content-Disposition: form-data; name=\"message[bg_media]\"; filename=\"media.caf\"\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Type: audio/mpeg\r\n\r\n".data(using: String.Encoding.utf8)!)//video/quicktime
                    uploadData.append(imageData as Data)
                }
            }
            else if(key == "medias[][media]BACK")
            {
                if let imageData : NSData = value as? NSData
                {
                    //uploadData.appendData("Content-Disposition: form-data; name=\"medias[][frame_number]\"\r\n\r\n\(index)".dataUsingEncoding(NSUTF8StringEncoding)!)
                    uploadData.append("Content-Disposition: form-data; name=\"bubble_bg[image_data]\"; filename=\"media\(1).png\"\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                    
                    uploadData.append(imageData as Data)
                }
            }
            else
            {
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
            }
            
        }
        }
        // add parameters
        //var i : NSInteger = 0
        for (key, value) in parameters
        {
            /*
            if(key == "medias[][media]")
            {
                //i++
                if let imageData : NSData = value as? NSData
                {
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"media.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData(imageData)
                }
            }
            */
            print(key,"\n",value)
            
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        
        for (key, value) in contactIds
        {
            print(key)
            if let aryContactData:Dictionary<String, AnyObject> = value as? Dictionary<String, AnyObject>
            {
                for (key, value) in aryContactData
                {
                    uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                }
            }
        }
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.encode(mutableURLRequest as! ParameterEncoding) as! URLRequestConvertible, uploadData)
    }*/
    
    //MARK: Upload Audio Data
    func uploadAudioAndData(_ strURL : String, parameters : [String:AnyObject], uploadDataArray : NSMutableArray, dataType : String, serviceType : String, contactIds:Dictionary<String, AnyObject> ,completion: @escaping (_ result: String, _ data : AnyObject) -> Void){
        
        /*let URL = strURL
        let urlRequest = urlRequestWithComponentsAudio(strURL, parameters: parameters, aryImageData: uploadDataArray, serviceType: serviceType,contactIds: contactIds)
        //print(urlRequest)
        print(urlRequest.0)
        //print(urlRequest.1)
        
       upload(urlRequest.0, data: urlRequest.1).responseJSON
            {
                response in
                
                if let JSON:NSDictionary = response.result.value as NSDictionary {
                    print("JSON: \(JSON)")
                    if let dict: NSDictionary = JSON as? NSDictionary
                    {
                        print(dict)
                        
                        //print(dict)
                        if let str : String = dict["error"] as? String
                        {
                            completion(result: "error", data: str)
                        }
                        else if let status : Bool = dict["success"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else if let status : Bool = dict["status"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else
                        {
                            completion(result: "error", data: dict.valueForKey("message")!)
                        }
                    }
                    
                }
        }*/
    }
    
    
    /*//MARK: Sending audio in multipart
    func urlRequestWithComponentsAudio(_ urlString:String, parameters:Dictionary<String, AnyObject>, aryImageData:NSMutableArray, serviceType : String, contactIds:Dictionary<String, AnyObject>) -> (URLRequestConvertible, NSData) {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        if(serviceType == "POST")
        {
            mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        }
        else
        {
            mutableURLRequest.httpMethod = Alamofire.HTTPMethod.put.rawValue
        }
        
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        let mediaType : String = "audio/mpeg"
        /*
        for (key, value) in parameters
        {
            print(key)
            if(value as! String == "Sound Track")
            {
                mediaType = "audio/x-caf"
            }
        }
         */
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        if(aryImageData.count > 0)
        {
            if(serviceType == "POST")
            {
                for imageData in aryImageData
                {
                    uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Disposition: form-data; name=\"media[]\"; filename=\"media.caf\"\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Type: \(mediaType)\r\n\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append((imageData as! NSData) as Data)
                }
            }
            else
            {
                for imageData in aryImageData
                {
                    uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Disposition: form-data; name=\"medias[][media]\"; filename=\"media.caf\"\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Type: \(mediaType)\r\n\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append((imageData as! NSData) as Data)
                }
            }
        }
        
        // add parameters
        for (key, value) in parameters
        {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        //uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        //add contacts
        if(serviceType == "POST")
        {
            for (key, value) in contactIds
            {
                print(key)
                if let aryContactData:Dictionary<String, AnyObject> = value as? Dictionary<String, AnyObject>
                {
                    for (key, value) in aryContactData
                    {
                        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                        uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                    }
                }
            }
           
        }
        
         uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.encode(mutableURLRequest as! ParameterEncoding) as! URLRequestConvertible, uploadData)
    }*/
    

    //MARK: Forward message services
    func uploadForwardMessage(_ strURL : String, parameters : [String:AnyObject],contactIds:Dictionary<String, AnyObject> ,completion: @escaping (_ result: String, _ data : AnyObject) -> Void){
        
       /* let urlRequest = urlRequestWithComponentsForwardMessage(strURL, parameters: parameters,contactIds: contactIds)
        //print(urlRequest)
        print(urlRequest.0)
        //print(urlRequest.1)
        
       upload(urlRequest.0, data: urlRequest.1).responseJSON
            {
                response in
                
                if let JSON:NSDictionary = response.result.value as NSDictionary {
                    print("JSON: \(JSON)")
                    if let dict: NSDictionary = JSON as? NSDictionary
                    {
                        print(dict)
                        
                        //print(dict)
                        if let str : String = dict["error"] as? String
                        {
                            completion(result: "error", data: str)
                        }
                        else if let status : Bool = dict["success"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else if let status : Bool = dict["status"] as? Bool
                        {
                            if(status)
                            {
                                completion(result: "success", data: dict.valueForKey("message")!)
                            }
                            else
                            {
                                completion(result: "error", data: dict.valueForKey("message")!)
                            }
                        }
                        else
                        {
                            completion(result: "error", data: dict.valueForKey("message")!)
                        }
                    }
                    
                }
        }*/
    }
    
    //MARK: Forward message multipart
    /*func urlRequestWithComponentsForwardMessage(_ urlString:String, parameters:Dictionary<String, AnyObject>, contactIds:Dictionary<String, AnyObject>) -> (URLRequestConvertible, NSData)
    {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        
        mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        // create upload data to send
        let uploadData = NSMutableData()
        for (key, value) in parameters
        {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        for (key, value) in contactIds
        {
            print(key)
            if let aryContactData:Dictionary<String, AnyObject> = value as? Dictionary<String, AnyObject>
            {
                for (key, value) in aryContactData
                {
                    uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                    uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
                }
            }
        }
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)

        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.encode(mutableURLRequest as! ParameterEncoding) as! URLRequestConvertible, uploadData)
    }*/
    
    
    
    func rotateCameraImageToProperOrientation(_ imageSource : UIImage, maxResolution : CGFloat) -> UIImage {
        
        let imgRef = imageSource.cgImage;
        
        let width = CGFloat(imgRef!.width);
        let height = CGFloat(imgRef!.height);
        
        var bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        var scaleRatio : CGFloat = 1
        if (width > maxResolution || height > maxResolution) {
            
            scaleRatio = min(maxResolution / bounds.size.width, maxResolution / bounds.size.height)
            bounds.size.height = bounds.size.height * scaleRatio
            bounds.size.width = bounds.size.width * scaleRatio
        }
        
        var transform = CGAffineTransform.identity
        let orient = imageSource.imageOrientation
        let imageSize = CGSize(width: CGFloat(imgRef!.width), height: CGFloat(imgRef!.height))
        
        
        switch(imageSource.imageOrientation) {
        case .up :
            transform = CGAffineTransform.identity
            
        case .upMirrored :
            transform = CGAffineTransform(translationX: imageSize.width, y: 0.0);
            transform = transform.scaledBy(x: -1.0, y: 1.0);
            
        case .down :
            transform = CGAffineTransform(translationX: imageSize.width, y: imageSize.height);
            transform = transform.rotated(by: CGFloat(180));
            
        case .downMirrored :
            transform = CGAffineTransform(translationX: 0.0, y: imageSize.height);
            transform = transform.scaledBy(x: 1.0, y: -1.0);
            
        case .left :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: 0.0, y: imageSize.width);
            transform = transform.rotated(by: 3.0 * CGFloat(M_PI) / 2.0);
            
        case .leftMirrored :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: imageSize.height, y: imageSize.width);
            transform = transform.scaledBy(x: -1.0, y: 1.0);
            transform = transform.rotated(by: 3.0 * CGFloat(M_PI) / 2.0);
            
        case .right :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(translationX: imageSize.height, y: 0.0);
            transform = transform.rotated(by: CGFloat(M_PI) / 2.0);
            
        case .rightMirrored :
            let storedHeight = bounds.size.height
            bounds.size.height = bounds.size.width;
            bounds.size.width = storedHeight;
            transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
            transform = transform.rotated(by: CGFloat(M_PI) / 2.0);
            
        //default : ()
        }
        
        UIGraphicsBeginImageContext(bounds.size)
        let context = UIGraphicsGetCurrentContext()
        
        if orient == .right || orient == .left {
            context!.scaleBy(x: -scaleRatio, y: scaleRatio);
            context!.translateBy(x: -height, y: 0);
        } else {
            context!.scaleBy(x: scaleRatio, y: -scaleRatio);
            context!.translateBy(x: 0, y: -height);
        }
        
        context!.concatenate(transform);
        UIGraphicsGetCurrentContext()!.draw(imgRef!, in: CGRect(x: 0, y: 0, width: width, height: height));
        
        let imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return imageCopy!;
    }
    
    
    //MARK: ---- File manager ----
    func getDocumentFilePath(_ strAppendPath : String) -> String
    {
    // Find the video in the app's document directory
  
        /* let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory = paths[0]
        let dataPath = documentsDirectory.stringByAppendingPathComponent(strAppendPath)
        // .append(strAppendPath) //.appendingPathComponent(strAppendPath)
        
        return dataPath*/
        
       /* let paths: NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray;
        let path = (paths[0] as? AnyObject).appendingPathComponent(strAppendPath)
        
        return path*/
        
        let documentsPath:String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
//        let dataPath = documentsPath.appendingPathComponent(strAppendPath)
        
        
        //let dataPath = documentsPath + "\\" + strAppendPath
        let dataPath = documentsPath + "/" + strAppendPath
        
        return dataPath
        

        
    }
    
    func getFilePathURL(_ strAppendPath : String) -> URL
    {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsDirectory.appendingPathComponent(strAppendPath)

        return fileURL
    }
    
    func killFileOnPath(_ strAppendFileName:String) -> String
    {
        /*  Distroying previous saved video
        let documentsPath: String = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as String
        
        let url = NSURL(fileURLWithPath: documentsPath)
        let filePath = url.URLByAppendingPathComponent(strAppendFileName).absoluteString

        
        let killFile = NSFileManager.defaultManager()
        
        print("search file path \(filePath)")
        if (killFile.isDeletableFileAtPath(filePath))
        {
            do {
                try killFile.removeItemAtPath(filePath)
                print("file killed on path\(strAppendFileName)")
                return "File Killed"
            }
            catch let error as NSError {
                print(error.description)
                print("No file on path\(strAppendFileName)")
                return "No file"
            }
        }
        else
        {
            print("return no file detected path\(strAppendFileName)")
                return "No file"
        }
        */
        
        let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let logsPath = documentsPath.appendingPathComponent(strAppendFileName)
        let fileManager = FileManager.default
        print(logsPath.path)
        
        if fileManager.fileExists(atPath: logsPath.path){
            do {
                try fileManager.removeItem(atPath: logsPath.path)
                print("FILE AVAILABLE AND REMOVED")
                return "File Killed"
            }catch let error as NSError{
                print(error.description)
                print("No file on path\(strAppendFileName)")
                return "No file"
            }
        }else
        {
            print("FILE NOT AVAILABLE")
            return "No file"
        }
        
        
        /*
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        
         NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.URLByAppendingPathComponent(strAppendFileName).absoluteString
        print("search file path url\(filePath)")
        let fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(filePath)
        {
            do {
                try fileManager.removeItemAtPath(filePath)
            print("FILE AVAILABLE AND REMOVED")
                return "File Killed"
            }
            catch let error as NSError
            {
                print(error.description)
                print("No file on path\(strAppendFileName)")
                return "No file"
            }
        } else {
            print("FILE NOT AVAILABLE")
            return "No file"
        }
        */
        
        
        //---- old original code ----
        /*
        let documentsPath: NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as NSString
        let killFile = NSFileManager.defaultManager()
        if (killFile.isDeletableFileAtPath(documentsPath.stringByAppendingFormat("/xvideo.mov") as String)){
            do {
                try killFile.removeItemAtPath(documentsPath.stringByAppendingFormat("/xvideo.mov") as String)
            }
            catch let error as NSError {
                print(error.description)
            }
        }
        */
    }

    func killVideoFilesRecorded(_ pickedVideo : URL) -> String
    {
        //----- New Code ----
        
        //--- Save video file to personal folder ---
        let moviePath: String = pickedVideo.path
        
        NSLog("path from image picker: %@", moviePath)
        
        let fileManager: FileManager = FileManager.default
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true) as [AnyObject]
        //(.DocumentDirectory,.UserDomainMask,true)[0] as NSString
        
        let documentsDir: String = paths[0] as! String
        //documentsPath.stringByAppendingFormat("/xvideo.mov") as String)
        let savePath: String = documentsDir.appendingFormat("/\(pickedVideo.lastPathComponent)")
        
        NSLog("%@", savePath)
        
        var done : Bool = false
         _ = AppTheme().killFileOnPath(pickedVideo.lastPathComponent)
        do {
            try fileManager.moveItem(atPath: moviePath, toPath: savePath)
            print("FILE AVAILABLE AND MOVED FROM USER GALLERY")
            done = true
        }
        catch let error as NSError
        {
            
            print(error.description)
            print("No file on path")
            done = false
        }
        
        
//        do {
//            let files: [AnyObject] = try fileManager.contentsOfDirectory(atPath: documentsDir) as [AnyObject]
//            print("FILE AVAILABLE NOW")
//            
//            
//            
//            
//        }
//        catch let error as NSError
//        {
//            
//            print(error.description)
//            print("No file on path")
//            
//        }
        
        if(done)
        {
        return "File Moved"
        }
        else
        {
        return "No file found"
        }
        
        //-----
        
        
        //-- Old code changed
        //        let fileManager = NSFileManager.defaultManager()
        //        print(urlPath.relativePath)
        //
        //        if fileManager.fileExistsAtPath(urlPath.relativePath!)
        //        {
        //            do {
        //                try fileManager.removeItemAtPath(urlPath.relativePath!)
        //                print("FILE AVAILABLE AND REMOVED")
        //                return "File Killed"
        //            }
        //            catch let error as NSError
        //            {
        //                
        //                print(error.description)
        //                print("No file on path\(urlPath.relativePath)")
        //                return "No file"
        //            }
        //        } else {
        //            print("FILE NOT AVAILABLE")
        //            return "No file"
        //        }
    }
    
}

//MARK: Image extension class for image rotation
extension UIImage {
    public func imageRotatedByDegrees(_ degrees: CGFloat, flip: Bool) -> UIImage {
        /*let radiansToDegrees: (CGFloat) -> CGFloat = {
            return $0 * (180.0 / CGFloat(M_PI))
        }*/
        
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(M_PI)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap!.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);
        
        //   // Rotate the image context
        bitmap!.rotate(by: degreesToRadians(degrees));
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap!.scaleBy(x: yFlip, y: -1.0)
        bitmap!.draw(cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
