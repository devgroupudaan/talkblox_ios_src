//
//  ActivityViewController.swift
//  SharedDemo
//
//  Created by Mac on 9/17/16.
//  Copyright © 2016 Sumit Jagdev. All rights reserved.
//

import UIKit

class ActivityViewController: UIActivityViewController {
    
    func _shouldExcludeActivityType(_ activity: UIActivity) -> Bool {
        let activityTypesToExclude = [
            "com.apple.reminders.RemindersEditorExtension",
            "com.apple.mobilenotes.SharingExtension",
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            "com.google.Drive.ShareExtension"
        ] as [Any]
        
        //TODO:
//        if let actType = activity.activityType {
//            if activityTypesToExclude.contains(where: actType) {
//                return true
//            }
//            else if super.excludedActivityTypes != nil {
//                return super.excludedActivityTypes!.contains(actType)
//            }
//        }
        return false
    }
    
}
