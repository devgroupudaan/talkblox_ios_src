//
//  Utilities.swift
//  EcoMailSwift
//
//  Created by MacMini on 31/07/18.
//  Copyright © 2018 MacMini. All rights reserved.
//

import UIKit

let gradient: CAGradientLayer = CAGradientLayer()


class Utilities: NSObject
{
    static let shared = Utilities()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
    
    func showAlertwith(message : String, onView view: UIViewController ) -> Void {
        let alertView = UIAlertController.init(title:"", message:message, preferredStyle: UIAlertController.Style.alert)
        alertView.addAction(UIAlertAction.init(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        view.present(alertView, animated: true, completion: nil);
    }
    
    //validate textField with specific type
     func validate(str : String, withField field:String ) -> Bool{
        var regex:String?
        
        if (field == "TaxID")
        {
            regex = "[0-9A-Za-z -]{1,100}"
        }
        if (field == "username")
        {
            regex = "[0-9A-Za-z -_.]{1,100}"
        }
        if (field == "name")
        {
            regex = "[A-Za-z ]{1,100}"
        }
        else if (field == "phone")
        {
            regex = "\\+?[0-9]{10,14}"
        }
        else if (field == "email")
        {
            regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}";
        }
        else if (field == "streetAddress")
        {
            regex = "[0-9A-Za-z /]{1,100}"
        }
        else if (field == "city" )
        {
            regex = "[A-Za-z ]{1,100}"
        }
        else if (field == "state" )
        {
            regex = "[A-Za-z ]{1,100}"
        }
        else if (field == "zip")
        {
            regex = "[0-9]{5,10}"
        }
        let predicate = NSPredicate.init(format: "self matches %@" , regex!)
        var isMatch:Bool = false
        if str.count > 0
        {
            isMatch = predicate.evaluate(with: str)
        }
        else
        {
            isMatch = false
        }
        return isMatch
        
    }
  
    //make circular of any UIelement
     func makeInRoundShape(object : AnyObject) -> Void{
        if #available(iOS 13.0, *) {
            let layer = object.layer
            layer?.cornerRadius = (layer?.frame.size.height)! / 2
            layer?.borderWidth = 0.5
            layer?.borderColor = UIColor.black.cgColor
            layer?.masksToBounds = true
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func setBorderWithCornerRadiusof(object:AnyObject, borderColor:UIColor, borderWidth:CGFloat, cornerRadius:CGFloat) {
        if #available(iOS 13.0, *) {
            let layer = object.layer
            layer?.cornerRadius = cornerRadius
            layer?.borderWidth = borderWidth
            layer?.borderColor = borderColor.cgColor
            layer?.masksToBounds = true
        } else {
         //   let layer = object.layer
            let view = object as! UIView
            view.layer.cornerRadius = cornerRadius
            view.layer.borderWidth = borderWidth
            view.layer.borderColor = borderColor.cgColor
            view.layer.masksToBounds = true
            
            // Fallback on earlier versions
        }
       
    }
     func setCornerRadius(radii : CGFloat, ofObject object:AnyObject) -> Void{
        if #available(iOS 13.0, *) {
            let layer = object.layer
            layer?.masksToBounds = true
            layer?.cornerRadius = radii
        } else {
            // Fallback on earlier versions
        }
       
    }
  
    func setCornerRadiusAndShadowOf(object:AnyObject, cornerRadius:CGFloat, shadowColor:UIColor ) {
        if #available(iOS 13.0, *) {
            let layer = object.layer
            layer?.cornerRadius = cornerRadius
            layer?.masksToBounds = false
            layer?.shadowColor = shadowColor.cgColor
            layer?.shadowOffset = CGSize.init(width: 1, height: 1)
            layer?.shadowRadius = 1
            layer?.shadowOpacity = 1.0
        } else {
            // Fallback on earlier versions
        }
        
    }
    func setShadowOf(object:AnyObject, shadowColor:UIColor ) {
        if #available(iOS 13.0, *) {
            let layer = object.layer
            layer?.masksToBounds = false
            layer?.shadowColor = shadowColor.cgColor
            layer?.shadowOffset = CGSize.init(width: 2, height:2)
            layer?.shadowRadius = 2
            layer?.shadowOpacity = 2.0
        } else {
            // Fallback on earlier versions
        }
        
    }
     func getDateFrom(string:String, usingFormat format:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let tmpDate:Date = dateFormatter.date(from: string)!
        return tmpDate
    }
   
     func getStringFrom(date:Date, usingFormat format:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let tmpDate:String = dateFormatter.string(from: date)
        return tmpDate
    }
    func getLocalDateFromUTC(strDate:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let convertedDate = formatter.date(from: strDate)
        formatter.dateFormat = "dd MMM, YYYY"
        if convertedDate != nil {
            print(formatter.string(from: convertedDate!))
            return formatter.string(from: convertedDate!)
        }else {
             return ""
        }
    }
    
    func getLocalTimeFromUTC(strDate:String)-> String  {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let convertedDate = formatter.date(from: strDate)
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm"
        print(formatter.string(from: convertedDate!))
        return formatter.string(from: convertedDate!)
    }
    func getChangedDate(date: String) -> String{
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat  = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
       let Date = dateFormatter.date(from: date)
       dateFormatter.dateFormat = "MMM d, yyyy h:mm a"
       let DateOnly = dateFormatter.string(from: Date!)
       return DateOnly
   }
    func getLocatlDateAndTimeFromUTC(strDate:String)-> String  {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let convertedDate = formatter.date(from: strDate)
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss" 
        print(formatter.string(from: convertedDate!))
        return formatter.string(from: convertedDate!)
    }
    func getLocalDateFromUTC(strDate:String,format:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let convertedDate = formatter.date(from: strDate)
        formatter.dateFormat = format
        print(formatter.string(from: convertedDate!))
        return formatter.string(from: convertedDate!)
    }
    func getUTCFromLocal(strDate:String)-> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        let convertedDate = formatter.date(from: strDate)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        print(formatter.string(from: convertedDate!))
        return formatter.string(from: convertedDate!)
    }
     func addOverlayOn(viewController : UIViewController) -> Void{
        let overlayView = UIView()
        overlayView.frame = UIScreen.main.bounds
        overlayView.backgroundColor  = UIColor.black
        overlayView.alpha = 0.7
        overlayView.tag = 222
        viewController.view.addSubview(overlayView)
    }
     func removeOverlayFrom(viewController : UIViewController) -> Void {
        let overlayView:UIView = (UIApplication.shared.keyWindow?.viewWithTag(222))!
        overlayView.removeFromSuperview()
    }
     func isContainBlankSpaceIn(string : String) -> Bool {
        let whitespace = CharacterSet.whitespacesAndNewlines
        let trimmed = string.trimmingCharacters(in: whitespace)
        if trimmed.count == 0
        {
            return true
        }
        else
        {
            return false
        }
    }
     func setUserDefaultWith(object:Any, key:String) -> Void {
        UserDefaults.standard.set(object, forKey: key)
    }
     func getUserDefaultValueFor(key:String) -> Any {
        let value =  UserDefaults.standard.value(forKey: key)
        return value!
    }
    
    //get main storyboard
     func getStoryBoard() -> UIStoryboard {
        return UIStoryboard.init(name: "Main", bundle: nil)
    }
    
     func getStoryBoardName() -> String
    {
        return (Bundle.main.infoDictionary!["UIMainStoryboardFile"]) as! String
    }
   
    //get device udid
     func getDeviceUDID() -> String
    {
        let udid = UIDevice.current.identifierForVendor?.uuidString
        let finalUdid = udid?.replacingOccurrences(of: "-", with: "")
        return finalUdid!
    }
    
    //get navigation for any view controller
     func getNavigationControllerFor(viewController : UIViewController) -> UINavigationController
    {
        let navigationController = UINavigationController.init(rootViewController: viewController)
        return navigationController
    }
    
    //get json string
     func getJsonStringFrom(object : Any) -> String{
        let jsonData = try?JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
        let jsonString = String(data: jsonData!, encoding: .utf8)
        return jsonString!
    }
    
    //check device type
     func isCurrentDeviceiPhone() -> Bool{
        let deviceType = UIDevice.current.model
        guard deviceType == "iPhone" else {
            return false
        }
        return true
    }
   
    //check presence of key in dictionary
     func isKeyPresentIn(dictionary:[ String:Any ], key:String) -> Bool{
        var isKeyPresent = false
        
        if((dictionary[key]) != nil)
        {
            isKeyPresent = true
        }
        else
        {
            isKeyPresent = false
        }
        
        return isKeyPresent
    }
    
     func getTodayDateStringWith(format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let tmpDate:String = dateFormatter.string(from:Date() )
        return tmpDate
    }
    
    func getImageFromBase64(string:String) -> UIImage  {
        let temp = string.components(separatedBy:  ",")
        let dataDecoded : Data = Data(base64Encoded: temp[1], options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage!
    }
  
    func setStatusBarColor(_ color:UIColor)  {
        let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //set gradient of any object
    func setGradientOnObject(_ object : AnyObject)  {
        let color1 = UIColor.init(red: 255.0/255.0, green: 192.0/255.0, blue: 71.0/255.0, alpha: 1)
        let color2 = UIColor.init(red: 232.0/255.0, green: 153.0/255.0, blue: 0.0/255.0, alpha: 1)
        gradient.colors = [color2.cgColor,color1.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        if #available(iOS 12.0, *) {
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: object.frame.size.width, height: object.frame.size.height)
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 13.0, *) {
            object.layer.insertSublayer(gradient, at: 0)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getImageFromURL(url:URL) -> UIImage {
        if let data = try? Data(contentsOf: url) {
            return  UIImage(data: data)!
         }
        return #imageLiteral(resourceName: "slider_icon")
      }
    
    func setShadownOnlyAtBottomOfObject(_ object:AnyObject)  {
        if #available(iOS 13.0, *) {
            let layer = object.layer
            layer?.shadowOffset = CGSize.init(width: 0, height: 3)
            layer?.shadowOpacity = 2.0
            layer?.shadowRadius = 2.0
            layer?.shadowColor = UIColor.lightGray.cgColor
            layer?.masksToBounds = false
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    func getPreviousVCOf(_ vc:UIViewController) -> UIViewController?{
        let lenght = (vc.navigationController?.viewControllers.count)!
        let previousViewController: UIViewController? = lenght >= 2 ? vc.navigationController?.viewControllers[lenght-2] : nil
        return previousViewController
    }
    
    func getTimeDifferencefromDate(_ date:Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDate = formatter.string(from: Date())
        print(currentDate)
        let difference = Calendar.current.dateComponents([.year, .day ,.hour , .minute , .second], from:date , to:formatter.date(from: currentDate)! )
        var timeDifference = ""
        print(difference)
        if difference.year != 0 {
            timeDifference = "\(difference.year ?? 0) years ago"
        }else if difference.day != 0{
            timeDifference = "\(difference.day ?? 0) days ago"
        }else if difference.hour! != 0{
            timeDifference = "\(difference.hour ?? 0) hours ago"
        }else if difference.minute  != 0 {
            timeDifference = "\(difference.minute ?? 0) minutes ago"
        }else {
            timeDifference = "\(difference.second ?? 0) seconds ago"
        }
        return timeDifference
    }
    func navigateToLogin()  {
        let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        for controller in navController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                navController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    func getCurrentTimeStamp() -> UInt64 {
        return  UInt64(floor(Date().timeIntervalSince1970 * 1000))
    }
    
    func isFileAvailableInDevice(filePath:String) -> Bool {
         return FileManager.default.fileExists(atPath: filePath)
    }
    func deleteTemporaryData()  {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let folderPath = documentsPath.appendingPathComponent("temp-folder")
        do {
            try FileManager.default.removeItem(at: folderPath!)
        }catch {
            print("Unable to delete Error is :\(error.localizedDescription)")
        }
    }
    //Activity VC for share something on message,social media etc.
    func showActivityViewControllerOn(vc:UIViewController, image:UIImage?, text:String?, url:String?) {
        let arrShare = NSMutableArray()
        if image != nil {
            arrShare.add(image!)
        }
        if text != nil {
            arrShare.add(text!)
        }
        if url != nil {
            let shareURL = NSURL(string:url!)
            arrShare.add(shareURL!)
        }
        let activityViewController = UIActivityViewController(activityItems: arrShare as! [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = vc.view
        vc.present(activityViewController, animated: true, completion: nil)
    }
    func getEngageFolderPath()->URL  {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let folderPath = documentsPath.appendingPathComponent("Engage")
        return folderPath!
    }
    
    func isAnimatedImage(_ imageData: Data) -> Bool {
        if let source = CGImageSourceCreateWithData(imageData as CFData, nil) {
            let count = CGImageSourceGetCount(source)
            return count > 1
        }
        return false
    }
    
    func convertHexToUIColor(hexColor : String) -> UIColor {
        
        // define character set (include whitespace, newline character etc.)
        let characterSet = CharacterSet.whitespacesAndNewlines as CharacterSet
        
        //trim unnecessary character set from string
        var colorString : String = hexColor.trimmingCharacters(in: characterSet)
        
        // convert to uppercase
        colorString = colorString.uppercased()
        
        //if # found at start then remove it.
        if colorString.hasPrefix("#") {
            let index = colorString.index(colorString.startIndex, offsetBy: 1)
            colorString =  String(colorString[..<index])
        }
        
        if colorString.count != 6 {
            return UIColor.black
        }
        
        // split R,G,B component
        var rgbValue: UInt32 = 0
        Scanner(string:colorString).scanHexInt32(&rgbValue)
        let valueRed    = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let valueGreen  = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
        let valueBlue   = CGFloat(rgbValue & 0x0000FF) / 255.0
        let valueAlpha  = CGFloat(1.0)
        
        // return UIColor
        return UIColor(red: valueRed, green: valueGreen, blue: valueBlue, alpha: valueAlpha)
    }
    
    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
        let imageRef = image.cgImage
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // Set the quality level to use when rescaling
        context!.interpolationQuality = CGInterpolationQuality.high
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
        
        context!.concatenate(flipVertical)
        // Draw into the context; this scales the image
        context!.draw(imageRef!, in: newRect)
        
        let newImageRef = context!.makeImage()! as CGImage
        let newImage = UIImage(cgImage: newImageRef)
        
        // Get the resized image from the context and a UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}
