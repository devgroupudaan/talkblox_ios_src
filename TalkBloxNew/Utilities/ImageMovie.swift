//
//  ImageMovie.swift
//  TalkBlox
//
//  Created by Mac on 30/04/16.
//  Copyright © 2016 MobiWebTech. All rights reserved.
//

import UIKit
import AVFoundation

class ImageMovie: NSObject
{
    //var choosenPhotos: [UIImage] = [UIImage(named:"dog6.jpg")!] // *** your array of UIImages ***
    var outputSize = CGSize(width: 480, height: 480)
    
    //func build(outputSize outputSize: CGSize, photos: [UIImage], audioURL: NSURL, moviePathUrl: NSURL, savePathUrl: NSURL ,completion: (result: String, data : AnyObject) -> Void)
    func build(outputSize: CGSize, photos: [UIImage],completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        var choosenPhotos : [UIImage] = photos
        
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        guard let documentDirectory: URL = urls.first else
        {
            fatalError("documentDir Error")
        }
        
        /*
        let documentsPath: NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as NSString
        let killFile = NSFileManager.defaultManager()
        if (killFile.isDeletableFileAtPath(documentsPath.stringByAppendingFormat("/OutputVideo.mp4") as String)){
            do {
                try killFile.removeItemAtPath(documentsPath.stringByAppendingFormat("/OutputVideo.mp4") as String)
            }
            catch let error as NSError {
                error.description
                print(error.description)
            }
        }
        */
        
     //   AppTheme().killFileOnPath("OutputVideo.mp4")
        
        
        let videoOutputURL = documentDirectory.appendingPathComponent("OutputVideo.mp4")
        
        if FileManager.default.fileExists(atPath: videoOutputURL.path) {
            do {
                try FileManager.default.removeItem(atPath: videoOutputURL.path)
            } catch {
                fatalError("Unable to delete file: \(error) : \(#function).")
            }
        }
        
        guard let videoWriter = try? AVAssetWriter(outputURL: videoOutputURL, fileType: AVFileType.mp4) else {
            fatalError("AVAssetWriter error")
        }
        
        let outputSettings = [AVVideoCodecKey : AVVideoCodecH264, AVVideoWidthKey : NSNumber(value: Float(self.outputSize.width) as Float), AVVideoHeightKey : NSNumber(value: Float(self.outputSize.height) as Float)] as [String : Any]
        
        guard videoWriter.canApply(outputSettings: outputSettings, forMediaType: AVMediaType.video) else {
            fatalError("Negative : Can't apply the Output settings...")
        }
        
        let videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: outputSettings)
        let sourcePixelBufferAttributesDictionary = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_32ARGB as UInt32), kCVPixelBufferWidthKey as String: NSNumber(value: Float(self.outputSize.width) as Float), kCVPixelBufferHeightKey as String: NSNumber(value: Float(self.outputSize.height) as Float)]
        let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput, sourcePixelBufferAttributes: sourcePixelBufferAttributesDictionary)
        
        if videoWriter.canAdd(videoWriterInput) {
            videoWriter.add(videoWriterInput)
        }
        
        if videoWriter.startWriting() {
            videoWriter.startSession(atSourceTime: CMTime.zero)
            assert(pixelBufferAdaptor.pixelBufferPool != nil)
            
            let media_queue = DispatchQueue(label: "mediaInputQueue", attributes: [])
            
            videoWriterInput.requestMediaDataWhenReady(on: media_queue, using: { () -> Void in
                let fps: Int32 = 1
                let frameDuration = CMTimeMake(value: 1, timescale: fps)
                
                var frameCount: Int64 = 0
                var appendSucceeded = true
                
                while (!choosenPhotos.isEmpty) {
                    if (videoWriterInput.isReadyForMoreMediaData) {
                        let nextPhoto = choosenPhotos.remove(at: 0)
                        let lastFrameTime = CMTimeMake(value: frameCount, timescale: fps)
                        let presentationTime = frameCount == 0 ? lastFrameTime : CMTimeAdd(lastFrameTime, frameDuration)
                        
                        var pixelBuffer: CVPixelBuffer? = nil
                        let status: CVReturn = CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, pixelBufferAdaptor.pixelBufferPool!, &pixelBuffer)
                        
                        if let pixelBuffer = pixelBuffer, status == 0 {
                            let managedPixelBuffer = pixelBuffer
                            
                            CVPixelBufferLockBaseAddress(managedPixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                            
                            let data = CVPixelBufferGetBaseAddress(managedPixelBuffer)
                            let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
                            let context = CGContext(data: data, width: Int(self.outputSize.width), height: Int(self.outputSize.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(managedPixelBuffer), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
                            
                            context!.clear(CGRect(x: 0, y: 0, width: CGFloat(self.outputSize.width), height: CGFloat(self.outputSize.height)))
                            
                            let horizontalRatio = CGFloat(self.outputSize.width) / nextPhoto.size.width
                            let verticalRatio = CGFloat(self.outputSize.height) / nextPhoto.size.height
                            //aspectRatio = max(horizontalRatio, verticalRatio) // ScaleAspectFill
                            let aspectRatio = min(horizontalRatio, verticalRatio) // ScaleAspectFit
                            
                            let newSize:CGSize = CGSize(width: nextPhoto.size.width * aspectRatio, height: nextPhoto.size.height * aspectRatio)
                            
                            let x = newSize.width < self.outputSize.width ? (self.outputSize.width - newSize.width) / 2 : 0
                            let y = newSize.height < self.outputSize.height ? (self.outputSize.height - newSize.height) / 2 : 0
                            
                            context!.draw(nextPhoto.cgImage!, in: CGRect(x: x, y: y, width: newSize.width, height: newSize.height))
                            
                            CVPixelBufferUnlockBaseAddress(managedPixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                            
                            appendSucceeded = pixelBufferAdaptor.append(pixelBuffer, withPresentationTime: presentationTime)
                        } else {
                            print("Failed to allocate pixel buffer")
                            appendSucceeded = false
                        }
                    }
                    if !appendSucceeded {
                        break
                    }
                    frameCount += 1
                }
                videoWriterInput.markAsFinished()
                videoWriter.finishWriting { () -> Void in
                    print("FINISHED!!!!!")
                    
                   
                    completion("success", videoOutputURL as AnyObject)
                    
                    
//                   let testAudioFile = AppTheme().getFilePathURL("backgroundAudio.caf")
//                    
//                    self.addAudioToFileAtPath(audioURL, filePath: AppTheme().getDocumentFilePath("OutputVideo.mp4") as String, toPath: AppTheme().getDocumentFilePath("OutputAudio1.mp4"))
 
//                    self.mergeAudio(audioURL, moviePathUrl: videoOutputURL, savePathUrl: savePathUrl, completion:
//                    { (result, data) in
//                        
//                        print("success video")
//                         completion(result: "success", data: videoOutputURL)
//                    })

                    
                    
                    
                    
//                    if (killFile.isDeletableFileAtPath(documentsPath.stringByAppendingFormat("/OutputAudio2.mp4") as String)){
//                        do {
//                            try killFile.removeItemAtPath(documentsPath.stringByAppendingFormat("/OutputAudio2.mp4") as String)
//                        }
//                        catch let error as NSError {
//                            error.description
//                            print(error.description)
//                        }
//                    }
//                    
//                    let audioURL = documentDirectory.URLByAppendingPathComponent("bloxAudio1.caf")
//                    let saveConverted = documentDirectory.URLByAppendingPathComponent("OutputAudio2.mp4")
//                    
                                        
                }
            })
        }
    }
    
    func build2(outputSize: CGSize, photos: [UIImage],completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        var choosenPhotos : [UIImage] = photos
        
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        guard let documentDirectory: URL = urls.first else
        {
            fatalError("documentDir Error")
        }
        
        /*
         let documentsPath: NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as NSString
         let killFile = NSFileManager.defaultManager()
         if (killFile.isDeletableFileAtPath(documentsPath.stringByAppendingFormat("/OutputVideo.mp4") as String)){
         do {
         try killFile.removeItemAtPath(documentsPath.stringByAppendingFormat("/OutputVideo.mp4") as String)
         }
         catch let error as NSError {
         error.description
         print(error.description)
         }
         }
         */
        
        //   AppTheme().killFileOnPath("OutputVideo.mp4")
        
        
        let videoOutputURL = documentDirectory.appendingPathComponent("OutputVideo.mp4")
        
        if FileManager.default.fileExists(atPath: videoOutputURL.path) {
            do {
                try FileManager.default.removeItem(atPath: videoOutputURL.path)
            } catch {
                fatalError("Unable to delete file: \(error) : \(#function).")
            }
        }
        
        guard let videoWriter = try? AVAssetWriter(outputURL: videoOutputURL, fileType: AVFileType.mp4) else {
            fatalError("AVAssetWriter error")
        }
        
        let outputSettings = [AVVideoCodecKey : AVVideoCodecH264, AVVideoWidthKey : NSNumber(value: Float(self.outputSize.width) as Float), AVVideoHeightKey : NSNumber(value: Float(self.outputSize.height) as Float)] as [String : Any]
        
        guard videoWriter.canApply(outputSettings: outputSettings, forMediaType: AVMediaType.video) else {
            fatalError("Negative : Can't apply the Output settings...")
        }
        
        let videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: outputSettings)
        let sourcePixelBufferAttributesDictionary = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_32ARGB as UInt32), kCVPixelBufferWidthKey as String: NSNumber(value: Float(self.outputSize.width) as Float), kCVPixelBufferHeightKey as String: NSNumber(value: Float(self.outputSize.height) as Float)]
        let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput, sourcePixelBufferAttributes: sourcePixelBufferAttributesDictionary)
        
        if videoWriter.canAdd(videoWriterInput) {
            videoWriter.add(videoWriterInput)
        }
        
        if videoWriter.startWriting() {
            videoWriter.startSession(atSourceTime: CMTime.zero)
            assert(pixelBufferAdaptor.pixelBufferPool != nil)
            
            let media_queue = DispatchQueue(label: "mediaInputQueue", attributes: [])
            
            videoWriterInput.requestMediaDataWhenReady(on: media_queue, using: { () -> Void in
                let fps: Int32 = 1
                let frameDuration = CMTimeMake(value: 1, timescale: fps)
                
                var frameCount: Int64 = 0
                var appendSucceeded = true
                
                while (!choosenPhotos.isEmpty) {
                    if (videoWriterInput.isReadyForMoreMediaData) {
                        let nextPhoto = choosenPhotos.remove(at: 0)
                        let lastFrameTime = CMTimeMake(value: frameCount, timescale: fps)
                        let presentationTime = frameCount == 0 ? lastFrameTime : CMTimeAdd(lastFrameTime, frameDuration)
                        
                        var pixelBuffer: CVPixelBuffer? = nil
                        let status: CVReturn = CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, pixelBufferAdaptor.pixelBufferPool!, &pixelBuffer)
                        
                        if let pixelBuffer = pixelBuffer, status == 0 {
                            let managedPixelBuffer = pixelBuffer
                            
                            CVPixelBufferLockBaseAddress(managedPixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                            
                            let data = CVPixelBufferGetBaseAddress(managedPixelBuffer)
                            let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
                            let context = CGContext(data: data, width: Int(self.outputSize.width), height: Int(self.outputSize.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(managedPixelBuffer), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
                            
                            context!.clear(CGRect(x: 0, y: 0, width: CGFloat(self.outputSize.width), height: CGFloat(self.outputSize.height)))
                            
                            let horizontalRatio = CGFloat(self.outputSize.width) / nextPhoto.size.width
                            let verticalRatio = CGFloat(self.outputSize.height) / nextPhoto.size.height
                            //aspectRatio = max(horizontalRatio, verticalRatio) // ScaleAspectFill
                            let aspectRatio = min(horizontalRatio, verticalRatio) // ScaleAspectFit
                            
                            let newSize:CGSize = CGSize(width: nextPhoto.size.width * aspectRatio, height: nextPhoto.size.height * aspectRatio)
                            
                            let x = newSize.width < self.outputSize.width ? (self.outputSize.width - newSize.width) / 2 : 0
                            let y = newSize.height < self.outputSize.height ? (self.outputSize.height - newSize.height) / 2 : 0
                            
                            context!.draw(nextPhoto.cgImage!, in: CGRect(x: x, y: y, width: newSize.width, height: newSize.height))
                            
                            CVPixelBufferUnlockBaseAddress(managedPixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                            
                            appendSucceeded = pixelBufferAdaptor.append(pixelBuffer, withPresentationTime: presentationTime)
                        } else {
                            print("Failed to allocate pixel buffer")
                            appendSucceeded = false
                        }
                    }
                    if !appendSucceeded {
                        break
                    }
                    frameCount += 1
                }
                videoWriterInput.markAsFinished()
                videoWriter.finishWriting { () -> Void in
                    print("FINISHED!!!!!")
                    
                    
                    completion("success", videoOutputURL as AnyObject)
                    
                    
                    //                   let testAudioFile = AppTheme().getFilePathURL("backgroundAudio.caf")
                    //
                    //                    self.addAudioToFileAtPath(audioURL, filePath: AppTheme().getDocumentFilePath("OutputVideo.mp4") as String, toPath: AppTheme().getDocumentFilePath("OutputAudio1.mp4"))
                    
                    //                    self.mergeAudio(audioURL, moviePathUrl: videoOutputURL, savePathUrl: savePathUrl, completion:
                    //                    { (result, data) in
                    //
                    //                        print("success video")
                    //                         completion(result: "success", data: videoOutputURL)
                    //                    })
                    
                    
                    
                    
                    
                    //                    if (killFile.isDeletableFileAtPath(documentsPath.stringByAppendingFormat("/OutputAudio2.mp4") as String)){
                    //                        do {
                    //                            try killFile.removeItemAtPath(documentsPath.stringByAppendingFormat("/OutputAudio2.mp4") as String)
                    //                        }
                    //                        catch let error as NSError {
                    //                            error.description
                    //                            print(error.description)
                    //                        }
                    //                    }
                    //
                    //                    let audioURL = documentDirectory.URLByAppendingPathComponent("bloxAudio1.caf")
                    //                    let saveConverted = documentDirectory.URLByAppendingPathComponent("OutputAudio2.mp4")
                    //
                    
                }
            })
        }
    }
    
    
    //****** Adding audio to video ******
    func addAudioToFileAtPath(_ audioFilePath : URL,filePath: String, toPath outFilePath: String)
    {
        //var error: NSError? = nil
        let composition: AVMutableComposition = AVMutableComposition()
        let videoAsset: AVURLAsset = AVURLAsset(url: (URL(fileURLWithPath: filePath)))
        let videoAssetTrack: AVAssetTrack = videoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let compositionVideoTrack: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        do
        {
         try compositionVideoTrack.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: videoAssetTrack, at: CMTime.zero)
        }
        catch let error as NSError
        {
            print("Failed to insert video/audio tracks!!!!\n")
            print(error.localizedDescription)
        }
        catch
        {
            print("Something went wrong!")
        }
        //var audioStartTime: CMTime = kCMTimeZero
        
        //let audioURL = audioFilePath// AppTheme().getDocumentFilePath("bloxAudio1.caf")
       
        
        //let pathString: String = audioURL//self.getAudioFilePath()
        let urlAsset: AVURLAsset = AVURLAsset(url: audioFilePath)
        let audioAssetTrack: AVAssetTrack = urlAsset.tracks(withMediaType: AVMediaType.audio)[0]
        let compositionAudioTrack: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        do{
            try compositionAudioTrack.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: urlAsset.duration), of: audioAssetTrack, at: CMTime.zero)
        } catch let error as NSError {
            print("Failed to insert video/audio tracks!!!!\n")
            print(error.localizedDescription)
        }
        catch {
            print("Something went wrong!")
        }
        
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)!
        //assetExport.videoComposition = mutableVideoComposition
        assetExport.outputFileType = AVFileType.mp4
        // @"com.apple.quicktime-movie";
        assetExport.outputURL = URL(fileURLWithPath: outFilePath)
        assetExport.exportAsynchronously(completionHandler: {() -> Void in
            switch assetExport.status {
            case  .completed:
                //                export complete
                NSLog("Export Complete")
                
                print(outFilePath)
                
            case .failed:
                NSLog("Export Failed")
                NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                //                export error (see exportSession.error)
                
            case .cancelled:
                NSLog("Export Failed")
                NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                //                export cancelled  
                
            default:
                //do nothing
                break
            }
            
        })
         }
    
    
    //--- New Merge Code ---
    func mergeAudio(_ audioURL: URL, moviePathUrl: URL, savePathUrl: URL,completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {

            let composition = AVMutableComposition()
        let trackVideo:AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: CMPersistentTrackID())!
        let trackAudio:AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
            let option = NSDictionary(object: true, forKey: "AVURLAssetPreferPreciseDurationAndTimingKey" as NSCopying)
            let sourceAsset = AVURLAsset(url: moviePathUrl, options: option as? [String : AnyObject])
            let audioAsset = AVURLAsset(url: audioURL, options: option as? [String : AnyObject])
        
        print("play duration: \(sourceAsset.duration)")
        
            print(sourceAsset)
            print("playable: \(sourceAsset.isPlayable)")
            print("exportable: \(sourceAsset.isExportable)")
            print("readable: \(sourceAsset.isReadable)")
            
        let tracks = sourceAsset.tracks(withMediaType: AVMediaType.video)
        let audios = audioAsset.tracks(withMediaType: AVMediaType.audio)
            
            if tracks.count > 0 {
                let assetTrack:AVAssetTrack = tracks[0] as AVAssetTrack
                let assetTrackAudio:AVAssetTrack = audios[0] as AVAssetTrack
                
                let audioDuration:CMTime = assetTrackAudio.timeRange.duration
                let audioSeconds:Float64 = CMTimeGetSeconds(assetTrackAudio.timeRange.duration)
                print(audioSeconds)
                
                do{
                    try
                        trackVideo.insertTimeRange(CMTimeRangeMake(start: CMTime.zero,duration: audioDuration), of: assetTrack, at: CMTime.zero)
                } catch let error as NSError {
                    print("Failed to insert video/audio tracks!!!!\n")
                    print(error.localizedDescription)
                }
                catch {
                    print("Something went wrong!")
                }
                
                do{
                    try
                        trackAudio.insertTimeRange(CMTimeRangeMake(start: CMTime.zero,duration: audioDuration), of: assetTrackAudio, at: CMTime.zero)
                } catch let error as NSError {
                    print("Failed to insert video/audio tracks!!!!\n")
                    print(error.localizedDescription)
                }
                catch {
                    print("Something went wrong!")
                }
            }
            
            let assetExport: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType.mp4
            assetExport.outputURL = savePathUrl
            //self.tmpMovieURL = savePathUrl
            assetExport.shouldOptimizeForNetworkUse = true
            assetExport.exportAsynchronously(completionHandler: {
                
                switch assetExport.status {
                case  .completed:
                    //                export complete
                    NSLog("Export Complete")
                    
                    print(savePathUrl)
                    
                case .failed:
                    NSLog("Export Failed")
                    NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                    //                export error (see exportSession.error)
                    
                case .cancelled:
                    NSLog("Export Failed")
                    NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                    //                export cancelled
                    
                default:
                    //do nothing
                    break
                }
                
                print("converted video save here :- \(savePathUrl)")
                
                
                
                completion("success", savePathUrl as AnyObject)
            })
            
        }
        
    
    
    //--- New Code For adding audio file into Video File
    
    func createVideo(_ path: String, destinationPath: String, videoOutPutPath videoOutputPath: String, completion: @escaping (_ result: Bool, _ data : String) -> Void) {
        ////////////////////////////////////////////////////////////////////////////
        //////////////  OK now add an audio file to move file  /////////////////////
        let mixComposition: AVMutableComposition = AVMutableComposition()
        let soundOneNew: String = path
        let audio_inputFileUrl: URL = URL(fileURLWithPath: soundOneNew)
        var nextClipStartTime: CMTime = CMTime.zero
        // this is the video file that was just written above, full path to file is in --> videoOutputPath
        let video_inputFileUrl: URL = URL(fileURLWithPath: videoOutputPath)
        // create the final video output file as MOV file - may need to be MP4, but this works so far...
        let testVideoPath: String = videoOutputPath
        let outputFilePath: String = destinationPath
        // self.objTalkBloxFMC.strVideoPath = outputFilePath;
        let outputFileUrl: URL = URL(fileURLWithPath: outputFilePath)
        
//        if NSFileManager.defaultManager().fileExistsAtPath(outputFilePath)
//        {
//            NSFileManager.defaultManager().removeItemAtPath(outputFilePath)
//        }
        //--- new code for file delete
   //     AppTheme().killFileOnPath(outputFilePath)
        
        nextClipStartTime = CMTime.zero
        let videoAsset: AVURLAsset = AVURLAsset(url: video_inputFileUrl, options: nil)
        let video_timeRange: CMTimeRange = CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration)
        let a_compositionVideoTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        //-- error may occure here have to use try catch
        do{
            try a_compositionVideoTrack.insertTimeRange(video_timeRange, of: videoAsset.tracks(withMediaType: AVMediaType.video)[0], at: nextClipStartTime)
        
        } catch let error as NSError {
            print("Failed to insert video/audio tracks!!!!\n")
            print(error.localizedDescription)
        }
        catch {
            print("Something went wrong!")
        }
        //-----
        
        let audioAsset: AVURLAsset = AVURLAsset(url: audio_inputFileUrl, options: nil)
        let audio_timeRange: CMTimeRange = CMTimeRangeMake(start: CMTime.zero, duration: audioAsset.duration)
        let b_compositionAudioTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        //-- error may occure here have to use try catch
        do{
            try b_compositionAudioTrack.insertTimeRange(audio_timeRange, of: audioAsset.tracks(withMediaType: AVMediaType.audio)[0], at: nextClipStartTime)
        } catch let error as NSError {
            print("Failed to insert video/audio tracks!!!!\n")
            print(error.localizedDescription)
        }
        catch {
            print("Something went wrong!")
        }
        //-----
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType(rawValue: "public.mpeg-4")
        assetExport.outputURL = outputFileUrl
        assetExport.exportAsynchronously(completionHandler: {() -> Void in
            //[self saveVideoToAlbum:outputFilePath];
            ///// THAT IS IT DONE... the final video file will be written here...
            NSLog("DONE.....outputFilePath--->%@", outputFilePath)
//            if NSFileManager.defaultManager().fileExistsAtPath(testVideoPath)
//            {
//                NSFileManager.defaultManager().removeItemAtPath(testVideoPath)
//            }
            //--- new code for file delete
            
            //AppTheme().killFileOnPath(testVideoPath)
            
            DispatchQueue.main.async(execute: {() -> Void in
                completion(true, outputFilePath)
            })
        })
    }
    
    
    //--- New code for video compression ---
    func compressVideo(_ inputURL: URL, outputURL: URL,completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        guard let documentDirectory: URL = urls.first else
        {
            fatalError("documentDir Error")
        }

        print("\(documentDirectory)")
        
        let videoOutputURL = outputURL
        
        if FileManager.default.fileExists(atPath: videoOutputURL.path) {
            do {
                try FileManager.default.removeItem(atPath: videoOutputURL.path)
            }
            catch
            {
                fatalError("Unable to delete file: \(error)")
            }
        }
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        if let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) {
            exportSession.outputURL = videoOutputURL
            exportSession.outputFileType = AVFileType.mov
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronously
                { () -> Void in
                    
                    switch exportSession.status
                    {
                    case .unknown:
                        break
                    case .waiting:
                        break
                    case .exporting:
                        break
                    case .completed:
                        let data = try? Data(contentsOf: videoOutputURL)
                        let dataBefore =  try? Data(contentsOf: inputURL)
                        print("File size before \(Double(dataBefore!.count / 1048576)) mb after compression: \(Double(data!.count / 1048576)) mb")
                        
                        completion("success", videoOutputURL as AnyObject)
                        
                    case .failed:
                        break
                    case .cancelled:
                        break
                    }
                //handler(session: exportSession)
            }
        }
    }
    
}
