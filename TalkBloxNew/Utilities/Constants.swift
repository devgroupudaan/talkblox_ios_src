//
//  Constants.swift
//  RewinrAppSwift
//
//  Created by abdeali on 8/10/16.
//  Copyright © 2016 haider. All rights reserved.
//

import UIKit

let DEVICE_TYPE_IOS = "i"
let USER_DEFAULT = UserDefaults .standard
//let appSharedData = AppSharedData.sharedInstance

let VENDOR_KEY = "28e7cc85-3ac4-4867-848d-32d1e5445a1f"
let UD_USER = "UD_USER"
let phoneNumberlength = 11

struct WEB_URL {
    //static let BaseURL:NSString          = "http://api.talkblox.cruzata.com/" //Live
    static let BaseURL = "https://api.talkblox.info/"//"https://talkbloxapi.udaantechnologies.com/"//"http://18.221.229.116:3002/"//"http://3.23.52.49:3002/"  //"http://103.76.248.170:3001/" //Local
    static let BaseURL2:NSString         = "https://api.talkblox.info/"//"https://talkbloxapi.udaantechnologies.com/"//"http://18.221.229.116:3002/"//"http://3.23.52.49:3002/"//"http://api.talkbloxmedia.cruzata.com/"
    
    static let login:NSString            = "\(BaseURL)api/v1/login" as NSString
    static let getMessages:NSString      = "\(BaseURL)api/v1/sentmessage" as NSString
    static let sendinvite:NSString       = "\(BaseURL)api/v1/sendinvite" as NSString
    static let getPages:NSString         = "\(BaseURL)api/v1/pages" as NSString
    static let getConversation:NSString  = "\(BaseURL)api/v1/allmessages" as NSString
    static let preview:NSString          = "\(BaseURL)api/v1/preview" as NSString
    static let getProfile:NSString       = "\(BaseURL)api/v1/profiledetail" as NSString
    static let updateProfile:NSString    = "\(BaseURL)api/v1/profile" as NSString
    static let updatePhoto:NSString      = "\(BaseURL)api/v1/updatephoto" as NSString
    static let UserRegister:NSString     = "\(BaseURL)api/v1/registerUser" as NSString
    static let getCountries:NSString     = "\(BaseURL)api/v1/country" as NSString
    static let forgotPassword:NSString   = "\(BaseURL)api/v1/forgotPassword" as NSString
    static let OTPVerfy:NSString         = "\(BaseURL)api/v1/validateOtp" as NSString
    static let getAlbumList:NSString     = "\(BaseURL)api/v1/albumlist" as NSString
    static let getBackGround:NSString    = "\(BaseURL)api/v1/background" as NSString
    static let searchMedia:NSString      = "\(BaseURL)api/v1/search" as NSString
    static let forwardMessage:NSString   = "\(BaseURL)api/v1/forward" as NSString
    static let updatePassword:NSString   = "\(BaseURL)api/v1/updatepassword" as NSString
    static let resetPassword:NSString    = "\(BaseURL)api/v1/reset" as NSString
    static let getCategory:NSString      = "\(BaseURL)api/v1/category" as NSString
    static let getCollection:NSString    = "\(BaseURL)api/v1/collection" as NSString
    static let eraseAccount:NSString     = "\(BaseURL)api/v1/profilestatus" as NSString
    static let UserLogin:NSString        = "\(BaseURL)TalkBloxService.svc/login" as NSString
    static let ForgetPassword:NSString   = "\(BaseURL)TalkBloxService.svc/forgotPassword" as NSString
    static let GetlistGalleries:NSString = "\(BaseURL)TalkBloxService.svc/listGalleries" as NSString
    static let GetContacts:NSString      = "\(BaseURL)TalkBloxService.svc/contacts" as NSString
    static let AddContact:NSString       = "\(BaseURL)TalkBloxService.svc/addcontacts" as NSString
    static let AddBunchContacts:NSString = "\(BaseURL)TalkBloxService.svc/addbunchcontacts" as NSString
    static let UpdateContact:NSString    = "\(BaseURL)TalkBloxService.svc/updatecontacts" as NSString
    static let DelContact:NSString       = "\(BaseURL)TalkBloxService.svc/deletecontact" as NSString
    static let UpdateProfile:NSString    = "\(BaseURL)TalkBloxService.svc/profile" as NSString
    static let FavGallery:NSString       = "\(BaseURL)TalkBloxService.svc/addfavourite" as NSString
    static let DeleteGallery:NSString    = "\(BaseURL2)MediaHandlerService.svc/deleteGallery" as NSString
    static let AddGallery:NSString       = "\(BaseURL2)MediaHandlerService.svc/addGallery" as NSString
    static let UpdateGallery:NSString    = "\(BaseURL2)MediaHandlerService.svc/updateGallery" as NSString
    static let GetGalleryDetail:NSString = "\(BaseURL)TalkBloxService.svc/listMedia" as NSString
    static let Message:NSString = "\(BaseURL)api/v1/message" as NSString
    static let sentbloxmessage:NSString = "\(BaseURL)api/v1/sentbloxmessage" as NSString //13Jul21
    static let UploadFile:NSString = "\(BaseURL)defaultgallery" as NSString
    
    static let sentMessages:NSString = "\(BaseURL2)MediaHandlerService.svc/sentmessages" as NSString
    static let receivedMessages:NSString = "\(BaseURL2)MediaHandlerService.svc/recievedmessages" as NSString
    static let sentMessageDetail:NSString = "\(BaseURL2)MediaHandlerService.svc/sentmessagesdetail" as NSString
    static let receivedMessageDetail:NSString = "\(BaseURL2)MediaHandlerService.svc/recievedmessageDetail" as NSString
    static let deleteMessage:NSString = "\(BaseURL2)MediaHandlerService.svc/deletemessages" as NSString
    static let forwardsMessage:NSString = "\(BaseURL2)MediaHandlerService.svc/forwordmessages" as NSString
    static let conversation:NSString = "\(BaseURL2)MediaHandlerService.svc/conversation" as NSString
    static let replyMessage:NSString = "\(BaseURL2)MediaHandlerService.svc/rplymessage" as NSString
    static let replyContacts:NSString = "\(BaseURL2)MediaHandlerService.svc/conversationcontacts" as NSString
    static let resendOTP:NSString = "\(BaseURL)TalkBloxService.svc/resendotp" as NSString
    
    ///April 2021
    static let allUsersListForSubscribe:NSString = "\(BaseURL)api/v1/allUsersListForSubscribe" as NSString
    static let subscribe:NSString = "\(BaseURL)api/v1/subscribe" as NSString
    static let unsubscribe:NSString = "\(BaseURL)api/v1/unsubscribeFromSubscriberList" as NSString
    static let mySubscribeList:NSString = "\(BaseURL)api/v1/mySubscribeList" as NSString
    static let sendBloxCastToSubscriber:NSString = "\(BaseURL)api/v1/sendBloxCastToSubscriber" as NSString
    static let seeBloxCastOfOurSubscriber:NSString = "\(BaseURL)api/v1/seeBloxCastOfOurSubscriber" as NSString
    static let seeBloxCastWithRating:NSString = "\(BaseURL)api/v1/seeBloxCastWithRating" as NSString
    static let postRating:NSString = "\(BaseURL)api/v1/postRating" as NSString
    static let postComment:NSString = "\(BaseURL)api/v1/postComment" as NSString
    static let topRatngsBlox:NSString = "\(BaseURL)api/v1/topRatngsBlox" as NSString
    static let viewComment:NSString = "\(BaseURL)api/v1/viewComment" as NSString
    static let myBloxCastList:NSString = "\(BaseURL)api/v1/myBloxCastList" as NSString//Mahendra https://api.talkblox.info/api/v1/topTenBloxWithAllFrames
    static let recentBloxcast:NSString = "\(BaseURL)api/v1/topTenBloxWithAllFrames" as NSString
    static let topUsersWhoseMaxFollowers:NSString = "\(BaseURL)api/v1/topUsersWhoseMaxFollowers" as NSString
    static let notificationLogs:NSString = "\(BaseURL)api/v1/notificationLogs" as NSString
    static let detailsOfChatId:NSString = "\(BaseURL)api/v1/detailsOfChatId" as NSString
    static let listGroup:NSString = "\(BaseURL)api/v1/listGroup" as NSString
    static let getAllGroup:NSString = "\(BaseURL)api/v1/getAllGroup" as NSString
    static let createGroup:NSString = "\(BaseURL)api/v1/createGroup" as NSString
    static let deleteGroup:NSString = "\(BaseURL)api/v1/deletGroup" as NSString
    static let getMembersOfGroup:NSString = "\(BaseURL)api/v1/getMembersOfGroup" as NSString
    static let addUserInGroup:NSString = "\(BaseURL)api/v1/addUserInGroup" as NSString
    static let removeUserFromGroup:NSString = "\(BaseURL)api/v1/removeUserFromGroup" as NSString
    static let uploadGroupImage:NSString = "\(BaseURL)uploadGroupImage" as NSString
    static let getGroupDetail:NSString = "\(BaseURL)api/v1/getGroupDetail" as NSString
    static let updateGroup:NSString = "\(BaseURL)api/v1/updateGroup" as NSString
    static let exitFromGroup:NSString = "\(BaseURL)api/v1/exitFromGroup" as NSString
}

struct THEME_COLOR {
    static let talkBoxBlue:UIColor = UIColor(red: 0.0/255.0, green: 161.0/255.0, blue: 228.0/255.0, alpha: 1)
    static let themeColorBlue:UIColor = UIColor(red: 0.0/255.0, green: 161.0/255.0, blue: 228.0/255.0, alpha: 1)
}

struct NOTIF {
    static let USER_PASSWORD:String         = "USER_PASSWORD"
    static let USER_NAME:String             = "USER_NAME"
    static let ACCESS_TOKEN:String             = "ACCESS_TOKEN"
    static let MOBILE_NUMBER:String             = "MOBILE_NUMBER"
    static let PHONE_CODE:String             = "PHONE_CODE"
    static let UD_KEEP_ME_LOGGED_IN:String   = "UD_KEEP_ME_LOGGED_IN"
    static let USER_LOGIN:String   = "USER_LOGIN"
    
    static let IS_LOGIN:String              = "IS_LOGIN"
    
    static let LIVE_RESPONSE:String         = "LIVE_RESPONSE"
    static let LIVE_TAB_HIDE:String         = "LIVE_TAB_HIDE"
    static let LIVE_TAB_SHOW:String         = "LIVE_TAB_SHOW"
    static let PUSH_PROGRAM_DETAIL:String   = "PUSH_PROGRAM_DETAIL"
    static let SHOW_LOGIN_VC:String         = "SHOW_LOGIN_VC"
    static let CHARITY_RESPONSE:String      = "CHARITY_RESPONSE"
    static let DID_DISMISS_INTERSTITIAL:String = "DID_DISMISS_INTERSTITIAL"
    static let TV_SHOWS_HIDE:String         = "TV_SHOWS_HIDE"
    static let TV_SHOWS_SHOW:String         = "TV_SHOWS_SHOW"
    static let NETWORK_ISSUE:String         = "NETWORK_ISSUE"
    static let RELOAD_LIVE_DATA:String      = "RELOAD_LIVE_DATA"
    static let SHOW_SUB_CATEGORIES:String   = "SHOW_SUB_CATEGORIES"
    static let SHOW_PROGRAMS:String         = "SHOW_PROGRAMS"
    static let SHOW_MOVIES:String           = "SHOW_MOVIES"
    static let SHOW_MOVIE_DETAIL:String     = "SHOW_MOVIE_DETAIL"
    static let PLAY_SELECTED_RADIO:String     = "PLAY_SELECTED_RADIO"
    static let SELECT_SIDE_MENU_ITEM:String     = "SELECT_SIDE_MENU_ITEM"
    static let DID_RECIEVE_CONTACT_LIST:String     = "DID_RECIEVE_CONTACT_LIST"
}

struct SCREEN {
    static let width:CGFloat   = UIScreen.main.bounds.width
    static let height:CGFloat  = UIScreen.main.bounds.height
}

/*struct ADMOB {
    static let APP_ID:String                = "ca-app-pub-9960154435694366~3039255569"
    static let BANNER:String                = "ca-app-pub-9960154435694366/9891781980"
    static let INTERSTITIAL:String          = "ca-app-pub-9960154435694366/1219085049"
    static let NATIVE_EXPRESS:String        = "ca-app-pub-9960154435694366/6084043699"
}*/

struct HTTP_STATUS_CODE {
    static let TOKEN_EXPIRE:NSInteger  = 401
    static let UPDATE_REQUIRED:NSInteger  = 406
    static let INVALID:NSInteger  = 400
    static let SUCCESS:NSInteger  = 200
    static let NO_DATA:NSInteger  = 204
    static let SERVER_ERROR:NSInteger  = 500
    static let SHOW_LOADER:NSInteger  = 226
}
// MARK: - CheckError
struct ErroCode  {
    static let Succes:Int = 200
    static let Fail:Int   = 501
    static let Pending:Int = 406
    static let NoData:Int = 400
}

struct GalleryType  {
    static let Sound:NSString = "sound"
    static let Image:NSString = "image"
    static let Backgroundimage: NSString = "backgroundimage"
    static let Video:NSString = "video"
    static let Text:NSString = "text"
    static let Gif:NSString = "gif"
}

enum ContactStatus {
    case none
    case inapp
  //  case following
}

struct Device {
    
    // MARK: - Singletons
    static var TheCurrentDevice: UIDevice {
        struct Singleton {
            static let device = UIDevice.current
        }
        return Singleton.device
    }
    
    static var TheCurrentDeviceVersion: Float {
        struct Singleton {
            static let version:Float = Float(UIDevice.current.systemVersion)!
        }
        return Singleton.version
    }
    
    static var TheCurrentDeviceHeight: CGFloat {
        struct Singleton {
            static let height = UIScreen.main.bounds.size.height
        }
        return Singleton.height
    }
    
    // MARK: - Device Idiom Checks
    static var PHONE_OR_PAD: String {
        if isPhone() {
            return "iPhone"
        } else if isPad() {
            return "iPad"
        }
        return "Not iPhone nor iPad"
    }
    
    static var DEBUG_OR_RELEASE: String {
        #if DEBUG
            return "Debug"
        #else
            return "Release"
        #endif
    }
    
    static var SIMULATOR_OR_DEVICE: String {
        #if (arch(i386) || arch(x86_64)) && os(iOS)
            return "Simulator"
        #else
            return "Device"
        #endif
    }
    
    static func isPhone() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .phone
    }
    
    static func isPad() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .pad
    }
    
    static func isDebug() -> Bool {
        return DEBUG_OR_RELEASE == "Debug"
    }
    
    static func isRelease() -> Bool {
        return DEBUG_OR_RELEASE == "Release"
    }
    
    static func isSimulator() -> Bool {
        return SIMULATOR_OR_DEVICE == "Simulator"
    }
    
    static func isDevice() -> Bool {
        return SIMULATOR_OR_DEVICE == "Device"
    }
    
    let IS_IPHONE_5_SE = UIScreen.main.bounds.height == 568
    let IS_IPHONE_6_7_8_SE2 = UIScreen.main.bounds.height == 667
    let IS_IPHONE_6P_7P_8P = UIScreen.main.bounds.height == 736
    let IS_IPHONE_X_Xs_11_11Pro = UIScreen.main.bounds.height == 812
    let IS_IPHONE_XsMax_Xr_11ProMax = UIScreen.main.bounds.height == 896
    
    // MARK: - Device Version Checks
    enum Versions: Float {
        case Five = 5.0
        case Six = 6.0
        case Seven = 7.0
        case Eight = 8.0
        case Nine = 9.0
    }
    
    static func isVersion(version: Versions) -> Bool {
        return TheCurrentDeviceVersion >= version.rawValue && TheCurrentDeviceVersion < (version.rawValue + 1.0)
    }
    
    static func isVersionOrLater(version: Versions) -> Bool {
        return TheCurrentDeviceVersion >= version.rawValue
    }
    
    static func isVersionOrEarlier(version: Versions) -> Bool {
        return TheCurrentDeviceVersion < (version.rawValue + 1.0)
    }
    
    static var CURRENT_VERSION: String {
        return "\(TheCurrentDeviceVersion)"
    }
    
    // MARK: iOS 5 Checks
    static func IS_OS_5() -> Bool {
        return isVersion(version: .Five)
    }
    
    static func IS_OS_5_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Five)
    }
    
    static func IS_OS_5_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Five)
    }
    
    // MARK: iOS 6 Checks
    static func IS_OS_6() -> Bool {
        return isVersion(version: .Six)
    }
    
    static func IS_OS_6_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Six)
    }
    
    static func IS_OS_6_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Six)
    }
    
    // MARK: iOS 7 Checks
    static func IS_OS_7() -> Bool {
        return isVersion(version: .Seven)
    }
    
    static func IS_OS_7_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Seven)
    }
    
    static func IS_OS_7_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Seven)
    }
    
    // MARK: iOS 8 Checks
    static func IS_OS_8() -> Bool {
        return isVersion(version: .Eight)
    }
    
    static func IS_OS_8_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Eight)
    }
    
    static func IS_OS_8_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Eight)
    }
    
    // MARK: iOS 9 Checks
    static func IS_OS_9() -> Bool {
        return isVersion(version: .Nine)
    }
    
    static func IS_OS_9_OR_LATER() -> Bool {
        return isVersionOrLater(version: .Nine)
    }
    
    static func IS_OS_9_OR_EARLIER() -> Bool {
        return isVersionOrEarlier(version: .Nine)
    }
    
    // MARK: - Device Size Checks
    enum Heights: CGFloat {
        case Inches_3_5 = 480
        case Inches_4 = 568
        case Inches_4_7 = 667
        case Inches_5_5 = 736
    }
    
    static func isSize(height: Heights) -> Bool {
        return TheCurrentDeviceHeight == height.rawValue
    }
    
    static func isSizeOrLarger(height: Heights) -> Bool {
        return TheCurrentDeviceHeight >= height.rawValue
    }
    
    static func isSizeOrSmaller(height: Heights) -> Bool {
        return TheCurrentDeviceHeight <= height.rawValue
    }
    
    static var CURRENT_SIZE: String {
        if IS_3_5_INCHES() {
            return "3.5 Inches"
        } else if IS_4_INCHES() {
            return "4 Inches"
        } else if IS_4_7_INCHES() {
            return "4.7 Inches"
        } else if IS_5_5_INCHES() {
            return "5.5 Inches"
        }
        return "\(TheCurrentDeviceHeight) Points"
    }
    
    // MARK: Retina Check
    static func IS_RETINA() -> Bool {
        return UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale))
    }
    
    // MARK: 3.5 Inch Checks
    static func IS_3_5_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_3_5)
    }
    
    static func IS_3_5_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_3_5)
    }
    
    static func IS_3_5_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrSmaller(height: .Inches_3_5)
    }
    
    // MARK: 4 Inch Checks
    static func IS_4_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_4)
    }
    
    static func IS_4_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_4)
    }
    
    static func IS_4_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrSmaller(height: .Inches_4)
    }
    
    // MARK: 4.7 Inch Checks
    static func IS_4_7_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_4_7)
    }
    
    static func IS_4_7_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_4_7)
    }
    
    static func IS_4_7_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_4_7)
    }
    
    // MARK: 5.5 Inch Checks
    static func IS_5_5_INCHES() -> Bool {
        return isPhone() && isSize(height: .Inches_5_5)
    }
    
    static func IS_5_5_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_5_5)
    }
    
    static func IS_5_5_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrLarger(height: .Inches_5_5)
    }
}

func parse<T>(parameter: T) {
    if parameter is Int {
        print("Int")
    } else if (parameter is Float) || (parameter is Double) {
        print("Double")
    } else if parameter is String {
        print("String")
    } else if parameter is Bool {
        print("Bool")
    } else {
        assert(false, "Unsupported type")
    }
}
