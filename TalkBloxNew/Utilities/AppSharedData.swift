//
//  AppSharedData.swift
//  SKTATTV
//
//  Created by abdeali on 12/16/16.
//  Copyright © 2016 haider. All rights reserved.
//

import UIKit
import SSKeychain


class AppSharedData: NSObject {
    

    var navViewControllerRef:UINavigationController!
    var countries:NSMutableArray = NSMutableArray.init()
    var currentTappedViewController:UIViewController!
    var webUrl:NSString!
    var isCam : Bool = false
    var accessToken = ""
    var arrBlox : NSMutableArray = NSMutableArray()
    var selectedBlox = -1
    var currentUser:UserDetail?
  //  var imageData : Data = Data()

    static let sharedInstance = AppSharedData()
    private override init() {} //This prevents others from using the default '()' initializer for this class.

    
    func getUniqueDeviceIdentifierAsString() -> (String) {
        
        let appName:String = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        var strApplicationUUID: String? = SSKeychain.password(forService: appName, account: "incoding")
        
        if strApplicationUUID == nil {
            strApplicationUUID = (UIDevice.current.identifierForVendor?.uuidString)!
            SSKeychain.setPassword(strApplicationUUID, forService: appName, account: "incoding")
        }
        
        return strApplicationUUID!
        
    }
    
    func showErrorAlert(message:String) {
        let alert:UIAlertView = UIAlertView(title: "", message: message, delegate: nil, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func isEmailValid(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    func isPhoneValidate(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func getUser() -> User? {
        let decoded:Data? = UserDefaults.standard.object(forKey: UD_USER) as? Data
        
        if decoded == nil {
            return nil
        }
        let decodedUser:User? = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? User
        return decodedUser
    }
    
    func saveUser(user:User?) {
        let userDefaults = UserDefaults.standard
        if user == nil {
            userDefaults.removeObject(forKey: UD_USER)
        } else {
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: user!)
            userDefaults.set(encodedData, forKey: UD_USER)
        }
        userDefaults.synchronize()
    }
    
    func getDayAndDate(date:String) -> (String, String) {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let aDate = formatter.date(from: date as String)!
        
        let formatter2  = DateFormatter()
        formatter2.dateFormat = "EEE"
        
        return (formatter2.string(from: aDate).capitalized,date.components(separatedBy: "-").last!)
    }
    
    func getTime(date:String) -> (String) {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        let aDate = formatter.date(from: date as String)!
        
        let formatter2  = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        
        return formatter2.string(from: aDate)
    }
    func showAlert(_ errorMessage : String , errorTitle : String) -> UIAlertController
    {
        let errorAlert : UIAlertController = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        errorAlert.view.backgroundColor = UIColor.white
        drawBorder(errorAlert.view, color: THEME_COLOR.talkBoxBlue, borderWidth: 3.0, cornerRadius: 12.0)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        {
            (action) in
            // ...
        }
        errorAlert.addAction(OKAction)
        /*
         self.presentViewController(errorAlert, animated: true)
         {
         // ...
         }
         */
        return errorAlert
    }
    
    func drawBorder(_ view: UIView, color: UIColor, borderWidth:CGFloat, cornerRadius:CGFloat)
    {
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
    }
    
    func drawShadow(_ viewToAddLayer : AnyObject)
    {
        if #available(iOS 13.0, *) {
            let layer = viewToAddLayer.layer

            layer?.shadowColor = UIColor.black.cgColor
            layer?.shadowOffset = CGSize(width: 10, height: 10)
            layer?.shadowOpacity = 0.7
            layer?.shadowRadius = 5
        } else {
            // Fallback on earlier versions
        }
        
    }
    func logout(){
        let userDefaults = UserDefaults.standard
        userDefaults.set(nil, forKey: NOTIF.ACCESS_TOKEN)
        userDefaults.synchronize()
        let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        for controller in navController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                navController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
