//
//  LoadingIndicatorView.swift
//  SwiftLoadingIndicator
//
//  Created by Vince Chan on 12/2/15.
//  Copyright © 2015 Vince Chan. All rights reserved.
//
import UIKit

class LoadingIndicatorView {
    
    static var currentOverlay : UIView?
    
    static func show() {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            print("No main window.")
            return
        }
        show(currentMainWindow)
    }
    
    static func show(_ loadingText: String) {
        DispatchQueue.main.async{
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            print("No main window.")
            return
        }
        show(currentMainWindow, loadingText: loadingText)
        }
    }
    
    static func show(_ overlayTarget : UIView) {
        show(overlayTarget, loadingText: nil)
    }
    
    static func show(_ overlayTarget : UIView, loadingText: String?) {
        DispatchQueue.main.async{
        // Clear it first in case it was already shown
        hide()
        
        // Create the overlay
        let overlay = UIView(frame: overlayTarget.frame)
        overlay.center = overlayTarget.center
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        overlayTarget.addSubview(overlay)
        overlayTarget.bringSubviewToFront(overlay)
        
        // Create and animate the activity indicator
        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        indicator.center = overlay.center
        indicator.startAnimating()
//        overlay.addSubview(indicator)
        
        // Create label
        if let textString = loadingText {
            let label = UILabel()
            label.text = textString
            label.textColor = UIColor.white
            label.sizeToFit()
            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
//            overlay.addSubview(label)
        }
        
        // Animate the overlay to show
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        overlay.alpha = overlay.alpha > 0 ? 0 : 0.5
        UIView.commitAnimations()
            
            let ImgView = UIImageView(frame: CGRect(x: overlayTarget.center.x - 25, y: overlayTarget.center.y - 25, width: 50, height: 50))
//            ImgView.image = UIImage.gif(asset: "Loader20f")
            let image = UIImage.gifImageWithName("Loader20f")
            ImgView.image = UIImage.gifImageWithName("Loader20f")
            overlay.addSubview(ImgView)
        
        currentOverlay =  overlay
//            currentOverlay = ImgView //overlay
        }
    }
    
    static func hide() {
        if currentOverlay != nil {
                currentOverlay?.removeFromSuperview()
                currentOverlay =  nil
           
        }
    }
}
//extension UIImage {
//  public class func gif(asset: String) -> UIImage? {
//    if let asset = NSDataAsset(name: asset) {
//        return UIImage.gif(asset: asset.name)
//    }
//    return nil
//  }
//}
