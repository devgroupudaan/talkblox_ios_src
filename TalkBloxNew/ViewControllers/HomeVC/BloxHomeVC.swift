//
//  BloxHomeVC.swift
//  TalkBloxNew
//
//  Created by Dex_Mac2 on 2/2/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import FSPagerView
import  Gloss
import Cosmos
import AVKit

class TrendingCell: UICollectionViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var views: UILabel!
}

class BloxHomeVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var myCollectionView: UICollectionView!
//    @IBOutlet weak var welcomeLbl:UILabel!
    
    @IBOutlet weak var myImage: DesignableImageView!
    @IBOutlet weak var myShortName: UILabel!
    @IBOutlet weak var notificationCount: DesignableLabel!
    
    //  fileprivate let imageNames = ["Beach.png","Bright Colors.png","Classic Car.png","Galaxy.png","Grafitti Face.png","Grafitti Wall.png","Man on the Moon.png","Mountains.png","Palm Tree.png","People Waving.png","Pink Mountains.png","Street.png"]
    var imageNames = [BackGroundData]()
    @IBOutlet weak var bloxImage:UIImageView!
    var imageData : BackGroundData?
    var selectedIndex = 0
    
    var messageID = 0
    var arrConversations = [Conversation]()
    var arrTopBloxCasts = [BloxCast]()
    var arrRecentBloxCasts = [RecentBloxCast]()
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    var arrInblox = [Inblox]()
    
    var inputImageBgID: String!
    var inputSoundBgID: String!
    
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    
    var inbloxData:Inblox?
    
    //----- Code for showing message from sent and recieve list ----
    var dictUserMessageData : NSMutableDictionary!
    
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var chatID = 0
    
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    
    var bloxRating: Double = 0.0
    
    var bloxIDsArr = [bloxIds]()
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.isInfinite = true
//            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.register(UINib(nibName:"CustomFSPagerViewCell", bundle: nil), forCellWithReuseIdentifier: "CustomFSPagerViewCell")
            self.pagerView.transformer = FSPagerViewTransformer(type: .overlap)
            let screenSize = UIScreen.main.bounds
            let screenWidth = screenSize.width
            let screenHeight = screenSize.height
            print(screenWidth)
            let actualHeight = (screenHeight-250)/1000
            let actualWidth = screenWidth/1000
//            let transform = CGAffineTransform(scaleX: (actualHeight*actualWidth)+0.09, y:actualHeight )
            let transform = CGAffineTransform(scaleX: actualHeight * 1.0, y:actualHeight * 1.5 )
            self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
            self.pagerView.decelerationDistance = 1 //FSPagerView.automaticDistance
            self.pagerView.automaticSlidingInterval = 5.0
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        let user = AppSharedData.sharedInstance.currentUser
//        if user != nil{
//            welcomeLbl.text = "Welcome " + (user?.displayName  ?? "")
//        }
        
        let gestureProfile1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchtoProfile(_:)))
        gestureProfile1.numberOfTapsRequired = 1
        myImage.addGestureRecognizer(gestureProfile1)
        
        let gestureProfile2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchtoProfile(_:)))
        gestureProfile2.numberOfTapsRequired = 1
        myShortName.addGestureRecognizer(gestureProfile2)
        // Do any additional setup after loading the view.
        
//        self.serverCallForGetBackGroundImages()
//        self.serverCallForGetProfileDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
//        self.serverCallForGetRecentBloxcast()
        self.pagerView.automaticSlidingInterval = 5.0
        self.serverCallForGetProfileDetail()
    }

    @IBAction func bell_icon_tapped(_ sender: UIButton) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.pushViewController(VC!, animated: false)
    }
        override func viewDidAppear(_ animated: Bool) {

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.view.removeGestureRecognizer((navigationController?.interactivePopGestureRecognizer)!)
        let delayInSeconds = 20.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
        })
//        DispatchQueue.global(qos: .background).async {
//            self.serverCallForGetProfileDetail()
//        }
    }
    
    @objc func switchtoProfile(_ sender: UITapGestureRecognizer){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
        vc.isSelfProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func configure() {
        // view.layoutIfNeeded()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 8, bottom: 10, right: 8.0)
        
        let width = UIScreen.main.bounds.width
        print(width)
        layout.itemSize = CGSize(width: width/4.0-16.0 , height: width/4.0-16.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        myCollectionView!.collectionViewLayout = layout
    }
    
    
    func serverCallForPreview(chatID:Int)  {
        let params:NSDictionary = ["message_id":messageID,"chat_id":chatID ]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.preview, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            //            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let dict : NSDictionary = response {
                        
                        print("\n\ndictionary for message all urls :", dict, "\n\n")
                        self.generateDictionaryForPreview(dict.mutableCopy() as! NSMutableDictionary )
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
    //MARK:- Helper Methods
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        self.present(errorAlert, animated: true ,completion: nil)
    }
    func generateDictionaryForPreview(_ dictMessageData : NSMutableDictionary)
    {
//        if let imageBg = dictMessageData.value(forKey: "imageBg")
        if let imageBg = dictMessageData.value(forKey: "chatImage") // changed on 28Dec2020
        {
            inputImageBgID = imageBg as! String
        }
        if let imageBg = dictMessageData.value(forKey: "chatSound") // changed on 28Dec2020
        {
            inputSoundBgID = imageBg as! String
        }
        if let contactId = dictMessageData.value(forKey: "contact_id")
        {
            dictMessagePreivewData.setValue(contactId, forKey: "contact_id")
        }
        if let messageRecList = dictMessageData.value(forKey: "message_receiver_list")
        {
            dictMessagePreivewData.setValue(messageRecList, forKey: "message_receiver_list")
        }
        if let messageUserId = dictMessageData.value(forKey: "message_user_id")
        {
            dictMessagePreivewData.setValue(messageUserId, forKey: "message_user_id")
        }
        if let messageUserName = dictMessageData.value(forKey: "message_user_name")
        {
            dictMessagePreivewData.setValue(messageUserName, forKey: "message_user_name")
        }
        
        if(messageParentId != 0)
        {
            dictMessagePreivewData.setValue(messageParentId, forKey: "parentId")
        }
        if(strMessageUserName != "")
        {
            dictMessagePreivewData.setValue(strMessageUserName, forKey: "messageUserName")
        }
        
        dictMessagePreivewData.setValue(strMessageId, forKey: "id")
        
        dictMessagePreivewData.setValue("Inbox", forKey: "MessagePreviewType")
        
        
        //-- Code to download BG bubble image ---
        
        if(previewBubbleImageURL != "")
        {
          
            previewBubbleImageURL = previewBubbleImageURL.replacingOccurrences(of: " ", with: "%20")
            var imageData : UIImage = UIImage()
            let img : UIImage = AppTheme().getImageFromUrl(previewBubbleImageURL)
            if img != nil
            {
                imageData = img
            }
            else
            {
                previewBubbleImageURL = ""
            }
            if(imageData.size.height > 0)
            {
                bubblePreviewImage = imageData
            }
            else
            {
                previewBubbleImageURL = ""
            }
        }
        //----
        
        /////new logic for bg Music
        if var strBgURL : String = dictMessageData.value(forKey: "chatSound") as? String
        {
              strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
            //AppTheme().killFileOnPath("previewBg.caf")
            
            //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
            AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                
                if(result == "success")
                {
                    self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
                else
                {
                    self.showAlert(data as! String, strTitle: "Error")
                }
                
            })
        }
        
        
        if let aryBgMedia : NSArray = dictMessageData.value(forKey: "bgMedia") as? NSArray
        {
            if let dictBgMedia : NSDictionary = aryBgMedia.object(at: 0) as? NSDictionary
            {
                if var strBgURL : String = dictBgMedia.value(forKey: "mediaUrl") as? String
                {
                      strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
                    //AppTheme().killFileOnPath("previewBg.caf")
                    
                    //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                            if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                            {
                                if aryMedia.count > 0
                                {
                                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                                    self.processResponce()
                                }
                            }
                        }
                        else
                        {
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                    })
                }
                else
                {
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
            }
            else
            {
                if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                {
                    if aryMedia.count > 0
                    {
                        self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                        self.processResponce()
                    }
                }
            }
        }
        else
        {
            if let aryMedia : NSArray =  dictMessageData.value(forKey: "frames") as? NSArray
            {
                if aryMedia.count > 0
                {
                    let result: NSArray = aryMedia.sorted(by: {(int1, int2)  -> Bool in
                        return ((int1 as! NSDictionary).value(forKey: "frameNo") as! Int) < ((int2 as! NSDictionary).value(forKey: "frameNo") as! Int) // It sorted the values and return to the mySortedArray
                    }) as NSArray
                    self.aryMessageMedia = result.mutableCopy() as! NSMutableArray
                    self.processResponce()
                }
            }
        }
        
        
    }
    
    var aryCountIncr : NSInteger = 0
    func processResponce()
    {
        if(aryMessageMedia.count > aryCountIncr) {
            
            if let dictMedia : NSDictionary = aryMessageMedia.object(at: aryCountIncr) as? NSDictionary
            {
                //bloxDetails
                dictBloxDetails = NSMutableDictionary()
                //var strBloxType : String = String()
                if let strCaption : String = dictMedia.value(forKey: "caption") as? String
                {
                    dictBloxDetails.setValue(strCaption, forKey: "caption")
                }
                else
                {
                    dictBloxDetails.setValue("", forKey: "caption")
                }
                
                if var soundURL : String = dictMedia.value(forKey: "soundURL") as? String
                {
                        soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    dictBloxDetails.setValue(soundURL, forKey: "soundURL")
                }
                
                
                if let duration : NSNumber = dictMedia.value(forKey: "animationDuration") as? NSNumber
                {
                    dictBloxDetails.setValue(duration, forKey: "animationDuration")
                }
                else
                {
                    dictBloxDetails.setValue(1.0, forKey: "animationDuration")
                }
                
                if let strType : String = dictMedia.value(forKey: "mediaType") as? String
                {
                    
                    if(strType == GalleryType.Text as String)
                    {
                        strMessageBloxType = GalleryType.Text as String
                        
                        if let txtMessage : String = dictMedia.value(forKey: "text") as? String
                        {
                            self.txtMessage = txtMessage
                            dictBloxDetails.setValue(txtMessage, forKey: "message")
                        }
                        if let textColor : String = dictMedia.value(forKey: "textColor") as? String
                        {
                            self.txtColor = textColor
                            dictBloxDetails.setValue(textColor, forKey: "textColor")
                        }
                        if let txtSize : String = dictMedia.value(forKey: "textSize") as? String
                        {
                            guard let n = NumberFormatter().number(from: txtSize) else { return }
                            self.txtSize = CGFloat(n)
                            dictBloxDetails.setValue(txtSize, forKey: "fontSize")
                        }
                        if let txtStyle : String = dictMedia.value(forKey: "textStyle") as? String
                        {
                            self.txtStyle = txtStyle
                            dictBloxDetails.setValue(txtStyle, forKey: "fontName")
                        }
                        if let textBgMediaId : NSInteger = dictMedia.value(forKey: "textBgMediaId") as? NSInteger
                        {
                            dictBloxDetails.setValue(textBgMediaId, forKey: "textBgMediaId")
                        }
                        loadTextMessage()
                        
                    }
                    
                    
                    if(strType == GalleryType.Image as String)
                    {
                        strMessageBloxType = GalleryType.Image as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                    else if(strType == GalleryType.Video as String)
                    {
                        strMessageBloxType = GalleryType.Video as String
                        /*
                         if let strURL : String = dictMedia.valueForKey("thumb_url") as? String
                         {
                         if let img : UIImage = AppTheme().getImageFromUrl(strURL)
                         {
                         let data : NSData = UIImagePNGRepresentation(img)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         }
                         else
                         {
                         let data : NSData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         */
                        if var strVideoURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                             strVideoURL = strVideoURL.replacingOccurrences(of: " ", with: "%20")
                            //AppTheme().killFileOnPath("previewFile\(i).mp4")
                            //dictBloxDetails.setValue("previewFile\(i).mp4", forKey: "video")
                            var strImageThumbURL : String = String()
                            if var strURL : String = dictMedia.value(forKey: "thumbUrl") as? String {
                                   strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                                strImageThumbURL = strURL
                            }
                            loadVideoDataForURL(strVideoURL, fileName: "previewFile\(aryCountIncr).mp4", strURL: strImageThumbURL)
                            /*
                             AppTheme().callGetServiceForDownload(strVideoURL, fileNameToBeSaved: "previewFile\(i).mp4", completion: { (result, data) in
                             if(result == "success")
                             {
                             self.dictBloxDetails.setValue(data, forKey: "video")
                             }
                             })
                             */
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                        
                    }else if(strType == GalleryType.Sound as String){
                        strMessageBloxType = GalleryType.Sound as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
//                            self.playAudio(strURL)
//                            if var strMp3URL : String = dictMedia.value(forKey: "soundURL") as? String{
//                                strMp3URL = strMp3URL.replacingOccurrences(of: " ", with: "%20")
//                                self.playAudio(strMp3URL)
//                            }
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    } else if(strType == GalleryType.Gif as String){
                        strMessageBloxType = GalleryType.Gif as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            dictBloxDetails.setValue(strURL, forKey: "image")
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
                            
                            aryMessagePreivewData.add(dictTemp)
                            aryCountIncr += 1
                            processResponce()
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                }
                
            }
        }
        else
        {
            aryCountIncr = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
            vc.aryUserMessageData = self.aryMessagePreivewData
            vc.dictUserMessageData = self.dictUserMessageData
            vc.inbloxData = self.inbloxData
            vc.chatId = self.chatID
            vc.previewKind = "Bloxcast"
            vc.isSentByMe = self.isSentByMe
            vc.userID_for_profile = self.userID_for_profile
            vc.inputImageBgID = self.inputImageBgID
//            vc.bgImageName = self.bgImageName
            vc.strBgAudioPath = self.inputSoundBgID
            
            vc.bloxSubject = self.bloxSubject
            vc.user_name = self.bloxcastUsername
            vc.user_image = self.Blox_user_image
            vc.bloxDate = self.BloxDate
            //    vc.imgViewBackground.image = self.imgBackground.image
            
//            selectedIndex = nil
            self.navigationController?.pushViewController(vc, animated: true)
            
            //self.perform(#selector(MessageConversationVC.allSetUpWithPreviewData), with: nil, afterDelay: 1.0)
        }
        
    }
    
    
    
    func loadTextMessage() {
        
        dictBloxDetails.setValue(UIColor.white, forKey: "backgroundColor")
//        let imageData : Data = UIImageJPEGRepresentation(screenShot(), 0.8)!
        let imageData : Data = screenShot().jpegData(compressionQuality: 0.8)!
        
        dictBloxDetails.setValue(imageData, forKey: "image")
        
        let dictTemp : NSMutableDictionary = NSMutableDictionary()
        dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
        dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
        
        aryMessagePreivewData.add(dictTemp)
        aryCountIncr += 1
        processResponce()
    }
    
    func loadImageFromURL(_ strURL:String)
    {
        var imageData : Data = Data()
        
        let fileName = URL(fileURLWithPath: strURL).pathExtension
        if fileName == "gif"{
            let urlNew:String = WEB_URL.BaseURL + strURL//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let imageData1 = try? Data(contentsOf: URL(string: urlNew)!)
            {
                let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
                print("isAnimated: \(isImageAnimated)")
                imageData = imageData1
            } else {
                
            }
        } else {
            let img : UIImage = AppTheme().getImageFromUrl(WEB_URL.BaseURL + strURL)
            //        if let img : UIImage = AppTheme().getImageFromUrl("http://103.76.248.170:3001/" + strURL)
            if img != nil
            {
                imageData = img.pngData()!  //UIImagePNGRepresentation(img)!//
            } else {
                imageData = UIImage(named: "SingleBlox")!.pngData()!  //UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)//
            }
        }
        let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
        print("isAnimated: \(isImageAnimated)")
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
            
            aryMessagePreivewData.add(dictTemp)
            aryCountIncr += 1
            processResponce()
        }
    }
    
    func loadVideoDataForURL(_ strVideoURL:String, fileName: String, strURL : String)
    {
        var imageData : Data = Data()
//        if let img : UIImage = AppTheme().getImageFromUrl(strURL)  //commented on 25Jan
        let img : UIImage = AppTheme().getImageFromUrl(strVideoURL)
        if img != nil
        {
            imageData = img.pngData()! //UIImagePNGRepresentation(img)!
        }
        else
        {
//            imageData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!  //commented on 25Jan
            imageData = UIImage(named: "signup_background")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "signup_background"))!//
        }
        
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
        }
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
            {
                // do some task
                let url = WEB_URL.BaseURL + strVideoURL//"http://103.76.248.170:3001/"
//            let url = "http://103.76.248.170:3001/" + strVideoURL
                AppTheme().callGetServiceForDownload(url, fileNameToBeSaved: fileName, completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async {
                            // update some UI
                            self.dictBloxDetails.setValue(data, forKey: "video")
                            //  self.dictBloxDetails.setValue(64, forKey: "id")
                            
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(self.dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue("Video", forKey: "bloxType")
                            
                            self.aryMessagePreivewData.add(dictTemp)
                            self.aryCountIncr += 1
                            self.processResponce()
                        }
                    } else {
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
        }
    }
    func screenShot() -> UIImage {
        var DynamicView=UILabel(frame: CGRect(x:68, y:0, width:240, height:240))
        DynamicView.backgroundColor=UIColor.white
        //DynamicView.layer.cornerRadius=25
        //DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
        DynamicView.text = self.txtMessage
        DynamicView.textAlignment = NSTextAlignment.center
        DynamicView.font = UIFont(name:self.txtStyle, size: self.txtSize)
        let clr = Utilities.shared.convertHexToUIColor(hexColor: self.txtColor)
        DynamicView.textColor = clr
        //DynamicView.font = UIFont.systemFont(ofSize: self.txtSize)
        // DynamicView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DynamicView.lineBreakMode = .byWordWrapping
        DynamicView.numberOfLines = 0
        
        var frameRect : CGRect = DynamicView.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        //var textView=UITextView(frame: CGRect(x:68, y:0, width:240, height:240))
        
        
        DynamicView.layer.borderColor = UIColor.clear.cgColor
        UIGraphicsBeginImageContextWithOptions(DynamicView.bounds.size, DynamicView.isOpaque, 0.0)
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        DynamicView.layer.borderColor = AppTheme().themeColorBlue.cgColor
        // txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
    
//    func convertHexToUIColor(hexColor : String) -> UIColor {
//        
//        // define character set (include whitespace, newline character etc.)
//        let characterSet = CharacterSet.whitespacesAndNewlines as CharacterSet
//        
//        //trim unnecessary character set from string
//        var colorString : String = hexColor.trimmingCharacters(in: characterSet)
//        
//        // convert to uppercase
//        colorString = colorString.uppercased()
//        
//        //if # found at start then remove it.
//        if colorString.hasPrefix("#") {
//            let index = colorString.index(colorString.startIndex, offsetBy: 1)
//            colorString =  String(colorString[..<index])
//        }
//        
//        if colorString.count != 6 {
//            return UIColor.black
//        }
//        
//        // split R,G,B component
//        var rgbValue: UInt32 = 0
//        Scanner(string:colorString).scanHexInt32(&rgbValue)
//        let valueRed    = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
//        let valueGreen  = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
//        let valueBlue   = CGFloat(rgbValue & 0x0000FF) / 255.0
//        let valueAlpha  = CGFloat(1.0)
//        
//        // return UIColor
//        return UIColor(red: valueRed, green: valueGreen, blue: valueBlue, alpha: valueAlpha)
//    }
    
//    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
//        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
//        let imageRef = image.cgImage
//        
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
//        let context = UIGraphicsGetCurrentContext()
//        
//        // Set the quality level to use when rescaling
//        context!.interpolationQuality = CGInterpolationQuality.high
//        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
//        
//        context!.concatenate(flipVertical)
//        // Draw into the context; this scales the image
//        context!.draw(imageRef!, in: newRect)
//        
//        let newImageRef = context!.makeImage()! as CGImage
//        let newImage = UIImage(cgImage: newImageRef)
//        
//        // Get the resized image from the context and a UIImage
//        UIGraphicsEndImageContext()
//        
//        return newImage
//    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async {
            do {
                let asset = AVAsset(url: url)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let timestamp = CMTimeMakeWithSeconds(0.1, preferredTimescale: 600)
                print("Timestemp:   \(timestamp)")
                let cgImage = try imgGenerator.copyCGImage(at: timestamp, actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                DispatchQueue.main.async {
                    completion(thumbnail)
                }
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}
extension BloxHomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        arrConversations.count
        arrTopBloxCasts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingCell", for: indexPath) as! TrendingCell
//        let conversation = arrConversations[indexPath.row]
        let conversation = arrTopBloxCasts[indexPath.row]
        var subject  = conversation.subject
//        if subject == "" || subject == " "{
//            subject = "NA"
//        }
        cell.titleLbl.text = subject
            var urlstring = WEB_URL.BaseURL + (conversation.mediaUrl ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            if urlstring.hasSuffix(".mp4") || urlstring.hasSuffix(".gif") {
                var img: UIImage!
                DispatchQueue.global(qos: .userInitiated).async {
                     img  = AppTheme().getImageFromUrl(urlstring)
                    DispatchQueue.main.async {
                        cell.imgView.image = img
                    }
                }
//                cell.imgView.image = img
            } else {
                cell.imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        cell.starRating.rating = conversation.rating!
        cell.views.text = (conversation.views)!.formattedWithSeparator + " views"
    
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let collectionwidth = collectionView.bounds.width
        //    print( collectionwidth/4-6)
//        let  collectionwidth = UIScreen.main.bounds.width
        print(self.view.frame.size.width)
        return CGSize(width: (collectionwidth - 50)/3.5, height: (collectionwidth - 30)/3.5)
//        return CGSize(width: 100, height: 105)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let conversation = arrConversations[indexPath.row]
        let conversation = arrTopBloxCasts[indexPath.row]
        self.aryMessagePreivewData.removeAllObjects()
        self.chatID = conversation.chat_id ?? 0
//        if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
//           self.isSentByMe = true
//
//        }else {
//            self.isSentByMe = false
//        }
        self.bloxSubject = conversation.subject!
        self.Blox_user_image = conversation.user_image!
        self.bloxcastUsername = conversation.displayName!
        self.BloxDate =  Utilities.shared.getChangedDate(date: conversation.created_at!)
        
        self.aryMessagePreivewData.removeAllObjects()
        self.serverCallForPreview(chatID: conversation.chat_id ?? 0)
        self.chatID = conversation.chat_id ?? 0
        self.userID_for_profile = conversation.userid ?? 0
        self.bloxRating = conversation.rating!
        
       
    }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 15
        }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 2
    //    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //        return UIEdgeInsets(top: 5, left: 2, bottom: 0, right: 2)
    //    }
}
extension BloxHomeVC : FSPagerViewDataSource,FSPagerViewDelegate{
    // MARK:- FSPagerViewDataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
//        return imageNames.count
        return arrRecentBloxCasts.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "CustomFSPagerViewCell", at: index) as! CustomFSPagerViewCell
//        cell.imageView?.image = UIImage(named: "Beach.png")
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        // let imgBackground = cell.viewWithTag(100) as! UIImageView
//        let data = imageNames[index]
        let data = arrRecentBloxCasts[index]
        var urlstring =   data.chat_imageBg!//WEB_URL.BaseURL + data.imageBg!
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        //        imgBackground.kf.setImage(with: URL.init(string:urlstring))
        cell.imageView!.kf.setImage(with: URL.init(string:urlstring), placeholder: UIImage(named: "signup_background.png"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.bloxUser.text = data.displayName
        cell.bloxDate.text = Utilities.shared.getChangedDate(date: data.created_at!)
        cell.bloxSubject.text = data.subject
        var userImg = ""
        if data.user_image != nil {
            userImg = data.user_image!
        }
        var urlstring2 =  WEB_URL.BaseURL + userImg//data.user_image!
        urlstring2 = urlstring2.replacingOccurrences(of: " ", with: "%20")
        //        imgBackground.kf.setImage(with: URL.init(string:urlstring))
        cell.userImg!.kf.setImage(with: URL.init(string:urlstring2), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        
        var bgImg = ""
        if data.chat_imageBg != nil {
            bgImg = data.chat_imageBg!
        }
        var urlstring3 =  WEB_URL.BaseURL + bgImg//data.user_image!
        urlstring3 = urlstring3.replacingOccurrences(of: " ", with: "%20")
        //        imgBackground.kf.setImage(with: URL.init(string:urlstring))
        cell.imageBg!.kf.setImage(with: URL.init(string:urlstring3), placeholder: #imageLiteral(resourceName: "signup_background"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.ratingView.rating = data.rating!
        
        cell.contentView.bringSubviewToFront(cell.titleLogo)
        cell.contentView.bringSubviewToFront(cell.optionLayer)
        cell.contentView.bringSubviewToFront(cell.centreLogo)
//        cell.contentView.bringSubviewToFront(cell.collectionView)
        cell.contentView.bringSubviewToFront(cell.bottomView)
        cell.contentView.bringSubviewToFront(cell.ratingView)
        cell.configure(with: data)
        
        cell.cornerRadius = 10
        
        return cell
    }
    public func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {

//          let data = imageNames[index]
//       // print(data)
//        self.imageData = data
        return true
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        self.selectedIndex = pagerView.currentIndex
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
        self.pagerView.automaticSlidingInterval = 0.0
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
//        let backgroundImage = imageNames[index]
        
//        let backGroundImageID = backgroundImage.bgi_id ?? 0
//        let bgImageName = backgroundImage.bgi_name ?? ""
//        UserDefaults.standard.setValue(bgImageName, forKey: "BgImage")
//        UserDefaults.standard.setValue(backGroundImageID, forKey: "backGroundImageID")
//        let backgroundImage = arrRecentBloxCasts[index]
//        let bgImageName = backgroundImage.imageBg ?? ""
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC")
//            as! TalkBoxHomeVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
        let conversation = arrRecentBloxCasts[index]
        self.aryMessagePreivewData.removeAllObjects()
        self.chatID = conversation.chat_id ?? 0
        self.bloxSubject = conversation.subject!
        self.Blox_user_image = conversation.user_image!
        self.bloxcastUsername = conversation.displayName!
        self.BloxDate =  Utilities.shared.getChangedDate(date: conversation.created_at!)
        
        self.aryMessagePreivewData.removeAllObjects()
        self.serverCallForPreview(chatID: conversation.chat_id ?? 0)
        self.chatID = conversation.chat_id ?? 0
        self.userID_for_profile = conversation.userid ?? 0
        self.bloxRating = conversation.rating!
        
  
    }
}
extension BloxHomeVC{
    //BUTTON ACTION
    // ===============================================================
    // ===============================================================
    @IBAction func menuBtn(sender:UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func clickOnBtn(sender:UIButton){
        switch sender.tag {
        case 1:
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InBloxVC") as! InBloxVC
//           // self.navigationController?.pushViewController(vc, animated: true)
//            let transition = CATransition()
//            transition.duration = 0.5
//            transition.type = kCATransitionPush
//            transition.subtype = kCATransitionFromLeft
//            transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
//            view.window!.layer.add(transition, forKey: kCATransition)
//            vc.modalPresentationStyle = .fullScreen
//            present(vc, animated: false, completion: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InBloxVC") as! InBloxVC
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    @IBAction func allBloxCast(sender:UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxCastViewController") as! BloxCastViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func plusButton(sender:UIButton){
//        let backgroundImage = imageNames[self.selectedIndex]
//        let backGroundImageID = 0//backgroundImage.bgi_id ?? 0
//        let bgImageName = ""//backgroundImage.bgi_name ?? ""
//        UserDefaults.standard.setValue(bgImageName, forKey: "BgImage")
//        UserDefaults.standard.setValue(backGroundImageID, forKey: "backGroundImageID")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC")
            as! TalkBoxHomeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickOnBgBtn(sender:UIButton){
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC")
                          as! TalkBoxHomeVC
//               vc.backgroundImage = self.imageData
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push //kCATransitionPush//
        transition.subtype = CATransitionSubtype.fromBottom //kCATransitionReveal //
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut) //CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut) //
        view.window!.layer.add(transition, forKey: kCATransition)
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: false, completion: nil)
        // self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //WEB SERVICES
    // ===============================================================
    // ===============================================================
    //MARK:- Server Calls
    func serverCallForGetProfileDetail(){
        let params:NSDictionary = ["userid":AppSharedData.sharedInstance.getUser()?.id ?? ""]
        NetworkManager.sharedInstance.executeService(WEB_URL.getProfile, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            if success == true {
                if response?["header"] != nil {
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        let userData = [UserDetail].from(jsonArray: response?["result"] as! [JSON])
                        AppSharedData.sharedInstance.currentUser = userData?[0]
                        let user = AppSharedData.sharedInstance.currentUser
                        DispatchQueue.main.async{
 //                       self.welcomeLbl.text = "Welcome " + (user?.displayName  ?? "")
                        
                        let myName = user?.displayName  ?? ""
                        let nameArr = myName.split(separator: " ")
                        if nameArr.count == 1 {
                            let firstLetter = String(nameArr[0])
                            self.myShortName.text = String(Array(firstLetter)[0])
                        } else if nameArr.count == 2 {
                            let firstLetter = String(nameArr[0])
                            let secondLetter: String = String(nameArr[1])
                            self.myShortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                        } else {
                            self.myShortName.text = ""
                        }
                       let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
                       self.myImage.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
//                        self.serverCallForGetInBlox()
                        self.serverCallForGetTopBloxcast()
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                        
                        
                    } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                        
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                } else {
//                    AppSharedData.sharedInstance.showErrorAlert(message: "Some error occured")
                    self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
//    func serverCallForGetInBlox(){
//
//        let mobile  = "\(USER_DEFAULT.value(forKey: NOTIF.PHONE_CODE) ?? "")\(USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) ?? "")"
//        let params:NSDictionary = ["sender_id":AppSharedData.sharedInstance.currentUser?.userid ?? 0]
//
//
//        LoadingIndicatorView.show("Loading...")
//        NetworkManager.sharedInstance.executeService(WEB_URL.getMessages, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
//
////            LoadingIndicatorView.hide()
//
//            if success == true {
//                if let statusDic = response?["header"] as? NSDictionary {
//                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                        let inbloxData = [Inblox].from(jsonArray: response?["result"] as! [JSON])
//                        let result:[Inblox] = inbloxData!
//                        var msgIDs = [Int]()
//                        for inblox in result{
//                            msgIDs.append(inblox.message_id!)
//                        }
//
//                        self.messageID = msgIDs.randomElement()!
//                        self.serverCallForGetConversation()
//                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
//
//
//                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
////                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
//                    }
//                }
//            }
//        })
//    }
    func serverCallForGetBackGroundImages(){
        LoadingIndicatorView.show("Loading...")
        let param = ["type":"image" ]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getBackGround, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
            if success {
                print("Get Album Response is ----->>>>>>> \(response!)")
                if let statusDic = response?["header"] as? NSDictionary{
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        if let result = response?["result"] as? NSArray {
                            for _ in result {
                                self.imageNames = [BackGroundData].from(jsonArray: result as! [JSON])!
                                //self.collectionView.reloadData()
                                self.pagerView.reloadData()
                            }
                        }
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            let bgImageID = UserDefaults.standard.object(forKey: "backGroundImageID")
                            if bgImageID != nil {
                                for (index,item) in self.imageNames.enumerated() {
                                    let bgImgID = item.bgi_id
                                    if bgImgID == bgImageID as? Int {
                                        self.selectedIndex = index
                                        self.pagerView.scrollToItem(at: index, animated: true)
                                    }
                                }
                            }
                            LoadingIndicatorView.hide()
                        }
                    }
                }else {
                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
                }
            }
        }
    }
    
//    func serverCallForGetConversation(){
//        let params:NSDictionary = ["message_id":messageID]
//
//            LoadingIndicatorView.show("Loading...")
//
//        NetworkManager.sharedInstance.executeService(WEB_URL.getConversation, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
//            if success == true {
//                let statusDic = response?["header"] as! NSDictionary
//                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                    let inbloxData = [Conversation].from(jsonArray: response?["result"] as! [JSON])
//                    let result = inbloxData!.sorted(by: {
//                        (first: Conversation, second: Conversation) -> Bool in
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                        let dateAString = first.text_added
//                        let dateBString = second.text_added
//                        let dateA = dateFormatter.date(from: dateAString!)
//                        let dateB = dateFormatter.date(from: dateBString!)
//                        if dateA == nil || dateB == nil {
//                            return true
//                        }
//                        return dateA!.compare(dateB!) == .orderedAscending
//                    })
//                    self.arrConversations = result
//
//                    self.myCollectionView.reloadData()
//                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
//
//
//                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//
////                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
//                }
//            }
//        })
//    }
    func serverCallForGetTopBloxcast(){
//        let params:NSDictionary = ["message_id":messageID]
        
            LoadingIndicatorView.show("Loading...")
        
//        NetworkManager.sharedInstance.executeService(WEB_URL.topRatngsBlox, postParameters: params, completionHandler: {
            NetworkManager.sharedInstance.executeGetService(WEB_URL.topRatngsBlox, parameters: "", completionHandler: {
            (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [BloxCast].from(jsonArray: response?["Result"] as! [JSON])
//                    let result = inbloxData!.sorted(by: {
//                        (first: TopBloxCast, second: TopBloxCast) -> Bool in
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                        let dateAString = first.text_added
//                        let dateBString = second.text_added
//                        let dateA = dateFormatter.date(from: dateAString!)
//                        let dateB = dateFormatter.date(from: dateBString!)
//                        if dateA == nil || dateB == nil {
//                            return true
//                        }
//                        return dateA!.compare(dateB!) == .orderedAscending
//                    })
//                    self.arrConversations = result
                    
                    self.arrTopBloxCasts = inbloxData!
                    for item in self.arrTopBloxCasts {
                        let IDs = bloxIds(chat_id: item.chat_id!, message_id:0 )
                        self.bloxIDsArr.append(IDs)
                    }
                    DispatchQueue.main.async{
                        self.myCollectionView.reloadData()
                    }
                    let when = DispatchTime.now() + 0.5
                    DispatchQueue.main.asyncAfter(deadline: when){
                        self.serverCallForGetRecentBloxcast()
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
    func serverCallForGetRecentBloxcast(){
            LoadingIndicatorView.show("Loading...")
            NetworkManager.sharedInstance.executeGetService(WEB_URL.recentBloxcast, parameters: "", completionHandler: {
            (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [RecentBloxCast].from(jsonArray: response?["Result"] as! [JSON])
                    
                    self.arrRecentBloxCasts = inbloxData!
                    for item in self.arrRecentBloxCasts {
                        let IDs = bloxIds(chat_id: item.chat_id!, message_id:0 )
                        self.bloxIDsArr.append(IDs)
                    }
                    self.pagerView.reloadData()
                    
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
}



extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        return formatter
    }()
}
extension Numeric {
    var formattedWithSeparator: String { Formatter.withSeparator.string(for: self) ?? "" }
}
