//
//  HomeVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/4/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import  Gloss

class HomeVC: UIViewController,UITableViewDelegate,UICollectionViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate {

    @IBOutlet var tableView:UITableView!
    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet var viewOptions:UIView!
    @IBOutlet weak var welcomeLbl: UILabel!
    var arrOptions = [UIImage]()
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
       
        arrOptions.append(#imageLiteral(resourceName: "edit_text_icon"))
        arrOptions.append(#imageLiteral(resourceName: "tb_icon"))
        arrOptions.append(#imageLiteral(resourceName: "search_icon"))
        arrOptions.append(#imageLiteral(resourceName: "music_icon"))
        arrOptions.append(#imageLiteral(resourceName: "camera_icon"))
        arrOptions.append(#imageLiteral(resourceName: "paint_icon"))
        arrOptions.append(#imageLiteral(resourceName: "setting_icon"))
        arrOptions.append(#imageLiteral(resourceName: "blox_icon"))
        arrOptions.append(#imageLiteral(resourceName: "message_icon"))
        
        let user = AppSharedData.sharedInstance.currentUser
        if user != nil{
            welcomeLbl.text = "Welcome " + (user?.displayName  ?? "")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.view.removeGestureRecognizer((navigationController?.interactivePopGestureRecognizer)!)
        
        DispatchQueue.global(qos: .background).async {
            self.serverCallForGetProfileDetail()
        }
        
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
                                      })
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self; self.navigationController?.view.addGestureRecognizer((navigationController?.interactivePopGestureRecognizer)!)
        
    }
    //MARK:- UITableView Delegates and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell")
        return cell!
    }
    
    //MARK:- UICollectionView Delegate and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionCell", for: indexPath)
        let img = cell.viewWithTag(100) as! UIImageView
        img.image = arrOptions[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.viewOptions.layoutIfNeeded()
        let width = self.viewOptions.frame.size.width / 3 - 10
        let height = self.viewOptions.frame.size.height / 3 - 30
        return CGSize.init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1 {
//            fatalError()
        } else if indexPath.row == 6 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 8 {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewInbloxViewController") as! NewInbloxViewController
//            self.navigationController?.pushViewController(vc, animated: true)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InBloxVC") as! InBloxVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Server Calls
    func serverCallForGetProfileDetail(){
        let params:NSDictionary = ["userid":AppSharedData.sharedInstance.getUser()?.id ?? ""]
        NetworkManager.sharedInstance.executeService(WEB_URL.getProfile, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                if response?["header"] != nil {
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        let userData = [UserDetail].from(jsonArray: response?["result"] as! [JSON])
                        AppSharedData.sharedInstance.currentUser = userData?[0]
                        let user = AppSharedData.sharedInstance.currentUser
                        self.welcomeLbl.text = "Welcome " + (user?.displayName  ?? "")
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                        
                        
                    } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                        
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                } else {
//                    AppSharedData.sharedInstance.showErrorAlert(message: "Some error occured")
                    self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
                }
            }
        })
    }
}
