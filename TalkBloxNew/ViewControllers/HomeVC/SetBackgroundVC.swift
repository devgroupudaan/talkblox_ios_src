//
//  SetBackgroundVC.swift
//  TalkBloxNew
//
//  Created by Apple on 06/02/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit

class SetBackgroundVC: UIViewController {
     @IBOutlet var imgBackground : UIImageView!
    @IBOutlet var btnPaint : UIButton!
    @IBOutlet var btnSend : UIButton!
    @IBOutlet var btnAudio : UIButton!
    @IBOutlet var btnPreview : UIButton!
    var backGroundImageID:Int = 0
     var bgImageName = ""
    var backgroundImage : BackGroundData?
    override func viewDidLoad() {
        super.viewDidLoad()
               btnPaint.layer.cornerRadius = 5.0
               btnPaint.clipsToBounds = true
               
               btnPreview.layer.cornerRadius = 5.0
               btnPreview.clipsToBounds = true
               
               btnSend.layer.cornerRadius = 5.0
               btnSend.clipsToBounds = true
               
               btnAudio.layer.cornerRadius = 5.0
               btnAudio.clipsToBounds = true
        if let imageData = backgroundImage{
            showBgImage(backgroundImage:imageData)
        }
        else{
            self.imgBackground.image = UIImage(named:"Beach.png")
        }
        
    }
    func showBgImage(backgroundImage:BackGroundData){
        self.backGroundImageID = backgroundImage.bgi_id ?? 0
               self.bgImageName = backgroundImage.bgi_name ?? ""
               UserDefaults.standard.setValue(self.bgImageName, forKey: "BgImage")
               UserDefaults.standard.setValue(self.backGroundImageID, forKey: "backGroundImageID")
               var stringUrl = WEB_URL.BaseURL + backgroundImage.bgi_name!
               stringUrl = stringUrl.replacingOccurrences(of: " ", with: "%20")
               
               self.imgBackground.kf.setImage(with: URL.init(string: stringUrl))
    }
    @IBAction func cickBackBtn(sender:UIButton){
       // self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
               transition.duration = 0.5
               transition.type = CATransitionType.push
               transition.subtype = CATransitionSubtype.fromRight
               transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut)
               view.window!.layer.add(transition, forKey: kCATransition)
               self.dismiss(animated: true, completion: nil)
    }
}
