//
//  CustomFSPagerViewCell.swift
//  TalkBloxNew
//
//  Created by Mahendra Lariya on 12/02/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import FSPagerView
import Cosmos

class CustomFSPagerViewCell: FSPagerViewCell, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var imageBg: UIImageView!
    @IBOutlet weak var titleLogo: UIImageView!
    @IBOutlet weak var optionLayer: UIImageView!
    @IBOutlet weak var centreLogo: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewWidth: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var bgIconYPos: NSLayoutConstraint!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var bloxUser: UILabel!
    @IBOutlet weak var bloxDate: UILabel!
    @IBOutlet weak var bloxSubject: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    var arrRecentBloxcast: RecentBloxCast!
    var frames = [bloxFrame]()
    let gradientLayer = CAGradientLayer()
    
    override func layoutSubviews() {
        collectionView.layoutIfNeeded()
        collectionView.setNeedsDisplay()
        super.layoutSubviews()
        self.layoutIfNeeded()
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.collectionView.register(UINib(nibName: "RecentBCCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RecentBCCollectionViewCell")
        self.bringSubviewToFront(titleLogo)
        self.bringSubviewToFront(optionLayer)
        self.bringSubviewToFront(centreLogo)
//        self.bringSubviewToFront(collectionView)
        self.bringSubviewToFront(bottomView)
    }
    
    func configure(with conversationArr: RecentBloxCast){
        DispatchQueue.main.async{
            //        collectionView.delegate = self
            //        collectionView.dataSource = self
            self.arrRecentBloxcast = conversationArr
            self.frames = conversationArr.frames!
            
            let currentFrame = self.frames[0]
            var urlstring = WEB_URL.BaseURL + (currentFrame.mediaUrl ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            if urlstring.hasSuffix(".mp4") || urlstring.hasSuffix(".gif"){
                DispatchQueue.global(qos: .userInitiated).async {
                    let img : UIImage = AppTheme().getImageFromUrl(urlstring)
                    DispatchQueue.main.async {
                        self.centreLogo.image = img
                    }
                }
//                let img : UIImage = AppTheme().getImageFromUrl(urlstring)
//                self.centreLogo.image = img
            } else {
                self.centreLogo.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
//        self.layoutSubviews()
//        self.layoutIfNeeded()
//        collectionView.reloadData()
        
//        let width = self.collectionView.frame.width
//        var height: CGFloat!
//        let screenSize = self.bounds
//        if frames.count == 1 {
//            height = screenSize.width * 0.55
//        } else if frames.count == 2 {
//            height = ((screenSize.width * 0.55) - 5 )/2
//        }
        
/*        let  H: CGFloat!, M: CGFloat
        let screenSize = self.bounds //UIScreen.main.bounds
        switch frames.count {
        case 1:
            H = screenSize.width * 0.55
            M = 1.065 
        case 2:
            H = ((screenSize.width * 0.55) - 10 ) * 2/3
            M  = 0.9
        case 3:
            H = ((screenSize.width * 0.55) - 10 )/3
            M  = 0.9
        case 4:
            H = ((screenSize.width * 0.55) - 10 ) * 2/3
            M  = 1
        case 5:
            H = ((screenSize.width * 0.55) - 10 ) * 2/3
            M = 1
        case 6:
            H = ((screenSize.width * 0.55) - 10 ) * 2/3
            M = 1
        default:
            H = screenSize.width * 0.55
            M = 1.065
        }
        self.collectionViewHeight.constant = H
//        self.bgIconYPos.constant = M
        self.layoutIfNeeded()
        self.layoutSubviews()
//        bottomView.layer.sublayers?.removeAll()
//        if let _ = (bottomView.layer.sublayers?.compactMap { $0 as? CAGradientLayer })?.first {
//            print("gradientLayer has not been removed")
//        } else {
//            print("yay it's removed")
//        }
 */
 
        if ((bottomView.layer.sublayers?.contains(gradientLayer)) != nil){
            for layer in bottomView.layer.sublayers! {
                if layer == gradientLayer {
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        gradientLayer.colors = [#colorLiteral(red: 0.02603700012, green: 0.2324451804, blue: 0.35076195, alpha: 1).withAlphaComponent(0.9).cgColor,
                                UIColor.black.withAlphaComponent(0.0).cgColor]
//        let frame = CGRect(x: 0, y: -15, width: self.frame.width, height: 100 )
        gradientLayer.frame = bottomView.bounds
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        bottomView.layer.insertSublayer(gradientLayer, at: 0)
//        collectionView.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        
        self.layoutSubviews()
        self.layoutIfNeeded()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        frames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentBCCollectionViewCell", for: indexPath) as! RecentBCCollectionViewCell
        let currentFrame = frames[indexPath.row]
        var urlstring = WEB_URL.BaseURL + (currentFrame.mediaUrl ?? "")
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        if urlstring.hasSuffix(".mp4") || urlstring.hasSuffix(".gif"){
            DispatchQueue.global(qos: .userInitiated).async {
                let img : UIImage = AppTheme().getImageFromUrl(urlstring)
                DispatchQueue.main.async {
                    cell.imgView.image = img
                }
            }
//            let img : UIImage = AppTheme().getImageFromUrl(urlstring)
//            cell.imgView.image = img
        } else {
        cell.imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        cell.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let  collectionwidth = UIScreen.main.bounds.width
        let width  = self.collectionView.frame.width
        let cellWidth = (width - 10)/3
        
        let W: CGFloat!, H: CGFloat!
        let screenSize = self.bounds //UIScreen.main.bounds
        switch frames.count {
        case 1:
            W = screenSize.width * 0.55
            H = screenSize.width * 0.55
        case 2:
            W = ((screenSize.width * 0.55) - 5 )/2
            H = screenSize.width * 0.55
        case 3:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = screenSize.width * 0.55
        case 4:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = ((screenSize.width * 0.55) - 5 )/2
        case 5:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = ((screenSize.width * 0.55) - 5 )/2
        case 6:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = ((screenSize.width * 0.55) - 5 )/2
        case 7:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = ((screenSize.width * 0.55) - 10 )/3
        case 8:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = ((screenSize.width * 0.55) - 10 )/3
        case 9:
            W = ((screenSize.width * 0.55) - 10 )/3
            H = ((screenSize.width * 0.55) - 10 )/3
        default:
            W = screenSize.width * 0.55
            H = screenSize.width * 0.55
        }
        
        return CGSize(width: W, height: W)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
////        let conversation = arrConversations[indexPath.row]
//        let conversation = arrTopBloxCasts[indexPath.row]
//        self.aryMessagePreivewData.removeAllObjects()
//        self.chatID = conversation.chat_id ?? 0
////        if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
////           self.isSentByMe = true
////
////        }else {
////            self.isSentByMe = false
////        }
//        self.bloxSubject = conversation.subject!
//        self.Blox_user_image = conversation.user_image!
//        self.bloxcastUsername = conversation.displayName!
//        self.BloxDate =  Utilities.shared.getChangedDate(date: conversation.created_at!)
//
//        self.aryMessagePreivewData.removeAllObjects()
//        self.serverCallForPreview(chatID: conversation.chat_id ?? 0)
//        self.chatID = conversation.chat_id ?? 0
//        self.userID_for_profile = conversation.userid ?? 0
//    }

}
