//
//  TabViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 10/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit

class TabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        tabBar.delegate = self
       
        
//        if #available(iOS 13.0, *) {
//            let appearance = tabBar.standardAppearance
//            appearance.shadowImage = nil
//            appearance.shadowColor = nil
//            self.tabBar.standardAppearance = appearance;
//        } else {
//            // Fallback on earlier versions
//            self.tabBar.shadowImage = UIImage()
//            self.tabBar.backgroundImage = UIImage()
//        }
        UITabBar.appearance().backgroundColor = UIColor.clear
        if #available(iOS 13.0, *) {
            self.tabBar.standardAppearance.backgroundColor = UIColor.clear
        } else {
            // Fallback on earlier versions
        }
        self.tabBar.backgroundImage = getColoredImage(color: .clear, size: CGSize(width: view.frame.width, height: 100))
        self.tabBar.unselectedItemTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        // Do any additional setup after loading the view.
    }
    
    func getColoredImage(color: UIColor, size: CGSize) -> UIImage {
            let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: size)
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            color.setFill()
            UIRectFill(rect)
            guard let image:UIImage = UIGraphicsGetImageFromCurrentImageContext() else { return UIImage() }
            UIGraphicsEndImageContext()
            return image
        }
    
//    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        <#code#>
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
