//
//  BloxPreviewVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/7/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit

class BloxPreviewVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet var viewPreview: UIView!
    @IBOutlet var collectionView:UICollectionView!
    
    //MARK- UIViewCOntroller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     
    }
    //MARK:- UICollectionView Delegates and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviewCell", for: indexPath)
        let imgPreview = cell.viewWithTag(100) as! UIImageView
        imgPreview.image = #imageLiteral(resourceName: "option_layer")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.viewPreview.layoutIfNeeded()
        let width = self.viewPreview.frame.size.width / 3 - 10
        let height = self.viewPreview.frame.size.height / 3 - 30
        return CGSize.init(width: width, height: height)
    }

}
