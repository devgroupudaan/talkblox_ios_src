//
//  TalkBoxHomeVC.swift
//  TalkBox
//
//  Created by mac on 11/23/15.
//  Copyright © 2015 MobiWebTech. All rights reserved.
//

import Foundation
import UIKit
import Gloss
import Alamofire
import JGProgressHUD
import RAReorderableLayout
import ContactsUI
import IQKeyboardManagerSwift

struct mixedContacts {
    var type: String
    var name: String
    var gInfo: Group
    var cInfo: CNContact
}

class selectContactsCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var cName: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
}

class TalkBoxHomeVC : UIViewController, UITextFieldDelegate, RAReorderableLayoutDelegate, RAReorderableLayoutDataSource,BackgroundSelectionDelegates,CNContactPickerDelegate,BackGroundSoundVCDelegates,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet var btnGallery : UIButton!
    @IBOutlet var btnMessages : UIButton!
    @IBOutlet var btnMyAccount : UIButton!
    @IBOutlet var btnPaint : UIButton!
    @IBOutlet var btnSend : UIButton!
    @IBOutlet var btnAudio : UIButton!
    @IBOutlet var btnPreview : UIButton!
    @IBOutlet var btnBloxCollection : [UIButton]!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var imgBackground : UIImageView!
      @IBOutlet var txtSubject : UITextField!
    @IBOutlet weak var AllComponentViewHeightLayout: NSLayoutConstraint!
        @IBOutlet var imgBloxBubble : UIImageView!
    //var dictBloxData : NSMutableDictionary!
    @IBOutlet weak var contactsPickerView: UIView!
    @IBOutlet weak var contactsSearchBar: UISearchBar!
    @IBOutlet weak var contactsTable: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    //***** Blox Bubble Layouts *****
    @IBOutlet weak var imgBubbleHeight: NSLayoutConstraint!
    @IBOutlet weak var imgBubbleWidth: NSLayoutConstraint!
    @IBOutlet weak var viewBubbleHeight: NSLayoutConstraint!
    @IBOutlet weak var viewBubbleWidth: NSLayoutConstraint!
    
    //----- Collection view setting -----
    @IBOutlet weak var collTopLayout: NSLayoutConstraint!
    @IBOutlet weak var collBottomLayout: NSLayoutConstraint!

    @IBOutlet var lblReplyTitle : UILabel!
    @IBOutlet var vwOption : UIView!
    
    @IBOutlet weak var optionView: UIView!
    
    
    var isReply : Bool = false

    var aryReplyTalkBloxContacts = [Contact]()
    var arrContactIds:[AnyObject] = [AnyObject]()
    //@IBOutlet var lblLoginUserName : UILabel!
    var dictUserDetails : NSMutableDictionary!
    var inbloxData : Inblox?
    
    var backGroundImageID:Int = 0
    var bgImageName = ""
    var bgAudioPath = ""
    
    var backGroundSoundID:Int = 0
    var bloxIndex : NSInteger = 0
    var isDoubleTapped : Bool = false
    var selectedIndexPath : IndexPath = IndexPath()
    
    var loading : JGProgressHUD = JGProgressHUD(style: .dark)
    
    var imagesForSection0: [UIImage] = []
    
    var arrBloxDetails : NSMutableArray = NSMutableArray()
    //***** send Message Data
    var allDataArray : [String : AnyObject]!
    var allDataA : [AnyObject]!
    var bgAudioId : String = ""
//    var arrContacts = NSMutableArray()
    var arrContacts = [String]()
    var arrGroupIds = [Int]()
    var contactIds : [String : AnyObject]!

    var strReplyAllMessageId : NSInteger = 0
    var aryContactRelpyAll : NSMutableArray = NSMutableArray()
    
    //----- Preview stuff support -----
    var dictReplyData : NSMutableDictionary!
    var dictDetails : NSDictionary!
    var isCamera : Bool = false
    
    let contactStore = CNContactStore()
    var searchActive : Bool = false
    var contacts = [CNContact]()
    var searchResults = [mixedContacts]()
    
    var allContacts = [mixedContacts]()
    
    var groupSearchActive : Bool = false
    var groups = [Group]()
    var searchGroupResults = [Group]()
    var arrGroupIDs = [Int]()
    
    var selectionType = "contacts"
    
    //MARK:- View did load
    override func viewDidLoad(){
        super.viewDidLoad()
      
        contactsSearchBar.delegate = self
        contactsSearchBar.backgroundColor = .clear
//        contactsSearchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            contactsSearchBar.backgroundImage = UIImage()
            contactsSearchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        contactsTable.delegate = self
        contactsTable.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
        btnPaint.layer.cornerRadius = 5.0
        btnPaint.clipsToBounds = true
        
        btnPreview.layer.cornerRadius = 5.0
        btnPreview.clipsToBounds = true
        
        btnSend.layer.cornerRadius = 5.0
        btnSend.clipsToBounds = true
        
        btnAudio.layer.cornerRadius = 5.0
        btnAudio.clipsToBounds = true
        
//        if let BgImage = UserDefaults.standard.object(forKey: "BgImage")  as? String
//        {
//            self.bgImageName = BgImage
//            self.backGroundImageID = UserDefaults.standard.object(forKey: "backGroundImageID") as! Int
//            if self.bgImageName != "" {
//                var stringUrl = WEB_URL.BaseURL + BgImage
//                stringUrl = stringUrl.replacingOccurrences(of: " ", with: "%20")
//
//                self.imgBackground.kf.setImage(with: URL.init(string: stringUrl))
//            }
//        }
//
//        if((dictUserDetails) != nil){
//            //print(dictUserDetails)
//
//            //lblLoginUserName.text =  String(format: "Welcome %@",dictUserDetails["username"] as! String)
//        }
//        /*
//         for btnBlox in btnBloxCollection
//         {
//         btnBlox.addTarget(self, action: "showMessage:", forControlEvents: UIControlEvents.TouchUpInside)
//         }
//         */
//        //        let titleImageView : UIView = AppTheme().setNavigationBarTitleImage()
//        //        titleImageView.frame = CGRectMake(0, 0, 80, 22)
//        //        self.navigationItem.titleView = titleImageView
//
//        //running code
//        /*
//         let imgViewTitle : UIImageView = UIImageView(image: UIImage(named: "TalkbloxLogo"))
//         imgViewTitle.tag = 123456
//         imgViewTitle.frame = CGRectMake(self.view.frame.size.width/2 - 60, 7, 120, (navigationController?.navigationBar.frame.size.height)! - 14)
//         */
//
//
//        //self.view.addSubview(imgViewTitle)
//        //navigationController?.navigationBar.addSubview(imgViewTitle)
//
//        //loading.showInView(self.view)
//        //loadBloxContent()
//
//
//        //--------
//       // if((dictReplyData) != nil)
//
////        if arrContactIds.count > 0 {
////            callSendMessage()
////        }
//        if aryReplyTalkBloxContacts.count > 0 {
//
//            self.navigationItem.hidesBackButton = true
//            btnBack.isHidden = false
//
//
//            lblReplyTitle.isHidden = false
//            vwOption.isHidden = true
//
//            //-- To empty the arry to add new data for reply --
//            let userDefaults = UserDefaults.standard
//            if let key = userDefaults.object(forKey: "bloxArray")  as? Data
//            {
//                let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
//                let array : NSMutableArray = arrayData! as! NSMutableArray
//                array.removeAllObjects()
//
//                let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
//                let data = NSKeyedArchiver.archivedData(withRootObject: arr)
//
//                if(AppTheme().killFileOnPath("backgroundAudio.caf") == "File Killed"){
//                    print("file deleted")
//                }
//
//                userDefaults.removeObject(forKey: "bloxBubbleAudio")
//                userDefaults.set(data, forKey: "bloxArray")
//                userDefaults.synchronize()
//            }
//        }
//        else {
//            self.navigationItem.leftBarButtonItem = nil
//            self.navigationItem.hidesBackButton = true
//            btnBack.isHidden = true
//            lblReplyTitle.isHidden = true
//            vwOption.isHidden = false
//        }
//
//        //--------
//        //****
//
//        //        let nib = UINib(nibName: "verticalCell", bundle: nil)
//        //        self.collectionView.registerNib(nib, forCellWithReuseIdentifier: "cell")
//        self.collectionView.delegate = self as UICollectionViewDelegate
//        self.collectionView.dataSource = self as UICollectionViewDataSource
//        for index in 0..<9 {
//          /*  let name = "SingleBlox"
//            let image = UIImage(named: name)
//            self.imagesForSection0.append(image!)*/
//        }
//
//        if(self.view.frame.size.height < 486)
//        {
//          // AllComponentViewHeightLayout.constant = 476
//        }
//        else
//        {
//           // AllComponentViewHeightLayout.constant = self.view.frame.size.height - 64
//        }
//        //        //Adding Double Tap Gesture
//        //
//
//        //        doubleTapGesture.numberOfTapsRequired = 2  // add double tap
//        //        self.collectionView.addGestureRecognizer(doubleTapGesture)
//
//        print(self.view.frame)
//
//
//        // Condition for setting up the bubble blox width height
//
//        let bounds = UIScreen.main.bounds
//        let height = bounds.size.height
//        let width = bounds.size.width
//
//        switch height {
//        case 480.0:
//            print("iPhone 3,4")
//        case 568.0:
//            print("iPhone 5")
//        case 667.0:
//
//            viewBubbleWidth.constant = width - 10
//            viewBubbleHeight.constant = width + 43
//            imgBubbleWidth.constant = width - 10
//            imgBubbleHeight.constant = width + 43
//
//
//            let userDefaults = UserDefaults.standard
//            if let key = userDefaults.object(forKey: "bubbleImage")  as? String{
//                if(key != ""){
//                    if(key == "message_background9"){
//                        collTopLayout.constant = 19
//                        collBottomLayout.constant = -54
//                    }
//                }
//            }
//
//            print("iPhone 6")
//        case 736.0:
//            print("iPhone 6+")
//            viewBubbleWidth.constant = width - 10
//            viewBubbleHeight.constant = width + 60
//            imgBubbleWidth.constant = width - 10
//            imgBubbleHeight.constant = width + 60
//            collTopLayout.constant = 19
//            collBottomLayout.constant = -54
//            let userDefaults = UserDefaults.standard
//            if let key = userDefaults.object(forKey: "bubbleImage")  as? String
//            {
//                if(key != "")
//                {
//                    if(key == "message_background9")
//                    {
//                        collTopLayout.constant = 23
//                        collBottomLayout.constant = -50
//                    }
//                }
//            }
//
//        default:
//            print("not an iPhone")
//
//            viewBubbleHeight.constant = (width * 1078)/923
//            imgBubbleHeight.constant = (width * 1078)/923
//            viewBubbleWidth.constant = width - 10//(width * 923)/1078
//            imgBubbleWidth.constant = width - 10//(width * 923)/1078
//
//            print(width)
//        }
        
    }
    
    //MARK:- Functions calls
    func firstCall(){
        if let BgImage = UserDefaults.standard.object(forKey: "BgImage")  as? String
        {
            self.bgImageName = BgImage
            self.backGroundImageID = UserDefaults.standard.object(forKey: "backGroundImageID") as! Int
            if self.bgImageName != "" {
                var stringUrl = WEB_URL.BaseURL + BgImage
                stringUrl = stringUrl.replacingOccurrences(of: " ", with: "%20")
                
                self.imgBackground.kf.setImage(with: URL.init(string: stringUrl))
            }
        } else {
            self.imgBackground.image = #imageLiteral(resourceName: "signup_background")
        }
        
        if((dictUserDetails) != nil){
            //print(dictUserDetails)
            
            //lblLoginUserName.text =  String(format: "Welcome %@",dictUserDetails["username"] as! String)
        }
        /*
         for btnBlox in btnBloxCollection
         {
         btnBlox.addTarget(self, action: "showMessage:", forControlEvents: UIControlEvents.TouchUpInside)
         }
         */
        //        let titleImageView : UIView = AppTheme().setNavigationBarTitleImage()
        //        titleImageView.frame = CGRectMake(0, 0, 80, 22)
        //        self.navigationItem.titleView = titleImageView
        
        //running code
        /*
         let imgViewTitle : UIImageView = UIImageView(image: UIImage(named: "TalkbloxLogo"))
         imgViewTitle.tag = 123456
         imgViewTitle.frame = CGRectMake(self.view.frame.size.width/2 - 60, 7, 120, (navigationController?.navigationBar.frame.size.height)! - 14)
         */
        
        
        //self.view.addSubview(imgViewTitle)
        //navigationController?.navigationBar.addSubview(imgViewTitle)
        
        //loading.showInView(self.view)
        //loadBloxContent()
        
        
        //--------
       // if((dictReplyData) != nil)
        
//        if arrContactIds.count > 0 {
//            callSendMessage()
//        }
        if aryReplyTalkBloxContacts.count > 0 {
         
            self.navigationItem.hidesBackButton = true
            btnBack.isHidden = false
            
            
            lblReplyTitle.isHidden = false
            vwOption.isHidden = true
            
            //-- To empty the arry to add new data for reply --
            let userDefaults = UserDefaults.standard
            if let key = userDefaults.object(forKey: "bloxArray")  as? Data
            {
                let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                let array : NSMutableArray = arrayData! as! NSMutableArray
                array.removeAllObjects()
                
                let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
                let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                
                if(AppTheme().killFileOnPath("backgroundAudio.caf") == "File Killed"){
                    print("file deleted")
                }
                
                userDefaults.removeObject(forKey: "bloxBubbleAudio")
                userDefaults.set(data, forKey: "bloxArray")
                userDefaults.synchronize()
            }
        }
        else {
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.hidesBackButton = true
            btnBack.isHidden = true
            lblReplyTitle.isHidden = true
            vwOption.isHidden = false
        }

        //--------
        //****
        
        //        let nib = UINib(nibName: "verticalCell", bundle: nil)
        //        self.collectionView.registerNib(nib, forCellWithReuseIdentifier: "cell")
        self.collectionView.delegate = self as UICollectionViewDelegate
        self.collectionView.dataSource = self as UICollectionViewDataSource
        for index in 0..<9 {
          /*  let name = "SingleBlox"
            let image = UIImage(named: name)
            self.imagesForSection0.append(image!)*/
        }
        
        if(self.view.frame.size.height < 486)
        {
          // AllComponentViewHeightLayout.constant = 476
        }
        else
        {
           // AllComponentViewHeightLayout.constant = self.view.frame.size.height - 64
        }
        //        //Adding Double Tap Gesture
        //
        
        //        doubleTapGesture.numberOfTapsRequired = 2  // add double tap
        //        self.collectionView.addGestureRecognizer(doubleTapGesture)
        
        print(self.view.frame)
        

        // Condition for setting up the bubble blox width height
        
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        let width = bounds.size.width
        
        switch height {
        case 480.0:
            print("iPhone 3,4")
        case 568.0:
            print("iPhone 5")
        case 667.0:
            
            viewBubbleWidth.constant = width - 10
            viewBubbleHeight.constant = width + 43
            imgBubbleWidth.constant = width - 10
            imgBubbleHeight.constant = width + 43
            
            
            let userDefaults = UserDefaults.standard
            if let key = userDefaults.object(forKey: "bubbleImage")  as? String{
                if(key != ""){
                    if(key == "message_background9"){
                        collTopLayout.constant = 19
                        collBottomLayout.constant = -54
                    }
                }
            }
            
            print("iPhone 6")
        case 736.0:
            print("iPhone 6+")
            viewBubbleWidth.constant = width - 10
            viewBubbleHeight.constant = width + 60
            imgBubbleWidth.constant = width - 10
            imgBubbleHeight.constant = width + 60
            collTopLayout.constant = 19
            collBottomLayout.constant = -54
            let userDefaults = UserDefaults.standard
            if let key = userDefaults.object(forKey: "bubbleImage")  as? String
            {
                if(key != "")
                {
                    if(key == "message_background9")
                    {
                        collTopLayout.constant = 23
                        collBottomLayout.constant = -50
                    }
                }
            }
            
        default:
            print("not an iPhone")
            
            viewBubbleHeight.constant = (width * 1078)/923
            imgBubbleHeight.constant = (width * 1078)/923
            viewBubbleWidth.constant = width - 10//(width * 923)/1078
            imgBubbleWidth.constant = width - 10//(width * 923)/1078
          
            print(width)
        }
    }

    
    override func viewWillAppear(_ animated: Bool)
    {
        
        IQKeyboardManager.shared.enable = false
        let userDefaults = UserDefaults.standard
        if let key = userDefaults.object(forKey: "bloxArray")  as? Data
        {
            if USER_DEFAULT .bool(forKey: NOTIF.IS_LOGIN) == true {
                userDefaults.removeObject(forKey: "bloxArray")
            } else {
            let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
            arrBloxDetails = arrayData! as! NSMutableArray
//            self.loading.dismiss()
                LoadingIndicatorView.hide()
            self.collectionView.reloadData()
            }
        } else {
            arrBloxDetails.removeAllObjects()
            self.collectionView.reloadData()
        }
        
        if let key = userDefaults.object(forKey: "bubbleImage")  as? String{
            if(key != "")
            {
                imgBloxBubble.image = UIImage(named: key)
            }
        }
        
        
//        let userDefaults = UserDefaults.standardUserDefaults
//        if let key = userDefaults.object(forKey: "bloxArray")  as? NSData
//         {
//            let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key as Data)
//            let array : NSMutableArray = (arrayData! as AnyObject).mutableCopy() as! NSMutableArray
//         self.arrBloxDetails = array.mutableCopy() as! NSMutableArray
//         self.collectionView.reloadData()
//         }
         
        
        firstCall()
        
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
//                                        self.loading.dismiss()
                                      })
        
        tabBarController?.tabBar.isHidden = true
        
//        self.serverCallForGetAllGroups()
    }
    
    //MARK:- Helper Methods
//    func showContactPicker()  {
//        let cnPicker = CNContactPickerViewController()
//        cnPicker.delegate = self
//        self.present(cnPicker, animated: true, completion: nil)
//    }
    
    //MARK:- ContactPicker Delegates
//    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
//        contacts.forEach { contact in
//            for number in contact.phoneNumbers {
//                let tmpNumber =   number.value
//                let phoneNumber = tmpNumber.value(forKey: "digits") as! String
//                print("number is = \(phoneNumber)")
//                arrContacts.add(phoneNumber)
//            }
//            arrContacts.add("7837203756")
//            arrContacts.add("6280395545")
//
//        }
//        // self.callSendMessage()
//        self.uploadAllTextScreenShots()
//    }
    

    
    @objc func replyMessageCancel()
    {
        let viewsToPop = 2
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(viewsToPop)
        navigationController?.setViewControllers(viewControllers!, animated: true)
        //self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.txtSubject.resignFirstResponder()
        self.contactsSearchBar.resignFirstResponder()
    }
    
    //MARK: loadBloxContent
    func loadBloxContent ()
    {
        let userDefaults = UserDefaults.standard
        if let key = userDefaults.object(forKey: "bloxArray")  as? Data
        {
            let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
            
            let array : NSMutableArray = arrayData! as! NSMutableArray
            
            //let array : NSMutableArray = userDefaults.valueForKey("bloxArray") as! NSMutableArray
            
            //print(array)
            if(array.count > 0)
            {
                for i in 0 ..< array.count
                {
                    //print(array[i])
                    if let bloxDict : NSMutableDictionary = array[i] as? NSMutableDictionary
                    {
                        if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                        {
                            //print(bloxDict.objectForKey("bloxDetails"))
                            let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                            //var bloxButtonIndex : NSInteger = i + 101
                            switch bloxType
                            {
                            case "TextMessage":
                                if let strMessage : NSString = bloxDetail.value(forKey: "message") as? NSString
                                {
                                    let button : UIButton = self.view.viewWithTag(i + 101) as! UIButton
                                    
                                    button.setTitle(strMessage as String, for: UIControl.State())
                                    button.setImage(UIImage(), for: UIControl.State())
                                    button.backgroundColor = UIColor.clear
                                    button.titleLabel?.textColor = UIColor.black
                                    button.setTitleColor(UIColor.black, for: UIControl.State())
                                    button.titleLabel?.numberOfLines = 5
                                    button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
                                    
                                    if let txtColor : UIColor = bloxDetail.value(forKey: "textColor") as? UIColor
                                    {
                                        button.setTitleColor(txtColor, for: UIControl.State())
                                    }
                                    if let txtBgColor : UIColor = bloxDetail.value(forKey: "backgroundColor") as? UIColor
                                    {
                                        button.backgroundColor = txtBgColor
                                    }
                                    
                                    if let strFontFName : String = bloxDetail.value(forKey: "fontName") as? String
                                    {
                                        if let strSize : CGFloat = bloxDetail.value(forKey: "fontSize") as? CGFloat
                                        {
                                            button.titleLabel?.font = UIFont(name: strFontFName, size: strSize)
                                            if(strSize == 13)
                                            {
                                                button.titleLabel?.numberOfLines = 4
                                            }
                                            else if(strSize == 24)
                                            {
                                                button.titleLabel?.numberOfLines = 3
                                            }
                                            else if(strSize == 48)
                                            {
                                                button.titleLabel?.numberOfLines = 1
                                            }
                                        }
                                    }
                                }
                                break
                                
                            case "Camera":
                                if let data : Data = bloxDetail.value(forKey: "image") as? Data
                                {
                                    if let imageData : UIImage = UIImage(data: data)
                                    {
                                        let button : UIButton = self.view.viewWithTag(i + 101) as! UIButton
                                        button.setTitle("", for: UIControl.State())
                                        button.setImage(imageData, for: UIControl.State())
                                        button.backgroundColor = UIColor.white
                                        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
                                        
                                    }
                                }
                                break
                            case  "GalleryImage":
                                if let data : Data = bloxDetail.value(forKey: "image") as? Data
                                {
                                    if let imageData : UIImage = UIImage(data: data)
                                    {
                                        let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
                                        print("isAnimated: \(isImageAnimated)")
                                        if(isImageAnimated){
                                            let img = UIImage.gifImageWithData(data)
                                            let button : UIButton = self.view.viewWithTag(i + 101) as! UIButton
                                            button.setTitle("", for: UIControl.State())
                                            button.setImage(img, for: UIControl.State())
                                            button.backgroundColor = UIColor.white
                                            button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
                                        }else{
                                            let button : UIButton = self.view.viewWithTag(i + 101) as! UIButton
                                            button.setTitle("", for: UIControl.State())
                                            button.setImage(imageData, for: UIControl.State())
                                            button.backgroundColor = UIColor.white
                                            button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
                                        }
                                    }
                                }
                                break
                                
                            case "Video":
                                if let data : Data = bloxDetail.value(forKey: "image") as? Data
                                {
                                    if let imageData : UIImage = UIImage(data: data)
                                    {
                                        let button : UIButton = self.view.viewWithTag(i + 101) as! UIButton
                                        
                                        button.setTitle("", for: UIControl.State())
                                        button.setImage(imageData, for: UIControl.State())
                                        button.backgroundColor = UIColor.white
                                        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
                                    }
                                }
                                break
                                
                            default:
                                break
                            }
                            
                            
                        }
                    }
                }
            }
            else if(array.count == 0)
            {
                for btnBlox in btnBloxCollection
                {
                    btnBlox.setImage(UIImage(named:"SingleBlox"), for: UIControl.State())
                    btnBlox.backgroundColor = UIColor.clear
                    btnBlox.titleLabel?.text = ""
                }
            }
        }
        else
        {
            for btnBlox in btnBloxCollection
            {
                btnBlox.setImage(UIImage(named:"SingleBlox"), for: UIControl.State())
                btnBlox.backgroundColor = UIColor.clear
                btnBlox.titleLabel?.text = ""
            }
        }
        
        loading.dismiss(afterDelay: 0.5)
    }
    
    
    
    @IBAction func btnInviteFriendAction(_ sender : UIButton)
    {
        let text2share = "Hi, please click on the link to install TalkBlox app: https://itunes.apple.com/us/app/talkblox/id938782906?ls=1&mt=8 and enjoy sharing images and videos in blox with your friends and relatives"
        let objects2Share = [text2share]
        
        let activityVC = ActivityViewController(activityItems: objects2Share, applicationActivities: nil)
        
        
        let excludeActivities = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.copyToPasteboard , UIActivity.ActivityType.mail, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToWeibo,UIActivity.ActivityType.print, UIActivity.ActivityType.copyToPasteboard,
                                 UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.saveToCameraRoll, UIActivity.ActivityType.postToFlickr,
                                 UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToTencentWeibo]
        
        
        activityVC.excludedActivityTypes = excludeActivities
        self.present(activityVC, animated: true, completion: nil)
    }
    
    /*
     @IBAction func multipleTap(sender: AnyObject, withEvent event: UIEvent)
     {
     let touch: UITouch = event.allTouches()!.first!
     print(touch.tapCount)
     if touch.tapCount == 1
     {
     // do action.
     print("didDouble Tap", sender)
     }
     else if touch.tapCount == 2
     {
     // do action.
     ignoreTap = true
     print("didDouble bTap", sender)
     print("did Tap", sender)
     }
     }
     
     var ignoreTap = false
     func didDoubleTap(sender: UIButton)
     {
     ignoreTap = true
     print("didDouble bTap", sender)
     }
     */
    
    
    //Blox method
    @IBAction func showMessage(_ senderBlox : UIButton)
    {
        /*
         if ignoreTap
         {
         ignoreTap = false
         print("ignoretap", senderBlox)
         return
         }
         print("didTap", senderBlox)
         */
        print("BloxButton rowTag\(senderBlox.tag)")
        bloxIndex = senderBlox.tag - 101
        //-------- New functionality -----------
        performSegue(withIdentifier: "CreateBloxSegue", sender: self)
        
        /*
         let strURL : String = "http://virtualtrainr.com/apis/registration"
         
         let param = ["age" : "25",
         "city" : "Indore",
         "country":" India",
         "dispaly_name":"harsh",
         "email":"abc@gmail.com",
         "gender":"male",
         "name":"harsh",
         "password":"333",
         "state":"Madhya Pradesh",
         "type":"trainee",
         "zip":"4534534"]
         
         Alamofire.request(strURL, method: .post, parameters: param, headers: nil)
         .responseJSON
         { response in
         print(response.request)  // original URL request
         print(response.response) // URL response
         print(response.data)     // server data
         print(response.result)   // result of response serialization
         print(response.description)
         if let JSON:NSDictionary = response.result.value as NSDictionary
         {
         print(JSON)
         }
         else
         {
         
         }
         }
         */
        
        /*
         let alertController = UIAlertController(title: "Please select Blox type", message: "", preferredStyle: .Alert)
         
         let profileAction = UIAlertAction(title: "Text", style: .Default) { (_) in }
         let contactsAction = UIAlertAction(title: "Galleries", style: .Default) { (_) in }
         let signOutAction = UIAlertAction(title: "Live Photo/Video", style: .Default) { (_) in }
         let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
         
         alertController.addAction(profileAction)
         alertController.addAction(contactsAction)
         alertController.addAction(signOutAction)
         alertController.addAction(cancelAction)
         
         alertController.view.backgroundColor = UIColor.whiteColor()
         AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
         
         
         presentViewController(alertController, animated: true, completion: nil)
         
         */
 }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       // animateViewMoving(false, moveValue: 100)
    }
    
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    //MARK:- BUtton Actions
    @IBAction func btnBackAction(_ sender : UIButton)
    {
        if(APP_DELEGATE.isReplyMessage)
        {
            let alertController = UIAlertController(title: "Reply alert", message: "Discard reply message", preferredStyle: .alert)
            
            let ReplyAction = UIAlertAction(title: "YES", style: .default)
            {
                (action) -> Void in
                
                let userDefaults = UserDefaults.standard
                if let key = userDefaults.object(forKey: "bloxArray")  as? Data
                {
                    let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                    
                    let array : NSMutableArray = arrayData! as! NSMutableArray
                    
                    array.removeAllObjects()
                    
                    let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
                    let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                    
                    if(AppTheme().killFileOnPath("backgroundAudio.caf") == "File Killed")
                    {
                        print("file deleted")
                    }
                    
                    userDefaults.removeObject(forKey: "bloxBubbleAudio")
                    
                    userDefaults.set(data, forKey: "bloxArray")
                    userDefaults.synchronize()
                    
                    
                }
                
//                self.loading.dismiss(afterDelay: 1.0)
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when){
                    LoadingIndicatorView.hide()
                }
                APP_DELEGATE.isReplyMessage = false
                
                self.perform(#selector(TalkBoxHomeVC.replyMessageCancel), with: nil, afterDelay: 1.1)
                
            }
            let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (_) in }
            alertController.addAction(ReplyAction)
            alertController.addAction(cancelAction)
            alertController.view.backgroundColor = UIColor.white
            AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
            
            present(alertController, animated: true, completion: nil)
        }
        if(APP_DELEGATE.isNewChat)
        {
            let alertController = UIAlertController(title: "Alert", message: "Discard New Chat", preferredStyle: .alert)
            
            let ReplyAction = UIAlertAction(title: "YES", style: .default)
            {
                (action) -> Void in
                
                let userDefaults = UserDefaults.standard
                if let key = userDefaults.object(forKey: "bloxArray")  as? Data
                {
                    let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                    
                    let array : NSMutableArray = arrayData! as! NSMutableArray
                    
                    array.removeAllObjects()
                    
                    let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
                    let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                    
                    if(AppTheme().killFileOnPath("backgroundAudio.caf") == "File Killed")
                    {
                        print("file deleted")
                    }
                    
                    userDefaults.removeObject(forKey: "bloxBubbleAudio")
                    
                    userDefaults.set(data, forKey: "bloxArray")
                    userDefaults.synchronize()
                    
                    
                }
                
//                self.loading.dismiss(afterDelay: 1.0)
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when){
                    LoadingIndicatorView.hide()
                }
                APP_DELEGATE.isReplyMessage = false
                
                self.perform(#selector(TalkBoxHomeVC.replyMessageCancel), with: nil, afterDelay: 1.1)
                
            }
            let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (_) in }
            alertController.addAction(ReplyAction)
            alertController.addAction(cancelAction)
            alertController.view.backgroundColor = UIColor.white
            AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
            
            present(alertController, animated: true, completion: nil)
        }

    }
    @IBAction func btnHomeAction(_ sender:Any){
        self.popToHome()
        tabBarController?.selectedIndex = 0
    }
    
    @IBAction func btnGalleryAction(_ sender : UIButton)
    {
        //sender.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        /*
         btnGallery.backgroundColor = UIColor(red: 36.0/255.0, green: 92.0/255.0, blue: 174.0/255.0, alpha: 1.0)
         btnMessages.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
         btnMyAccount.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
         */
        
        btnGallery.setImage(UIImage(named: "Main-Gallery_1"), for: UIControl.State())
        btnMessages.setImage(UIImage(named: "InBlox-Sent_2"), for: UIControl.State())
        btnMyAccount.setImage(UIImage(named: "Settings_2"), for: UIControl.State())
        
        let GalleryVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryVC")
        self.navigationController?.pushViewController(GalleryVC!, animated: false)
    }
    
    @IBAction func btnMessageAction(_ sender : UIButton)
    {   //36 92 174
        //49 139 226
        
        //sender.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        /*
         btnGallery.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
         btnMessages.backgroundColor = UIColor(red: 36.0/255.0, green: 92.0/255.0, blue: 174.0/255.0, alpha: 1.0)
         btnMyAccount.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
         */
        
        btnGallery.setImage(UIImage(named: "Main-Gallery_2"), for: UIControl.State())
        btnMessages.setImage(UIImage(named: "InBlox-Sent_1"), for: UIControl.State())
        btnMyAccount.setImage(UIImage(named: "Settings_2"), for: UIControl.State())
        self.performSegue(withIdentifier: "ShowMessageSegue", sender: self)
    }
    
    @IBAction func btnMyAccountAction(_ sender : UIButton)
    {
        //sender.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        /*
         btnGallery.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
         btnMessages.backgroundColor = UIColor(red: 36.0/255.0, green: 139.0/255.0, blue: 226.0/255.0, alpha: 1.0)
         btnMyAccount.backgroundColor = UIColor(red: 36.0/255.0, green: 92.0/255.0, blue: 174.0/255.0, alpha: 1.0)
         */
        
        btnGallery.setImage(UIImage(named: "Main-Gallery_2"), for: UIControl.State())
        btnMessages.setImage(UIImage(named: "InBlox-Sent_2"), for: UIControl.State())
        btnMyAccount.setImage(UIImage(named: "Settings_1"), for: UIControl.State())
        
        let alertController = UIAlertController(title: "My Account", message: "", preferredStyle: .alert)
        
        let profileAction = UIAlertAction(title: "Profile", style: .default) { (action) -> Void in
            self.performSegue(withIdentifier: "ProfileSegue", sender: self)
        }
        let contactsAction = UIAlertAction(title: "Contacts", style: .default) {
            (action) -> Void in
            self.performSegue(withIdentifier: "ShowContactSegue", sender: self)
            
        }
        let signOutAction = UIAlertAction(title: "Sign Out", style: .default)
        { (action) -> Void in
            
            self.signOutAction()
            
            print("Sign Out Button Pressed")
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addAction(profileAction)
        alertController.addAction(contactsAction)
        alertController.addAction(signOutAction)
        alertController.addAction(cancelAction)
        
        alertController.view.backgroundColor = UIColor.white
        AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
        
        present(alertController, animated: true, completion: nil)
    }
    @IBAction func contactsPickerBack(_ sender: UIButton) {
        contactsPickerView.isHidden = true
        arrContacts.removeAll()
        arrGroupIds.removeAll()
        contactsSearchBar.resignFirstResponder()
        contactsSearchBar.text = ""
    }
    
    @IBAction func contactsPickerViewDone(_ sender: UIButton) {
        if arrContacts.count == 0 && arrGroupIds.count == 0{
//            AppSharedData.sharedInstance.showErrorAlert(message: "No contact selected.")
            self.view.makeToast("No Contact selected.", duration: 2.0, position: .bottom)
        } else {
            self.uploadAllTextScreenShots(type: "contacts")
        }
        contactsSearchBar.resignFirstResponder()
    }
    @IBAction func selectContacts(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(searchActive)
            {
                let currentContactInfo = searchResults[sender.tag]
                if currentContactInfo.type == "contact" {
                    if currentContactInfo.cInfo.phoneNumbers.count > 0 {
                        let currentContact = (currentContactInfo.cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        for (index,contact) in arrContacts.enumerated(){
                            if contact == currentContact {
                                arrContacts.remove(at: index)
                            }
                        }
                    }
                } else {
                    for (index,contact) in arrGroupIds.enumerated(){
                        if contact == currentContactInfo.gInfo.group_id {
                            arrGroupIds.remove(at: index)
                        }
                    }
                }
            } else {
                let currentContactInfo = allContacts[sender.tag]
                if currentContactInfo.type == "contact" {
                    if currentContactInfo.cInfo.phoneNumbers.count > 0 {
                        let currentContact = (currentContactInfo.cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        for (index,contact) in arrContacts.enumerated(){
                            if contact == currentContact {
                                arrContacts.remove(at: index)
                            }
                        }
                    }
                } else {
                    for (index,contact) in arrGroupIds.enumerated(){
                        if contact == currentContactInfo.gInfo.group_id {
                            arrGroupIds.remove(at: index)
                        }
                    }
                }
            }
        } else {
            sender.isSelected = true
            if(searchActive)
            {
                let currentContactInfo = searchResults[sender.tag]
                if currentContactInfo.type == "contact" {
                    if currentContactInfo.cInfo.phoneNumbers.count > 0 {
                        let currentContactStr = (currentContactInfo.cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        var currentContact = String()
                        if currentContactStr.count > 10 {
                            currentContact = String(currentContactStr.suffix(10))
                        } else {
                            currentContact = currentContactStr
                        }
                        if !arrContacts.contains(currentContact){
                            arrContacts.append(currentContact)
                        }
                    }
                } else {
                    if !arrGroupIds.contains(currentContactInfo.gInfo.group_id!){
                        arrGroupIds.append(currentContactInfo.gInfo.group_id!)
                    }
                }
            } else {
                let currentContactInfo = allContacts[sender.tag]
                if currentContactInfo.type == "contact" {
                    if currentContactInfo.cInfo.phoneNumbers.count > 0 {
                        let currentContactStr = (currentContactInfo.cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        var currentContact = String()
                        if currentContactStr.count > 10 {
                            currentContact = String(currentContactStr.suffix(10))
                        } else {
                            currentContact = currentContactStr
                        }
                        if !arrContacts.contains(currentContact){
                            arrContacts.append(currentContact)
                        }
                    }
                } else {
                    if !arrGroupIds.contains(currentContactInfo.gInfo.group_id!){
                        arrGroupIds.append(currentContactInfo.gInfo.group_id!)
                    }
                }
            }
        }
    }
    
    func signOutAction(){
        
        USER_DEFAULT.set(false, forKey: NOTIF.UD_KEEP_ME_LOGGED_IN)
        AppSharedData.sharedInstance.saveUser(user:nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    /*{
       
       
        let userDefaults = UserDefaults.standard
        let strURL : String = String(format:"%@users/sign_out.json", AppTheme().AppUrl)
        
        //let strURL : String = String(format:"%@users/sign_out.json?authentication_token=%@", AppTheme().AppUrl,userDefaults.valueForKey("authentication_token") as! String)
        
        
        let parameters : [String: AnyObject] =
            ["authentication_token": userDefaults.value(forKey: "authentication_token") as AnyObject!]
        let param = ["session" : parameters]
        
        Alamofire.request(strURL, method: .delete, parameters: param, headers: nil)
            .responseJSON
            { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                print(response.description)
                
                
                if let JSON:NSDictionary = response.result.value as! NSDictionary
                {
                    print("JSON: \(JSON)")
                    
                    
                    if(JSON.value(forKey: "success") as! Bool == true)
                    {
                        //let appDel = AppDelegate().appDelegate()
                        APP_DELEGATE.isAppRunningFirstTime = true
                        APP_DELEGATE.isLogedIn = false
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    else
                    {
                        if let aryCheck : NSArray = JSON.value(forKey: "message") as? NSArray
                        {
                        if aryCheck.count > 0
                        {
                            if aryCheck.object(at: 0) as! String == "User has already Signed Out "
                            {
                                APP_DELEGATE.isAppRunningFirstTime = true
                                APP_DELEGATE.isLogedIn = false
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                        }
                    }
                    
                    /*
                     if(JSON.count > 1)
                     {
                     /*
                     let errorAlert : UIAlertController = UIAlertController(title: "Sign In Alert", message: "Login Successfull", preferredStyle: UIAlertControllerStyle.Alert)
                     
                     errorAlert.addAction(self.callback())
                     self.presentViewController(errorAlert, animated: true ,completion: nil)
                     */
                     
                     self.navigationController?.popToRootViewControllerAnimated(true)
                     }
                     else
                     {
                     let errorAlert = AppTheme().showAlert("Error in Sign Out. Please try again", errorTitle: "Sign Out Error")
                     
                     //self.presentViewController(errorAlert, animated: true ,completion: nil)
                     }
                     */
                }
        }
        
    }*/
    
    //MARK: PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "ProfileSegue")
        {
          //  let objProfileVC : ProfileVC = segue.destination as! ProfileVC
            let userDefaults = UserDefaults.standard
            let dictUser : NSMutableDictionary = NSMutableDictionary()
            
            dictUser["authentication_token"] =  userDefaults.value(forKey: "authentication_token")
            dictUser["user"] = userDefaults.value(forKey: "user")
            
           // objProfileVC.dictUserDeatils = dictUser
        }
        else if(segue.identifier == "CreateBloxSegue")
        {
            let objCreateBloxVC : CreateBloxVC = segue.destination as! CreateBloxVC
            objCreateBloxVC.bloxArrayIndex = bloxIndex
            //objCreateBloxVC.delegate = self
        }
        else if(segue.identifier == "segueShowContactList")
        {
          /*  let objContactList : ContactListVC = segue.destination as! ContactListVC
            objContactList.contactIdsArry = contactIds
            objContactList.strReplyAllMessageId = strReplyAllMessageId
            //var ary : [String : AnyObject] =
            print(allDataArray.count)
            objContactList.allDataArray = allDataArray as [String : AnyObject]
            objContactList.allDataA = allDataA as [AnyObject]
            objContactList.bgSoundId = bgAudioId*/
        }
        else if( segue.identifier == "segueShowBubbleImages")
        {
            
        }  //btnHome
    }
    
    //MARK: Show Audio gallery
    @IBAction func btnAudioGalleryAction(_ sender : UIButton)
    {
        let userDefaults = UserDefaults.standard
        if (userDefaults.object(forKey: "bloxBubbleAudio")  as? String) != nil
        {
            let videoPath = AppTheme().getDocumentFilePath("backgroundAudio.caf")
            if let _ : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
            {
                let alertController = UIAlertController(title: "Background Audio options", message: "", preferredStyle: .actionSheet)
                let addNewAction = UIAlertAction(title: "Add New", style: .default)
                { (_) in
                    
                    self.performSegue(withIdentifier: "segueShowAudioGallery", sender: self)
                }
                let deleteAction = UIAlertAction(title: "Delete Audio", style: .default)
                { (_) in
                    
                    if(AppTheme().killFileOnPath("backgroundAudio.caf") == "File Killed")
                    {
                        print("file deleted")
                        userDefaults.removeObject(forKey: "bloxBubbleAudio")
                        self.showAlert("Background Audio removed", strTitle: "Delete Audio Alert")
                    }
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                { (_) in }
      
                alertController.addAction(addNewAction)
                alertController.addAction(deleteAction)
                alertController.addAction(cancelAction)
                
                alertController.view.backgroundColor = UIColor.white
                //AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
                
                present(alertController, animated: true, completion: nil)
            }
            else
            {
              //  self.performSegue(withIdentifier: "segueShowAudioGallery", sender: self)
            }
        }
        else{
          //  self.performSegue(withIdentifier: "segueShowAudioGallery", sender: self)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundSoundsVC") as! BackgroundSoundsVC
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- BackgroundSelectionDelegates
    func didBackgroundSelected(backgroundImage: BackGroundData) {
        self.backGroundImageID = backgroundImage.bgi_id ?? 0
        self.bgImageName = backgroundImage.bgi_name ?? ""
        UserDefaults.standard.setValue(self.bgImageName, forKey: "BgImage")
        UserDefaults.standard.setValue(self.backGroundImageID, forKey: "backGroundImageID")
        var stringUrl = WEB_URL.BaseURL + backgroundImage.bgi_name!
        stringUrl = stringUrl.replacingOccurrences(of: " ", with: "%20")
        
        self.imgBackground.kf.setImage(with: URL.init(string: stringUrl))
    }
    
    //MARK:- BackgroundSoundVC Delegates
    func didSoundTrackSelected(sound: BackGroundMusic) {
        self.bgAudioPath = sound.bgm_name ?? ""
        self.bgAudioPath = self.bgAudioPath.replacingOccurrences(of: " ", with: "%20")
        self.backGroundSoundID = sound.bgm_id ?? 0
        
    }
    
    //MARK: Show Talk Bubble Gallery
    @IBAction func btnShowBubbleImagesAction (_ sender : UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BackgroundSelectionVC") as! BackgroundSelectionVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func postToSubscriber(_ sender: UIButton) {
        uploadAllTextScreenShots(type: "subscriber")
        self.optionView.isHidden = true
    }
    @IBAction func sendToContacts(_ sender: UIButton) {
        //*** new method for sending method
        self.fetchContacts()
        self.contactsPickerView.isHidden = false
        self.arrContacts.removeAll()
        self.arrGroupIds.removeAll()
        self.optionView.isHidden = true
    }
    
    //MARK: Send Message Method
    @IBAction func btnSendAction (_ sender : UIButton)
    {
     
 //*** new method for sending method
        var count : NSInteger = 0
        if(arrBloxDetails.count > 0)
        {
            for dict in arrBloxDetails
            {
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if (bloxDict.object(forKey: "bloxType") as? NSString) != nil
                    {
                        count += 1
                    }
                }
            }
        }
        
        if(count>0)
        {
            let dictReply = APP_DELEGATE.dictReplyMessageDetails
            if (APP_DELEGATE.isReplyMessage) || (APP_DELEGATE.isNewChat)
            {
                self.uploadAllTextScreenShots(type: "contacts")
            }
            else
            {
                self.optionView.isHidden = false
            }
        }
        else
        {
            showAlert("Can not send empty message.", strTitle: "Alert")
        }
        
        
    }
    @IBAction func closeOptionView(_ sender: Any) {
        self.optionView.isHidden = true
    }
    
   /* func bloxDetails(_ resp: NSDictionary?, _ bool: Bool) {
        dictDetails = resp
        isCamera = bool
    }*/
    func uploadAllTextScreenShots(type:String)  {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        var txtMessageCount = 0
        //let txtMsgCount = arrBloxDetails.flatMap(({"\(($0 as! NSDictionary)["bloxType"]!)" == "TextMessage"})).count
        let ff = arrBloxDetails.map { (a) in
            if let ddd = a as? NSDictionary{
                if  (ddd.value(forKey: "bloxType") as! String) == "TextMessage" {
                    print("count")
                    txtMessageCount = txtMessageCount + 1
                }
            }
        }
        
        
        print("text message count \(arrBloxDetails.count)")
        if txtMessageCount > 0 {
            for  (index,dict) in arrBloxDetails.enumerated() {
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        
                        if bloxType == "TextMessage" {
                            
                            
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in TextMessage")
                                LoadingIndicatorView.show()
                                queue.addOperation {
                                    NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Image as String, arrDataToSend: [data], postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
//                                        LoadingIndicatorView.hide()
                                        //self.loading.dismiss()
                                        if success == true {
                                            txtMessageCount = txtMessageCount - 1
                                            // let statusDic = response?["header"] as! NSDictionary
                                            let detail = response?["result"] as! NSArray
                                            if detail.count > 0 {
                                                let dictDetail = detail[0] as! NSDictionary
                                                // dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
                                                let id = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
                                                
                                                let dictBlox = self.arrBloxDetails[index] as! NSDictionary
                                                let newDictBlox:NSMutableDictionary = NSMutableDictionary.init(dictionary: dictBlox)
                                                
                                                let tmpBloxDetail : NSDictionary = dictBlox.object(forKey: "bloxDetails") as! NSDictionary
                                                
                                                let newBloXDetail:NSMutableDictionary = NSMutableDictionary.init(dictionary: tmpBloxDetail)
                                                newBloXDetail.setValue(id, forKey: "id")
                                                
                                                newDictBlox.setValue(newBloXDetail, forKey:  "bloxDetails")
                                                
                                                self.arrBloxDetails.removeObject(at: index)
                                                self.arrBloxDetails.insert(newDictBlox, at: index)
                                                
                                                // bloxDetail.setValue(id, forKey: "id")
                                            }
                                            
                                            //   self.bloxType =  GalleryType.Video
                                            //self.delegate?.bloxDetails(detail, true)
                                            
                                            /* if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                                             
                                             }*/
                                            if txtMessageCount == 0 {
                                                if type == "subscriber" {
                                                    self.apiCallforSendToSubscriber()
                                                } else {
                                                    self.callSendMessage()
                                                }
                                            }
                                        }
                                    })
                                }
                            }
                        }
                        
                    }
                }
            }
            queue.waitUntilAllOperationsAreFinished()
        }else {
            if type == "subscriber" {
                self.apiCallforSendToSubscriber()
            } else {
                self.callSendMessage()
            }
        }
    }
    
    func callSendMessage(){
//        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        
        allDataArray = [String : AnyObject]()
        
        allDataA = [AnyObject]()
        if(arrBloxDetails.count > 0)
        {
            var frameNo : NSInteger = 0
            for dict in arrBloxDetails
            {
                var dataArray : [String : AnyObject] = [String : AnyObject]()
                //print(array[i])
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        //print(bloxDict.objectForKey("bloxDetails"))
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        
                        dataArray.updateValue(bloxType as AnyObject,forKey: "mediaType")
                        dataArray.updateValue(frameNo as AnyObject,forKey: "frameNo")
                        
                        //var bloxButtonIndex : NSInteger = i + 101
                        switch bloxType
                        {
                        case "Camera":
                            //   if isCamera {
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in camera")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let bloxType = bloxDetail.object(forKey: "fileType") as? NSString
                                
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Sound, forKey: "mediaType")
                                
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    bgAudioId = bgMediaId! as String
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                          dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                        }
                                    }
                                }
                                else
                                {
                                    bgAudioId = "0"
                                }
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                            }
                            break
                            
                        case  "GalleryImage":
                            
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n ---in gallery--- \n")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
                                    dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                }
                                dataArray.updateValue(bloxId as AnyObject, forKey: "mediaId")
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    bgAudioId = bgMediaId! as String
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                        }
                                    }
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    
                                }
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                            }
                            break
                            
                        case "Video":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                print("\n\n\n in Video")
                                print("\((data as NSData).bytes)")
                                
                                
                                let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                
                                if var dataPath : String = bloxDetail.value(forKey: "video") as? String
                                {
                                     dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                    //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                    let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                    if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                    {
                                        dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                    }
                                }
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                            }
                            break
                        case "TextMessage":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in TextMessage")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Image, forKey: "mediaType")
                                
                    
                                ///////////////////////////////////////////////old code
//                                dataArray.updateValue(GalleryType.Text as AnyObject,forKey: "mediaType")
//
//                                //aryMediaImageData.addObject(data)
//
//                                if let txtMessage : String = bloxDetail.value(forKey: "message") as? String
//                                {
//                                    dataArray.updateValue(txtMessage as AnyObject, forKey: "text")
//
//                                }
//                                if let txtColor : String = bloxDetail.value(forKey: "textColor") as? String
//                                {
//                                    dataArray.updateValue(txtColor as AnyObject, forKey: "textcolor")
//
//                                }
//                                if let txtSize : CGFloat = bloxDetail.value(forKey: "fontSize") as? CGFloat
//                                {
//                                    dataArray.updateValue(txtSize as AnyObject, forKey: "textsize")
//
//                                }
//                                if let fontName : String = bloxDetail.value(forKey: "fontName") as? String
//                                {
//                                    dataArray.updateValue(fontName as AnyObject, forKey: "textstyle")
//
//                                }
//                                if let textBgMediaId : String = bloxDetail.value(forKey: "textBgMediaId") as? String
//                                {
//                                    dataArray.updateValue(textBgMediaId as AnyObject, forKey:"textBgMediaId")
//                                }
                                
                                
                                ///////////////////////////////////////////////old code
                                
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    bgAudioId = bgMediaId! as String
                                                                        
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    //"imageVideo"
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                        }
                                    }
                                }
                                else
                                {
                                    bgAudioId = "0"
                                }
                                
                                //-----
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                print("data array contains",dataArray)
                                dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                loading.dismiss()
                            }
                        default:
                            //do nothing
                            break
                        }
                        
                        if(dataArray.count>0)
                        {
                            allDataArray.updateValue(dataArray as AnyObject, forKey: String(format:"AllData%d",frameNo))
                            allDataA.append(dataArray as AnyObject)
                        }
                        frameNo += 1
                    }
                }
            }
            
        }
        
        contactIds  = [String : AnyObject]()
        //let appDelegat = AppDelegate().appDelegate()
        let dictReply = APP_DELEGATE.dictReplyMessageDetails
        if(APP_DELEGATE.isReplyMessage)
            //if((dictReplyData) != nil)
        {
//            self.arrContacts.add(inbloxData?.user_ids ?? "")
            let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
            if selfNo == inbloxData?.user_phone_1 {
                self.arrContacts.append(inbloxData?.user_phone_2 ?? "")
            } else if selfNo == inbloxData?.user_phone_2 {
                self.arrContacts.append(inbloxData?.user_phone_1 ?? "")
            } else {
//                AppSharedData.sharedInstance.showErrorAlert(message: "No contact Selected")
                self.view.makeToast("No Contact selected.", duration: 2.0, position: .bottom)
                return
            }
            
            var imageBg = ""
            if self.backGroundImageID != 0 {
                imageBg = "\(self.backGroundImageID)"
            }
            var soundBg = ""
            if self.backGroundSoundID != 0 {
                soundBg = "\(self.backGroundSoundID)"
            }
            
            let parameter : [String : AnyObject] =
                ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":self.inbloxData?.message_id as AnyObject ,"contactIds":arrContacts as AnyObject, "groupIds": arrGroupIds as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":(self.txtSubject.text ?? "") as AnyObject]
            print(parameter)
            
            NetworkManager.sharedInstance.executeService(WEB_URL.sentbloxmessage, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if success {
                    
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        APP_DELEGATE.isReplyMessage = false
                        UserDefaults.standard.set(nil, forKey: "bloxArray")
                        UserDefaults.standard.setValue(nil, forKey: "BgImage")
                        UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")
                        UserDefaults.standard.setValue(nil, forKey: "BgImage")

                        UserDefaults.standard.synchronize()
                        self.contactsPickerView.isHidden = true
                        self.popToHome()
                        self.tabBarController?.selectedIndex = 0
//                        AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                    } else {
                        self.contactsPickerView.isHidden = true
                        self.popToHome()
                        self.tabBarController?.selectedIndex = 0
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
                print(response!)
            })
        } else if(APP_DELEGATE.isNewChat){
                        
                        var imageBg = ""
                        if self.backGroundImageID != 0 {
                            imageBg = "\(self.backGroundImageID)"
                        }
                        var soundBg = ""
                        if self.backGroundSoundID != 0 {
                            soundBg = "\(self.backGroundSoundID)"
                        }
                        
                        let parameter : [String : AnyObject] =
                            ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":self.inbloxData?.message_id as AnyObject ,"contactIds":arrContacts as AnyObject, "groupIds": arrGroupIds as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":(self.txtSubject.text ?? "") as AnyObject]
                        print(parameter)
                        
                        NetworkManager.sharedInstance.executeService(WEB_URL.sentbloxmessage, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                            if success {
                                
                                let statusDic = response?["header"] as! NSDictionary
                                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                                    APP_DELEGATE.isNewChat = false
                                    UserDefaults.standard.set(nil, forKey: "bloxArray")
                                    UserDefaults.standard.setValue(nil, forKey: "BgImage")
                                    UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")

                                    UserDefaults.standard.synchronize()
                                    self.contactsPickerView.isHidden = true
//                                    self.popToHome()
//                                    self.tabBarController?.selectedIndex = 0
                                    self.navigationController?.popViewController(animated: true)
//                                    AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                                } else {
                                    self.contactsPickerView.isHidden = true
//                                    self.popToHome()
//                                    self.tabBarController?.selectedIndex = 0
                                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                                    let when = DispatchTime.now() + 1.8
                                    DispatchQueue.main.asyncAfter(deadline: when){
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
//                                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                                    
                                }
                            }
                            print(response!)
                        })
                    }
        else
        {
            var imageBg = ""
            if self.backGroundImageID != 0 {
                imageBg = "\(self.backGroundImageID)"
            }
            var soundBg = ""
            if self.backGroundSoundID != 0 {
                soundBg = "\(self.backGroundSoundID)"
            }
            let parameter : [String : AnyObject] =
                ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":"" as AnyObject,"contactIds":arrContacts as AnyObject, "groupIds": arrGroupIds as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":(self.txtSubject.text ?? "") as AnyObject]
            print(parameter)
            
            NetworkManager.sharedInstance.executeService(WEB_URL.sentbloxmessage, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if success {
                    if let statusDic = response?["header"] as? NSDictionary {
                        if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                            UserDefaults.standard.set(nil, forKey: "bloxArray")
                            UserDefaults.standard.setValue(nil, forKey: "BgImage")
                            UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")
                            UserDefaults.standard.synchronize()
                            self.contactsPickerView.isHidden = true
                            self.popToHome()
                            self.tabBarController?.selectedIndex = 0
//                            AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                        } else {
                            self.contactsPickerView.isHidden = true
                            self.popToHome()
                            self.tabBarController?.selectedIndex = 0
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                            self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                        }
                    }
                }
            })
        }
    }
    
    func apiCallforSendToSubscriber() {
//        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        allDataArray = [String : AnyObject]()
        
        allDataA = [AnyObject]()
        if(arrBloxDetails.count > 0)
        {
            var frameNo : NSInteger = 0
            for dict in arrBloxDetails
            {
                var dataArray : [String : AnyObject] = [String : AnyObject]()
                //print(array[i])
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        //print(bloxDict.objectForKey("bloxDetails"))
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        
                        dataArray.updateValue(bloxType as AnyObject,forKey: "mediaType")
                        dataArray.updateValue(frameNo as AnyObject,forKey: "frameNo")
                        
                        //var bloxButtonIndex : NSInteger = i + 101
                        switch bloxType
                        {
                        case "Camera":
                            //   if isCamera {
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in camera")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let bloxType = bloxDetail.object(forKey: "fileType") as? NSString
                                
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Sound, forKey: "mediaType")
                                
                                //dataArray.updateValue(data, forKey:"medias[][media]IMAGE")
                                //aryMediaImageData.addObject(data)
                                //--  new code 27052016 --
                                
                                
                                
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    //dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    //--- old code ---
                                    /*
                                     print("\(data.bytes)")
                                     dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                     let videoPath = AppTheme().getDocumentFilePath(strVideoFile)
                                     if let videoData : NSData = NSData(contentsOfFile: videoPath)
                                     {
                                     dataArray.updateValue(videoData, forKey:"medias[][media]VIDEO")
                                     }
                                     //---
                                     */
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                          dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //  dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    //dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //   dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    // dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "medias[][caption]")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    //  dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                                
                            }
                            //    }
                            break
                            
                        case  "GalleryImage":
                            
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n ---in gallery--- \n")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
//                                print("isAnimated: \(isImageAnimated)")
//                                if(isImageAnimated){
//                                    dataArray.updateValue(GalleryType.Gif as AnyObject,forKey: "mediaType")
//                                } else {
                                    dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                }
                                dataArray.updateValue(bloxId as AnyObject, forKey: "mediaId")
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    
                                    // dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    //-- old code --
                                    /*
                                     print("\(data.bytes)")
                                     dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                     let videoPath = AppTheme().getDocumentFilePath(strVideoFile)
                                     if let videoData : NSData = NSData(contentsOfFile: videoPath)
                                     {
                                     dataArray.updateValue(videoData, forKey:"medias[][media]VIDEO")
                                     }
                                     */
                                    //---
                                    
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //  dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                    
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    
                                    // dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                //dataArray.updateValue(data, forKey:"medias[][media]IMAGE")
                                //aryMediaImageData.addObject(data)
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //    dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    // dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "medias[][caption]")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    // dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                            }
                            break
                            
                        case "Video":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                print("\n\n\n in Video")
                                print("\((data as NSData).bytes)")
                                
                                /*  if isCamera {
                                 // let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                 let bloxType = bloxDetail.object(forKey: "fileType") as? NSString
                                 
                                 // dataArray.updateValue(bloxId!, forKey: "mediaId")
                                 dataArray.updateValue(bloxType!, forKey: "mediaType")
                                 }*/
                                
                                //dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                /*
                                 dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"video_default_img",@"key",[NSString stringWithFormat:@"media%d.png",i],@"filename",UIImagePNGRepresentation(image),@"value",[NSNumber numberWithInteger:DataTypeImage],@"type", nil];
                                 */
                                
                                let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                
                                if var dataPath : String = bloxDetail.value(forKey: "video") as? String
                                {
                                     dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                    //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                    let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                    if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                    {
                                        dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                        // dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                    }
                                    //aryMediaImageData.addObject(data)
                                }
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //   dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    //   dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "caption")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    // dataArray.updateValue("" as AnyObject, forKey: "caption")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                            }
                            break
                        case "TextMessage":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in TextMessage")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Image, forKey: "mediaType")
                                
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    //dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                    //  dataArray.updateValue(bloxId!, forKey: "mediaId")
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    //"imageVideo"
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //   dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                } else {
                                    bgAudioId = "0"
                                }
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                } else {
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                } else {
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                print("data array contains",dataArray)
                                dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                loading.dismiss()
                            }
                        default:
                            //do nothing
                            break
                        }
                        
                        if(dataArray.count>0)
                        {
                            allDataArray.updateValue(dataArray as AnyObject, forKey: String(format:"AllData%d",frameNo))
                            allDataA.append(dataArray as AnyObject)
                        }
                        frameNo += 1
                    }
                }
            }
        }
        
        contactIds  = [String : AnyObject]()
        //let appDelegat = AppDelegate().appDelegate()
        let dictReply = APP_DELEGATE.dictReplyMessageDetails
        
            var imageBg = ""
            if self.backGroundImageID != 0 {
                imageBg = "\(self.backGroundImageID)"
            }
            var soundBg = ""
            if self.backGroundSoundID != 0 {
                soundBg = "\(self.backGroundSoundID)"
            }
//            let parameter : [String : AnyObject] =
//                ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":"" as AnyObject,"contactIds":arrContacts as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":(self.txtSubject.text ?? "") as AnyObject]
        let parameter : [String : AnyObject] =
            ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"userid":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":"" as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":(self.txtSubject.text ?? "") as AnyObject,"name":AppSharedData.sharedInstance.getUser()?.display_name as AnyObject ]
            print(parameter)
            
            NetworkManager.sharedInstance.executeService(WEB_URL.sendBloxCastToSubscriber, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if success {
                    if let statusDic = response?["header"] as? NSDictionary {
                        if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                            UserDefaults.standard.set(nil, forKey: "bloxArray")
                            UserDefaults.standard.setValue(nil, forKey: "BgImage")
                            UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")
                            UserDefaults.standard.synchronize()
                            self.popToHome()
                            self.tabBarController?.selectedIndex = 0
//                            self.navigationController?.popViewController(animated: true)
//                            AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                        }
                    }else {
                        if let statusDic = response?["result"] as? NSDictionary {
                            if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                                AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                                self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                            }
                        }
                    }
                }
            })
    }
    
    func serverCallForGetAllGroups(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d", AppSharedData.sharedInstance.currentUser?.userid ?? 0)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.getAllGroup, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [Group].from(jsonArray: response?["group"] as! [JSON])
                    
                    self.groups = inbloxData!
                    for item in inbloxData! {
                        let c_Info = CNContact()
                        let g_Info = mixedContacts(type: "group", name: item.group_name ?? "", gInfo: item, cInfo: c_Info)
                        self.allContacts.append(g_Info)
                    }
                    let result = self.allContacts.sorted(by: {
                        (firt: mixedContacts, second: mixedContacts) -> Bool in firt.name < second.name
                    })
                    self.allContacts = result
                    self.contactsTable.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    @objc func replyMessageSent()
    {
        //self.navigationController?.popViewControllerAnimated(true)
        var viewsToPop = 1
        if isReply
        {
            viewsToPop = 3
        }
        else
        {
            viewsToPop = 2
        }
        
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(viewsToPop)
        navigationController?.setViewControllers(viewControllers!, animated: true)

    }
    
    @IBAction func btnPreviewAction (_ sender:UIButton)
    {
        if(arrBloxDetails.count > 0)
        {
            let ary : NSMutableArray = NSMutableArray()
            for i in 0 ..< arrBloxDetails.count
            {
                //print("value at index\(i)")
                if let dict  = arrBloxDetails.object(at: i) as? NSMutableDictionary
                {
                    ary.add(dict)
                }
                else
                {
                    print("empty")
                }
            }
            if(ary.count > 0)
                
            {
               
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
                vc.bloxSubject = self.txtSubject.text ?? ""
                vc.bgImageName = self.bgImageName
                vc.isFromTalkBloxVC = true
                vc.strBgAudioPath = WEB_URL.BaseURL + self.bgAudioPath
//                if(self.bgAudioPath != ""){
//                    vc.bgAudioExists = true
//                }
                vc.previewKind = "Preview"
                vc.inbloxData = self.inbloxData
                vc.backGroundImageID = self.backGroundImageID
                vc.isSentByMe = true
                vc.backGroundSoundID = self.backGroundSoundID
                
                vc.user_name = (AppSharedData.sharedInstance.currentUser?.displayName)!
                vc.user_image = (AppSharedData.sharedInstance.currentUser?.user_image)!
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM d, yyyy h:mm a"
                let DateNow = dateFormatter.string(from: date)
                vc.bloxDate = DateNow
                
                vc.arrGroupIds = self.arrGroupIds
                vc.arrContacts = self.arrContacts
            //    vc.imgViewBackground.image = self.imgBackground.image
                self.navigationController?.pushViewController(vc, animated: true)
               // self.performSegue(withIdentifier: "PreviewSegue", sender: self)
            }
            else
            {
                showAlert("Can not preview empty message.", strTitle: "Alert")
            }
        }
        else
        {
            showAlert("Can not preview empty message.", strTitle: "Alert")
        }
        
        /*
         var delayValue : NSTimeInterval = 0.0;
         for btnBlox in btnBloxCollection
         {
         
         self.performSelector("animateThroughCenter:", withObject: btnBlox, afterDelay: delayValue)
         
         //doAnimation(btnBlox)
         delayValue += 3.5
         
         }
         */
    }
    
    func animateThroughCenter(_ btnBlox: UIButton)
    {
        //var delayValue : NSTimeInterval = 0.0;
        
        let buttonBloxFrame : CGRect = btnBlox.frame
        let options = UIView.AnimationOptions()
        UIView.animate(withDuration: 1.0, delay: 0.0, options: options, animations:
            {
                var frame : CGRect = btnBlox.frame
                frame.origin = CGPoint(x: self.imgBloxBubble.frame.size.width/2 - btnBlox.frame.size.width/2 + 5, y: self.imgBloxBubble.frame.size.height/2 - btnBlox.frame.size.height/2 - 15)
                
                // any changes entered in this block will be animated
                btnBlox.frame = frame;
                
                self.view.bringSubviewToFront(btnBlox)
            },
                                   completion:
            { finished in
                
                self.perform(#selector(TalkBoxHomeVC.doAnimation(_:)), with: btnBlox, afterDelay: 0.2)
                
                //doAnimation(btnBlox)
                //delayValue += 1.5
                
                
                let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(1.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                    // your function here
                    self.setButtonFrame(btnBlox, buttonFrame: buttonBloxFrame)
                    
                })
                
                //btnBlox.frame = buttonBloxFrame;
        })
        
        
    }
    
    func setButtonFrame(_ btnBlox: UIButton, buttonFrame:CGRect)
    {
        let options = UIView.AnimationOptions()
        UIView.animate(withDuration: 1.0, delay: 0.0, options: options, animations:
            {
                btnBlox.frame = buttonFrame
            },
                                   completion:
            {
                finished in
        })
        
    }
    
    @objc func doAnimation(_ sender: UIButton)
    {
        UIView.animate(withDuration: 0.6 ,
                                   animations: {
                                    sender.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
            },
                                   completion:
            { finish in
                UIView.animate(withDuration: 0.6, animations: {
                    sender.transform = CGAffineTransform.identity
                })
        })
    }
    
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        
        self.present(errorAlert, animated: true ,completion: nil)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        self.collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: RAReorderableLayout delegate datasource
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        //let width = bounds.size.width
        
        switch height {
        case 480.0:
            print("iPhone 3,4")
        case 568.0:
            print("iPhone 5")
        case 667.0:
            print("iPhone 6")
            return CGSize(width: 111, height: 109)
        case 736.0:
            print("iPhone 6+")
            return CGSize(width: 124, height: 122)
            
        default:
            
            let bounds = UIScreen.main.bounds
           
            let width = bounds.size.width
            let w = width - 10//(width * 923)/1078
            let w1 = (w-32)/3
            
             return CGSize(width: w1, height: w1)
          
            print("not an iPhone")
             //return CGSize(width: 124, height: 122)
            
        }
        return CGSize(width: 92, height: 93)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 2.0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 9
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "verticalCell", for: indexPath) as! RACollectionViewCell
        Utilities.shared.setBorderWithCornerRadiusof(object: cell, borderColor: .blue, borderWidth: 1.0, cornerRadius: 10)
        var isEmpty : Bool = false
       /* if USER_DEFAULT .bool(forKey: NOTIF.IS_LOGIN) == true {
            USER_DEFAULT .set(false, forKey: NOTIF.IS_LOGIN)
            
            if arrBloxDetails.count > 0 {
                arrBloxDetails.removeAllObjects()
            }else{
            }
        }*/
        if(arrBloxDetails.count > indexPath.row)
        {
            
            //print(array[i])
            if let bloxDict : NSMutableDictionary = arrBloxDetails[indexPath.row] as? NSMutableDictionary
            {
                if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                {
                    //print(bloxDict.objectForKey("bloxDetails"))
                    let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                    
                    //var bloxButtonIndex : NSInteger = i + 101
                    switch bloxType
                    {
                    case "TextMessage":
                        if let strMessage : NSString = bloxDetail.value(forKey: "message") as? NSString
                        {
                            //**** Old Code ****
                            /*
                            let button  = cell.btnBlox
                            
                            button?.setTitle(strMessage as String, for: UIControl.State.normal)
                            //                             button.setImage(UIImage(), forState: UIControlState.normal)
                            button?.backgroundColor = UIColor.clear
                            button?.titleLabel?.textColor = UIColor.black
                            button?.setTitleColor(UIColor.black, for: UIControl.State.normal)
                            button?.titleLabel?.numberOfLines = 5
                            button?.titleLabel?.font = UIFont.systemFont(ofSize: 13)
                            
                            if let txtColor : UIColor = bloxDetail.value(forKey: "textColor") as? UIColor
                            {
                                button?.setTitleColor(txtColor, for: UIControl.State.normal)
                            }
                            if let txtBgColor : UIColor = bloxDetail.value(forKey: "backgroundColor") as? UIColor
                            {
                                button?.backgroundColor = txtBgColor
                            }
                            
                            if let strFontFName : String = bloxDetail.value(forKey: "fontName") as? String
                            {
                                if let strSize : CGFloat = bloxDetail.value(forKey: "fontSize") as? CGFloat
                                {
                                    button?.titleLabel?.font = UIFont(name: strFontFName, size: strSize)
                                    if(strSize == 13)
                                    {
                                        button?.titleLabel?.numberOfLines = 4
                                    }
                                    else if(strSize == 24)
                                    {
                                        button?.titleLabel?.numberOfLines = 3
                                    }
                                    else if(strSize == 48)
                                    {
                                        button?.titleLabel?.numberOfLines = 1
                                    }
                                }
                            }
                             */
                            //******
                        }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                //AppSharedData.sharedInstance.imageData = data

                                if let imageData : UIImage = UIImage(data: data)
                                {

                                    let button = cell.btnBlox

                                    button?.setTitle("", for: UIControl.State())
                                    button?.setImage(imageData, for: UIControl.State())
                                    button?.backgroundColor = UIColor.clear
                                    button?.imageView?.contentMode = UIView.ContentMode.scaleToFill

                                }
                            }
//                        }
                        break
                        
                    case "Camera":
                        if let data : Data = bloxDetail.value(forKey: "image") as? Data
                        {
                            if let imageData : UIImage = UIImage(data: data)
                            {
                                let button = cell.btnBlox
                                
                                button?.setTitle("", for: UIControl.State())
                                button?.setImage(imageData, for: UIControl.State())
                                button?.backgroundColor = UIColor.clear
                                button?.imageView?.contentMode = UIView.ContentMode.scaleToFill
                                
                            }
                        }
                        break
                        
                    case  "GalleryImage":
                        if let data : Data = bloxDetail.value(forKey: "image") as? Data
                        {
                            if let imageData : UIImage = UIImage(data: data)
                            {
                                let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
                                print("isAnimated: \(isImageAnimated)")
                                if(isImageAnimated){
                                    let img = UIImage.gifImageWithData(data)
                                    let button = cell.btnBlox
                                    button?.setTitle("", for: UIControl.State())
                                    button?.setImage(img, for: UIControl.State())
                                    button?.backgroundColor = UIColor.clear
                                    button?.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
                                }else{
                                    let button = cell.btnBlox
                                    button?.setTitle("", for: UIControl.State())
                                    button?.setImage(imageData, for: UIControl.State())
                                    button?.backgroundColor = UIColor.clear
                                    button?.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
                                }
                            }
                        }
                        break
                        
                    case "Video":
                        if let data : Data = bloxDetail.value(forKey: "image") as? Data
                        {
                            if let imageData : UIImage = UIImage(data: data)
                            {
                                let button = cell.btnBlox
                                
                                button?.setTitle("", for: UIControl.State())
                                button?.setImage(imageData, for: UIControl.State())
                                button?.backgroundColor = UIColor.clear
                                button?.imageView?.contentMode = UIView.ContentMode.scaleToFill
                            }
                        }
                        break
                        
                    default:
                        isEmpty = true
                        let button  = cell.btnBlox
                        button?.setTitle("", for: UIControl.State())
                        button?.setImage(nil, for: UIControl.State())
                        button?.backgroundColor = UIColor.clear
                        button?.imageView?.contentMode = UIView.ContentMode.scaleToFill
                        
                        break
                    }
                    
                    if(!isEmpty)
                    {
                        let gestureDoubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TalkBoxHomeVC.doubleTap(_:)))
                        gestureDoubleTap.numberOfTapsRequired = 2
                        cell.addGestureRecognizer(gestureDoubleTap)
                        /*
                         var gestureSingleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "singleTap:")
                         gestureSingleTap.numberOfTapsRequired = 1
                         gestureSingleTap.delaysTouchesBegan = true
                         cell.addGestureRecognizer(gestureSingleTap)
                         */
//                        cell.delBlox.isHidden = false
//                        let gestureSingleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TalkBoxHomeVC.deleteFrame(_:)))
//                        gestureSingleTap.numberOfTapsRequired = 1
//                        gestureSingleTap.delaysTouchesBegan = true
//                        cell.delBlox.addGestureRecognizer(gestureSingleTap)
                    }
                }
                else
                {
                    isEmpty = true
                }
            }
            else
            {
                isEmpty = true
            }
        }
        else if(arrBloxDetails.count == 0)
        {
            isEmpty = true
        }
        
        if(isEmpty)
        {
//            cell.delBlox.isHidden = true
            cell.btnBlox.setImage(UIImage(named:"SingleBlox"), for: UIControl.State())
            cell.btnBlox.backgroundColor = UIColor.clear
            cell.btnBlox.titleLabel?.text = ""
            
            let button = cell.btnBlox
            
            button?.setTitle("", for: UIControl.State())
            button?.setImage(nil, for: UIControl.State())
            button?.backgroundColor = UIColor.clear
            button?.imageView?.contentMode = UIView.ContentMode.scaleToFill
        }
        
        cell.btnBlox.tag = indexPath.row + 101
//        cell.delBlox.tag = indexPath.row
        //cell.btnBlox.addTarget(self, action: "didDoubleTap:", forControlEvents: UIControlEvents.TouchDownRepeat)
        
        //cell.btnBlox.addTarget(self, action: "multipleTap:withEvent:", forControlEvents: .TouchDownRepeat)
        
        cell.btnBlox.addTarget(self, action: #selector(TalkBoxHomeVC.showMessage(_:)), for: UIControl.Event.touchUpInside)
        
        
        // cell.imageView.image = self.imagesForSection0[indexPath.item]
        
        // disabling userInteraction will call its touchup inside method in celldidselect method
        cell.btnBlox.isUserInteractionEnabled = false
        
        return cell
    }
    
//    func moveDataItem(_ sIndex: Int, _ dIndex: Int) {
//        let item = arrBloxDetails.remove(at: sIndex)
//        arrBloxDetails.insert(item, at:dIndex)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        let sIndex = index(ofAccessibilityElement: sourceIndexPath)
//        let dIndex = index(ofAccessibilityElement: destinationIndexPath)
//        
//        moveDataItem(sIndex, dIndex)
//    }
    
    func collectionView(_ collectionView: UICollectionView, allowMoveAt indexPath: IndexPath) -> Bool
    {
        if collectionView.numberOfItems(inSection: indexPath.section) <= 1
        {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView,at atIndexPath: IndexPath, willMoveTo toIndexPath: IndexPath)
    {
        
        /*
         var photo: UIImage
         photo = self.imagesForSection0.removeAtIndex(atIndexPath.item)
         self.imagesForSection0.insert(photo, atIndex: toIndexPath.item)
         */
        
        let atCell = collectionView.cellForItem(at: atIndexPath)  as! RACollectionViewCell
        let toCell = collectionView.cellForItem(at: toIndexPath)  as! RACollectionViewCell
        
        atCell.btnBlox.tag = toIndexPath.item + 101
        toCell.btnBlox.tag = atIndexPath.item + 101
        
        //arrBloxDetails.swap
        
        var objTemp: AnyObject
        objTemp = arrBloxDetails[atIndexPath.item] as AnyObject
        self.arrBloxDetails.removeObject(at: atIndexPath.item)
        self.arrBloxDetails.insert(objTemp, at: toIndexPath.item)
        
        /*
         var tempObj: AnyObject = arrBloxDetails[atIndexPath.item]
         arrBloxDetails[atIndexPath.item] = arrBloxDetails[atIndexPath.item]
         arrBloxDetails[atIndexPath.item] = tempObj
         */
        
        
        /*
         if let atBloxDict : NSMutableDictionary = arrBloxDetails[atIndexPath.item] as? NSMutableDictionary
         {
         arrBloxDetails.replaceObjectAtIndex(toIndexPath.item, withObject: atBloxDict)
         }
         else if let atBloxEmptyStr : String = arrBloxDetails[atIndexPath.item] as? String
         {
         arrBloxDetails.replaceObjectAtIndex(toIndexPath.item, withObject: atBloxEmptyStr)
         }
         else if let atBloxAny : AnyObject = arrBloxDetails[atIndexPath.item] as AnyObject
         {
         arrBloxDetails.replaceObjectAtIndex(toIndexPath.item, withObject: atBloxAny)
         }
         
         if let toBloxDict : NSMutableDictionary = arrBloxDetails[toIndexPath.item] as? NSMutableDictionary
         {
         arrBloxDetails.replaceObjectAtIndex(atIndexPath.item, withObject: toBloxDict)
         }
         else if let toBloxEmptyStr : String = arrBloxDetails[toIndexPath.item] as? String
         {
         arrBloxDetails.replaceObjectAtIndex(atIndexPath.item, withObject: toBloxEmptyStr)
         }
         else if let toBloxAny : AnyObject = arrBloxDetails[toIndexPath.item] as AnyObject
         {
         arrBloxDetails.replaceObjectAtIndex(atIndexPath.item, withObject: toBloxAny)
         }
         */
        
        
        let userDefaults = UserDefaults.standard
        let data = NSKeyedArchiver.archivedData(withRootObject: arrBloxDetails)
        userDefaults.set(data, forKey: "bloxArray")
        userDefaults.synchronize()
        
        print(toIndexPath.item)
        print(atIndexPath.item)
        
    //    collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("indexPath.Row :- \(indexPath.row) indexPath.Item :- \(indexPath.item)")
        
        let cellSelected = collectionView.cellForItem(at: indexPath)  as! RACollectionViewCell
        
        //self.performSegueWithIdentifier("showMessage:", sender: cellSelected.btnBlox)
        print("cellSelected.btnBlox.tag :- \(cellSelected.btnBlox.tag)")
        
        cellSelected.btnBlox.tag = indexPath.item + 101
        
        print("cellSelected.btnBlox.tag :- \(cellSelected.btnBlox.tag)")
        
        // Forcefully calling button method so that We get accurate row index for tag value of button in cell did select
        //cellSelected.btnBlox.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
   
        selectedIndexPath = indexPath
        AppSharedData.sharedInstance.selectedBlox = indexPath.row// + 1  // added on 23Feb2021
        var isEmptyBlox : Bool = false
        if(arrBloxDetails.count > indexPath.row)
        {
            if let bloxDict : NSMutableDictionary = arrBloxDetails[indexPath.row] as? NSMutableDictionary
            {
                if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                {
                    print(bloxType)
                }
                else
                {
                    isEmptyBlox = true
                }
            }
            else
            {
                isEmptyBlox = true
            }
        }
        else
        {
            isEmptyBlox = true
        }
        
        if(isEmptyBlox)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateBloxVC") as! CreateBloxVC
            vc.bloxArrayIndex = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
         //   self.perform(#selector(TalkBoxHomeVC.performButtonActionWithDelay(_:)), with: cellSelected.btnBlox)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateBloxVC") as! CreateBloxVC
             vc.bloxArrayIndex = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
           // self.perform(#selector(TalkBoxHomeVC.performButtonActionWithDelay(_:)), with: cellSelected.btnBlox, afterDelay: 0.5)
        }
    }
    
    @objc  func performButtonActionWithDelay(_ sender : UIButton)
    {
        if(!isDoubleTapped)
        {
            sender.sendActions(for: UIControl.Event.touchUpInside)
        }
        else
        {
            isDoubleTapped = false
        }
    }
    @objc func deleteFrame(_ sender: UITapGestureRecognizer){
        isDoubleTapped = true
        
        //var cell = self.collectionView.cellForItemAtIndexPath(indexPath)
        
        NSLog("double tap")
//        print("selectedIndexPath :- \(selectedIndexPath), row Index :- \(selectedIndexPath.item)")
        print("row Index :- \(sender.view?.tag)")
        
//        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        self.loading.show(in: self.view)
//        LoadingIndicatorView.show("Loading...")
        
        let alertController = UIAlertController(title: "Blox Alert", message: "Are you sure you want to delete the current blox.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Done", style: .default)
        { (action) -> Void in
            
//            if(self.arrBloxDetails.count > self.selectedIndexPath.item)
            if(self.arrBloxDetails.count > sender.view!.tag)
            {
                
//                self.loading.dismiss(afterDelay: 0.5)
                let when = DispatchTime.now() + 0.5
                DispatchQueue.main.asyncAfter(deadline: when){
                    LoadingIndicatorView.hide()
                }
                
//                self.arrBloxDetails.removeObject(at: self.selectedIndexPath.item)
//                self.arrBloxDetails.insert("", at: self.selectedIndexPath.item)
                self.arrBloxDetails.removeObject(at: sender.view!.tag)
                self.arrBloxDetails.insert("", at: sender.view!.tag)
                
                
                let userDefaults = UserDefaults.standard
                if let key = userDefaults.object(forKey: "bloxArray")  as? Data
                {
                    let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                    
                    let array : NSMutableArray = arrayData! as! NSMutableArray
                    
                    array.removeAllObjects()
                    
                    let arr: NSMutableArray = self.arrBloxDetails
                    let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                    
                    userDefaults.set(data, forKey: "bloxArray")
                    userDefaults.synchronize()
                    
                    
                    self.collectionView.reloadData()
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
//            self.loading.dismiss()
            LoadingIndicatorView.hide()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(doneAction)
        
        alertController.view.backgroundColor = UIColor.white
        AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    @objc func doubleTap(_ sender: UITapGestureRecognizer)
    {
        isDoubleTapped = true
        
        //var cell = self.collectionView.cellForItemAtIndexPath(indexPath)
        
        NSLog("double tap")
        print("selectedIndexPath :- \(selectedIndexPath), row Index :- \(selectedIndexPath.item)")
        
//        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        self.loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        
        let alertController = UIAlertController(title: "Blox Alert", message: "Are you sure you want to delete the current blox.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "Done", style: .default)
        { (action) -> Void in
            
            if(self.arrBloxDetails.count > self.selectedIndexPath.item)
            {
                
//                self.loading.dismiss(afterDelay: 0.5)
                let when = DispatchTime.now() + 0.5
                DispatchQueue.main.asyncAfter(deadline: when){
                    LoadingIndicatorView.hide()
                }
                
                self.arrBloxDetails.removeObject(at: self.selectedIndexPath.item)
                self.arrBloxDetails.insert("", at: self.selectedIndexPath.item)
                
                
                let userDefaults = UserDefaults.standard
                if let key = userDefaults.object(forKey: "bloxArray")  as? Data
                {
                    let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                    
                    let array : NSMutableArray = arrayData! as! NSMutableArray
                    
                    array.removeAllObjects()
                    
                    let arr: NSMutableArray = self.arrBloxDetails
                    let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                    
                    userDefaults.set(data, forKey: "bloxArray")
                    userDefaults.synchronize()
                    
                    
                    self.collectionView.reloadData()
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
//            self.loading.dismiss()
            LoadingIndicatorView.hide()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(doneAction)
        
        alertController.view.backgroundColor = UIColor.white
        AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    //Not so usefull have used alternative
    /*
     func singleTap(sender: UITapGestureRecognizer)
     {
     if (sender.state == UIGestureRecognizerState.Ended)
     {
     var btnTemp : UIButton = UIButton()
     btnTemp.tag = 102
     NSLog("single tap")
     self.performSelector("showMessage:", withObject: btnTemp)
     }
     else
     {
     // Handle other UIGestureRecognizerState's
     NSLog("handle other tap")
     }
     //NSLog("single tap")
     }
     */
    
    
    
    func scrollTrigerEdgeInsetsInCollectionView(_ collectionView: UICollectionView) -> UIEdgeInsets
    {
        return UIEdgeInsets.init(top: 100.0, left: 100.0, bottom: 100.0, right: 100.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, reorderingItemAlphaInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func scrollTrigerPaddingInCollectionView(_ collectionView: UICollectionView) -> UIEdgeInsets
    {
        return UIEdgeInsets.init(top: self.collectionView.contentInset.top, left: 0, bottom: self.collectionView.contentInset.bottom, right: 0)
    }
    
    func fetchContacts (){
        self.contacts.removeAll()
        self.allContacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                        let g_Info = Group(json: [:])
                        let c_Info = mixedContacts(type: "contact", name: "\(contact.givenName) \(contact.familyName)" , gInfo: g_Info!, cInfo: contact)
                        self.allContacts.append(c_Info)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                    }
                    
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
//            let result2  = mixedContacts.sorted(by: {
//
//            })
            self.contacts = result
            self.serverCallForGetAllGroups()

            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTable.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }

    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(searchActive) {
                return searchResults.count
            } else {
                return allContacts.count
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectContactsCell") as! selectContactsCell
            if(searchActive) {
                cell.cName.text =  "\(searchResults[indexPath.row].name)"
                if searchResults[indexPath.row].type == "contact" {
                    let currentContact = (searchResults[indexPath.row].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if currentContact != nil {
                        if arrContacts.contains(currentContact) {
                            cell.checkBtn.isSelected = true
                        } else {
                            cell.checkBtn.isSelected = false
                        }
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                    cell.imgIcon.image = #imageLiteral(resourceName: "manage_contact")
                } else {
                    if arrGroupIds.contains(searchResults[indexPath.row].gInfo.group_id!) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                    cell.imgIcon.image = #imageLiteral(resourceName: "icons8-people-1")
                }
            } else {
                cell.cName.text =  "\(allContacts[indexPath.row].name)"
                if allContacts[indexPath.row].type == "contact" {
                    let currentContact = (allContacts[indexPath.row].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
                    cell.imgIcon.image = #imageLiteral(resourceName: "manage_contact")
            } else {
                if arrGroupIds.contains(allContacts[indexPath.row].gInfo.group_id!) {
                    cell.checkBtn.isSelected = true
                } else {
                    cell.checkBtn.isSelected = false
                }
                cell.imgIcon.image = #imageLiteral(resourceName: "icons8-people-1")
            }
            
            }
        cell.checkBtn.tag = indexPath.row
        return cell
    }
    
    
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
//        contactsTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        contactsTable.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchResults = allContacts.filter{($0.name.lowercased().contains(searchText.lowercased()))}

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.contactsTable.reloadData()
    }
}

