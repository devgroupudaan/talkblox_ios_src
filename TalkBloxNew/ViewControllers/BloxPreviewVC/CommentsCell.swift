//
//  CommentsCell.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 03/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit

class CommentsCell: UITableViewCell {
    @IBOutlet weak var user_img: DesignableImageView!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var user_comment: UILabel!
    @IBOutlet weak var comment_age: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
