
//  PreviewBloxVC.swift
//  TalkBlox
//
//  Created by Mac on 02/02/16.
//  Copyright © 2016 MobiWebTech. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD
import AVKit
import AVFoundation
import MediaPlayer
import Gloss
import Contacts
import ContactsUI
import Cosmos
import IQKeyboardManagerSwift


class selContactsCell: UITableViewCell {
    @IBOutlet weak var cImgIcon: UIImageView!
    @IBOutlet weak var cName: UILabel!
    @IBOutlet weak var checkbtn: UIButton!
    
}
class PreviewBloxVC : UIViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegate, UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CNContactPickerDelegate,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    
    @IBOutlet var viewBlox : UIView!
    @IBOutlet var imageViewBlox : UIImageView!
    @IBOutlet var imgViewBackground : UIImageView!
    
    @IBOutlet weak var bloxViewWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var bloxViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var bloxViewCenterLayout: NSLayoutConstraint!
    
    @IBOutlet weak var bloxImageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var bloxImageWidthConstant: NSLayoutConstraint!
    
    //----- Collection view setting -----
    @IBOutlet weak var bloxCollectionTopLayout: NSLayoutConstraint!
    @IBOutlet weak var bloxCollectionBottomLayout: NSLayoutConstraint!
    
    @IBOutlet weak var videoImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoImageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var btnBloxCollection : [UIButton]!
    
    @IBOutlet var btnPreviewBlox : UIButton!
    
    @IBOutlet var btnPreview : UIButton!
    @IBOutlet var lblCaption : UILabel!
    @IBOutlet weak var newPreviewButtonOutlet: UIButton!
    
    @IBOutlet var collectionView: UICollectionView!
    
    //***** ANIMATION VIEW NEW CODE *****
    @IBOutlet var videoLayer : UIView!
    @IBOutlet var playerLayerView : UIView!
    @IBOutlet var videoImageView : UIImageView!
    
    @IBOutlet var vwChameleon : UIView!
    
    //----- Code for send/reply ----
    @IBOutlet var btnReply : UIButton!

    @IBOutlet var btnSend : UIButton!
    
    @IBOutlet var btnForward : UIButton!
    @IBOutlet weak var viewForward: UIView!
    @IBOutlet weak var btnForwardNew: UIButton!
    @IBOutlet weak var viewForwardNew: UIView!
    
    //--- Text Message Stuff ---
    @IBOutlet var btnTextMessage : UIButton!
    
    //--- Message text View ---
    @IBOutlet var vwMessage : UIView!
    @IBOutlet var btnCancelTextMessage : UIButton!
    @IBOutlet var btnSendTextMessage : UIButton!
    @IBOutlet var txtViewMessage : UITextView!
    @IBOutlet weak var viewMessageYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var contactsPickerView: UIView!
    @IBOutlet weak var contactsSearchBar: UISearchBar!
    @IBOutlet weak var contactsTable: UITableView!
    
    @IBOutlet weak var bloxcastBottomView: UIView!
    @IBOutlet weak var bloxcast_user_img: UIImageView!
    @IBOutlet weak var bloxcast_user: UILabel!
    @IBOutlet weak var blox_date: UILabel!
    @IBOutlet weak var bloxcast_subject: UILabel!
    @IBOutlet weak var bloxcast_rating: CosmosView!
   
    @IBOutlet weak var ratingBtn: UIButton!
    @IBOutlet weak var optionView: UIView!
    
    @IBOutlet weak var ratingPopUp: UIView!
    @IBOutlet weak var ratingBgView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingBtnBg: UIView!
    
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var commentsBgView: UIView!
    @IBOutlet weak var commentsTable: UITableView!
//    @IBOutlet weak var bloxComment: UITextView!
    @IBOutlet weak var bloxComment: UITextField!
    @IBOutlet weak var commentSViewHeight: NSLayoutConstraint!
    @IBOutlet weak var commentsViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var viewSendBtn: UIView!
    @IBOutlet weak var viewReplyBtn: UIView!
    @IBOutlet weak var viewRatingComment: UIView!
    @IBOutlet weak var viewReplyheight: NSLayoutConstraint!
    @IBOutlet weak var viewCommentTable: DesignableView!
    
    @IBOutlet weak var topGview: UIView!
        
    var previewKind = ""
     var avPlayer:AVPlayer?
    var bgAudioId : String = ""
    
    var aryTalkBloxContacts = [Contact]()
    var inbloxData:Inblox?
    var chatId = 0
    var isSentByMe = false
    var bgImageName = ""
    var backGroundImageID = 0
    var isFromSendMessage = false
   // var aryTalkBloxContacts : NSMutableArray = NSMutableArray()
    var isReplyMsg : Bool = false
    var backGroundSoundID:Int = 0

    var dictBloxData : NSMutableDictionary!
//    var arrContacts = NSMutableArray()
    var arrContacts = [String]()
    
    var bloxIncre : Int = 0
    var bloxArray : NSMutableArray = NSMutableArray()
    
    //-----
    var loading : JGProgressHUD = JGProgressHUD(style: .dark)
    var arrBloxDetails : NSMutableArray = NSMutableArray()
    
    var aryMessageData : NSMutableArray = NSMutableArray()
    
    var sliderDuration : Double = 5.0
    
    var strGalleryType : String = ""
    var playerLayer : AVPlayerLayer = AVPlayerLayer()
    var videoPlayer :AVPlayer = AVPlayer()
    
    fileprivate var PlayerStatusObservingContext = UnsafeMutablePointer<Int>(bitPattern: 1)
    fileprivate var  _notificationHandle = NSObject()
    
    //------
    var aryIndexPath : NSMutableArray = NSMutableArray()
    var viewForPlayerLayer : UIView = UIView()
    
    var imgTempCell : UIImageView = UIImageView()
    var textTempCell : UILabel = UILabel()// added on 22July2021
    var indexToBeAnimated : NSInteger = 0
    
    var animatedImage : UIImageView = UIImageView()
    var animatedImageRect : CGRect = CGRect()
    
    //----- Code for audio player playing behind if Bg track exists ---
//    var bgAudioPlayer : AVAudioPlayer! // = AVAudioPlayer()
    var bgAudioPlayer = AVPlayer()
    
    var bgAudioExists : Bool = false
    var strBgAudioPath : String = String()
    
    
    var isPreviewFromMessage : Bool = false
    
    //----- Code for showing message from sent and recieve list ----
    var aryUserMessageData : NSMutableArray!  = NSMutableArray()
    var dictUserMessageData : NSMutableDictionary!
    
    var contactIds : [String : AnyObject]!
    var strReplyAllMessageId : NSInteger = 0
    
    //----- Send Message Data
    var allDataArray : [String : AnyObject]!
    var allDataA : [AnyObject]!
    
    //----- Forward message data
    var strMessageId : String = ""
    
    var dictReplyDetails : NSMutableDictionary =  NSMutableDictionary()
    
    
    //--- Cell Resizing ---
    var cellCurrentSize : CGSize = CGSize(width: 89, height: 89)
    var lblCaptionTemp : UILabel = UILabel(frame: CGRect(x: 0,y: 0,width:30,height:22))
    
    
    //--- Double/Single Tap functionality ---
    var selectedIndexPath : IndexPath = IndexPath()
    var isDoubleTapped : Bool = false
    var isTapped : Bool = false

    var btnBloxDoubleTapped:UIImageView = UIImageView()
    var buttonBloxFrameDoubleTapped:CGRect = CGRect()
    
    var gestureSingleTap: UITapGestureRecognizer = UITapGestureRecognizer()
    var gestureDoubleTap: UITapGestureRecognizer = UITapGestureRecognizer()
    
    var havePlayedVideo : Bool = false
    
    var convId : NSInteger = 0
    
    //--- New change for forward and reply button, Stop animation thing ---
    var lastIndexAnimatedBeforeLeaving : NSInteger = 0
    var isGoneOutToChild : Bool = false
    
    
    //--- Preview Bubble Image from new service ---
    var serverBubbleImage : String!
    var bubbleImageFromUrl : UIImage!
    var inputImageBgID: String!
    var user_image : String = ""
    var user_name : String = ""
    var bloxSubject : String = ""
    var bloxDate : String = ""
    
    let contactStore = CNContactStore()
    var searchActive : Bool = false
    var contacts = [CNContact]()
    var searchResults = [mixedContacts]()
    
    var allContacts = [mixedContacts]()
    
    var groupSearchActive : Bool = false
    var groups = [Group]()
    var searchGroupResults = [Group]()
    var arrGroupIds = [Int]()
    
    var arrBloxcastComments = [BloxcastComment]()
    
    var selectedBtnBlox = UIImageView()
    var isFromTalkBloxVC: Bool!
    var userID_for_profile: Int!
    
    var ratingFromBlox:Double = 0.0
    var pageNo = 1
    
    var selectionType = "contacts"
    
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var strMessageBloxType : String = String()
    var messageID = 0
    
    var bloxIDsArr :[bloxIds]!
    
    //MARK: View did load
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        contactsSearchBar.delegate = self
        contactsSearchBar.backgroundColor = .clear
        contactsSearchBar.tintColor = .clear
        
        contactsTable.delegate = self
        contactsTable.dataSource = self
        
        btnPreviewBlox.isHidden = true
        
        viewForPlayerLayer.isHidden = true
        for btnBlox in btnBloxCollection
        {
            btnBlox.isHidden = true
        }
        if bgImageName != "" {

            var urlString =  WEB_URL.BaseURL + self.bgImageName
            urlString = urlString.replacingOccurrences(of: " ", with: "%20")
            self.imgViewBackground.kf.setImage(with: URL.init(string: urlString))
        }
        print(strBgAudioPath)
        strBgAudioPath = strBgAudioPath.replacingOccurrences(of: " ", with: "%20")
        self.playBgAudio(self.strBgAudioPath)
//        self.playSound(strUrl: self.strBgAudioPath)
//       self.playAudio(self.strBgAudioPath)
        
        lblCaption.text = ""
        btnPreview.layer.cornerRadius = 5.0
        btnSend.layer.cornerRadius = 5.0
        btnReply.layer.cornerRadius = 5.0
        btnForward.layer.cornerRadius = 5.0
        btnPreview.isEnabled = false
        
        btnTextMessage.layer.cornerRadius = 5.0
        btnCancelTextMessage.layer.cornerRadius = 5.0
        btnSendTextMessage.layer.cornerRadius = 5.0
        AppTheme().drawBorder(vwMessage, color: AppTheme().themeColorBlue, borderWidth: 2.0, cornerRadius: 12.0)
        vwMessage.isHidden = true
        
        //loadBloxContent()
       
        videoImageViewWidthConstraint.constant = self.view.frame.width// - 10
        videoImageViewHeightConstraint.constant = self.view.frame.width// - 10
        collectionView.isUserInteractionEnabled = true
        super.view.bringSubviewToFront(collectionView)
        self.view.bringSubviewToFront(collectionView)
        
        loadAllDataForPreview()
    
        
        gestureDoubleTap.delegate = self
        gestureSingleTap.delegate = self
        
        
        //--- Text message view ---
        let layer = vwMessage.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 10, height: 10)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 5
        
        if self.inputImageBgID != nil && !self.inputImageBgID.contains("null"){
            var urlString =  self.inputImageBgID
            urlString = urlString!.replacingOccurrences(of: " ", with: "%20")
            self.imgViewBackground.kf.setImage(with: URL.init(string: urlString!))
//            self.imgViewBackground.kf.setImage(with: URL.init(string:self.inputImageBgID))
        }

        
        let gesturehideRatingPopup: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.hideRatingPopup(_:)))
        gesturehideRatingPopup.numberOfTapsRequired = 1
        ratingBgView.addGestureRecognizer(gesturehideRatingPopup)
        
        let gesturehideCommentPopup: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.hideCommentPopup(_:)))
        gesturehideCommentPopup.numberOfTapsRequired = 1
        commentsBgView.addGestureRecognizer(gesturehideCommentPopup)
        
        commentsTable.register(UINib(nibName: "CommentsCell", bundle: nil), forCellReuseIdentifier: "CommentsCell")
        commentsTable.rowHeight = UITableView.automaticDimension
        commentsTable.estimatedRowHeight = 200
        commentsTable.delegate = self
        commentsTable.dataSource = self
        
        let swipeGesture2 = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeGesture2.direction = .down
        self.view.addGestureRecognizer(swipeGesture2)
        
        let gestureImageUserTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.imageUserTap(_:)))
        gestureImageUserTap.numberOfTapsRequired = 1
        bloxcast_user_img.addGestureRecognizer(gestureImageUserTap)
        let gestureUserLblTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.imageUserTap(_:)))
        gestureUserLblTap.numberOfTapsRequired = 1
        bloxcast_user.addGestureRecognizer(gestureUserLblTap)
        
        self.ratingView.didTouchCosmos = didTouchCosmos(_:)
        self.ratingView.rating = self.ratingFromBlox
        
        self.videoImageView.backgroundColor = .clear
        self.imgTempCell.backgroundColor = .clear
        self.animatedImage.backgroundColor = .clear
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
        if let key = userDefaults.object(forKey: "bubbleImage")  as? String
        {
            if(key != "")
            {
                imageViewBlox.image = UIImage(named: key)
            }
        }
        
        var isImageEmpty : Bool = false
        if((bubbleImageFromUrl) != nil)
        {
            if(bubbleImageFromUrl.size.height > 0)
            {
                imageViewBlox.image = bubbleImageFromUrl
            }
            else { isImageEmpty = true }
        }
        else  { isImageEmpty = true }
        
        if(isImageEmpty)
        {
            if((serverBubbleImage) != nil)
            {
                if(serverBubbleImage != "")
                {
                    imageViewBlox.image = UIImage(named: serverBubbleImage)
                }
            }
        }
        
//        LoadingIndicatorView.hide()
//        self.loading.dismiss()
        
        //--- Adding new code when view came back when reply is clicked in between msg preview
        if(aryMessageData.count > lastIndexAnimatedBeforeLeaving && lastIndexAnimatedBeforeLeaving != 0)
        {
            indexToBeAnimated = lastIndexAnimatedBeforeLeaving
            if(isGoneOutToChild)
            {
                if(bgAudioExists)
                {
                    bgAudioPlayer.play()
                    self.bgAudioPlayer.volume = 0.8
                    debugPrint("Bg volume increased to 0.8")
                }
                stopCellImageAnimation(animatedImage, buttonBloxFrame: animatedImageRect)
                isGoneOutToChild = false
            }
        }
       
        if previewKind == "Bloxcast" {
//            self.btnReply.isHidden = true
            self.viewSendBtn.isHidden = true
            self.viewReplyBtn.isHidden = true
            self.viewRatingComment.isHidden = false
            if isSentByMe {
                self.viewForwardNew.isHidden = false
            } else {
                self.viewForwardNew.isHidden = true
            }
            self.bloxcast_rating.isHidden = false
//            btnForward.isHidden = true
//            btnSend.isHidden = true
        } else if previewKind == "Preview" {
//            self.btnReply.isHidden = true
            self.viewSendBtn.isHidden = false
            self.viewReplyBtn.isHidden = true
            self.viewRatingComment.isHidden = true
            self.bloxcast_rating.isHidden = true
        } else if previewKind == "InBlox"{
//            ratingBtn.isHidden = true
            self.viewSendBtn.isHidden = true
            self.viewReplyBtn.isHidden = false
            if isSentByMe {
                self.viewForward.isHidden = false
            } else {
                self.viewForward.isHidden = true
            }
            self.viewRatingComment.isHidden = true
            self.bloxcast_rating.isHidden = true
        }
        bloxcast_user.text = user_name
        bloxcast_subject.text = bloxSubject
        var urlString = WEB_URL.BaseURL + user_image
        urlString = urlString.replacingOccurrences(of: " ", with: "%20")
        self.bloxcast_user_img.kf.setImage(with: URL.init(string: urlString))
        blox_date.text = bloxDate
        
        self.commentSViewHeight.constant = self.view.frame.size.height * 0.82
        self.view.layoutIfNeeded()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.black.withAlphaComponent(1.0).cgColor,
                                UIColor.black.withAlphaComponent(0.0).cgColor]
        //gradientLayer.locations = [0.0, 1.0]
        let window = UIApplication.shared.keyWindow
        let bottomPadding = window?.safeAreaInsets.bottom ?? 0
        let frame = CGRect(x: 0, y: -65, width: self.view.frame.width, height: 230 + bottomPadding)
        gradientLayer.frame = frame//bloxcastBottomView.bounds
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        bloxcastBottomView.layer.insertSublayer(gradientLayer, at: 0)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        if previewKind != "Preview"{
        self.serverCallForGetBloxcastDetails(chatId: self.chatId)
        }
//        self.serverCallForGetGroups()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        LoadingIndicatorView.hide()
//        self.loading.dismiss()
    }
    

    
    private func didTouchCosmos(_ rating: Double) {
        debugPrint("rating called:",rating)
        self.apiCallforSubmitRating()
      }
    
    @objc func hideRatingPopup(_ sender: UITapGestureRecognizer){
        self.ratingPopUp.isHidden = true
        self.ratingBtnBg.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.ratingBtnBg.alpha = 0.35
    }
    @objc func hideCommentPopup(_ sender: UITapGestureRecognizer){
//        self.commentsView.isHidden = true
        self.bloxComment.resignFirstResponder()
    }
    
    @objc func handleSwipe(_ gesture: UISwipeGestureRecognizer) {
        let window = UIApplication.shared.keyWindow!
        let viewHeight = window.frame.size.height - window.safeAreaInsets.bottom
             if gesture.direction == .down {
                
                UIView.animate(withDuration: 1.0, animations: {
                    self.commentSViewHeight.constant = 0//viewHeight - 150
//                    var frame = self.viewCommentTable.frame
//                    frame.size.height = 0
//                    self.viewCommentTable.frame = frame
                    self.view.layoutIfNeeded()
                    let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when){
                        self.commentsView.isHidden = true
                        self.commentSViewHeight.constant = self.view.frame.size.height * 0.82
                    }
                    
                    debugPrint("swipe called")
                })
        }
    }
    
    @IBAction func contactsPickerBack(_ sender: UIButton) {
        contactsPickerView.isHidden = true
        arrContacts.removeAll()
        self.arrGroupIds.removeAll()
        contactsSearchBar.resignFirstResponder()
        contactsSearchBar.text = ""
    }
    @IBAction func contactsPickerViewDone(_ sender: UIButton) {
        if arrContacts.count == 0 && arrGroupIds.count == 0{
//            AppSharedData.sharedInstance.showErrorAlert(message: "No user selected.")
            self.view.makeToast("No User Selected.", duration: 2.0, position: .bottom)
        } else {
            if isFromSendMessage {
                self.uploadAllTextScreenShots(type: "contacts")
                //  self.callSendMessage()
            }else {
                self.serverCallForForwardMessage()
            }
        }
        contactsSearchBar.resignFirstResponder()
    }
    @IBAction func selectContacts(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(searchActive)
            {
                let currentContactInfo = searchResults[sender.tag]
                if currentContactInfo.type == "contact" {
                    if searchResults[sender.tag].cInfo.phoneNumbers.count > 0 {
                        let currentContact = (searchResults[sender.tag].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        for (index,contact) in arrContacts.enumerated(){
                            if contact == currentContact {
                                arrContacts.remove(at: index)
                            }
                        }
                    }
                } else {
                    for (index,contact) in arrGroupIds.enumerated(){
                        if contact == currentContactInfo.gInfo.group_id {
                            arrGroupIds.remove(at: index)
                        }
                    }
                }
            } else {
                let currentContactInfo = allContacts[sender.tag]
                if currentContactInfo.type == "contact" {
                    if allContacts[sender.tag].cInfo.phoneNumbers.count > 0 {
                        let currentContact = (allContacts[sender.tag].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        for (index,contact) in arrContacts.enumerated(){
                            if contact == currentContact {
                                arrContacts.remove(at: index)
                            }
                        }
                    }
                } else {
                    for (index,contact) in arrGroupIds.enumerated(){
                        if contact == currentContactInfo.gInfo.group_id {
                            arrGroupIds.remove(at: index)
                        }
                    }
                }
            }
        } else {
            sender.isSelected = true
            if(searchActive)
            {
                let currentContactInfo = searchResults[sender.tag]
                if currentContactInfo.type == "contact" {
                    if searchResults[sender.tag].cInfo.phoneNumbers.count > 0 {
                    let currentContactStr = (searchResults[sender.tag].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
                } else {
                    if !arrGroupIds.contains(currentContactInfo.gInfo.group_id!){
                        arrGroupIds.append(currentContactInfo.gInfo.group_id!)
                    }
                }
            } else {
                let currentContactInfo = allContacts[sender.tag]
                if currentContactInfo.type == "contact" {
                    if allContacts[sender.tag].cInfo.phoneNumbers.count > 0 {
                        let currentContactStr = (allContacts[sender.tag].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
                } else {
                    if !arrGroupIds.contains(currentContactInfo.gInfo.group_id!){
                        arrGroupIds.append(currentContactInfo.gInfo.group_id!)
                    }
                }
            }
        }
    }
    @IBAction func commentTapped(_ sender: UIButton) {
//        self.commentsView.isHidden = false
        self.serverCallForGetBloxcastComments(chatId: self.chatId, pageNo: pageNo)
    }
    @IBAction func forwardNewTapped(_ sender: UIButton) {
        isFromSendMessage = false
        isGoneOutToChild = true
        APP_DELEGATE.isReplyMessage = false

        self.fetchContacts()
        self.contactsPickerView.isHidden = false
        self.arrContacts.removeAll()
        self.arrGroupIds.removeAll()
    }
    
    @IBAction func rateBlox(_ sender: UIButton) {
        self.ratingPopUp.isHidden = false

        self.ratingBtnBg.backgroundColor = #colorLiteral(red: 0.1530402899, green: 0.6953415871, blue: 0.9678348899, alpha: 1)
        self.ratingBtnBg.alpha = 1.0
    }
    @IBAction func ratingCancel(_ sender: UIButton) {
    }
    @IBAction func ratingSubmit(_ sender: UIButton) {
        ratingPopUp.isHidden = true
        self.ratingBtnBg.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.ratingBtnBg.alpha = 0.35
//        self.apiCallforSubmitRating()
        
//
    }
    @IBAction func commentsTopDone(_ sender: UIButton) {
        self.commentsView.isHidden = true
    }
    @IBAction func postComment(_ sender: UIButton) {
        if bloxComment.text!.count == 0 {
            showAlert("Can't send empty comment", strTitle: "Alert")
        } else {
            self.apiCallforSubmitComment()
        }
    }
  
    
    func days(from: String) -> Int? {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"

        let date2 = Date()
        guard
            let date1 = formatter.date(from: from)
        else {
            return nil
        }

        return Calendar.current.dateComponents([.day], from: date1, to: date2).day
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.bloxComment.endEditing(true)
    }
    
    //MARK: Tap gesture methods
    
    @objc func selfSingleTap(_ tapGesture : UITapGestureRecognizer)
    {
        if(isDoubleTapped)
        {
        if tapGesture.state == UIGestureRecognizer.State.ended
        {
            
            let point:CGPoint = tapGesture.location(in: videoImageView)
            if point != nil
            {
                if(point.x > videoImageView.frame.origin.x && point.x < videoImageView.frame.size.width && point.y > videoImageView.frame.origin.y && point.y < videoImageView.frame.size.height)
                {
                    btnPreview.isEnabled = true
                    btnPreview.isUserInteractionEnabled = true
                    stopCellImageAnimation(animatedImage, buttonBloxFrame: animatedImageRect)
                }
                
            }
            else
            {
                print("\n\n touched other \n\n")
            }
        }
            else
        {
            print("\n\n touched other \n\n")
            }
        }
    }
    
    @objc func selfDoubleTap(_ tapGesture : UITapGestureRecognizer)
    {
        
            if tapGesture.state == UIGestureRecognizer.State.ended
            {

                let point:CGPoint = tapGesture.location(in: collectionView)
                if point != nil
                {
                    if(point.x > collectionView.frame.origin.x && point.x < collectionView.frame.size.width && point.y > collectionView.frame.origin.y && point.y < collectionView.frame.size.height)
                    {
                        print("its here")
                        let indexPath : IndexPath = collectionView.indexPathForItem(at: point)!
                        if indexPath != nil
                        {
                            let row : NSInteger = indexPath.row //as? NSInteger
                            if row != nil
                            {
                                print("image taped \(row)")
                                indexToBeAnimated = row
                                isDoubleTapped = true
                                startCellImageAnimation()
                            }
                            else{
                                print("Do Some Other Stuff Here That Isnt Related")
                            }
                        }
                    }
                    
                }
                else
                {
                    print("\n\n touched other \n\n")
                }
                
            }
            else
            {
                print("\n\n touched other \n\n")
        }
    }
    ///tap on image
    @objc func imageUserTap(_ tapGesture : UITapGestureRecognizer){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        if previewKind == "Bloxcast" {
            vc.user_id = self.userID_for_profile
            if self.userID_for_profile == AppSharedData.sharedInstance.currentUser?.userid {
                vc.isSelfProfile = true
            } else {
                vc.isSelfProfile = false
            }
        } else if previewKind == "Preview" {
            vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
            vc.isSelfProfile = true
        } else if previewKind == "InBlox"{
            if isSentByMe {
                vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
                vc.isSelfProfile = true
            } else {
                vc.user_id = Int(self.inbloxData?.user_ids ?? "0")
                vc.isSelfProfile = false
            }
        }
        vc.previewKind = self.previewKind
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
        print("\n\n\n Tapped \n\n\n")
        if (touch.view!.isKind(of: UIButton.self)) {
            return false
        }
        return true
    }
    
    
    
    func loadAllDataForPreview()
    {
//        if((aryUserMessageData) != nil)
        if previewKind != "Preview"
        {
            if((dictUserMessageData) != nil)
            {
                if(dictUserMessageData.count>0)
                {
                    if let keyExists = dictUserMessageData.value(forKey: "bloxBubbleAudio")  as? String
                    {
                        print("background file exists with name\(keyExists)")
                        bgAudioExists = true
                        strBgAudioPath = keyExists
                    }
                    if let strType : String = dictUserMessageData.value(forKey: "MessagePreviewType") as? String
                    {
                        if(strType == "Inbox")
                        {
                            //--- for text message change ---
                            
                            btnReply.isHidden = false
                            btnTextMessage.isHidden = false
                        }
                        else
                        {
                            btnReply.isHidden = true
                            btnTextMessage.isHidden = true
                        }
                    }
                    else
                    {
                        btnReply.isHidden = false
                        btnTextMessage.isHidden = false
                    }
                }
            }
            if(aryUserMessageData.count>0)
            {
//                replyContact()
                aryMessageData = aryUserMessageData
            }
            isPreviewFromMessage = true
            
            btnForward.isHidden = false
            
            btnSend.isHidden = true
            
        }
        else
        {
            let userDefaults = UserDefaults.standard
            if let key = userDefaults.object(forKey: "bloxArray")  as? Data
            {
                if let keyExists = userDefaults.object(forKey: "bloxBubbleAudio")  as? String
                {
                    print("Background file exists with name \(keyExists)")
                    bgAudioExists = true
                    strBgAudioPath = keyExists
                }
                
                let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                arrBloxDetails = arrayData!  as! NSMutableArray
                print(arrBloxDetails.count)
                
                if(arrBloxDetails.count>0)
                {
                    for i in 0 ..< arrBloxDetails.count
                    {
                        //print("value at index\(i)")
                        if let dict  = arrBloxDetails.object(at: i) as? NSMutableDictionary
                        {
                            aryMessageData.add(dict)
//                            let bloxDetail : NSDictionary = dict.object(forKey: "bloxDetails") as! NSDictionary
//                            let bdaudioid = bloxDetail["bgAudioId"] as! String
//                            if bdaudioid.count > 0 {
//                                bgAudioExists = true
//                            }
                        }
                        else
                        {
                            print("empty")
                        }
                    }
                }
                
                
            }
            isPreviewFromMessage = false
            btnReply.isHidden = true
            btnForward.isHidden = true
            btnTextMessage.isHidden = true
            btnSend.isHidden = false
            
        }
        
        LoadingIndicatorView.hide()
//        self.loading.dismiss()
        if(aryMessageData.count > 0){
            
            
            //--- New Code for bubble resizing ---
            
            let bounds = UIScreen.main.bounds
            let height = bounds.size.height
            let width = bounds.size.width
            let userDefaults = UserDefaults.standard
            switch aryMessageData.count {
            case 1:
                bloxImageWidthConstant.constant = width - 70;
                bloxViewWidthConstant.constant = width - 70;
                
                bloxCollectionBottomLayout.constant = -55
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxCollectionBottomLayout.constant = -55
                    bloxImageHeightConstant.constant = width - 30;
                    bloxViewHeightConstant.constant = width - 30;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width - 30;
                    bloxViewHeightConstant.constant = width - 30;
                    
                    
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -65
                    bloxImageHeightConstant.constant = width - 20;
                    bloxViewHeightConstant.constant = width - 20;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -65
                    bloxImageHeightConstant.constant = width - 20;
                    bloxViewHeightConstant.constant = width - 20;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                default:
                     print("not an iPhone")
                    bloxCollectionBottomLayout.constant = -65
                    bloxImageHeightConstant.constant = width - 20;
                    bloxViewHeightConstant.constant = width - 20;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                    
                }
            case 2:
                bloxImageWidthConstant.constant = width - 50;
                bloxViewWidthConstant.constant = width - 50;
                
                bloxCollectionBottomLayout.constant = -32
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxCollectionBottomLayout.constant = -31
                    bloxImageHeightConstant.constant = width - 155;
                    bloxViewHeightConstant.constant = width - 155;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width - 155;
                    bloxViewHeightConstant.constant = width - 155;
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -43
                    bloxImageHeightConstant.constant = width - 170;
                    bloxViewHeightConstant.constant = width - 170;
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -43
                    bloxImageHeightConstant.constant = width - 190;
                    bloxViewHeightConstant.constant = width - 190;
                    break
                default:
                     print("not an iPhone")
                    bloxCollectionBottomLayout.constant = -43
                    bloxImageHeightConstant.constant = width - 170;
                    bloxViewHeightConstant.constant = width - 170;
                    break
                  
                }
            case 3:
                bloxImageWidthConstant.constant = width - 10;
                bloxViewWidthConstant.constant = width - 10;
                
                bloxCollectionBottomLayout.constant = -35
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxCollectionBottomLayout.constant = -30
                    bloxImageHeightConstant.constant = width - 184;
                    bloxViewHeightConstant.constant = width - 184;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width - 180;
                    bloxViewHeightConstant.constant = width - 180;
                    break
                case 667.0:
                    print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -35
                    bloxImageHeightConstant.constant = width - 215;
                    bloxViewHeightConstant.constant = width - 215;
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxImageHeightConstant.constant = width - 240;
                    bloxViewHeightConstant.constant = width - 240;
                    break
                default:
                     print("not an iPhone")
                    bloxImageHeightConstant.constant = width - 215;
                    bloxViewHeightConstant.constant = width - 215;
                    break
                   
                }
                
            case 4:
                bloxImageWidthConstant.constant = width - 50;
                bloxViewWidthConstant.constant = width - 50;
                
                bloxCollectionBottomLayout.constant = -60
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxCollectionBottomLayout.constant = -50
                    bloxImageHeightConstant.constant = width - 5;
                    bloxViewHeightConstant.constant = width - 5;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width - 5;
                    bloxViewHeightConstant.constant = width - 5;
                    
                    
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -68
                    bloxImageHeightConstant.constant = width + 5;
                    bloxViewHeightConstant.constant = width + 5;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 18
                                bloxCollectionBottomLayout.constant = -51
                            }
                        }
                    }
                    break
                case 736.0:
                   
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 20;
                    bloxViewHeightConstant.constant = width + 20;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                default:
                    print("not an iPhone")
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 20;
                    bloxViewHeightConstant.constant = width + 20;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                     break
                    
                }
            case 5:
                bloxImageWidthConstant.constant = width - 10;
                bloxViewWidthConstant.constant = width - 10;
                
                bloxCollectionBottomLayout.constant = -45
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxCollectionBottomLayout.constant = -49
                    bloxImageHeightConstant.constant = width - 70;
                    bloxViewHeightConstant.constant = width - 70;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width - 75;
                    bloxViewHeightConstant.constant = width - 75;
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -55
                    bloxImageHeightConstant.constant = width - 82;
                    bloxViewHeightConstant.constant = width - 82;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -65
                    bloxImageHeightConstant.constant = width - 85;
                    bloxViewHeightConstant.constant = width - 85;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 20
                                bloxCollectionBottomLayout.constant = -49
                            }
                        }
                    }
                    break
                default:
                     print("not an iPhone")
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -55
                    bloxImageHeightConstant.constant = width - 82;
                    bloxViewHeightConstant.constant = width - 82;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
               
                }
            case 6:
                bloxImageWidthConstant.constant = width - 10;
                bloxViewWidthConstant.constant = width - 10;
                
                bloxCollectionBottomLayout.constant = -45
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxCollectionBottomLayout.constant = -49
                    bloxImageHeightConstant.constant = width - 70;
                    bloxViewHeightConstant.constant = width - 70;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width - 75;
                    bloxViewHeightConstant.constant = width - 75;
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -55
                    bloxImageHeightConstant.constant = width - 82;
                    bloxViewHeightConstant.constant = width - 82;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 18
                                bloxCollectionBottomLayout.constant = -51
                            }
                        }
                    }
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -65
                    bloxImageHeightConstant.constant = width - 85;
                    bloxViewHeightConstant.constant = width - 85;
                   
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 20
                                bloxCollectionBottomLayout.constant = -49
                            }
                        }
                    }
                    break
                default:
                     print("not an iPhone")
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -55
                    bloxImageHeightConstant.constant = width - 82;
                    bloxViewHeightConstant.constant = width - 82;

                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 18
                                bloxCollectionBottomLayout.constant = -51
                            }
                        }
                    }
                    break
                 
                }
            case 7:
                bloxImageWidthConstant.constant = width - 10;
                bloxViewWidthConstant.constant = width - 10;
                
                bloxCollectionBottomLayout.constant = -65
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxViewCenterLayout.constant = 0
                    bloxCollectionBottomLayout.constant = -58
                    bloxImageHeightConstant.constant = width + 35 ;
                    bloxViewHeightConstant.constant = width + 35 ;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width + 40;
                    bloxViewHeightConstant.constant = width + 40;
                    
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -70
                    bloxImageHeightConstant.constant = width + 50;
                    bloxViewHeightConstant.constant = width + 50;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 19
                                bloxCollectionBottomLayout.constant = -50
                            }
                        }
                    }
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 65;
                    bloxViewHeightConstant.constant = width + 65;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                default:
                     print("not an iPhone")
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 65;
                    bloxViewHeightConstant.constant = width + 65;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                   
                }
            case 8:
                bloxImageWidthConstant.constant = width - 10;
                bloxViewWidthConstant.constant = width - 10;
                
                bloxCollectionBottomLayout.constant = -65
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxViewCenterLayout.constant = 0
                    bloxCollectionBottomLayout.constant = -58
                    bloxImageHeightConstant.constant = width + 35 ;
                    bloxViewHeightConstant.constant = width + 35 ;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width + 40;
                    bloxViewHeightConstant.constant = width + 40;
                    
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -70
                    bloxImageHeightConstant.constant = width + 50;
                    bloxViewHeightConstant.constant = width + 50;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 19
                                bloxCollectionBottomLayout.constant = -50
                            }
                        }
                    }
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 65;
                    bloxViewHeightConstant.constant = width + 65;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                default:
                     print("not an iPhone")
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 65;
                    bloxViewHeightConstant.constant = width + 65;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                   
                }
            case 9:
                bloxImageWidthConstant.constant = width - 10;
                bloxViewWidthConstant.constant = width - 10;
                
                bloxCollectionBottomLayout.constant = -65
                
                switch height
                {
                case 480.0:
                    //print("iPhone 3,4")
                    bloxViewCenterLayout.constant = 0
                    bloxCollectionBottomLayout.constant = -58
                    bloxImageHeightConstant.constant = width + 35;
                    bloxViewHeightConstant.constant = width + 35;
                    break
                case 568.0:
                    //print("iPhone 5")
                    bloxImageHeightConstant.constant = width + 40;
                    bloxViewHeightConstant.constant = width + 40;
                    
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 17
                                bloxCollectionBottomLayout.constant = -52
                            }
                        }
                    }
                    break
                case 667.0:
                    //print("iPhone 6")
                    bloxCollectionBottomLayout.constant = -70
                    bloxImageHeightConstant.constant = width + 50;
                    bloxViewHeightConstant.constant = width + 50;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 19
                                bloxCollectionBottomLayout.constant = -50
                            }
                        }
                    }
                    break
                case 736.0:
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 65;
                    bloxViewHeightConstant.constant = width + 65;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                default:
                     print("not an iPhone")
                    //print("iPhone 6+")
                    bloxCollectionBottomLayout.constant = -85
                    bloxImageHeightConstant.constant = width + 65;
                    bloxViewHeightConstant.constant = width + 65;
                    if let key = userDefaults.object(forKey: "bubbleImage")  as? String
                    {
                        if(key != "")
                        {
                            if(key == "message_background9")
                            {
                                bloxCollectionTopLayout.constant = 23
                                bloxCollectionBottomLayout.constant = -46
                            }
                        }
                    }
                    break
                   
                }
            default:
                break
            }
            
            
            print("message array count :- \(aryMessageData.count)")
            self.collectionView.reloadData()
        }
        
        
        if self.isSentByMe == true {
            self.btnReply.isHidden = true
            self.viewReplyheight.constant = 0
        }else {
            self.btnReply.isHidden = false
            self.viewReplyheight.constant = 55
        }
    }
    
    func replyContact() {
        let params:NSDictionary = ["authentication_Token":AppSharedData.sharedInstance.getUser()!.authentication_token!,"conversationId":convId]
        //LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.replyContacts, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            // LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                
                //   self.loading.dismiss()
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let JSON : NSArray = response?["contacts"] as? NSArray
                    {
                        print("JSON: \(JSON)")
                        
                        if(JSON.count > 1 || JSON.count > 0)
                        {
                          //  self.aryTalkBloxContacts = JSON.mutableCopy() as! NSMutableArray
                            self.aryTalkBloxContacts = [Contact].from(jsonArray: JSON as! [JSON])!

                            if(self.aryTalkBloxContacts.count > 0)
                            {
                               // self.tblContactList.reloadData()
                            }
                        }
                        else
                        {
                            //self.tblContactList.reloadData()
                            let errorAlert = AppTheme().showAlert("TalkBlox contact empty", errorTitle: "Contact Alert")
                            self.present(errorAlert, animated: true ,completion: nil)
                        }
                        
                    }
                    else
                    {
                        let errorAlert = AppTheme().showAlert("Error in getting data", errorTitle: "Contact Alert")
                        self.present(errorAlert, animated: true ,completion: nil)
                    }
                    
                }
                else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
                   // AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })

    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        //
        NotificationCenter.default.removeObserver(self)
        
        
        //--- Added this code to stop preview when it goes on other view ---
        lastIndexAnimatedBeforeLeaving = indexToBeAnimated
        
        indexToBeAnimated = aryMessageData.count
        if(isGoneOutToChild)
        {
            if(animatedImage.tag - 1230000 >= 0)
            {
                stopCellImageAnimation(animatedImage, buttonBloxFrame: animatedImageRect)
            }
        }
        self.avPlayer?.pause()
        self.videoPlayer.pause()
        self.bgAudioPlayer.pause()
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    //MARK: Collection view delegate methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let bounds = UIScreen.main.bounds
        //let height = bounds.size.height
        let width = bounds.size.width
        
        switch aryMessageData.count
        {
        case 1:
            cellCurrentSize = CGSize(width: width - 100, height: width - 100)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: width - 100, height: width - 100)
        case 2:
            cellCurrentSize = CGSize(width: (width - 80)/2, height: (width - 80)/2)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 80)/2, height: (width - 80)/2)
        case 3:
            cellCurrentSize = CGSize(width: (width - 42)/3, height: (width - 42)/3)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 42)/3, height: (width - 42)/3)
        case 4:
            cellCurrentSize = CGSize(width: (width - 80)/2, height: (width - 80)/2)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 80)/2, height: (width - 80)/2)
        case 5:
            cellCurrentSize = CGSize(width: (width - 42)/3, height: (width - 42)/3)
            return CGSize(width: (width - 42)/3, height: (width - 42)/3)
        case 6:
            cellCurrentSize = CGSize(width: (width - 42)/3, height: (width - 42)/3)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 42)/3, height: (width - 42)/3)
        case 7:
            cellCurrentSize = CGSize(width: (width - 42)/3, height: (width - 42)/3)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 42)/3, height: (width - 42)/3)
        case 8:
            cellCurrentSize = CGSize(width: (width - 42)/3, height: (width - 42)/3)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 42)/3, height: (width - 42)/3)
        case 9:
            cellCurrentSize = CGSize(width: (width - 42)/3, height: (width - 42)/3)
            print("\n ---- sizeForItemAtIndexPath \(cellCurrentSize) ----")
            return CGSize(width: (width - 42)/3, height: (width - 42)/3)
        default:
            cellCurrentSize = CGSize(width: 88, height: 89)
            return CGSize(width: 88, height: 89)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if((aryMessageData.count > 4 && aryMessageData.count < 7) )
        {
            return 6
        }
        else if(aryMessageData.count > 6)
        {
            return 9
        }
        else
        {
            return aryMessageData.count
        }
        
        //return aryMessageData.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "verticalCell", for: indexPath)
        
        //let button : UIButton = UIButton(frame: cell.frame)
        //button.setTitle("", forState: UIControlState.normal)
        
        var imgFrame : CGRect = cell.frame
        imgFrame.origin.x = 0
        imgFrame.origin.y = 0
        let button : UIImageView = UIImageView(frame: imgFrame)
        button.image = UIImage(named:"SingleBlox")
        button.contentMode = .scaleAspectFill
        button.tag = 1230000 + indexPath.row
        
        button.backgroundColor = UIColor.clear
        cell.addSubview(button)
        
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        
        
        button.image = UIImage(named:"SingleBlox")
        button.contentMode = UIView.ContentMode.scaleAspectFill
        button.backgroundColor = UIColor.clear
        
        if(aryMessageData.count > indexPath.row)
        {
            aryIndexPath.add(indexPath)
        }
        
        print("ary count",aryMessageData.count,"row count",indexPath.row)
        
        //-- commented due to new blox functionality for animation
        if(aryMessageData.count > 4 && aryMessageData.count < 7)//(aryMessageData.count > 4)
        {
            if(indexPath.row == 5)//(indexPath.row == 8)
            {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1) / Double(NSEC_PER_SEC))
                {
                    if(self.bgAudioExists)
                    {
                        self.playAudio(self.strBgAudioPath)
                    }
                    self.startCellImageAnimation()
                }
            }
        }
        else if(aryMessageData.count > 6)
        {
            if(aryMessageData.count-1 == indexPath.row)
            {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1) / Double(NSEC_PER_SEC))
                {
                    if(self.bgAudioExists)
                    {
                        self.playAudio(self.strBgAudioPath)
                    }
                    self.startCellImageAnimation()
                }
            }
        }
        else
        {
            if(aryMessageData.count-1 == indexPath.row)
            {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1) / Double(NSEC_PER_SEC))
                {
                    if(self.bgAudioExists)
                    {
                        self.playAudio(self.strBgAudioPath)
                    }
                    self.startCellImageAnimation()
                }
            }
        }
        
        //--- commented due client changed her mind on 23/05/2016
        
        //---- For new design update ----
//        if(aryMessageData.count-1 == indexPath.row)
//        {
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1) / Double(NSEC_PER_SEC))
//
////            dispatch_after(dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1), dispatch_get_main_queue())
//            {
//                if(self.bgAudioExists)
//                {
//                    self.playBgAudio(self.strBgAudioPath)
//                }
//                self.startCellImageAnimation()
//            }
//        }

        print("cell data added")
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        selectedIndexPath = indexPath
        print("cell selected")
    }
    
    
//    func startAnimation(_ limit : Int)
//    {
//        var delayValue : TimeInterval = 0.0;
//
//        for i in 0 ..< limit
//        {
//            let btnBlox = self.view.viewWithTag(1230000 + i) as! UIImageView
//            btnBlox.tag = (1230000 + i)
//            //let btnBlox = btnBloxCollection[i]
//
//            //self.performSelector("animateThroughCenter:", withObject: btnBlox, afterDelay: delayValue)
//
//            //*** new
//            //animateImage(image : UIImage)
//            //loadDataForCaption(btnBlox)
//            //self.performSelector("animateThroughCenter:", withObject: btnBlox, afterDelay: delayValue)
//
//            loadDataForCaption(btnBlox)
//            self.perform(#selector(PreviewBloxVC.animateCellImage(_:)), with: btnBlox, afterDelay: delayValue)
//            //animateCellImage(btnBlox)
//
//            //delayValue += sliderDuration
//
//            //if(sliderDuration == )
//            //self.performSelector("animateImage:", withObject: btnBlox, afterDelay: delayValue)
//            print(sliderDuration)
//            //***
//
//            //doAnimation(btnBlox)
//            delayValue += 2.6 + sliderDuration
//            if(strGalleryType == "video")
//            {
//                delayValue += 6.0 + sliderDuration
//            }
//
//
//        }
//        //btnPreview.enabled = true
//    }
    
    //------
    
    func loadDataForCaption(_ btnBlox: UIImageView)
    {
        if let bloxDict : NSMutableDictionary = aryMessageData.object(at: btnBlox.tag - 1230000) as? NSMutableDictionary
        {
            if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
            {
                strGalleryType = bloxType as String
                let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                lblCaption.textAlignment = NSTextAlignment.center
                switch bloxType
                {
                case "TextMessage":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption//" " + "\(strCaption)  "
                        //lblCaption.text = strCaption
                        if(strCaption != "")
                        {
                        lblCaption.isHidden = false
                        }
                        lblCaption.sizeToFit()
                        //lblCaption.intrinsicContentSize()
                        if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                        {
                            if sliderValue == 0 {
                                sliderDuration = 5
                            } else {
                                sliderDuration = sliderValue
                            }
//                            sliderDuration = sliderValue
                        }
                        if let strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                        {
                            strGalleryType = "Video"
                        }
                        
                    }
                    break
                case "Camera":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption//" " + "\(strCaption)  "
                        //lblCaption.text = strCaption
                        lblCaption.isHidden = false
                        lblCaption.sizeToFit()
                        //lblCaption.intrinsicContentSize()
                        if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                        {
                            if sliderValue == 0 {
                                sliderDuration = 5
                            } else {
                                sliderDuration = sliderValue
                            }
//                            sliderDuration = sliderValue
                        }
                        if let strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                        {
                            strGalleryType = "Video"
                        }
                    }
                    break
                case  "GalleryImage":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        if(strCaption != "")
                        {
                            lblCaption.text = strCaption//" " + "\(strCaption)  "
                            //lblCaption.text = strCaption
                            lblCaption.isHidden = false
                            lblCaption.sizeToFit()
                            //lblCaption.intrinsicContentSize()
                            if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                            {
                                if sliderValue == 0 {
                                    sliderDuration = 5
                                } else {
                                    sliderDuration = sliderValue
                                }
//                                sliderDuration = sliderValue
                            }
                            if let strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                            {
                                strGalleryType = "Video"
                            }
                        }
                    }
                    break
                case "Video":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption//" " + "\(strCaption)  "
                        //lblCaption.text = strCaption
                        lblCaption.isHidden = false
                        lblCaption.sizeToFit()
                        //lblCaption.intrinsicContentSize()
                        strGalleryType = "Video"
                    }
                    break
                default:
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption//" " + "\(strCaption)  "
                        //lblCaption.text = strCaption
                        if(strCaption != "")
                        {
                        lblCaption.isHidden = false
                        }
                        lblCaption.sizeToFit()
                        //lblCaption.intrinsicContentSize()
                        if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                        {
                            if sliderValue == 0 {
                                sliderDuration = 5
                            } else {
                                sliderDuration = sliderValue
                            }
//                            sliderDuration = sliderValue
                        }
                       
                        
                    }
                    break
                }
                
            }
        }
    }
    
    //MARK: Start Cell animation
    
    func startCellImageAnimation()
    {
        self.gestureDoubleTap.isEnabled = false
       
        if(isDoubleTapped)
        {
            lblCaption.text = ""
            let btnBlox = self.view.viewWithTag(1230000 + indexToBeAnimated) as! UIImageView
            btnBlox.tag = (1230000 + indexToBeAnimated)
            
            let cell = btnBlox.superview
            print(cell?.frame as Any)
            
            loadDataForCaption(btnBlox)
            animateCellImage(btnBlox)
            
            //indexToBeAnimated = indexToBeAnimated + 1
            btnPreview.isUserInteractionEnabled = false
            //lblCaption.text = "Hi there prototype message"
            print(lblCaption.text!)
        }
        else
        {
        if(aryMessageData.count > indexToBeAnimated)
        {
            lblCaption.text = ""
            if self.view.viewWithTag(1230000 + indexToBeAnimated) != nil {
                let btnBlox = self.view.viewWithTag(1230000 + indexToBeAnimated) as! UIImageView
                btnBlox.tag = (1230000 + indexToBeAnimated)
                
                let cell = btnBlox.superview
                print(cell?.frame as Any)
                
                selectedBtnBlox = btnBlox
                animateCellImage(btnBlox)
                loadDataForCaption(btnBlox)
            }
            indexToBeAnimated = indexToBeAnimated + 1
            btnPreview.isUserInteractionEnabled = false
            //lblCaption.text = "Hi there prototype message"
            print(lblCaption.text!)
            
            
        }
        else
        {
            lblCaption.text = ""
            lblCaption.isHidden = true
            indexToBeAnimated = 0
            btnPreview.isUserInteractionEnabled = true
            btnPreview.isEnabled = true
            if(bgAudioExists)
            {
            bgAudioPlayer.pause()
            //NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: bgAudioPlayer.currentItem)
            }
            //---- adding double tap gesture ----
            gestureDoubleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfDoubleTap(_:)))
            gestureDoubleTap.numberOfTapsRequired = 2
            self.view.addGestureRecognizer(gestureDoubleTap)
            //------
        }
        }
    }
    
    //MARK: Cell animation
    @objc func animateCellImage(_ btnBlox: UIImageView)
    {
        imgTempCell = UIImageView()
        imgTempCell.contentMode = .scaleAspectFit
        viewForPlayerLayer = UIView()
        
        imgTempCell.frame = btnBlox.frame
        //viewForPlayerLayer.frame = imgTempCell.frame
        
//        textTempCell = UILabel()
//        textTempCell.frame = CGRect(x: 10, y: (btnBlox.frame.height-20)/2, width: btnBlox.frame.width - 20, height: 20)
//        textTempCell.numberOfLines = 1
//        textTempCell.lineBreakMode = .byTruncatingTail
//        textTempCell.adjustsFontSizeToFitWidth = true
//        textTempCell.minimumScaleFactor = 0.5
        
        vwChameleon.addSubview(imgTempCell)
//        vwChameleon.addSubview(textTempCell)
//        textTempCell.isHidden = true
        //vwChameleon.addSubview(lblCaption)
        
        
        //vwChameleon.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        imgTempCell.backgroundColor = UIColor.clear
        imgTempCell.contentMode = UIView.ContentMode.scaleToFill
        btnBlox.backgroundColor = UIColor.white
        btnBlox.isHidden = true
        
        let cell = btnBlox.superview
        print(cell?.frame)
        
        
        //------
        
        //var lblCaptionTemp : UILabel = UILabel(frame: CGRect(x: 0,y: 0,width:30,height:22))// lblCaption
        
//         lblCaptionTemp.intrinsicContentSize().width
//        print("temp cell frame :- \(imgTempCell.frame)")
//        print("current cell frmae :- \(cellCurrentSize)")
//        
//        var captionFrame : CGRect = imgTempCell.frame
//        captionFrame.origin.x = cell!.frame.origin.x + (cell!.frame.size.width/2 - lblCaptionTemp.frame.size.width/2)
//        captionFrame.origin.y = cell!.frame.origin.y + cell!.frame.size.height + 15
//        print("\n\n \t catption position new \(captionFrame)")
//        
//        lblCaption.frame.origin = captionFrame.origin
//        
//        lblCaptionTemp.frame.origin = captionFrame.origin
//        
//        lblCaptionTemp.textColor = UIColor.whiteColor()
//        lblCaptionTemp.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.7)
//        lblCaptionTemp.layer.cornerRadius = 5.0
//        lblCaptionTemp.clipsToBounds = true
//        lblCaptionTemp.hidden = false
//        //vwChameleon.addSubview(lblCaptionTemp)
        
        
        //btnBlox.image = imgTempCell.image
        
        if let bloxDict : NSMutableDictionary = aryMessageData.object(at: btnBlox.tag - 1230000) as? NSMutableDictionary
        {
            let indexPath : IndexPath = aryIndexPath.object(at: btnBlox.tag - 1230000) as! IndexPath
            let cell : UICollectionViewCell = collectionView.cellForItem(at: indexPath)! as UICollectionViewCell
            let rect : CGRect = cell.frame
            //            rect.origin.x += 14
            //            rect.origin.y += 13
            imgTempCell.frame = rect
            //viewForPlayerLayer.frame = rect
            
            
            
            //-----
            var captionFrame : CGRect = cell.frame
            switch aryMessageData.count {
            case 1:
                captionFrame.origin.x = cell.frame.origin.x + (cellCurrentSize.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height + 20

                //captionFrame.origin = lblCaption.frame.origin
            case 2:
                captionFrame.origin.x = cell.frame.origin.x + (cellCurrentSize.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + (cellCurrentSize.height * 1.5)// + 100
                break
            case 3:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + (cellCurrentSize.height * 1.8)// + 100
            case 4:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height + 5
            case 5:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height + 25
            case 6:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height + 25
            case 7:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height - 20
            case 8:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height - 20
            case 9:
                captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
                captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height - 20
            default:
                break
            }
            //captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
            //captionFrame.origin.y = cell.frame.origin.y + imgTempCell.frame.size.height
            lblCaption.frame.origin = captionFrame.origin//.y
            
            lblCaption.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            //print("\n\n\n\n\n captionFrame \(captionFrame) \n cell.frame \(cell.frame) \n imgTempCell.frame \(imgTempCell.frame) \n  cellCurrentSize \(cellCurrentSize) \n lblCaption \(lblCaption.frame)\n\n")
            //-----
            
            
//            var labelCaption : UILabel = lblCaption
//            labelCaption.frame = CGRectMake(cell.frame.origin.x + (cell.frame.size.width/2 - labelCaption.frame.size.width/2) , cell.frame.origin.y + cell.frame.size.height, labelCaption.frame.size.width, labelCaption.frame.size.height)
//            //labelCaption.text = "jhggfjdsfnmbndmfbmnbvcxmb"
//            vwChameleon.addSubview(labelCaption)
            
            
            
    
            if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
            {
                strGalleryType = bloxType as String
                let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                
                if let data : Data = bloxDetail.value(forKey: "image") as? Data
                {
//                    if bloxType != "TextMessage" {
                    if let imageData : UIImage = UIImage(data: data)
                    {
                        let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
                        print("isAnimated: \(isImageAnimated)")
                        if(isImageAnimated){
                            btnBlox.image = UIImage.gifImageWithData(data)//imageData
                            btnBlox.contentMode = UIView.ContentMode.scaleToFill
                            imgTempCell.image = UIImage.gifImageWithData(data)//imageData
                            imgTempCell.contentMode = UIView.ContentMode.scaleToFill

                        }else{
                            btnBlox.image = imageData
                            btnBlox.contentMode = UIView.ContentMode.scaleToFill
                            imgTempCell.image = imageData
                            imgTempCell.contentMode = UIView.ContentMode.scaleToFill
                        }
                        
//                        btnBlox.image = UIImage.gifImageWithData(data)//imageData
//                        btnBlox.contentMode = UIViewContentMode.scaleToFill
//                        imgTempCell.image = UIImage.gifImageWithData(data)//imageData
//                        imgTempCell.contentMode = UIViewContentMode.scaleToFill
                        
                    }
//                }
                }
                
                if var soundURL : String = bloxDetail.value(forKey: "soundURL") as? String
                {
                    
                    
                    soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    print(soundURL)
                    if !soundURL.contains("null") {
                        self.playSound(strUrl: soundURL)
                    }
                } else if var audioURLStr : String = bloxDetail.value(forKey: "audio") as? String
                {
                    let audioURL = AppTheme().getFilePathURL(audioURLStr)
                    
                    print(audioURL)
//                    if !audioURL.contains("null") {
                    if var strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                    {
                    } else {
                        self.playSound(strUrl: audioURL.relativeString)
                    }
//                    }
                }
            
                var videoPath : String = ""
                lblCaption.textAlignment = NSTextAlignment.center
                switch bloxType
                {
                case "TextMessage":
//                    if let strText: String = bloxDetail.value(forKey: "message") as? String{
//                        textTempCell.text = strText
//                        if let txtColor : UIColor = bloxDetail.value(forKey: "textColor") as? UIColor{
//                            textTempCell.textColor = txtColor
//                        }
//                        if let txtBgColor : UIColor = bloxDetail.value(forKey: "backgroundColor") as? UIColor {
//                            textTempCell.backgroundColor = txtBgColor
//                        }
//                        textTempCell.isHidden = false
//                    }
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption//" " + "\(strCaption)  "
                        lblCaption.isHidden = false
                        lblCaption.sizeToFit()
                        //lblCaption.text = strCaption
                        
                    }
                    if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                    {
                        if sliderValue == 0 {
                            sliderDuration = 5
                        } else {
                            sliderDuration = sliderValue
                        }
//                        sliderDuration = sliderValue
                    }
                    if var strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                    {
                        strGalleryType = "Video"
                   
                        strVideoFile = strVideoFile.replacingOccurrences(of: " ", with: "%20")
                        videoPath = strVideoFile
                    }
                    break
                case "Camera":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption//" " + "\(strCaption)  "
                        lblCaption.isHidden = false
                        lblCaption.sizeToFit()
                        //lblCaption.text = strCaption
                    }
                    if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                    {
                        if sliderValue == 0 {
                            sliderDuration = 5
                        } else {
                            sliderDuration = sliderValue
                        }
//                        sliderDuration = sliderValue
                    }
                    if var strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                    {
                        strGalleryType = "Video"
                        strVideoFile = strVideoFile.replacingOccurrences(of: " ", with: "%20")
                        videoPath = strVideoFile
                    }
                    break
                case  "GalleryImage":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        if(strCaption != "")
                        {
                            lblCaption.text = strCaption// " " + "\(strCaption)  "
                            lblCaption.isHidden = false
                            lblCaption.sizeToFit()
                            //lblCaption.text = strCaption
                        }
                    }
                    
                    if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                    {
                        if sliderValue == 0 {
                            sliderDuration = 5
                        } else {
                            sliderDuration = sliderValue
                        }
//                        sliderDuration = sliderValue
                    }
                    if var strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                    {
                        strGalleryType = "Video"
                         strVideoFile = strVideoFile.replacingOccurrences(of: " ", with: "%20")
                        videoPath = strVideoFile
                    }
                    
                    break
                case "Video":
                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
                    {
                        lblCaption.text = strCaption// " " + "\(strCaption)  "
                        lblCaption.isHidden = false
                        lblCaption.sizeToFit()
                        //lblCaption.text = strCaption
                    }
                    if var strPath : String = bloxDetail.value(forKey: "video") as? String
                    {
                        strPath = strPath.replacingOccurrences(of: " ", with: "%20")
                        videoPath = strPath
                    }
                    break
                default:
                    break
                }
                
                //                btnPreviewBlox.setImage(imgTempCell.image, forState: UIControlState.normal)
                //                btnPreviewBlox.backgroundColor = UIColor.whiteColor()
                //                btnPreviewBlox.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
                //                btnPreviewBlox.frame = imgTempCell.frame
                //                btnPreviewBlox.hidden = false
                viewForPlayerLayer.frame = imgTempCell.frame
                
                
                if let sliderValue : Double = bloxDetail.value(forKey: "animationDuration") as? Double
                {
                    if sliderValue == 0 {
                        sliderDuration = 5
                    } else {
                        sliderDuration = sliderValue
                    }
                }
//                let lblCaptionTemp : UILabel = lblCaption
//                var captionFrame : CGRect = imgTempCell.frame
//                captionFrame.origin.x = imgTempCell.frame.size.width/2 - lblCaption.frame.size.width/2
//                captionFrame.origin.y = imgTempCell.frame.size.height + 25
//                print("\n\n \t catption position new \(captionFrame)")
//                lblCaptionTemp.frame.origin = captionFrame.origin
//                lblCaptionTemp.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.7)
//                lblCaptionTemp.layer.cornerRadius = 5.0
//                lblCaptionTemp.clipsToBounds = true
//                vwChameleon.addSubview(lblCaptionTemp)
                
  
                
                print("caption label text\(lblCaption.text!)")
                if(lblCaption.text! == "" || (lblCaption.text?.contains("null"))! || lblCaption.text! == "    " || lblCaption.text! == "   " || lblCaption.text! == "  ")
                {
                    self.lblCaption.backgroundColor = UIColor.clear
                    lblCaption.text = ""
                    lblCaption.isHidden = true
                    print("in the condition where it should be")
                }
                else // added code recently
                {
                    lblCaption.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.7)
                    lblCaption.layer.cornerRadius = 5.0
                    lblCaption.clipsToBounds = true
                }
                

                //lblCaptionTemp.text = lblCaption.text
                //lblCaptionTemp.frame.size = lblCaption.frame.size
                
                let buttonBloxFrame : CGRect = imgTempCell.frame
                let options = UIView.AnimationOptions()
                self.view.bringSubviewToFront(lblCaption)
                applyPlainShadow(imgTempCell)
                
                
                
                var captionFrameUpdate : CGRect = self.lblCaption.frame
                captionFrameUpdate.origin.x = videoImageView.frame.size.width/2 - lblCaption.frame.size.width/2
                captionFrameUpdate.origin.y = videoImageView.frame.size.height + 10
                //self.lblCaption.frame = captionFrameUpdate
                
                
                // commented recently
//                if(lblCaption.text != "")
//                {
//                    lblCaption.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.7)
//                    lblCaption.clipsToBounds = true
//                    //lblCaption.layer.borderColor = AppTheme().themeColorGreen.CGColor
//                    //lblCaption.layer.borderWidth = 1.0
//                    lblCaption.layer.cornerRadius = 5.0
//                    //lblCaption.sizeToFit()
//                    //lblCaption.intrinsicContentSize().width;
//                    //lblCaption.intrinsicContentSize()
//                }
                if(bgAudioExists)
                {
                    self.bgAudioPlayer.volume = 0.8
                    debugPrint("Bg volume increased to 0.8")
                }
                //var videoImageFrame : CGRect = self.videoImageView.frame
                //videoImageFrame.size.width = self.view.frame.width - 20
                //videoImageFrame.size.height = self.view.frame.width - 20
                
//                videoImageViewWidthConstraint.constant = self.view.frame.width - 40
//                videoImageViewHeightConstraint.constant = self.view.frame.width - 40
                
                //self.videoImageView.frame.size = videoImageFrame.size
                
//MARK: -TypeCasting
                if(strGalleryType == "TextMessage" || strGalleryType ==  "Camera" || strGalleryType ==  "GalleryImage")
                {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: options, animations:
                        {
                            btnBlox.isHidden = true

                            //print(self.collectionView.center)
                            //print("center \(self.collectionView.frame.origin.x - self.collectionView.frame.size.width/2), \(self.collectionView.frame.origin.y - self.collectionView.frame.size.height/2)")
                            
                            self.imgTempCell.frame.size = self.videoImageView.frame.size
//                            self.imgTempCell.transform = CGAffineTransformMakeScale(2.58, 2.58)
                            self.imgTempCell.center =  CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            
                            
//                            var captionFrameUpdate : CGRect = self.imgTempCell.frame
//                            captionFrameUpdate.origin.x = self.cellCurrentSize.width/2 - lblCaptionTemp.frame.size.width/2
//                            captionFrameUpdate.origin.y = self.imgTempCell.frame.origin.y + self.cellCurrentSize.height + 50
//                            lblCaptionTemp.frame.origin = captionFrameUpdate.origin
                            
                            //self.btnPreviewBlox.transform = CGAffineTransformMakeScale(2.5, 2.5)
                            //self.btnPreviewBlox.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            
                            
                            self.lblCaptionTemp.frame = captionFrameUpdate// self.lblCaption.frame
                            //self.lblCaptionTemp.transform = CGAffineTransformMakeScale(1.5, 1.5)
                            self.lblCaption.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                            //self.lblCaption.frame = CGRectMake(self.lblCaption.frame.origin.x, self.lblCaption.frame.origin.y + 30, self.lblCaption.frame.size.width, self.lblCaption.frame.size.height)
                            
                            self.view.bringSubviewToFront(self.imgTempCell)
                        },
                                               completion:
                        { finished in
                            
                            if(self.isDoubleTapped)
                            {
                                //self.btnBloxDoubleTapped = btnBlox
                                //self.buttonBloxFrameDoubleTapped = buttonBloxFrame
                                self.animatedImage = btnBlox
                                self.animatedImageRect = buttonBloxFrame
                                
                                //---- new code for single tap close image ----
                                self.view.removeGestureRecognizer(self.gestureDoubleTap)
                                self.gestureSingleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfSingleTap(_:)))
                                self.gestureSingleTap.numberOfTapsRequired = 1
                                self.view.addGestureRecognizer(self.gestureSingleTap)
                                //------
                            }
                            else
                            {
                                if bloxDetail.value(forKey: "soundURL") != nil {
                                    var soundURL : String = (bloxDetail.value(forKey: "soundURL") as? String)!
                                    soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                                    if !soundURL.contains("null") {
                                        let delayInSeconds = self.sliderDuration//300.0
                                        let delayInNanoSeconds =
                                            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                      execute: {
                                                                        self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                        self.avPlayer?.pause()
                                                                        self.videoPlayer.pause()
                                                                      })
                                    } else {
                                        let delayInSeconds = self.sliderDuration//300.0
                                        let delayInNanoSeconds =
                                            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                      execute: {
                                                                        self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                        self.avPlayer?.pause()
                                                                        self.videoPlayer.pause()
                                                                      })
                                    }
                                } else {
                                    let delayInSeconds = self.sliderDuration//300.0
                                    let delayInNanoSeconds =
                                        DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                    DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                  execute: {
                                                                    self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                    self.avPlayer?.pause()
                                                                    self.videoPlayer.pause()
                                                                  })
                                }
                            }
                            
                    })
                    
                }
                else if strGalleryType ==  "sound" {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: options, animations:
                        {
                            btnBlox.isHidden = true

                            //print(self.collectionView.center)
                            //print("center \(self.collectionView.frame.origin.x - self.collectionView.frame.size.width/2), \(self.collectionView.frame.origin.y - self.collectionView.frame.size.height/2)")
                            
                            self.imgTempCell.frame.size = self.videoImageView.frame.size
//                            self.imgTempCell.transform = CGAffineTransformMakeScale(2.58, 2.58)
                            self.imgTempCell.center =  CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            
                            
//                            var captionFrameUpdate : CGRect = self.imgTempCell.frame
//                            captionFrameUpdate.origin.x = self.cellCurrentSize.width/2 - lblCaptionTemp.frame.size.width/2
//                            captionFrameUpdate.origin.y = self.imgTempCell.frame.origin.y + self.cellCurrentSize.height + 50
//                            lblCaptionTemp.frame.origin = captionFrameUpdate.origin
                            
                            //self.btnPreviewBlox.transform = CGAffineTransformMakeScale(2.5, 2.5)
                            //self.btnPreviewBlox.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            
                            
                            self.lblCaptionTemp.frame = captionFrameUpdate// self.lblCaption.frame
                            //self.lblCaptionTemp.transform = CGAffineTransformMakeScale(1.5, 1.5)
                            self.lblCaption.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                            //self.lblCaption.frame = CGRectMake(self.lblCaption.frame.origin.x, self.lblCaption.frame.origin.y + 30, self.lblCaption.frame.size.width, self.lblCaption.frame.size.height)
                            
                            self.view.bringSubviewToFront(self.imgTempCell)
                        },
                                               completion:
                        { finished in
                            
                            if(self.isDoubleTapped)
                            {
                                //self.btnBloxDoubleTapped = btnBlox
                                //self.buttonBloxFrameDoubleTapped = buttonBloxFrame
                                self.animatedImage = btnBlox
                                self.animatedImageRect = buttonBloxFrame

                                //---- new code for single tap close image ----
                                self.view.removeGestureRecognizer(self.gestureDoubleTap)
                                self.gestureSingleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfSingleTap(_:)))
                                self.gestureSingleTap.numberOfTapsRequired = 1
                                self.view.addGestureRecognizer(self.gestureSingleTap)
                                //------
                            }
                            else
                            {
//                                var soundURL : String = (bloxDetail.value(forKey: "soundURL") as? String)!
//                                soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
//                                if soundURL.contains("null") {
//                                    self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
//                                }
                                
                                    let delayInSeconds = self.sliderDuration//300.0
                                    let delayInNanoSeconds =
                                        DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                    DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                  execute: {
                                                                    self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                    self.avPlayer?.pause()
                                                                    self.videoPlayer.pause()
                                                                  })
                            }
                    })
                        
                } else if strGalleryType ==  "Video"{
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: options, animations:
                        {
                            //print(self.collectionView.center)
                            //print("center \(self.collectionView.frame.origin.x - self.collectionView.frame.size.width/2), \(self.collectionView.frame.origin.y - self.collectionView.frame.size.height/2)")
//                            btnBlox.isHidden = true
                            /*
                            self.imgTempCell.frame.size = self.videoImageView.frame.size
//                            self.imgTempCell.transform = CGAffineTransformMakeScale(2.58, 2.58)
                            self.imgTempCell.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            
                            
                            
                            //self.btnPreviewBlox.transform = CGAffineTransformMakeScale(2.5, 2.5)
                            //self.btnPreviewBlox.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            
                            //for playing video on it
                           // self.viewForPlayerLayer.transform = CGAffineTransformMakeScale(2.5, 2.5)
                           // self.viewForPlayerLayer.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            self.lblCaption.frame = captionFrameUpdate
                            self.lblCaption.transform = CGAffineTransformMakeScale(1.5, 1.5)
                            //self.lblCaption.frame = CGRectMake(self.lblCaption.frame.origin.x, self.lblCaption.frame.origin.y + 30, self.lblCaption.frame.size.width, self.lblCaption.frame.size.height)
                            
                            self.view.bringSubviewToFront(self.imgTempCell)
                            
                            */
                            
                            //---
                            self.imgTempCell.frame.size = self.videoImageView.frame.size
                            //                            self.imgTempCell.transform = CGAffineTransformMakeScale(2.58, 2.58)
                            self.imgTempCell.center =  CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                            self.lblCaptionTemp.frame = captionFrameUpdate
                            self.lblCaption.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                            self.view.bringSubviewToFront(self.imgTempCell)
                            //---
                        },
                                   completion:
                                    { finished in
                                        self.animatedImage = btnBlox
                                        self.animatedImageRect = buttonBloxFrame
                                        self.viewForPlayerLayer.isHidden = false
                                        
                                        
                                        self.playVideo(videoPath)  // 14JAn
   /*                                     if bloxDetail.value(forKey: "soundURL") != nil {
                                            var soundURL : String = (bloxDetail.value(forKey: "soundURL") as? String)!
                                            soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                                            if !soundURL.contains("null") {
                                                let delayInSeconds = self.sliderDuration//300.0
                                                let delayInNanoSeconds =
                                                    DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                              execute: {
                                                                                self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
//                                                                                self.avPlayer?.pause()
//                                                                                self.videoPlayer.pause()
                                                                              })
                                            } else {
//                                                self.animatedImage = btnBlox
//                                                self.animatedImageRect = buttonBloxFrame
//                                                self.viewForPlayerLayer.isHidden = false
                                                
                                                //---- new code for single tap close image ----
                                                //self.view.removeGestureRecognizer(self.gestureDoubleTap)
                                                //self.gestureSingleTap = UITapGestureRecognizer(target: self, action: "selfSingleTap:")
                                                //self.gestureSingleTap.numberOfTapsRequired = 1
                                                //self.view.addGestureRecognizer(self.ges tureSingleTap)
                                                //------
                                                
                                                self.playVideo(videoPath)
                                                //                                                            self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                let delayInSeconds = self.sliderDuration//300.0
                                                let delayInNanoSeconds =
                                                    DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                              execute: {
//                                                                                self.animatedImage = btnBlox
//                                                                                self.animatedImageRect = buttonBloxFrame
//                                                                                self.viewForPlayerLayer.isHidden = false
                                                                                self.imgTempCell.image = btnBlox.image
                                                                                self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
//                                                                                self.avPlayer?.pause()
//                                                                                self.videoPlayer.pause()
                                                                              })
                                            }
                                        } else {
//                                            self.animatedImage = btnBlox
//                                            self.animatedImageRect = buttonBloxFrame
//                                            self.viewForPlayerLayer.isHidden = false
                                            
                                            //---- new code for single tap close image ----
                                            //self.view.removeGestureRecognizer(self.gestureDoubleTap)
                                            //self.gestureSingleTap = UITapGestureRecognizer(target: self, action: "selfSingleTap:")
                                            //self.gestureSingleTap.numberOfTapsRequired = 1
                                            //self.view.addGestureRecognizer(self.ges tureSingleTap)
                                            //------
                                            if let audio = bloxDetail.value(forKey: "audio") as? String {
                                                let playURL = AppTheme().getFilePathURL(audio)
                                                //                                                            self.playAudio(playURL.relativePath)
                                                self.playSound(strUrl: playURL.absoluteString)
                                            }
                                            
                                            self.playVideo(videoPath)
                                            //                                                        self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                            let delayInSeconds = self.sliderDuration//300.0
                                            let delayInNanoSeconds =
                                                DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                            DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                          execute: {
                                                                            self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
//                                                                            self.avPlayer?.pause()
//                                                                            self.videoPlayer.pause()
                                                                          })
                                        }*/
                                        
                                    })
                    //showVideoImage(videoPath, videoImage: imgTempCell.image!)
                    
                }
                else
                {
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: options, animations:
                                    {
                                        //print(self.collectionView.center)
                                        //print("center \(self.collectionView.frame.origin.x - self.collectionView.frame.size.width/2), \(self.collectionView.frame.origin.y - self.collectionView.frame.size.height/2)")
                                        btnBlox.isHidden = true
                                        /*
                                         self.imgTempCell.frame.size = self.videoImageView.frame.size
                                         //                            self.imgTempCell.transform = CGAffineTransformMakeScale(2.58, 2.58)
                                         self.imgTempCell.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                                         
                                         
                                         
                                         //self.btnPreviewBlox.transform = CGAffineTransformMakeScale(2.5, 2.5)
                                         //self.btnPreviewBlox.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                                         
                                         //for playing video on it
                                         // self.viewForPlayerLayer.transform = CGAffineTransformMakeScale(2.5, 2.5)
                                         // self.viewForPlayerLayer.center = CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                                         self.lblCaption.frame = captionFrameUpdate
                                         self.lblCaption.transform = CGAffineTransformMakeScale(1.5, 1.5)
                                         //self.lblCaption.frame = CGRectMake(self.lblCaption.frame.origin.x, self.lblCaption.frame.origin.y + 30, self.lblCaption.frame.size.width, self.lblCaption.frame.size.height)
                                         
                                         self.view.bringSubviewToFront(self.imgTempCell)
                                         
                                         */
                                        
                                        //---
                                        self.imgTempCell.frame.size = self.videoImageView.frame.size
                                        //                            self.imgTempCell.transform = CGAffineTransformMakeScale(2.58, 2.58)
                                        self.imgTempCell.center =  CGPoint(x: self.collectionView.frame.size.width/2, y: self.collectionView.frame.size.height/2)
                                        self.lblCaptionTemp.frame = captionFrameUpdate
                                        self.lblCaption.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                                        self.view.bringSubviewToFront(self.imgTempCell)
                                        //---
                                    },
                                   completion:
                                    { finished in
                                        if bloxDetail.value(forKey: "soundURL") != nil {
                                            self.animatedImageRect = buttonBloxFrame
                                            var soundURL : String = (bloxDetail.value(forKey: "soundURL") as? String)!
                                            soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                                            if !soundURL.contains("null") {
                                                let delayInSeconds = self.sliderDuration//300.0
                                                let delayInNanoSeconds =
                                                    DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                              execute: {
                                                                                self.animatedImage = btnBlox
                                                                                self.animatedImageRect = buttonBloxFrame
                                                                                self.viewForPlayerLayer.isHidden = false
                                                                                self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                                self.avPlayer?.pause()
                                                                                self.videoPlayer.pause()
                                                                              })
                                            } else {
                                                self.animatedImage = btnBlox
                                                self.animatedImageRect = buttonBloxFrame
                                                self.viewForPlayerLayer.isHidden = false
                                                
                                                //---- new code for single tap close image ----
                                                //self.view.removeGestureRecognizer(self.gestureDoubleTap)
                                                //self.gestureSingleTap = UITapGestureRecognizer(target: self, action: "selfSingleTap:")
                                                //self.gestureSingleTap.numberOfTapsRequired = 1
                                                //self.view.addGestureRecognizer(self.ges tureSingleTap)
                                                //------
                                                
                                                //                                                            self.playVideo(videoPath)
                                                //                                                            self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                
                                                let delayInSeconds = self.sliderDuration//300.0
                                                let delayInNanoSeconds =
                                                    DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                              execute: {
                                                                                self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                                self.avPlayer?.pause()
                                                                                self.videoPlayer.pause()
                                                                              })
                                            }
                                        } else {
                                            self.animatedImage = btnBlox
                                            self.animatedImageRect = buttonBloxFrame
                                            self.viewForPlayerLayer.isHidden = false
                                            
                                            //---- new code for single tap close image ----
                                            //self.view.removeGestureRecognizer(self.gestureDoubleTap)
                                            //self.gestureSingleTap = UITapGestureRecognizer(target: self, action: "selfSingleTap:")
                                            //self.gestureSingleTap.numberOfTapsRequired = 1
                                            //self.view.addGestureRecognizer(self.ges tureSingleTap)
                                            //------
                                            
                                            //                                                        self.playVideo(videoPath)
                                            //                                                        self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                            
                                            let delayInSeconds = self.sliderDuration//300.0
                                            let delayInNanoSeconds =
                                                DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                            DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                                                          execute: {
                                                                            self.stopCellImageAnimation(btnBlox,buttonBloxFrame: buttonBloxFrame)
                                                                            self.avPlayer?.pause()
                                                                            self.videoPlayer.pause()
                                                                          })
                                        }
                                        
                                    })
                    //showVideoImage(videoPath, videoImage: imgTempCell.image!)
                    
                }
                //video
                
            }
        }
        
    }
    
   
    
    func applyPlainShadow(_ view: UIImageView)
    {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 10, height: 10)
        layer.shadowOpacity = 0.7
        layer.shadowRadius = 5
    }
    
    func stopCellImageAnimation(_ btnBlox:UIImageView, buttonBloxFrame:CGRect)
    {
        
        let indexPath : IndexPath = aryIndexPath.object(at: btnBlox.tag - 1230000) as! IndexPath
        let cell : UICollectionViewCell = collectionView.cellForItem(at: indexPath)! as UICollectionViewCell
        var captionFrame : CGRect = cell.frame
        captionFrame.origin.x = cell.frame.origin.x + (cell.frame.size.width/2 - lblCaption.frame.size.width/2)
        captionFrame.origin.y = cell.frame.origin.y + cellCurrentSize.height
        
        var delay : Double = 1.0
        if(isDoubleTapped)
        {
            delay = 0.0
        }
        else
        {
            if(!havePlayedVideo)
            {
//                delay = 5.0//self.sliderDuration
            }
            else
            {
                delay = 0.0
                havePlayedVideo = false
            }
        }
        if(strGalleryType != "video" && strGalleryType != "Video")
        {
            UIView.animate(withDuration: 0.5, delay: delay, options: UIView.AnimationOptions(), animations:
                {
                    self.imgTempCell.transform = CGAffineTransform.identity
                    
                    self.imgTempCell.frame = buttonBloxFrame
//                        self.imgTempCell.frame.origin.x = 40
                    //self.btnPreviewBlox.transform = CGAffineTransformIdentity
                    //self.btnPreviewBlox.frame = buttonBloxFrame
                    btnBlox.image = self.imgTempCell.image
                     self.perform(#selector(PreviewBloxVC.removeShadow), with: nil, afterDelay: delay)
                    
                    //btnBlox.hidden = false
                    //imgTempCell.hidden = true
                    //imgTempCell.removeFromSuperview()
                    
                    //-----
                    //self.lblCaption.frame.origin = captionFrame.origin
                    
                    self.lblCaption.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                    //print("\n\n\n\n\n\t  \(captionFrame) \t \(cell.frame) \n\n\n\n")
                    //-----
                    
                    //self.lblCaption.transform = CGAffineTransformMakeScale(1.0, 1.0)
                    //self.lblCaption.frame = CGRectMake(self.lblCaption.frame.origin.x, self.lblCaption.frame.origin.y + 30, self.lblCaption.frame.size.width, self.lblCaption.frame.size.height)
                },
                                       completion: { (success) in
                                        //self.hideButton()
                                        self.perform(#selector(PreviewBloxVC.hideButton), with: nil, afterDelay: 0.0)
                                        btnBlox.isHidden = false
                                        btnBlox.backgroundColor = .clear
                                        btnBlox.contentMode = UIView.ContentMode.scaleToFill
                                        self.imgTempCell.isHidden = true
                                        self.lblCaption.isHidden = true
                                        
                                        let image = self.animatedImage.image
                                        let image2 = self.imgTempCell.image
                                        self.lblCaption.backgroundColor = UIColor.clear
                                        
                                        self.lblCaption.layer.borderColor = UIColor.clear.cgColor
                                        if(!self.isDoubleTapped)
                                        {   self.startCellImageAnimation()  }
                                        else
                                        {
                                            self.view.removeGestureRecognizer(self.gestureSingleTap)
                                            //---- adding double tap gesture ----
                                            self.gestureDoubleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfDoubleTap(_:)))
                                            self.gestureDoubleTap.numberOfTapsRequired = 2
                                            self.gestureDoubleTap.delaysTouchesBegan = true
                                            self.view.addGestureRecognizer(self.gestureDoubleTap)
                                            self.isDoubleTapped = false
                                            //self.btnPreview.enabled = true
                                            //------
                                        }
                                        super.view.bringSubviewToFront(self.collectionView)
                                        self.view.bringSubviewToFront(self.collectionView)
                                        self.collectionView.backgroundColor = .clear
                                        
            })
        }
        else
        {
            UIView.animate(withDuration: 0.5, delay: delay, options: UIView.AnimationOptions(), animations:
                {
                    self.imgTempCell.transform = CGAffineTransform.identity
                    self.imgTempCell.frame = buttonBloxFrame
                    
                    //self.btnPreviewBlox.transform = CGAffineTransformIdentity
                    //self.btnPreviewBlox.frame = buttonBloxFrame
                    btnBlox.image =  self.imgTempCell.image
                    self.imgTempCell.layer.shadowOpacity = 0.0
                    
                    self.viewForPlayerLayer.transform = CGAffineTransform.identity
                    self.viewForPlayerLayer.frame = buttonBloxFrame
                    //self.lblCaption.frame.origin = captionFrame.origin
                    self.lblCaption.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    //self.lblCaption.frame = CGRectMake(self.lblCaption.frame.origin.x, self.lblCaption.frame.origin.y + 30, self.lblCaption.frame.size.width, self.lblCaption.frame.size.height)
                    
                    
                },
                                       completion: { (success) in
                                        //self.hideButton()
                                        self.perform(#selector(PreviewBloxVC.hideButton), with: nil, afterDelay: 0.0)
                                        //self.hidePlayerLayer()
                                        self.videoPlayer.pause()
                                        self.newPreviewButtonOutlet.isEnabled = true
                                        
                                        let image = self.animatedImage.image
                                        let image2 = self.imgTempCell.image
                                        self.playerLayerView.isHidden = true
                                        self.videoImageView.isHidden = true
                                        self.videoImageView.image = UIImage()
                                        self.playerLayer.isHidden = true
                                        self.playerLayerView.isHidden = true
                                        self.imgTempCell.isHidden = true
                                        
                                        btnBlox.isHidden = false
                                        btnBlox.contentMode = UIView.ContentMode.scaleToFill
                                        //self.lblCaption.hidden = true
                                        self.lblCaption.backgroundColor = UIColor.clear
                                        self.lblCaption.layer.borderColor = UIColor.clear.cgColor
                                        if(!self.isDoubleTapped)
                                        {   self.startCellImageAnimation() }
                                        else
                                        {
                                            self.view.removeGestureRecognizer(self.gestureSingleTap)
                                            //---- adding double tap gesture ----
                                            self.gestureDoubleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfDoubleTap(_:)))
                                            self.gestureDoubleTap.numberOfTapsRequired = 2
                                            self.gestureDoubleTap.delaysTouchesBegan = true
                                            self.view.addGestureRecognizer(self.gestureDoubleTap)
                                            self.isDoubleTapped = false
                                            //------
                                        }
                                        super.view.bringSubviewToFront(self.collectionView)
                                        self.view.bringSubviewToFront(self.collectionView)
                                        
            })
        }
        
        let lastIindex = aryMessageData.count - 1
        let currentIndex = btnBlox.tag - 1230000
        if lastIindex == currentIndex {
            self.bgAudioPlayer.pause()
        }
    }
    
    @objc func removeShadow()
    {
        self.imgTempCell.layer.shadowOpacity = 0.0
    }
    
    //func animateThroughCenter(btnBlox: UIButton)
//    func animateThroughCenter(_ btnBlox: UIImageView)
//    {
//        //var delayValue : NSTimeInterval = 0.0;
//
//        //lblCaption.hidden = false
//        lblCaption.text = ""
//        //****
//        /*
//         btnPreviewBlox.setTitle(btnBlox.titleLabel?.text, forState: UIControlState.normal)
//
//         btnPreviewBlox.setImage(btnBlox.imageView?.image, forState: UIControlState.normal)
//         btnPreviewBlox.backgroundColor = btnBlox.backgroundColor
//         btnPreviewBlox.titleLabel?.textColor = btnBlox.titleLabel?.textColor
//         btnPreviewBlox.setTitleColor(btnBlox.titleLabel?.textColor, forState: UIControlState.normal)
//         btnPreviewBlox.titleLabel?.numberOfLines = (btnBlox.titleLabel?.numberOfLines)!
//         btnPreviewBlox.titleLabel?.font = btnBlox.titleLabel?.font
//         btnPreviewBlox.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
//         btnPreviewBlox.frame = btnBlox.frame
//         btnPreviewBlox.hidden = false
//         */
//        //***** new method for image *****
//
//        //btnPreviewBlox.setTitle(btnBlox.titleLabel?.text, forState: UIControlState.normal)
//
//        btnPreviewBlox.setImage(btnBlox.image, for: UIControl.State())
//        btnPreviewBlox.backgroundColor = btnBlox.backgroundColor
//        //btnPreviewBlox.titleLabel?.textColor = btnBlox.titleLabel?.textColor
//        //btnPreviewBlox.setTitleColor(btnBlox.titleLabel?.textColor, forState: UIControlState.normal)
//        //btnPreviewBlox.titleLabel?.numberOfLines = (btnBlox.titleLabel?.numberOfLines)!
//        //btnPreviewBlox.titleLabel?.font = btnBlox.titleLabel?.font
//        btnPreviewBlox.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
//        btnPreviewBlox.frame = btnBlox.frame
//        btnPreviewBlox.isHidden = false
//        self.view.bringSubviewToFront(btnPreviewBlox)
//
//        //-----
//        if let bloxDict : NSMutableDictionary = aryMessageData.object(at: btnBlox.tag - 1230000) as? NSMutableDictionary
//        {
//            if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
//            {
//                let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
//                switch bloxType
//                {
//                case "TextMessage":
//                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
//                    {
//                        lblCaption.text = strCaption//" " + strCaption
//                        lblCaption.isHidden = false
//                        lblCaption.sizeToFit()
//                    }
//                    break
//                case "Camera":
//                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
//                    {
//                        lblCaption.text = strCaption//" " + strCaption
//                        lblCaption.isHidden = false
//                        lblCaption.sizeToFit()
//                    }
//                    break
//                case  "GalleryImage":
//                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
//                    {
//                        if(strCaption != "")
//                        {
//                            lblCaption.text = strCaption// " " + strCaption
//                            lblCaption.isHidden = false
//                            lblCaption.sizeToFit()
//                        }
//                    }
//                    break
//                case "Video":
//                    if let strCaption : String = bloxDetail.value(forKey: "caption") as? String
//                    {
//                        lblCaption.text = strCaption// " " + strCaption
//                        lblCaption.isHidden = false
//                        lblCaption.sizeToFit()
//                    }
//                    break
//                default:
//                    break
//                }
//            }
//        }
//        //-----
//
//        //****
//
//        let buttonBloxFrame : CGRect = btnBlox.frame
//        let options = UIView.AnimationOptions()
//        UIView.animate(withDuration: 1.5, delay: 0.0, options: options, animations:
//            {
//
//                btnBlox.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
//                //print(self.imageViewBlox.center)
//                btnBlox.center = self.imageViewBlox.center
//                self.btnPreviewBlox.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
//                self.btnPreviewBlox.center = self.imageViewBlox.center
//
//                self.lblCaption.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
//
//                //self.view.bringSubviewToFront(btnBlox)
//            },
//                                   completion:
//            { finished in
//                UIView.animate(withDuration: 1.5, animations: {
//                    btnBlox.transform = CGAffineTransform.identity
//                    btnBlox.frame = buttonBloxFrame
//
//                    self.btnPreviewBlox.transform = CGAffineTransform.identity
//                    self.btnPreviewBlox.frame = buttonBloxFrame
//
//                    self.lblCaption.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//
//                    self.perform(#selector(PreviewBloxVC.hideButton), with: nil, afterDelay: 1.6)
//                })
//
//                //
//
//
//                //
//                //
//        })
//
//
//    }
    
//    func doAnimation(_ sender: UIButton)
//    {
//        //btnPreviewBlox.alpha = 1.0
//        btnPreviewBlox.isHidden = false
//        //sender.alpha = 0.0
//
//
//        UIView.animate(withDuration: 0.6 ,
//                                   animations:
//            {
//                sender.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
//                self.btnPreviewBlox.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
//            },
//                                   completion:
//            {
//                finish in
//                UIView.animate(withDuration: 0.6, animations: {
//                    sender.transform = CGAffineTransform.identity
//                    //sender.alpha = 1.0
//                    self.btnPreviewBlox.transform = CGAffineTransform.identity
//                })
//
//                self.perform(#selector(PreviewBloxVC.hideButton), with: nil, afterDelay: 0.6)
//        })
//    }
    
    @objc func hideButton()
    {
        self.btnPreviewBlox.isHidden = true
        //lblCaption.hidden = true
        btnPreview.isEnabled = true
    }
    
    @IBAction func btnPreviewAction(_ sender:UIButton)
    {
        if(self.bgAudioExists)
        {
            self.playAudio(self.strBgAudioPath)
        }
        self.isDoubleTapped = false
        indexToBeAnimated = 0
        startCellImageAnimation()
        btnPreview.isEnabled = false
    }
    
    //MARK: ANIMATION CODE STARTS FROM HERE ON
    //******* NEW ANIMATION CODE ********
    
    //MARK: Image/Video Animation
//    func animateImage(_ imageVw : UIImageView)
//    {
//        LoadingIndicatorView.hide()
////        self.loading.dismiss()
//        self.playerLayerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        self.videoImageView.image = UIImage()
//        let img = imageVw.image
//        self.videoImageView.image = imageVw.image
//
//        newPreviewButtonOutlet.isEnabled = false
//
////        self.playerLayerView.isHidden = false
//
//        //******* New Animation Code ********
//        let duration = 0.8
//        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
//
//            self.playerLayerView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
//
//        }) { (finished) -> Void in
//            //sliderDuration
//            self.videoImageView.isHidden = false
//            self.perform(#selector(PreviewBloxVC.hidePlayerLayer), with: nil, afterDelay: self.sliderDuration)
//            //self.hidePlayerLayer()
//        }
//
//        /*
//         UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations:
//         { () -> Void in
//
//         self.playerLayerView.transform = CGAffineTransformMakeScale(0.1, 0.1)
//
//         }) { (finished) -> Void in
//
//         }
//         */
//
//    }
    
    //**** new code ****
//    func showVideoImage(_ strVideoURL : String, videoImage : UIImage)
//    {
//        LoadingIndicatorView.hide()
////        self.loading.dismiss()
//        self.playerLayerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        self.videoImageView.image = UIImage()
//        self.videoImageView.contentMode = UIView.ContentMode.scaleAspectFill
//        let img = videoImage
//        self.videoImageView.image = videoImage
//        self.videoImageView.isHidden = false
//
//
//        //print(videoImageView.frame)
//        //print(videoLayer.frame)
//
//         newPreviewButtonOutlet.isEnabled = false
////        self.playerLayerView.isHidden = false
//
//        //******* New Animation Code ********
//        let duration = 0.8
//        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
//
//            self.playerLayerView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
//
//        }) { (finished) -> Void in
//
//            //self.loading.showInView(self.view)
//            self.playVideo(strVideoURL)
//
//        }
//        //*********************
//    }
    //******************
    
    //MARK: audioPlayer method
    func playSound(strUrl:String){
        //  let player = AVPlayer()
        let url = URL.init(string: strUrl)
        avPlayer = AVPlayer.init(url: url!)
//        avPlayer?.play()
        avPlayer?.automaticallyWaitsToMinimizeStalling = false
        avPlayer?.playImmediately(atRate: 1.0)
//        let delayInSeconds = 30.0
//        let delayInNanoSeconds =
//        DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
//        execute: {
//            self.avPlayer?.pause()
//        //self.audioRecorder!.stop()
//        })
//        NotificationCenter.default.addObserver(self, selector: #selector(PreviewBloxVC.audioPlayerDidFinishPlaying(_:)),
//                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer?.currentItem)
    }
    func playAudio(_ strVideoURL : String)//, videoImage : UIImage)
    {
        if let url = URL.init(string: strVideoURL) {
            let playerItem = AVPlayerItem(url: url)
            self.videoPlayer = AVPlayer(playerItem: playerItem)
            if((videoPlayer.currentItem) != nil)
            {
                self.videoPlayer.replaceCurrentItem(with: playerItem)
            }
            else
            {
                self.videoPlayer = AVPlayer(playerItem: playerItem)
            }
//            self.videoPlayer.play()
            self.videoPlayer.automaticallyWaitsToMinimizeStalling = false
            self.videoPlayer.playImmediately(atRate: 1.0)
            
            if(bgAudioExists) {
                self.bgAudioPlayer.volume = 0.2
                debugPrint("Bg volume reduced to 0.2")
            }
            videoPlayer.volume = 1.0
//            NotificationCenter.default.addObserver(self, selector: #selector(PreviewBloxVC.audioPlayerDidFinishPlaying(_:)),
//                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer?.currentItem)
            //-- for animation ---
            /*     self.animationTimer = Timer.scheduledTimer(timeInterval: 0.3,
             target:self,
             selector:#selector(CreateBloxVC.animateRecordImage),
             userInfo:nil,
             repeats:true)
             let delayInSeconds = 45.0
             let delayInNanoSeconds =
             DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)*/
            /* DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
             execute: {
             self.stop(UIButton())
             //self.audioRecorder!.stop()
             })
             
             NotificationCenter.default.addObserver(self, selector: #selector(CreateBloxVC.playerDidFinishPlaying(_:)),
             name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)*/
            
            
//            let delayInSeconds = 30.0
//            let delayInNanoSeconds =
//            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//            DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
//            execute: {
//                self.videoPlayer.pause()
//            //self.audioRecorder!.stop()
//            })
        }
        
      
        
        
    }
    
    func playBgAudio(_ strPath : String)
    {
        
        if let url = URL.init(string: strPath) {
            let playerItem = AVPlayerItem(url: url)
            self.bgAudioPlayer = AVPlayer(playerItem: playerItem)
            if((bgAudioPlayer.currentItem) != nil)
            {
                self.bgAudioPlayer.replaceCurrentItem(with: playerItem)
            }
            else
            {
                self.bgAudioPlayer = AVPlayer(playerItem: playerItem)
            }
//            self.bgAudioPlayer.play()
            self.bgAudioPlayer.automaticallyWaitsToMinimizeStalling = false
            self.bgAudioPlayer.playImmediately(atRate: 1.0)
        }


    }
    
    @objc func audioPlayerDidFinishPlaying(_ note: Notification)
    {
        /*
        self.videoPlayer.pause()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: bgAudioPlayer.currentItem)
         */
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer?.currentItem)
        stopCellImageAnimation(selectedBtnBlox, buttonBloxFrame: animatedImageRect)
    }
    
    //MARK: video animation
    func playVideo(_ strVideoURL : String)//, videoImage :  UIImage)
    {
        if(bgAudioExists)
        {
            self.bgAudioPlayer.volume = 0.2
            debugPrint("Bg volume reduced to 0.2")
        }
        videoPlayer.volume = 1.0
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let videoFileURL = documentsDirectory.appendingPathComponent(strVideoURL)
        
        let dataPath = AppTheme().getDocumentFilePath(strVideoURL)
        
        let checkValidation = FileManager.default
        if checkValidation.fileExists(atPath: dataPath){
            print("video found")
        }
        else
        {
            print("video not found")
        }
        
        
        let playerItem = AVPlayerItem(url: videoFileURL)
        
        
        if((videoPlayer.currentItem) != nil)
        {
            self.videoPlayer.replaceCurrentItem(with: playerItem)
        }
        else
        {
            self.videoPlayer = AVPlayer(playerItem: playerItem)
        }
        
        //----
        var frameVideoLayer : CGRect =  videoImageView.frame
        frameVideoLayer.origin.x = 0
        frameVideoLayer.origin.y =  0
        
        let layer : AVPlayerLayer = AVPlayerLayer(player: self.videoPlayer)
        layer.frame = frameVideoLayer
        
        //**** new code to rezise the video ****
        layer.videoGravity = AVLayerVideoGravity.resize;
        layer.needsDisplayOnBoundsChange = true;
        //**********
        
        var nFrames = 0
        //---- New code to check frames in video for test
        let asset1 = AVURLAsset(url: videoFileURL, options: nil)
        do {
        let reader = try AVAssetReader(asset: asset1)//  AVAssetReader(asset: asset1, error: nil)
        
            let videoTrack = asset1.tracks(withMediaType: AVMediaType.video)[0]
        
        let readerOutput = AVAssetReaderTrackOutput(track: videoTrack, outputSettings: nil) // NB: nil, should give you raw frames
        reader.add(readerOutput)
        reader.startReading()
        
        while true {
            let sampleBuffer = readerOutput.copyNextSampleBuffer()
            if sampleBuffer == nil {
                break
            }
            nFrames += 1
        }
        
        print("\nNum frames: \(nFrames)")
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            
        }
        //--------
        
        
        //--- Video image of last frame ---
        print("\n in video animation \n ")
        //let url:NSURL = NSURL(fileURLWithPath: dataPath)
        let asset:AVAsset = AVAsset(url: videoFileURL)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceBefore = CMTime.zero
        assetImgGenerate.requestedTimeToleranceAfter = CMTime.zero
        var time: CMTime = asset.duration
        if(nFrames != 0) {
            time = CMTimeMakeWithSeconds(Float64(nFrames), preferredTimescale: 1)
        } else {
            time = CMTimeMakeWithSeconds(Float64(time.value), preferredTimescale: 1)
        }
        
        let lastFrameTime = Int64(CMTimeGetSeconds(asset.duration) * 1000.0)
        let timeMilli : CMTime = CMTimeMake(value: lastFrameTime, timescale: 2)
        
        //let thumbTime : CMTime = CMTimeMakeWithSeconds(CMTimeGetSeconds(asset.duration),1);
        //let hardTime : CMTime = CMTimeMake(247, 1)
        //time.value = min(time.value, 2)
        //let composition = AVVideoComposition(propertiesOfAsset: asset)
        //let time = CMTimeMakeWithSeconds(asset.duration.seconds, composition.frameDuration.timescale)
        
        print("\n in video time is:- \(time) & \n asset duration is :- \(asset.duration)")//  &\n last frame is \(lastFrameTime) \n ")

        if(time.seconds > 0)
        {
            print("video time is greater then 0 \(asset.duration)")
            var frameImg : UIImage = UIImage()
            do {
                let imageRef =  try assetImgGenerate.copyCGImage(at: asset.duration, actualTime: nil)
                frameImg = UIImage(cgImage: imageRef)
                print("\n generated video animation image \n ")
                //frameImg = frameImg.imageRotatedByDegrees(90, flip: false)
            }
            catch let error as NSError
            {
                print("Image generation failed with error \(error)")
                let assetImgGenerate2 : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                assetImgGenerate2.appliesPreferredTrackTransform = true
                do {
                    let imageRef =  try assetImgGenerate2.copyCGImage(at: timeMilli, actualTime: nil)
                    frameImg = UIImage(cgImage: imageRef)
                    print("\n generated video animation image \n ")
                    //frameImg = frameImg.imageRotatedByDegrees(90, flip: false)
                }
                catch let error as NSError
                {
                    print("Image generation failed with error \(error)")
                }
            }
            
            if(frameImg.size.height > 0)
            {
                animatedImage.image = frameImg
                print("\n generated image of video animation has been added \n ")
            }
        
            //----------
            
            
            
            //self.viewForPlayerLayer.layer.addSublayer(layer)
            
            //-----
            playerLayer = AVPlayerLayer()
            
            //frameVideoLayer.origin.x += 2
            //frameVideoLayer.origin.y += 5
            
            //self.videoImageView.frame = frameVideoLayer
            
            
            self.playerLayer = AVPlayerLayer(player: self.videoPlayer)
         
            self.playerLayer.frame =    frameVideoLayer//self.videoImageView.frame //  self.videoLayer.bounds
            
            
            
            //print("player layer frame \(playerLayer.frame)")
            
            //**** new code to rezise the video ****
            playerLayer.videoGravity = AVLayerVideoGravity.resize;
            playerLayer.needsDisplayOnBoundsChange = true;
            //**********
            
//            self.videoPlayer.play()
            self.videoPlayer.playImmediately(atRate: 1.0)
            videoImageView.isHidden = false
          videoImageView.layer.addSublayer(self.playerLayer)
            //print("\n\nplayerLayer video size\(playerLayer.videoRect)")
            //self.videoLayer.layer.addSublayer(self.playerLayer)
            
            self.playerLayer.isHidden = false
             newPreviewButtonOutlet.isEnabled = false
//            self.playerLayerView.isHidden = false
            
            //viewForPlayerLayer.layer.addSublayer(self.playerLayer)
            
            
            //self.videoPlayer.addObserver(self, forKeyPath: "currentItem.status", options: [NSKeyValueObservingOptions.Initial, NSKeyValueObservingOptions.New] , context:self.PlayerStatusObservingContext)
            
            //NSNotificationCenter.defaultCenter().addObserver(self, selector:"hidePlayerLayer",name: AVPlayerItemDidPlayToEndTimeNotification, object: playerItem)
            
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(PreviewBloxVC.playerDidFinishPlaying(_:)),
                                                             name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)
            
        }
        else
        {
            print("video time is not available or video either")
            
            print("Video Ended")
            self.videoPlayer.pause()
            self.videoImageView.isHidden = true
            self.videoImageView.image = UIImage()
            self.playerLayer.isHidden = true
            newPreviewButtonOutlet.isEnabled = true
            self.playerLayerView.isHidden = true
            let image = animatedImage.image
            self.imgTempCell.image = animatedImage.image
            
            havePlayedVideo = true
            if(!isDoubleTapped)
            {
                stopCellImageAnimation(animatedImage, buttonBloxFrame: animatedImageRect)
            }
            else
            {
                //---- new code for single tap close image ----
                print("single tap close image")
                self.view.removeGestureRecognizer(self.gestureDoubleTap)
                self.gestureSingleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfSingleTap(_:)))
                self.gestureSingleTap.numberOfTapsRequired = 1
                self.view.addGestureRecognizer(self.gestureSingleTap)
                //------
            }
        }
        
       
        
        
    }
    
    @objc func playerDidFinishPlaying(_ note: Notification)
    {
        print("Video Finished")
        
        self.videoImageView.isHidden = true
        self.videoImageView.image = UIImage()
        self.playerLayer.isHidden = true
         newPreviewButtonOutlet.isEnabled = true
        self.playerLayerView.isHidden = true
        let image = animatedImage.image
        self.imgTempCell.image = animatedImage.image
        havePlayedVideo = true
        if(!isDoubleTapped)
        {
        stopCellImageAnimation(animatedImage, buttonBloxFrame: animatedImageRect)
        }
        else
        {
            //---- new code for single tap close image ----
            print("single tap close image")
            self.view.removeGestureRecognizer(self.gestureDoubleTap)
            self.gestureSingleTap = UITapGestureRecognizer(target: self, action: #selector(PreviewBloxVC.selfSingleTap(_:)))
            self.gestureSingleTap.numberOfTapsRequired = 1
            self.view.addGestureRecognizer(self.gestureSingleTap)
            self.avPlayer?.pause()
            //------
            stopCellImageAnimation(animatedImage, buttonBloxFrame: animatedImageRect)
        }
        self.avPlayer?.pause()
        self.videoPlayer.pause()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                                         of object: Any?,
                                                  change: [NSKeyValueChangeKey : Any]?,
                                                  context: UnsafeMutableRawPointer?)
    {
        print("change in value\(change)")
        
        if let playerAV : AVPlayer = object as? AVPlayer
        {
            print(playerAV)
            let playerStatus = videoPlayer.currentItem!.status.rawValue as Int
            
            if playerStatus == AVPlayer.Status.failed.rawValue
            {
                print("falied to play")
            }
            else if playerStatus == AVPlayer.Status.readyToPlay.rawValue
            {
                print("Ready to Play")
                
                LoadingIndicatorView.hide()
//                self.loading.dismiss()
                self.playerLayer.isHidden = false
                 newPreviewButtonOutlet.isEnabled = false
//                self.playerLayerView.isHidden = false
                //self.videoImageView.hidden = true
                // update some UI
                
                
                
            }
            else if playerStatus == AVPlayer.Status.unknown.rawValue
            {
                print("\(videoPlayer.currentItem!.error)")
            }
        }
        
        
        
    }
    
    func availableDuration() -> CMTime
    {
        let range = self.videoPlayer.currentItem?.loadedTimeRanges.first
        if (range != nil){
            return CMTimeRangeGetEnd(range!.timeRangeValue)
        }
        return CMTime.zero
    }
    
    @objc func hidePlayerLayer()
    {
        //         UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
        //******* New Animation Code ********
        let duration = 0.5
        self.videoPlayer.pause()
        
        UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            () -> Void in
            self.playerLayerView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            })
        {
            (finished) -> Void in
            
            UIView.animate(withDuration: duration, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                () -> Void in
                self.playerLayerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }) { (finished) -> Void in
                self.newPreviewButtonOutlet.isEnabled = true
                self.playerLayerView.isHidden = true
                if(self.strGalleryType == "Video" || self.strGalleryType == "Sound Track" || self.strGalleryType == "sound" || self.strGalleryType == "Sound")
                {
                    self.videoImageView.isHidden = true
                    self.videoImageView.image = UIImage()
                    self.playerLayer.isHidden = true
                    self.newPreviewButtonOutlet.isEnabled = true
                    self.playerLayerView.isHidden = true
                    
                    //NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object: self.videoPlayer.currentItem)

                    //self.videoPlayer.removeObserver(self, forKeyPath: "currentItem.status", context: self.PlayerStatusObservingContext)
                }
            }
            
        }
        
        
    }

    //***********************************
    
    //MARK: Forward message button
    @IBAction func btnForwardAction(_ sender : UIButton)
    {
        isFromSendMessage = false
        isGoneOutToChild = true
        APP_DELEGATE.isReplyMessage = false

//        if(dictUserMessageData.count > 0)
//        {
//            if let strMsgId : NSInteger = dictUserMessageData.value(forKey: "parentId") as? NSInteger
//            {
//                strMessageId = "\(strMsgId)"
//            }
//            else if let strMsgId : NSInteger = dictUserMessageData.value(forKey: "id") as? NSInteger
//            {
//                strMessageId = "\(strMsgId)"
//            }
//
//        }
//
//        if(strMessageId != "")
//        {
//            self.performSegue(withIdentifier: "segueShowContactList", sender: self)
//        }
//          self.showContactPicker()
        self.fetchContacts()
        self.contactsPickerView.isHidden = false
        self.arrContacts.removeAll()
        self.arrGroupIds.removeAll()
    }
    
    
    //MARK: Reply message button
    @IBAction func btnReplyAction(_ sender: UIButton)
    {
        
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
        vc.inbloxData = self.inbloxData
        APP_DELEGATE.isReplyMessage = true
        self.navigationController?.pushViewController(vc, animated: true)
        
        isGoneOutToChild = true
        //let appDelegate = AppDelegate().appDelegate()
        
        let alertController = UIAlertController(title: "Message Reply", message: "", preferredStyle: .alert)
        
        let ReplyAction = UIAlertAction(title: "Reply", style: .default)
        {
            (action) -> Void in
            
            if(self.dictUserMessageData.count > 0)
            {

                //var messageId : NSInteger = 0
                //var contactId : NSInteger = 0
                if self.aryTalkBloxContacts.count > 0 {
                    self.strMessageId = ""
                self.performSegue(withIdentifier: "segueShowContactList", sender: self)
                } else {
                    let errorAlert = AppTheme().showAlert("No Contacts", errorTitle: "Contact Alert")
                    self.present(errorAlert, animated: true ,completion: nil)
                }

                self.dictReplyDetails.setObject(self.convId, forKey: "ConversationID" as NSCopying)
                
                if let msgParentId : NSInteger = self.dictUserMessageData.value(forKey: "parentId") as? NSInteger
                {
                    self.dictReplyDetails.setObject(msgParentId, forKey: "parentId" as NSCopying)
                }
                else if let msgId : NSInteger = self.dictUserMessageData.value(forKey: "id") as? NSInteger
                {
                   self.dictReplyDetails.setObject(msgId, forKey: "parentId" as NSCopying)
                }
                
                if let contId : NSInteger = self.dictUserMessageData.value(forKey: "contact_id") as? NSInteger
                {
                    self.dictReplyDetails.setObject(contId, forKey: "contactId" as NSCopying)
                }
                
              /*  if let contactId : String = self.aryTalkBloxContacts.value(forKey: "id") as? String
                {
                    self.contactIds.updateValue(contactId as AnyObject, forKey: "id")
                }
                */
                if let strMessageUsername : String = self.dictUserMessageData.value(forKey: "messageUserName") as? String
                {
                    self.dictReplyDetails.setObject(strMessageUsername, forKey: "messageUserName" as NSCopying)
                }
                
                self.dictReplyDetails.setObject("Reply", forKey: "type" as NSCopying)
                APP_DELEGATE.dictReplyMessageDetails = self.dictReplyDetails.mutableCopy() as! NSMutableDictionary
                APP_DELEGATE.isReplyMessage = true
                
                //self.performSegue(withIdentifier: "segueHomeReply", sender: self)
            }
            
           
        }
        
        let ReplyAllAction = UIAlertAction(title: "Reply All", style: .default) {
            (action) -> Void in
            
            
            
         //   APP_DELEGATE.dictReplyMessageDetails = self.dictReplyDetails.mutableCopy() as! NSMutableDictionary
            APP_DELEGATE.isReplyMessage = true
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
            vc.inbloxData = self.inbloxData
            self.navigationController?.pushViewController(vc, animated: true)
//            if(self.dictUserMessageData.count > 0)
//            {
//                if self.aryTalkBloxContacts.count > 0 {
//                    self.strMessageId = ""
//                    self.performSegue(withIdentifier: "segueHomeReply", sender: self)
//                } else {
//                    let errorAlert = AppTheme().showAlert("No Contacts", errorTitle: "Contact Alert")
//                    self.present(errorAlert, animated: true ,completion: nil)
//                }
//                self.dictReplyDetails.setObject(self.convId, forKey: "ConversationID" as NSCopying)
//
//                if let msgId : NSInteger = self.dictUserMessageData.value(forKey: "id") as? NSInteger
//                {
//                    self.dictReplyDetails.setObject(msgId, forKey: "messageId" as NSCopying)
//                }
//
//                if let aryReceiverList : NSArray = self.dictUserMessageData.value(forKey: "message_receiver_list") as? NSArray
//                {
//                    self.dictReplyDetails.setObject(aryReceiverList, forKey: "contactIds" as NSCopying)
//                }
//
//                if let contId : NSInteger = self.dictUserMessageData.value(forKey: "contact_id") as? NSInteger
//                {
//                    self.dictReplyDetails.setObject(contId, forKey: "contactId" as NSCopying)
//                }
//                if let msgParentId : NSInteger = self.dictUserMessageData.value(forKey: "parentId") as? NSInteger
//                {
//                    self.dictReplyDetails.setObject(msgParentId, forKey: "parentId" as NSCopying)
//                }
//
//                self.dictReplyDetails.setObject("Reply All", forKey: "type" as NSCopying)
//
//                APP_DELEGATE.dictReplyMessageDetails = self.dictReplyDetails.mutableCopy() as! NSMutableDictionary
//                APP_DELEGATE.isReplyMessage = true
//
//                //self.performSegue(withIdentifier: "segueHomeReply", sender: self)
//            }
            
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addAction(ReplyAction)
        alertController.addAction(ReplyAllAction)
        alertController.addAction(cancelAction)
        
        alertController.view.backgroundColor = UIColor.white
        AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
        
      //  present(alertController, animated: true, completion: nil)
    }
    
    //MARK: Text Message Reply button
    @IBAction func btnTextMessageAction(_ sender: UIButton)
    {
        vwMessage.isHidden = false
        txtViewMessage!.text = "Write message here..."
        txtViewMessage!.textColor = UIColor(red: 0.0 / 255.0, green: 161.0 / 255.0, blue: 228.0 / 255.0, alpha: 1)
        txtViewMessage.becomeFirstResponder()

        //showAlert("Functionality is in progress", strTitle: "Text Message")
    }
    
    @IBAction func btnCancelTextMessageAction(_ sender: UIButton)
    {
        vwMessage.isHidden = true
        txtViewMessage.resignFirstResponder()
        
        
    }
    
    @IBAction func btnSendTextMessageAction(_ sender: UIButton)
    {
        txtViewMessage.resignFirstResponder()
        
        if(txtViewMessage.text.count > 0 && txtViewMessage.text != "Write message here...")
        {
            //showAlert("Functionality is in progress", strTitle: "Text Message")
            
            
//            loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//            loading.textLabel.text = "Sending Message"
//            loading.show(in: self.view)
            LoadingIndicatorView.show("Loading...")
            
            if(self.dictUserMessageData.count > 0)
            {
                
                //--- Composing message ---
                allDataArray = [String : AnyObject]()
                
                var frameNo : NSInteger = 0
                
                var dataArray : [String : AnyObject] = [String : AnyObject]()
                
                dataArray.updateValue("image" as AnyObject,forKey: "medias[][media_type]")
                dataArray.updateValue(frameNo as AnyObject,forKey: "medias[][frame_number]")
                
                print("\n\n\n in TextMessage")
                
                dataArray.updateValue(UIImage(named: "chatText")!.pngData()! as AnyObject, forKey:"medias[][media]IMAGE")
                
                //--- New param ---
                let userDefaultss = UserDefaults.standard
                var imageName : String = "message_background1"
                if let key = userDefaultss.object(forKey: "bubbleImage")  as? String
                {
                    if(key != "")
                    {
                        imageName = key
                        //imageViewBlox.image = UIImage(named: key)
                    }
                }
                dataArray.updateValue(UIImage(named: imageName)!.pngData()! as AnyObject, forKey:"medias[][media]BACK")
                dataArray.updateValue(imageName as AnyObject, forKey: "bubble_bg[image_name]")
                //--- New param ---
                
                dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                
                dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                
                //--- Temp Code ---
//                let dictDataText : NSMutableDictionary = NSMutableDictionary()
//                dictDataText.setValue("Hi there this is caption", forKey: "caption")
//                dictDataText.setValue("Hi there this is Text Message", forKey: "textMessage")
//                dictDataText.setValue("Hi there this is Bubble Image", forKey: "bubbleImage")
//                dataArray.updateValue(dictDataText, forKey: "message[bg_media]")
                //-------
                
                dataArray.updateValue(txtViewMessage.text as AnyObject, forKey: "text_message")
                
                //dataArray.updateValue("", forKey: "bubble_bg[image_data]")
                
                
                //---- Code for adding BG image file ----
                let bgAudio :[String : AnyObject] = [String : AnyObject]()
                //bgAudio.updateValue(UIImagePNGRepresentation(UIImage(named: imageName)!)!, forKey: "medias[][media]BACK")
                //bgAudio.updateValue(imageName, forKey: "bubble_bg[image_name]")
                //----
                
                
                //bubble_bg[image_name] = 'hello'
                //bubble_bg[image_data] = file
                //text_message = "any message"
                
                print("data array contains",dataArray)
                
                if(dataArray.count>0)
                {
                    allDataArray.updateValue(dataArray as AnyObject, forKey: String(format:"AllData%d",frameNo))
                }
                frameNo += 1
                
                //--- Composing message ---
                
                
                let dictReply : NSDictionary = self.dictUserMessageData as NSDictionary
                var strURL : String = ""
                strURL = String(format:"%@messages.json", AppTheme().AppUrl)//messages/reply.json
                //let bgAudio :[String : AnyObject] = [String : AnyObject]()
                
                
                
                var strContactId : NSInteger = 0
                var strMessageId : NSInteger = 0
                if let strContId : NSInteger = dictReply.value(forKey: "contactId") as? NSInteger
                {
                    strContactId = strContId
                }
                
                if let strMsgId: NSInteger = dictReply.value(forKey: "parentId") as? NSInteger
                {
                    strMessageId = strMsgId
                }
                else if let strMsgId: NSInteger = dictReply.value(forKey: "messageId") as? NSInteger
                {
                    strMessageId = strMsgId
                }
                else if let strMsgId : NSInteger = dictReply.value(forKey: "id") as? NSInteger
                {
                    strMessageId = strMsgId
                }
                
                
                let userDefaults = UserDefaults.standard
                
                var parametr : [String : AnyObject] =
                    ["authentication_token": userDefaults.value(forKey: "authentication_token") as AnyObject, "message[parent_id]": strMessageId as AnyObject]
                parametr.updateValue("reply" as AnyObject, forKey: "message_type")
                
                var arrySendContacts : NSArray = NSArray()
                
                if let aryReceiverList : NSArray = dictReply.value(forKey: "message_receiver_list") as? NSArray
                {
                    arrySendContacts = aryReceiverList
                    //self.dictReplyDetails.setObject(aryReceiverList, forKey: "contactIds")
                }
                
                
                //let arySelectedContacts : NSArray =  dictReply.valueForKey("contactIds") as! NSArray
                contactIds  = [String : AnyObject]()
                for i in 0 ..< arrySendContacts.count
                {
                    if let dictContactDetails : NSMutableDictionary = arrySendContacts.object(at: i) as? NSMutableDictionary
                    {
                        var dataArray : [String : AnyObject] = ["contact_ids[]" : (dictContactDetails.value(forKey: "id") as? NSInteger)! as AnyObject]
                        dataArray.updateValue(dictContactDetails.value(forKey: "name")! as AnyObject, forKey: "name")
                        dataArray.updateValue(dictContactDetails.value(forKey: "contact_check")! as AnyObject, forKey: "contact_check")
                        contactIds.updateValue(dataArray as AnyObject, forKey:"\(i)")
                    }
                }
                
                
                var contactIdsParam:Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
                for i in 0 ..< arrySendContacts.count
                {
                    if let dictContactDetails : NSMutableDictionary = arrySendContacts.object(at: i) as? NSMutableDictionary
                    {
                        let dataArray : [String : AnyObject] = ["contact_ids[]" : (dictContactDetails.value(forKey: "id") as? NSInteger)! as AnyObject]
                        contactIdsParam.updateValue(dataArray as AnyObject, forKey:"\(i)")
                    }
                }
                
                print(contactIdsParam)
                
                
                AppTheme().uploadMessageData(strURL, parameters: parametr,uploadData: allDataArray, contactIds:contactIdsParam,bgAudio:bgAudio) { (result, data) -> Void in
                    if(result == "success")
                    {
//                        self.loading.indicatorView = JGProgressHUDSuccessIndicatorView()
//                        self.loading.show(in: self.view )
//                        self.loading.textLabel.text = "Message Sent Successfully"
                        
                        LoadingIndicatorView.show("Message Sent Successfully")

                        //--- Cleaning up all global variables
                        self.allDataArray = [String : AnyObject]()
                        self.contactIds = [String : AnyObject]()
                        //--- Cleaning up all global variables
                        
//                        self.loading.dismiss(afterDelay: 2.0)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            LoadingIndicatorView.hide()
                        }
                        
                        self.perform(#selector(PreviewBloxVC.popViewTextMessageSent), with: nil, afterDelay: 2.1)
                        print(data)
                    }
                    else
                    {
//                        self.loading.indicatorView = JGProgressHUDErrorIndicatorView()
//                        self.loading.show(in: self.view )
//                        self.loading.textLabel.text = data as? String
//                        self.loading.dismiss(afterDelay: 2.0)
                        
                        LoadingIndicatorView.show((data as? String)!)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            LoadingIndicatorView.hide()
                        }
                        
                        print(data)
                    }
                }
            }
        }
        else
        {
            showAlert("Can't send empty text message", strTitle: "Message Alert")
        }
        
        vwMessage.isHidden = true
        
    }
    
 
            //MARK: Send message button
    @IBAction func btnHomeAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func postToSubscriber(_ sender: UIButton) {
//        var count : NSInteger = 0
//        if(arrBloxDetails.count > 0)
//        {
//            for dict in arrBloxDetails
//            {
//                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
//                {
//                    if (bloxDict.object(forKey: "bloxType") as? NSString) != nil
//                    {
//                        count += 1
//                    }
//                }
//            }
//        }
//
//        if(count>0) {
//            apiCallforSendToSubscriber()
            uploadAllTextScreenShots(type: "subscriber")
//        } else {
//            showAlert("Can not send empty message.", strTitle: "Alert")
//        }
        self.optionView.isHidden = true
    }
    
    @IBAction func sendToContacts(_ sender: UIButton) {
        self.fetchContacts()
        self.contactsPickerView.isHidden = false
        self.arrContacts.removeAll()
        self.arrGroupIds.removeAll()
        self.optionView.isHidden = true
    }
    
    @IBAction func btnSendAction(_ sender: UIButton)
    {
                isFromSendMessage = true
                isGoneOutToChild = true
                let dictReply = APP_DELEGATE.dictReplyMessageDetails
        
        var count : NSInteger = 0
        if(arrBloxDetails.count > 0)
        {
            for dict in arrBloxDetails
            {
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if (bloxDict.object(forKey: "bloxType") as? NSString) != nil
                    {
                        count += 1
                    }
                }
            }
        }
        if(count>0)
        {
            let dictReply = APP_DELEGATE.dictReplyMessageDetails
            if(APP_DELEGATE.isReplyMessage) || (APP_DELEGATE.isNewChat)
            {
                self.uploadAllTextScreenShots(type: "contacts")
            }
            else
            {
//                self.fetchContacts()
//                self.contactsPickerView.isHidden = false
//                self.arrContacts.removeAll()
                self.optionView.isHidden = false
            }
        }
        else
        {
            showAlert("Can not send empty message.", strTitle: "Alert")
        }
        
        
    }
    @IBAction func closeOptionView(_ sender: UIButton) {
        self.optionView.isHidden = true
    }
    
    //MARK:- serverCalls
    func fetchContacts (){
        self.contacts.removeAll()
        self.allContacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                        let g_Info = Group(json: [:])
                        let c_Info = mixedContacts(type: "contact", name: "\(contact.givenName) \(contact.familyName)" , gInfo: g_Info!, cInfo: contact)
                        self.allContacts.append(c_Info)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                    }
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result
            self.serverCallForGetGroups()

            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTable.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    
    
    
    func serverCallForForwardMessage() {
//        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        let param = ["message_id":self.inbloxData?.message_id ?? 0, "chat_id":self.chatId ,"contactIds":self.arrContacts,"groupIds": arrGroupIds as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id ?? ""] as [String : Any]
//        let url = "http://3.23.52.49:3002/api/v1/forward"
        let url = "https://api.talkblox.info/api/v1/forward"
        NetworkManager.sharedInstance.executeService(url as NSString, postParameters: param as NSDictionary) { (success:Bool, response:NSDictionary?) in
//            self.loading.dismiss()
            LoadingIndicatorView.hide()
            if success  {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    
//                    self.popToHome()
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                    let when = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: when){
                        self.contactsPickerView.isHidden = true
                        self.arrContacts.removeAll()
                        self.arrGroupIds.removeAll()
                    }
                }
            }
        }
        
    }
 
    func uploadAllTextScreenShots(type:String)  {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        var txtMessageCount = 0
        //let txtMsgCount = arrBloxDetails.flatMap(({"\(($0 as! NSDictionary)["bloxType"]!)" == "TextMessage"})).count
        let ff = arrBloxDetails.map { (a) in
            if let ddd = a as? NSDictionary{
                if  (ddd.value(forKey: "bloxType") as! String) == "TextMessage" {
                    print("count")
                    txtMessageCount = txtMessageCount + 1
                }
            }
        }
        
        
        print("text message count \(arrBloxDetails.count)")
        if txtMessageCount > 0 {
            for  (index,dict) in arrBloxDetails.enumerated() {
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        
                        if bloxType == "TextMessage" {
                            
                            
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in TextMessage")
                                LoadingIndicatorView.show()
                                queue.addOperation {
                                    NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Image as String, arrDataToSend: [data], postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
//                                        LoadingIndicatorView.hide()
                                        //self.loading.dismiss()
                                        if success == true {
                                            txtMessageCount = txtMessageCount - 1
                                            // let statusDic = response?["header"] as! NSDictionary
                                            let detail = response?["result"] as! NSArray
                                            if detail.count > 0 {
                                                let dictDetail = detail[0] as! NSDictionary
                                                // dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
                                                let id = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
                                                
                                                let dictBlox = self.arrBloxDetails[index] as! NSDictionary
                                                let newDictBlox:NSMutableDictionary = NSMutableDictionary.init(dictionary: dictBlox)
                                                
                                                let tmpBloxDetail : NSDictionary = dictBlox.object(forKey: "bloxDetails") as! NSDictionary
                                                
                                                let newBloXDetail:NSMutableDictionary = NSMutableDictionary.init(dictionary: tmpBloxDetail)
                                                newBloXDetail.setValue(id, forKey: "id")
                                                
                                                newDictBlox.setValue(newBloXDetail, forKey:  "bloxDetails")
                                                
                                                self.arrBloxDetails.removeObject(at: index)
                                                self.arrBloxDetails.insert(newDictBlox, at: index)
                                                
                                                // bloxDetail.setValue(id, forKey: "id")
                                            }
                                            
                                            //   self.bloxType =  GalleryType.Video
                                            //self.delegate?.bloxDetails(detail, true)
                                            
                                            /* if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                                             
                                             }*/
                                            if txtMessageCount == 0 {
                                                if type == "subscriber" {
                                                    self.apiCallforSendToSubscriber()
                                                } else {
                                                    self.callSendMessage()
                                                }
                                            }
                                            
                                        }
                                    })
                                }
                            }
                        }
                        
                    }
                }
            }
            queue.waitUntilAllOperationsAreFinished()
        }else {
            if type == "subscriber" {
                self.apiCallforSendToSubscriber()
            } else {
                self.callSendMessage()
            }
        }
    }
    
    func callSendMessage(){   // changed whole method on 22feb2021
//        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        
        /*
         if let keyExists = userDefaults.objectForKey("bloxBubbleAudio")  as? String
         {
         parametr.updateValue("Sound", forKey: "message[media_type]")
         }
         */
        
        allDataArray = [String : AnyObject]()
        
        allDataA = [AnyObject]()
        if(arrBloxDetails.count > 0)
        {
            var frameNo : NSInteger = 0
            for dict in arrBloxDetails
            {
                var dataArray : [String : AnyObject] = [String : AnyObject]()
                //print(array[i])
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        //print(bloxDict.objectForKey("bloxDetails"))
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        
                        dataArray.updateValue(bloxType as AnyObject,forKey: "mediaType")
                        dataArray.updateValue(frameNo as AnyObject,forKey: "frameNo")
                        
                        //var bloxButtonIndex : NSInteger = i + 101
                        switch bloxType
                        {
                        case "Camera":
                            //   if isCamera {
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in camera")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let bloxType = bloxDetail.object(forKey: "fileType") as? NSString
                                
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Sound, forKey: "mediaType")
                                
                                //dataArray.updateValue(data, forKey:"medias[][media]IMAGE")
                                //aryMediaImageData.addObject(data)
                                //--  new code 27052016 --
                                
                                
                                
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    //dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    //--- old code ---
                                    /*
                                     print("\(data.bytes)")
                                     dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                     let videoPath = AppTheme().getDocumentFilePath(strVideoFile)
                                     if let videoData : NSData = NSData(contentsOfFile: videoPath)
                                     {
                                     dataArray.updateValue(videoData, forKey:"medias[][media]VIDEO")
                                     }
                                     //---
                                     */
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                          dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //  dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    //dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //   dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    // dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "medias[][caption]")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    //  dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                                
                            }
                            //    }
                            break
                            
                        case  "GalleryImage":
                            
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n ---in gallery--- \n")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
//                                print("isAnimated: \(isImageAnimated)")
//                                if(isImageAnimated){
//                                    dataArray.updateValue(GalleryType.Gif as AnyObject,forKey: "mediaType")
//                                } else {
                                    dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                }
                                dataArray.updateValue(bloxId as AnyObject, forKey: "mediaId")
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //  dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                    
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    
                                    // dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                //dataArray.updateValue(data, forKey:"medias[][media]IMAGE")
                                //aryMediaImageData.addObject(data)
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //    dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    // dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "medias[][caption]")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    // dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                            }
                            break
                            
                        case "Video":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                print("\n\n\n in Video")
                                print("\((data as NSData).bytes)")
                                
                                
                                let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                
                                if var dataPath : String = bloxDetail.value(forKey: "video") as? String
                                {
                                     dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                    //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                    let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                    if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                    {
                                        dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                        // dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                    }
                                    //aryMediaImageData.addObject(data)
                                }
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //   dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    //   dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "caption")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    // dataArray.updateValue("" as AnyObject, forKey: "caption")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                                
                            }
                            break
                        case "TextMessage":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in TextMessage")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Image, forKey: "mediaType")
                                
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    //dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                    //  dataArray.updateValue(bloxId!, forKey: "mediaId")
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    //"imageVideo"
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //   dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                    
                                    
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    //  dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    // dataArray.updateValue(sliderValue as AnyObject, forKey:"time_interval")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    //   dataArray.updateValue("1" as AnyObject, forKey:"time_interval")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //    dataArray.updateValue(strCaption, forKey: "caption")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    //   dataArray.updateValue("" as AnyObject, forKey: "caption")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                //                                if let strTextMessage : NSString = bloxDetail.valueForKey("chat_text") as? NSString
                                //                                {
                                //                                    dataArray.updateValue(strTextMessage, forKey: "chat_text")
                                //                                }
                                //                                else
                                //                                {
                                //                                    dataArray.updateValue("", forKey: "chat_text")
                                //                                }
                                print("data array contains",dataArray)
                                dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                loading.dismiss()
                            }
                        default:
                            //do nothing
                            break
                        }
                        
                        if(dataArray.count>0)
                        {
                            allDataArray.updateValue(dataArray as AnyObject, forKey: String(format:"AllData%d",frameNo))
                            allDataA.append(dataArray as AnyObject)
                        }
                        frameNo += 1
                    }
                }
            }
            
        }
        
        contactIds  = [String : AnyObject]()
        //let appDelegat = AppDelegate().appDelegate()
        let dictReply = APP_DELEGATE.dictReplyMessageDetails
        if(APP_DELEGATE.isReplyMessage)
            //if((dictReplyData) != nil)
        {
//            self.arrContacts.add(inbloxData?.user_ids ?? "")
            let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
            if selfNo == inbloxData?.user_phone_1 {
                self.arrContacts.append(inbloxData?.user_phone_2 ?? "")
            } else if selfNo == inbloxData?.user_phone_2 {
                self.arrContacts.append(inbloxData?.user_phone_1 ?? "")
            } else {
//                AppSharedData.sharedInstance.showErrorAlert(message: "No contact Selected")
                self.view.makeToast("No Contact Selected.", duration: 2.0, position: .bottom)
                return
            }
            
            var imageBg = ""
            if self.backGroundImageID != 0 {
                imageBg = "\(self.backGroundImageID)"
            }
            var soundBg = ""
            if self.backGroundSoundID != 0 {
                soundBg = "\(self.backGroundSoundID)"
            }
            
            let parameter : [String : AnyObject] =
                ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":self.inbloxData?.message_id as AnyObject ,"contactIds":arrContacts as AnyObject, "groupIds": arrGroupIds as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":self.bloxSubject as AnyObject]
            print(parameter)
            
            NetworkManager.sharedInstance.executeService(WEB_URL.sentbloxmessage, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if success {
                    
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        APP_DELEGATE.isReplyMessage = false
                        UserDefaults.standard.set(nil, forKey: "bloxArray")
                        UserDefaults.standard.setValue(nil, forKey: "BgImage")
                        UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")

                        UserDefaults.standard.synchronize()
//                        self.popToHome()
                        self.navigationController?.popViewController(animated: true)  //  added on 16Jan
//                        AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                    } else {
                        
                        
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                print(response!)
            })
            
            
           
        } else if(APP_DELEGATE.isNewChat){
            
            var imageBg = ""
            if self.backGroundImageID != 0 {
                imageBg = "\(self.backGroundImageID)"
            }
            var soundBg = ""
            if self.backGroundSoundID != 0 {
                soundBg = "\(self.backGroundSoundID)"
            }
            
            let parameter : [String : AnyObject] =
                ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":self.inbloxData?.message_id as AnyObject ,"contactIds":arrContacts as AnyObject, "groupIds": arrGroupIds as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":self.bloxSubject as AnyObject]
            print(parameter)
            
            NetworkManager.sharedInstance.executeService(WEB_URL.sentbloxmessage, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if success {
                    
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        APP_DELEGATE.isNewChat = false
                        UserDefaults.standard.set(nil, forKey: "bloxArray")
                        UserDefaults.standard.setValue(nil, forKey: "BgImage")
                        UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")

                        UserDefaults.standard.synchronize()
                        self.navigationController?.popViewController(animated: true)
//                        AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                    } else {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                print(response!)
            })
        }
        else
        {
            var imageBg = ""
            if self.backGroundImageID != 0 {
                imageBg = "\(self.backGroundImageID)"
            }
            var soundBg = ""
            if self.backGroundSoundID != 0 {
                soundBg = "\(self.backGroundSoundID)"
            }
            let parameter : [String : AnyObject] =
                ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":"" as AnyObject,"contactIds":arrContacts as AnyObject, "groupIds": arrGroupIds as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":self.bloxSubject as AnyObject]
            print(parameter)
            
            NetworkManager.sharedInstance.executeService(WEB_URL.sentbloxmessage, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if success {
                    if let statusDic = response?["header"] as? NSDictionary {
                        if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                            UserDefaults.standard.set(nil, forKey: "bloxArray")
                            UserDefaults.standard.setValue(nil, forKey: "BgImage")
                            UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")
                            UserDefaults.standard.synchronize()
//                            self.popToHome()
                            self.navigationController?.popViewController(animated: true)
//                            AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                        } else {
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                            self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                            let when = DispatchTime.now() + 2
                            DispatchQueue.main.asyncAfter(deadline: when){
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                    }
                   
                }
//                print(response!)
            })
        }
    
    }
    
    func apiCallforSendToSubscriber() {
//        loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
    
        allDataArray = [String : AnyObject]()
        
        allDataA = [AnyObject]()
        if(arrBloxDetails.count > 0)
        {
            var frameNo : NSInteger = 0
            for dict in arrBloxDetails
            {
                var dataArray : [String : AnyObject] = [String : AnyObject]()
                //print(array[i])
                if let bloxDict : NSMutableDictionary = dict as? NSMutableDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        //print(bloxDict.objectForKey("bloxDetails"))
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        
                        dataArray.updateValue(bloxType as AnyObject,forKey: "mediaType")
                        dataArray.updateValue(frameNo as AnyObject,forKey: "frameNo")
                        
                        //var bloxButtonIndex : NSInteger = i + 101
                        switch bloxType
                        {
                        case "Camera":
                            //   if isCamera {
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in camera")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let bloxType = bloxDetail.object(forKey: "fileType") as? NSString
                                
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Sound, forKey: "mediaType")
                                
                                //dataArray.updateValue(data, forKey:"medias[][media]IMAGE")
                                //aryMediaImageData.addObject(data)
                                //--  new code 27052016 --
                                
                                
                                
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    //dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    //--- old code ---
                                    /*
                                     print("\(data.bytes)")
                                     dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                     let videoPath = AppTheme().getDocumentFilePath(strVideoFile)
                                     if let videoData : NSData = NSData(contentsOfFile: videoPath)
                                     {
                                     dataArray.updateValue(videoData, forKey:"medias[][media]VIDEO")
                                     }
                                     //---
                                     */
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                          dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //  dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    //dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //   dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    // dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "medias[][caption]")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    //  dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                                
                            }
                            //    }
                            break
                            
                        case  "GalleryImage":
                            
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n ---in gallery--- \n")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                let isImageAnimated = Utilities.shared.isAnimatedImage(data)//isAnimatedImage(data)
//                                print("isAnimated: \(isImageAnimated)")
//                                if(isImageAnimated){
//                                    dataArray.updateValue(GalleryType.Gif as AnyObject,forKey: "mediaType")
//                                } else {
                                    dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                }
                                dataArray.updateValue(bloxId as AnyObject, forKey: "mediaId")
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    
                                    // dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    //-- old code --
                                    /*
                                     print("\(data.bytes)")
                                     dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                     let videoPath = AppTheme().getDocumentFilePath(strVideoFile)
                                     if let videoData : NSData = NSData(contentsOfFile: videoPath)
                                     {
                                     dataArray.updateValue(videoData, forKey:"medias[][media]VIDEO")
                                     }
                                     */
                                    //---
                                    
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //  dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                    //----
                                    
                                }
                                else
                                {
                                    bgAudioId = "0"
                                    
                                    // dataArray.updateValue(data as AnyObject, forKey:"medias[][media]IMAGE")
                                }
                                
                                //-----
                                
                                //dataArray.updateValue(data, forKey:"medias[][media]IMAGE")
                                //aryMediaImageData.addObject(data)
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //    dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    // dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "medias[][caption]")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    // dataArray.updateValue("" as AnyObject, forKey: "medias[][caption]")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                            }
                            break
                            
                        case "Video":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                print("\n\n\n in Video")
                                print("\((data as NSData).bytes)")
                                
                                /*  if isCamera {
                                 // let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                 let bloxType = bloxDetail.object(forKey: "fileType") as? NSString
                                 
                                 // dataArray.updateValue(bloxId!, forKey: "mediaId")
                                 dataArray.updateValue(bloxType!, forKey: "mediaType")
                                 }*/
                                
                                //dataArray.updateValue(data,forKey: "medias[][media]VIDEOIMAGE")
                                /*
                                 dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"video_default_img",@"key",[NSString stringWithFormat:@"media%d.png",i],@"filename",UIImagePNGRepresentation(image),@"value",[NSNumber numberWithInteger:DataTypeImage],@"type", nil];
                                 */
                                
                                let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                
                                if var dataPath : String = bloxDetail.value(forKey: "video") as? String
                                {
                                     dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                    //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                    let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                    if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                    {
                                        dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                        // dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                    }
                                    //aryMediaImageData.addObject(data)
                                }
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    //   dataArray.updateValue(sliderValue as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                }
                                else
                                {
                                    //   dataArray.updateValue("1" as AnyObject, forKey:"medias[][time_interval]")
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    //  dataArray.updateValue(strCaption, forKey: "caption")
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                }
                                else
                                {
                                    // dataArray.updateValue("" as AnyObject, forKey: "caption")
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                
                                
                            }
                            break
                        case "TextMessage":
                            if let soundID = bloxDetail.value(forKey: "soundID") as? String {
                                dataArray.updateValue(soundID as AnyObject, forKey: "soundID")
                            }else {
                                dataArray.updateValue("" as AnyObject, forKey: "soundID")
                            }
                            if let data : Data = bloxDetail.value(forKey: "image") as? Data
                            {
                                print("\n\n\n in TextMessage")
                                
                                let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                dataArray.updateValue(bloxId!, forKey: "mediaId")
                                dataArray.updateValue(GalleryType.Image, forKey: "mediaType")
                                
                                //--  new code 27052016 --
                                if let _ : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    //dataArray.updateValue(GalleryType.Video as AnyObject,forKey: "mediaType")
                                    let bloxId = bloxDetail.object(forKey: "id") as? NSString
                                    //  dataArray.updateValue(bloxId!, forKey: "mediaId")
                                    let bgMediaId = bloxDetail.object(forKey: "bgAudioId") as? NSString
                                    //dataArray.updateValue(bgMediaId as AnyObject,forKey: "bgMediaId")
                                    bgAudioId = bgMediaId! as String
                                    
                                    //-- New code for accuracy --
                                    let dictTemp : NSMutableDictionary = NSMutableDictionary()
                                    dictTemp.setObject(data, forKey: "medias[][media]VIDEOIMAGE" as NSCopying)
                                    //"imageVideo"
                                    if var dataPath : String = bloxDetail.value(forKey: "imageVideo") as? String
                                    {
                                         dataPath = dataPath.replacingOccurrences(of: " ", with: "%20")
                                        //NSData *videoData = [NSData dataWithContentsOfFile:strURL];
                                        let videoPath = AppTheme().getDocumentFilePath(dataPath)
                                        if let videoData : Data = try? Data(contentsOf: URL(fileURLWithPath: videoPath))
                                        {
                                            dictTemp.setObject(videoData, forKey: "medias[][media]VIDEO" as NSCopying)
                                            //   dataArray.updateValue(dictTemp, forKey:"medias[][media]VIDEO")
                                        }
                                    }
                                } else {
                                    bgAudioId = "0"
                                }
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    dataArray.updateValue(sliderValue as AnyObject, forKey:"animationDuration")
                                } else {
                                    dataArray.updateValue("1" as AnyObject, forKey:"animationDuration")
                                }
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    dataArray.updateValue(strCaption, forKey: "caption")
                                } else {
                                    dataArray.updateValue("" as AnyObject, forKey: "caption")
                                }
                                
                                print("data array contains",dataArray)
                                dataArray.updateValue(GalleryType.Image as AnyObject,forKey: "mediaType")
//                                loading.dismiss()
                            }
                        default:
                            //do nothing
                            break
                        }
                        
                        if(dataArray.count>0)
                        {
                            allDataArray.updateValue(dataArray as AnyObject, forKey: String(format:"AllData%d",frameNo))
                            allDataA.append(dataArray as AnyObject)
                        }
                        frameNo += 1
                    }
                }
            }
            
        }
        
        contactIds  = [String : AnyObject]()
        //let appDelegat = AppDelegate().appDelegate()
        let dictReply = APP_DELEGATE.dictReplyMessageDetails
        
        var imageBg = ""
        if self.backGroundImageID != 0 {
            imageBg = "\(self.backGroundImageID)"
        }
        var soundBg = ""
        if self.backGroundSoundID != 0 {
            soundBg = "\(self.backGroundSoundID)"
        }
        let parameter : [String : AnyObject] =
//            ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"sender_id":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":"" as AnyObject,"contactIds":arrContacts as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":self.bloxSubject as AnyObject]
            ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"userid":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"message_id":"" as AnyObject,"medias":allDataA as AnyObject,"imageBg":imageBg as AnyObject,"soundBg":soundBg as AnyObject,"subject":self.bloxSubject as AnyObject,"name":AppSharedData.sharedInstance.getUser()?.display_name  as AnyObject]
        print(parameter)
        
        NetworkManager.sharedInstance.executeService(WEB_URL.sendBloxCastToSubscriber, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                self.loading.dismiss()
            LoadingIndicatorView.hide()
            if success {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        UserDefaults.standard.set(nil, forKey: "bloxArray")
                        UserDefaults.standard.setValue(nil, forKey: "BgImage")
                        UserDefaults.standard.setValue(nil, forKey: "backGroundImageID")
                        UserDefaults.standard.synchronize()
                        self.popToHome()
//                        AppSharedData.sharedInstance.showErrorAlert(message: "Message sent successfully")
                    }
                }else {
                    if let statusDic = response?["result"] as? NSDictionary {
                        if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                            self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                        }
                    }
                }
               
            }
//                print(response!)
        })
    }
    
    func apiCallforSubmitRating() {
//        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        self.loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        
        let parameter : [String : AnyObject] =
            ["userid":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"chat_id": self.chatId as AnyObject,"rating":self.ratingView.rating as AnyObject, "name": AppSharedData.sharedInstance.getUser()?.display_name  as AnyObject]
        print(parameter)
        
        NetworkManager.sharedInstance.executeService(WEB_URL.postRating, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when){
                LoadingIndicatorView.hide()
//                self.loading.dismiss()
            }
            if success {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        DispatchQueue.main.async{
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                            self.ratingPopUp.isHidden = true
                            self.ratingBtnBg.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            self.ratingBtnBg.alpha = 0.35
                        }
                    }
                }else {
                    if let statusDic = response?["result"] as? NSDictionary {
                        if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                            self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                        }
                    }
                }
               
            }
        })
    }
    
    func serverCallForGetBloxcastComments(chatId: Int, pageNo: Int){
        
            LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "chatId=%d&page=%d&limit=10",chatId,pageNo)
        
            NetworkManager.sharedInstance.executeGetService(WEB_URL.viewComment, parameters: parameter, completionHandler: {
            (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if response?["result"] != nil {
                    let inbloxData = [BloxcastComment].from(jsonArray: response?["result"] as! [JSON])
                    let result = inbloxData!.sorted(by: {
                        (first: BloxcastComment, second: BloxcastComment) -> Bool in
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

                        let dateAString = first.created_at
                        let dateBString = second.created_at
                        let dateA = dateFormatter.date(from: dateAString!)
                        let dateB = dateFormatter.date(from: dateBString!)
                        if dateA == nil || dateB == nil {
                            return true
                        }
                        return dateA!.compare(dateB!) == .orderedAscending
                    })
                        if pageNo == 1 {
                            self.arrBloxcastComments = result
                        } else {
                            self.arrBloxcastComments.append(contentsOf: result)
                        }
//                        if self.arrBloxcastComments.count > 0 {
//                            self.commentsView.isHidden = false
//                            self.commentsTable.reloadData()
//                        } else {
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                        }
                    } else {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    }
                    self.commentsView.isHidden = false
                    self.commentsTable.reloadData()
                } else if statusDic["ErrorCode"] as! Int == 404{
                    self.commentsView.isHidden = false
                    self.commentsTable.reloadData()
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCallForGetBloxcastDetails(chatId: Int){
        
//            LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "chat_id=%d",chatId)
        
            NetworkManager.sharedInstance.executeGetService(WEB_URL.detailsOfChatId, parameters: parameter, completionHandler: {
            (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if response?["user"] != nil {
                    let inbloxData = [Bloxcaster].from(jsonArray: response?["user"] as! [JSON])
                        DispatchQueue.main.async {
                            self.bloxcast_user.text = inbloxData![0].displayName
                            let image = inbloxData![0].user_image
                            var urlString = WEB_URL.BaseURL + image!
                            urlString = urlString.replacingOccurrences(of: " ", with: "%20")
                            self.bloxcast_user_img.kf.setImage(with: URL.init(string: urlString))
                            let rating = inbloxData![0].rating
                            if rating! > 0 {
//                                self.bloxcast_rating.isHidden = false
                                self.bloxcast_rating.rating = rating!
                            } else {
//                                self.bloxcast_rating.isHidden = true
                                self.bloxcast_rating.rating = 0
                            }
                        }
                    } else {
                    }
                } else if statusDic["ErrorCode"] as! Int == 404 {
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Pending {
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail {
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func apiCallforSubmitComment() {
//        self.loading = JGProgressHUD(style: JGProgressHUDStyle.dark)
//        self.loading.show(in: self.view)
        LoadingIndicatorView.show("Loading...")
        
        let parameter : [String : AnyObject] =
            ["userid":AppSharedData.sharedInstance.getUser()?.id as AnyObject,"chat_id": self.chatId as AnyObject,"comment":self.bloxComment.text as AnyObject, "name": AppSharedData.sharedInstance.getUser()?.display_name  as AnyObject]
        print(parameter)
        
        NetworkManager.sharedInstance.executeService(WEB_URL.postComment, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
            let when = DispatchTime.now() + 1.0
            DispatchQueue.main.asyncAfter(deadline: when){
                LoadingIndicatorView.hide()
//                self.loading.dismiss()
            }
            if success {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        
                        DispatchQueue.main.async {
                            self.bloxComment.text = ""
                            self.bloxComment.resignFirstResponder()
//                            self.commentsTable.reloadData()//reloadSections([0], with: .automatic)
//                            self.viewCommentTable.layoutIfNeeded()
                            self.serverCallForGetBloxcastComments(chatId: self.chatId, pageNo: self.pageNo)
                        }
                    }
                }else {
                    if let statusDic = response?["result"] as? NSDictionary {
                        if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                            AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        }
                    }
                }
               
            }
        })
    }
    
    func serverCallForGetGroups(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d", AppSharedData.sharedInstance.currentUser?.userid ?? 0)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.getAllGroup, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [Group].from(jsonArray: response?["group"] as! [JSON])
                    
                    self.groups = inbloxData!
                    for item in inbloxData! {
                        let c_Info = CNContact()
                        let g_Info = mixedContacts(type: "group", name: item.group_name ?? "", gInfo: item, cInfo: c_Info)
                        self.allContacts.append(g_Info)
                    }
                    let result = self.allContacts.sorted(by: {
                        (firt: mixedContacts, second: mixedContacts) -> Bool in firt.name < second.name
                    })
                    self.allContacts = result
                    self.contactsTable.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    @objc func popViewTextMessageSent()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func replyMessageSent()
    {
        //self.navigationController?.popViewControllerAnimated(true)
        
        let viewsToPop = 3
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(viewsToPop)
        navigationController?.setViewControllers(viewControllers!, animated: true)
        
    }
    
    
    //MARK: PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {        
        if(segue.identifier == "segueShowContactList")
        {
          /*  let objContactList : ContactListVC = segue.destination as! ContactListVC
            objContactList.strPopType = "Preview"
            if(strMessageId != "")
            {
                objContactList.strMessageId = strMessageId
            }
            else if aryTalkBloxContacts.count > 0 {
                objContactList.aryTalkBloxContacts = aryTalkBloxContacts
            }else {
                objContactList.contactIdsArry = contactIds
                objContactList.strReplyAllMessageId = strReplyAllMessageId
                //var ary : [String : AnyObject] =
                print(allDataArray.count)
                objContactList.allDataArray = allDataArray as [String : AnyObject]
                objContactList.allDataA = allDataA as [AnyObject]
                objContactList.bgSoundId = bgAudioId

            }*/
            
        }
        else if(segue.identifier == "segueHomeReply")
        {
            let objHome : TalkBoxHomeVC = segue.destination as! TalkBoxHomeVC
            //var dictTemp : NSMutableDictionary = NSMutableDictionary()
            //dictTemp = dictReplyDetails.mutableCopy() as! NSMutableDictionary
            //objHome.dictReplyData = dictTemp
            
           objHome.aryReplyTalkBloxContacts = aryTalkBloxContacts
        }
    }
    
    //MARK: Show alert message
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        self.present(errorAlert, animated: true ,completion: nil)
    }
    
    
    func textViewShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    
    //  MARK:UITextView Delegates
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if txtViewMessage!.textColor == UIColor(red: 0.0 / 255.0, green: 161.0 / 255.0, blue: 228.0 / 255.0, alpha: 1)
        {
            txtViewMessage!.text = nil
            txtViewMessage!.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if txtViewMessage!.text.isEmpty {
            txtViewMessage!.text = "Write message here..."
            txtViewMessage!.textColor = UIColor(red: 0.0 / 255.0, green: 161.0 / 255.0, blue: 228.0 / 255.0, alpha: 1)        }
    }
    
    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == commentsTable {
            return arrBloxcastComments.count
        } else {
                if(searchActive) {
                    return searchResults.count
                } else {
                    return allContacts.count
                }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == commentsTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell") as! CommentsCell
            let conversation = arrBloxcastComments[indexPath.row]
            
            var urlstring = WEB_URL.BaseURL + (conversation.user_image ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            cell.user_img.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.user_name.text = conversation.displayName
            cell.user_comment.text = conversation.comment
            let commentDate = conversation.created_at
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let comment_date = formatter.date(from: commentDate!)
            cell.comment_age.text = comment_date?.timeAgo()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selContactsCell") as! selContactsCell
            if(searchActive) {
                cell.cName.text =  "\(searchResults[indexPath.row].name)"
                if searchResults[indexPath.row].type == "contact" {
                    let currentContact = (searchResults[indexPath.row].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if currentContact != nil {
                        if arrContacts.contains(currentContact) {
                            cell.checkbtn.isSelected = true
                        } else {
                            cell.checkbtn.isSelected = false
                        }
                    } else {
                        cell.checkbtn.isSelected = false
                    }
                    cell.cImgIcon.image = #imageLiteral(resourceName: "manage_contact")
                } else {
                if arrGroupIds.contains(searchResults[indexPath.row].gInfo.group_id!) {
                    cell.checkbtn.isSelected = true
                } else {
                    cell.checkbtn.isSelected = false
                }
                cell.cImgIcon.image = #imageLiteral(resourceName: "icons8-people-1")
            }
            } else {
                cell.cName.text =  "\(allContacts[indexPath.row].name)"
                if allContacts[indexPath.row].type == "contact" {
                let currentContact = (allContacts[indexPath.row].cInfo.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkbtn.isSelected = true
                    } else {
                        cell.checkbtn.isSelected = false
                    }
                } else {
                    cell.checkbtn.isSelected = false
                }
                    cell.cImgIcon.image = #imageLiteral(resourceName: "manage_contact")
                } else {
                    if arrGroupIds.contains(allContacts[indexPath.row].gInfo.group_id!) {
                        cell.checkbtn.isSelected = true
                    } else {
                        cell.checkbtn.isSelected = false
                    }
                    cell.cImgIcon.image = #imageLiteral(resourceName: "icons8-people-1")
                }
            }
            cell.checkbtn.tag = indexPath.row
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == commentsTable {
            if indexPath.row == (pageNo * 10) - 1 {
                pageNo = pageNo + 1
                self.serverCallForGetBloxcastComments(chatId: self.chatId, pageNo: pageNo)
            }
        }
    }
    
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
//        contactsTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        contactsTable.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchResults = allContacts.filter{($0.name.lowercased().contains(searchText.lowercased()))}

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.contactsTable.reloadData()
    }
    
    
}


//------ Extension class for UILabel ------
extension UILabel
{
    func resizeHeightToFit(_ heightConstraint: NSLayoutConstraint) {
        let attributes = [NSAttributedString.Key.font : font]
        numberOfLines = 0
        lineBreakMode = NSLineBreakMode.byWordWrapping
        let rect = text!.boundingRect(with: CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
        heightConstraint.constant = rect.height
        setNeedsLayout()
    }
}
extension Date {

func timeAgo() -> String {

    let secondsAgo = Int(Date().timeIntervalSince(self))

    let minute = 60
    let hour = 60 * minute
    let day = 24 * hour
    let week = 7 * day
    let month = 4 * week

    let quotient: Int
    let unit: String
    if secondsAgo < minute {
        quotient = secondsAgo
        unit = "second"
    } else if secondsAgo < hour {
        quotient = secondsAgo / minute
        unit = "min"
    } else if secondsAgo < day {
        quotient = secondsAgo / hour
        unit = "hour"
    } else if secondsAgo < week {
        quotient = secondsAgo / day
        unit = "day"
    } else if secondsAgo < month {
        quotient = secondsAgo / week
        unit = "week"
    } else {
        quotient = secondsAgo / month
        unit = "month"
    }
    return "\(quotient) \(unit)\(quotient == 1 ? "" : "s") ago"
}
}
