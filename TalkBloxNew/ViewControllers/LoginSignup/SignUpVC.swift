//
//  SignUpVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/4/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SignUpVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {

    @IBOutlet var txtDisplayName:UITextField!
    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var btnGetStarted:UIButton!
    @IBOutlet var txtConfirmPassword:UITextField!
    @IBOutlet var txtCountryCode:UITextField!
    @IBOutlet var txtUserName:UITextField!
    @IBOutlet var imgFlag:UIImageView!
    @IBOutlet var lblCountryCode:UILabel!
    
    var picker:UIPickerView = UIPickerView()
    var arrCountries = [Country]()
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDisplayName.setplaceHolderColor(color: .white)
        self.txtMobile.setplaceHolderColor(color: .white)
        self.txtPassword.setplaceHolderColor(color: .white)
        self.txtUserName.setplaceHolderColor(color: .white)
        self.createPicker()
        self.txtConfirmPassword.setplaceHolderColor(color: .white)
        
        let contryUSA = self.arrCountries.filter{($0.name == "United States")}
        if contryUSA.count > 0 {
            self.txtCountryCode.text = "+\(contryUSA[0].numcode ?? 0)"
            
            self.imgFlag.kf.setImage(with: URL.init(string: contryUSA[0].image ?? ""))
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    //MARK:- Helper Methods
    func isValidData() -> Bool{
        var msg = ""
        if self.txtUserName.text == "" {
            msg = "Please enter username."
        }else if self.txtDisplayName.text == ""{
            msg = "Please enter name."
        }else if self.txtMobile.text == ""{
            msg = "Please enter mobile number."
        }else if self.txtPassword.text == ""{
            msg = "Please enter password."
        }else if self.txtPassword.text != self.txtConfirmPassword.text {
            msg = "Confirm password not match with password."
        }
        if msg == ""{
            return true
        }else {
            Utilities.shared.showAlertwith(message: msg, onView: self)
            return false
        }
    }
    func createPicker(){
        picker.delegate = self
        picker.dataSource = self
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(pickerDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([doneButton,spaceButton], animated: false)
        toolbar.tintColor = UIColor.black
        
        txtCountryCode.inputAccessoryView = toolbar
        txtCountryCode.tintColor = .clear
        txtCountryCode.inputView = picker
    }
    func showOTPAlert(userID:String)  {
        let alert = UIAlertController.init(title: "COnfirm your account and start Bloxing", message: "Enter OTP", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
            let textField = alert.textFields![0] as UITextField
            if textField.text == ""{
//                  AppSharedData.sharedInstance.showErrorAlert(message: "Please enter OTP.")
                self.view.makeToast("Please enter OTP.", duration: 2.0, position: .bottom)
             }else {
                  self.serverCallForVarifyOTP(otp: textField.text ?? "", userID: userID)
             }
           
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter OTP"
        }
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- Selector Methods
    @objc func pickerDone(){
        self.view.endEditing(true)
    }
    
    //MARK:- UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 60))
        let myImageView = UIImageView(frame: CGRect(x: 20, y: 17, width: 25, height: 25))
        let myLabel = UILabel(frame: CGRect(x: 80, y: 0, width: pickerView.bounds.width - 90, height: 60))
        myLabel.text = "+\(arrCountries[row].numcode ?? 0) \(arrCountries[row].name ?? "")"
        myImageView.kf.setImage(with: URL.init(string: arrCountries[row].image!))
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        return myView
    }
    
    //MARK:- UIPickerViewDelegate
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.lblCountryCode.text = "+\(arrCountries[row].numcode ?? 0)"
        self.txtCountryCode.text = "+\(arrCountries[row].numcode ?? 0)"
        self.imgFlag.kf.setImage(with: URL.init(string: arrCountries[row].image ?? ""))
    }
    //MARK:- Button Actions
    @IBAction func btnGetStartedAction(_ sender:Any){
        if isValidData(){
            self.serverCallForRegisterUser()
        }
    }
    @IBAction func btnBackAction(_ sender:Any){
       self.navigationController?.popViewController(animated: true)
    }
    
    //MARK- Server Calls
    func serverCallForRegisterUser(){
        let strPhone = self.txtCountryCode.text!  + txtMobile.text!
        let fcmToken = UserDefaults.standard.value(forKey: "fcmToken")
        let params:NSDictionary = ["username":self.txtUserName.text ?? "",
            "password":txtPassword.text ?? "",
            "password_confirmation" : txtConfirmPassword.text ?? "",
            "displayName":txtDisplayName.text ?? "",
            "phone_code":self.txtCountryCode.text!,
            "phoneNo":txtMobile.text ?? "","deviceType":DEVICE_TYPE_IOS,"deviceId":AppSharedData.sharedInstance.getUniqueDeviceIdentifierAsString(),"fcmToken":fcmToken!]
        
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.UserRegister, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let data =  response?["data"] as! NSArray
                    let dictUser = data[0] as! NSDictionary
                    let userID  = dictUser.value(forKey: "userid") as! Int
                    self.showOTPAlert(userID: "\(userID)")
                    
                    
                   let alertController = UIAlertController(title: "Registration Success", message: "You’ve successfully registered, please Login", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default)
                    { (_) in
                      //  self.server
                       // self.navigationController?.popViewController(animated: true)
                       /* let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(vc, animated: true)*/
                      /*  let verifyVC:OTPVerfyViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVerfyViewController") as! OTPVerfyViewController
                        verifyVC.userID =  response?["userId"] as! NSString
                        self.navigationController?.pushViewController(verifyVC, animated: false)*/
                    }
                    
                    alertController.addAction(okAction)
                    
                    alertController.view.backgroundColor = UIColor.white
                    AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
                 //   self.serverCallForLogin()
                  //  self.showOTPAlert(userID: <#T##String#>)
                  //  self.present(alertController, animated: true, completion: nil)
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
                
            }
        })
    }
 
    func serverCallForLogin(){
        
        let params:NSDictionary = ["phoneNo":self.txtMobile.text ?? "",
                                   "phone_code":self.txtCountryCode.text ?? "",
                                   "password":self.txtPassword.text ?? "","deviceType":DEVICE_TYPE_IOS,"deviceId":AppSharedData.sharedInstance.getUniqueDeviceIdentifierAsString()]
        
        
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.login, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
           LoadingIndicatorView.hide()
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    
                    let userRes = response?["user"] as! NSDictionary
                    
                    let authentication_token:String = (response?["authentication_Token"] as? String) ?? ""
                    let address:String = (userRes["address"] as AnyObject? as? String) ?? ""
                    let age_range:String = (userRes["age_range"] as AnyObject? as? String) ?? ""
                    let birth_month:String = (userRes["birth_month"] as AnyObject? as? String) ?? ""
                    let city:String = (userRes["city"] as AnyObject? as? String) ?? ""
                    let country:String = (userRes["country"] as AnyObject? as? String) ?? ""
                    let display_name:String = (userRes["display_name"] as AnyObject? as? String) ?? ""
                    let email:String = (userRes["user_email"] as AnyObject? as? String) ?? ""
                    let gender:String = (userRes["gender"] as AnyObject? as? String) ?? ""
                    let id:String = "\(userRes["userid"] as! Int)"
                    let name:String = (userRes["name"] as AnyObject? as? String) ?? ""
                    let notificationChanel:String = (userRes["notificationChanel"] as AnyObject? as? String) ?? ""
                    let phone_number:String = (userRes["phone_number"] as AnyObject? as? String) ?? ""
                    let state:String = (userRes["state"] as AnyObject? as? String) ?? ""
                    let zip_code:String = (userRes["zip_code"] as AnyObject? as? String) ?? ""
                     let userName:String = (userRes["username"] as AnyObject? as? String) ?? ""
                    
                   let user:User = User(id: id, name: name, email: email, phone_number: phone_number, country: country, display_name: display_name, address: address, city: city, age_range: age_range, birth_month: birth_month, gender: gender, notificationChanel: notificationChanel , state: state, zip_code: zip_code, authentication_token: authentication_token,username:userName)
                    
                    
                    AppSharedData.sharedInstance.saveUser(user:user)
                //    if self.shoulRememberUser {
                        USER_DEFAULT.set(authentication_token, forKey: NOTIF.ACCESS_TOKEN)
                 //   }else {
                   //     AppSharedData.sharedInstance.accessToken = authentication_token
                 //   }
                    
                    //   UserDefaults.standard .set(self.txtPassword.text, forKey: "USER_PASSWORD")
                    USER_DEFAULT.set(phone_number, forKey: NOTIF.MOBILE_NUMBER)
                    USER_DEFAULT .set(self.txtMobile.text, forKey: NOTIF.USER_NAME)
                
                    USER_DEFAULT .set(self.txtPassword.text, forKey: NOTIF.USER_PASSWORD)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                    
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    let userRes = response?["user"] as! NSDictionary
                    let userID =  userRes["id"] as! String
                    self.showOTPAlert(userID: userID)
                    /*   let msgStr:String = statusDic["ErrorMessage"] as! String
                     let userRes = response?["user"] as! NSDictionary
                     
                     let alertController = UIAlertController(title: "", message:msgStr , preferredStyle: .alert)
                     let okAction = UIAlertAction(title: "Ok", style: .default)
                     
                     { (_) in
                     
                     let verifyVC:OTPVerfyViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVerfyViewController") as! OTPVerfyViewController
                     verifyVC.userID =  userRes["id"] as! NSString
                     self.navigationController?.pushViewController(verifyVC, animated: false)
                     }
                     alertController.addAction(okAction)
                     alertController.view.backgroundColor = UIColor.white
                     AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
                     self.present(alertController, animated: true, completion: nil)*/
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
    func serverCallForVarifyOTP(otp:String,userID:String){
     
        let params:NSDictionary = ["otp":otp,
                                   "userId":userID]
        
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.OTPVerfy, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    
                    let userRes = response?["data"] as! NSDictionary
                    
                    let authentication_token:String = (response?["authentication_token"] as? String) ?? ""
                    let address:String = (userRes["address"] as AnyObject? as? String) ?? ""
                    let age_range:String = (userRes["age_range"] as AnyObject? as? String) ?? ""
                    let birth_month:String = (userRes["birth_month"] as AnyObject? as? String) ?? ""
                    let city:String = (userRes["city"] as AnyObject? as? String) ?? ""
                    let country:String = (userRes["country"] as AnyObject? as? String) ?? ""
                    let display_name:String = (userRes["display_name"] as AnyObject? as? String) ?? ""
                    let email:String = (userRes["email"] as AnyObject? as? String) ?? ""
                    let gender:String = (userRes["gender"] as AnyObject? as? String) ?? ""
                     let id:String = "\(userRes["userid"] as! Int)"
                    let name:String = (userRes["name"] as AnyObject? as? String) ?? ""
                    let notificationChanel:String = (userRes["notificationChanel"] as AnyObject? as? String) ?? ""
                    let phone_number:String = (userRes["phoneNo"] as AnyObject? as? String) ?? ""
                    let phone_code:String = (userRes["phone_code"] as AnyObject? as? String) ?? ""
  
                    
                    let state:String = (userRes["state"] as AnyObject? as? String) ?? ""
                    let userName:String = (userRes["username"] as AnyObject? as? String) ?? ""
                    let zip_code:String = (userRes["zip_code"] as AnyObject? as? String) ?? ""
                   
                    let user:User = User(id: id, name: name, email: email, phone_number: phone_number, country: country, display_name: display_name, address: address, city: city, age_range: age_range, birth_month: birth_month, gender: gender, notificationChanel: notificationChanel , state: state, zip_code: zip_code, authentication_token: authentication_token,username:userName)
                    
                    AppSharedData.sharedInstance.saveUser(user:user)
                    USER_DEFAULT.set(phone_number, forKey: NOTIF.MOBILE_NUMBER)
                    USER_DEFAULT.set(phone_code, forKey: NOTIF.PHONE_CODE)
                  //  self.performSegue(withIdentifier: "ShowHome", sender: self)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
}
