//
//  LoginViewController.swift
//  TalkBloxNew
//
//  Created by codezilla-mac3 on 02/05/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import IQKeyboardManagerSwift
class LoginViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var btnSignIn:UIButton!
    @IBOutlet var btnSignUP:UIButton!
    @IBOutlet var txtCountryCode:UITextField!
    @IBOutlet var imgFlag:UIImageView!
    @IBOutlet var imgCheckBox:UIImageView!
    var picker:UIPickerView = UIPickerView()
    var arrCountries = [Country]()
    var shoulRememberUser = false
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtMobile.setplaceHolderColor(color: .white)
        self.txtPassword.setplaceHolderColor(color: .white)
        self.createPicker()
        self.serverCallForGetCountryList()
        self.navigationController?.navigationBar.isHidden = true
        if USER_DEFAULT.value(forKey: NOTIF.ACCESS_TOKEN) != nil {
             AppSharedData.sharedInstance.accessToken = USER_DEFAULT.value(forKey: NOTIF.ACCESS_TOKEN) as! String
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxHomeVC") as! BloxHomeVC ///added on 12feb2021
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController ///added on 10May2021
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    //Helper Methods
    func isValidData() -> Bool {
        var msg = ""
        if self.txtMobile.text == "" {
            msg = "Please enter mobile number."
        }else if self.txtPassword.text == "" {
            msg = "Please enter Password"
        }
        if msg == ""{
            return true
        }else {
            Utilities.shared.showAlertwith(message: msg, onView: self)
            return false
        }
    }
    
    func createPicker(){
       
        picker.delegate = self
        picker.dataSource = self
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(pickerDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([doneButton,spaceButton], animated: false)
        toolbar.tintColor = UIColor.black
        
        txtCountryCode.inputAccessoryView = toolbar
        txtCountryCode.tintColor = .clear
        txtCountryCode.inputView = picker
   
    }
    
    func showOTPAlert(userID:String)  {
        let alert = UIAlertController.init(title: "COnfirm your account and start Bloxing", message: "Enter OTP", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
             let textField = alert.textFields![0] as UITextField
              self.serverCallForVarifyOTP(otp: textField.text ?? "", userID: userID)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter OTP"
        }
        self.present(alert, animated: true, completion: nil)
    }
    func showForgotPasswordAlert()  {
        let alert = UIAlertController.init(title: "Enter your registed mobile number with country code.", message: "Enter mobile number", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
            let textField = alert.textFields![0] as UITextField
             self.serverCallForForgotPassword(mobileNumber: textField.text ?? "")
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Number"
        }
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- Selector Methods
    @objc func pickerDone(){
        self.view.endEditing(true)
    }
  
    //MARK:- UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 60))
        let myImageView = UIImageView(frame: CGRect(x: 20, y: 17, width: 25, height: 25))
        let myLabel = UILabel(frame: CGRect(x: 80, y: 0, width: pickerView.bounds.width - 90, height: 60))
        myLabel.text = "+\(arrCountries[row].numcode ?? 0) \(arrCountries[row].name ?? "")"
        myImageView.kf.setImage(with: URL.init(string: arrCountries[row].image!))
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        return myView
    }
   
    //MARK:- UIPickerViewDelegate
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtCountryCode.text = "+\(arrCountries[row].numcode ?? 0)"
        self.imgFlag.kf.setImage(with: URL.init(string: arrCountries[row].image ?? ""))
    }
    //MARK:- Button Actions
    @IBAction func btnSignInAction(_ sender:Any){
      //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
       // self.navigationController?.pushViewController(vc, animated: true)
        if self.isValidData() {
            self.serverCallForLogin()
        }
    }
    @IBAction func btnForgotPasswordAction(_ sender:Any){
        //self.showForgotPasswordAlert()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        vc.arrCountries = self.arrCountries
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnSignUpAction(_ sender:Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        vc.arrCountries = self.arrCountries
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnRememberMeAction(_ sender:Any){
        if shoulRememberUser {
            self.shoulRememberUser = false
            self.imgCheckBox.image = #imageLiteral(resourceName: "uncheck_gray")
        }else {
            self.shoulRememberUser = true
            self.imgCheckBox.image = #imageLiteral(resourceName: "check_gray")
        }
    }
    
    //MARK- Server Calls
    func serverCallForLogin(){
        
        let fcmToken = UserDefaults.standard.value(forKey: "fcmToken")
        let params:NSDictionary = ["phoneNo":self.txtMobile.text ?? "",
                                   "phone_code":self.txtCountryCode.text ?? "",
                                   "password":self.txtPassword.text ?? "","deviceType":DEVICE_TYPE_IOS,"deviceId":AppSharedData.sharedInstance.getUniqueDeviceIdentifierAsString(),"fcmToken":fcmToken!]
        
        
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.login, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    
                    let userRes = response?["user"] as! NSDictionary
                    
                    let authentication_token:String = (response?["authentication_Token"] as? String) ?? ""
                    let address:String = (userRes["address"] as AnyObject? as? String) ?? ""
                    let age_range:String = (userRes["age_range"] as AnyObject? as? String) ?? ""
                    let birth_month:String = (userRes["birth_month"] as AnyObject? as? String) ?? ""
                    let city:String = (userRes["city"] as AnyObject? as? String) ?? ""
                    let country:String = (userRes["country"] as AnyObject? as? String) ?? ""
                    let display_name:String = (userRes["displayName"] as AnyObject? as? String) ?? ""
                    let email:String = (userRes["user_email"] as AnyObject? as? String) ?? ""
                    let gender:String = (userRes["gender"] as AnyObject? as? String) ?? ""
                    let id:String = "\(userRes["userid"] as! Int)"
                    let name:String = (userRes["name"] as AnyObject? as? String) ?? ""
                    let notificationChanel:String = (userRes["notificationChanel"] as AnyObject? as? String) ?? ""
                    let phone_number:String = (userRes["phoneNo"] as AnyObject? as? String) ?? ""
                      let phone_code:String = (userRes["phone_code"] as AnyObject? as? String) ?? ""
                    let state:String = (userRes["state"] as AnyObject? as? String) ?? ""
                    let zip_code:String = (userRes["zip_code"] as AnyObject? as? String) ?? ""
                     let userName:String = (userRes["username"] as AnyObject? as? String) ?? ""
                    
                   
                    let user:User = User(id: id, name: name, email: email, phone_number: phone_number, country: country, display_name: display_name, address: address, city: city, age_range: age_range, birth_month: birth_month, gender: gender, notificationChanel: notificationChanel , state: state, zip_code: zip_code, authentication_token: authentication_token,username:userName)
                    
                    
                    AppSharedData.sharedInstance.saveUser(user:user)
                    USER_DEFAULT.set(phone_number, forKey: NOTIF.MOBILE_NUMBER)
                    USER_DEFAULT.set(phone_code, forKey: NOTIF.PHONE_CODE)
                    if self.shoulRememberUser {
                         USER_DEFAULT.set(authentication_token, forKey: NOTIF.ACCESS_TOKEN)
                    }else {
                        AppSharedData.sharedInstance.accessToken = authentication_token
                    }
                   
                    //   UserDefaults.standard .set(self.txtPassword.text, forKey: "USER_PASSWORD")
                    USER_DEFAULT .set(self.txtMobile.text, forKey: NOTIF.USER_NAME)
                    
                    USER_DEFAULT .set(self.txtPassword.text, forKey: NOTIF.USER_PASSWORD)
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxHomeVC") as! BloxHomeVC //added on 12feb2021
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController ///added on 10May2021
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    let userRes = response?["user"] as! NSDictionary
                    let userID =  userRes["id"] as! String
                    self.showOTPAlert(userID: userID)
                 /*   let msgStr:String = statusDic["ErrorMessage"] as! String
                    let userRes = response?["user"] as! NSDictionary
                    
                    let alertController = UIAlertController(title: "", message:msgStr , preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default)
                        
                    { (_) in
                        
                        let verifyVC:OTPVerfyViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OTPVerfyViewController") as! OTPVerfyViewController
                        verifyVC.userID =  userRes["id"] as! NSString
                        self.navigationController?.pushViewController(verifyVC, animated: false)
                    }
                    alertController.addAction(okAction)
                    alertController.view.backgroundColor = UIColor.white
                    AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
                    self.present(alertController, animated: true, completion: nil)*/
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func serverCallForVarifyOTP(otp:String,userID:String){
        let params:NSDictionary = ["otp":otp,
                                   "userId":userID]
        
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.OTPVerfy, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    
                    let userRes = response?["user"] as! NSDictionary
                    
                    let authentication_token:String = (response?["authentication_token"] as? String) ?? ""
                    let address:String = (userRes["address"] as AnyObject? as? String) ?? ""
                    let age_range:String = (userRes["age_range"] as AnyObject? as? String) ?? ""
                    let birth_month:String = (userRes["birth_month"] as AnyObject? as? String) ?? ""
                    let city:String = (userRes["city"] as AnyObject? as? String) ?? ""
                    let country:String = (userRes["country"] as AnyObject? as? String) ?? ""
                    let display_name:String = (userRes["display_name"] as AnyObject? as? String) ?? ""
                    let email:String = (userRes["email"] as AnyObject? as? String) ?? ""
                    let gender:String = (userRes["gender"] as AnyObject? as? String) ?? ""
                    let id:String = (userRes["id"] as AnyObject? as? String) ?? ""
                    let name:String = (userRes["name"] as AnyObject? as? String) ?? ""
                    let notificationChanel:String = (userRes["notificationChanel"] as AnyObject? as? String) ?? ""
                    let phone_number:String = (userRes["phone_number"] as AnyObject? as? String) ?? ""
                    let state:String = (userRes["state"] as AnyObject? as? String) ?? ""
                    let zip_code:String = (userRes["zip_code"] as AnyObject? as? String) ?? ""
                    let userName:String = (userRes["username"] as AnyObject? as? String) ?? ""
                    
                    
                    let user:User = User(id: id, name: name, email: email, phone_number: phone_number, country: country, display_name: display_name, address: address, city: city, age_range: age_range, birth_month: birth_month, gender: gender, notificationChanel: notificationChanel , state: state, zip_code: zip_code, authentication_token: authentication_token,username:userName)
                    
                    
                    AppSharedData.sharedInstance.saveUser(user:user)
                //    self.performSegue(withIdentifier: "ShowHome", sender: self)
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxHomeVC") as! BloxHomeVC //added on 12feb2021
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController ///added on 10May2021
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
                
            }
        })
        
    }
    
    func serverCallForGetCountryList(){
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getCountries, methodType: .post, postParameters: nil) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                     let arr = [Country].from(jsonArray: response?["result"] as! [JSON])
                    if arr != nil{
                        self.arrCountries = arr!
                        let contryUSA = self.arrCountries.filter{($0.name == "United States")}
                        if contryUSA.count > 0 {
                            self.txtCountryCode.text = "+\(contryUSA[0].numcode ?? 0)"
                            
                            self.imgFlag.kf.setImage(with: URL.init(string: contryUSA[0].image ?? ""))
                        }
                    }
                }
            }
        }
     }
    
    func serverCallForForgotPassword(mobileNumber:String){
        LoadingIndicatorView.show("Loading...")
        let param = ["phoneNo":mobileNumber]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.forgotPassword, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
}
