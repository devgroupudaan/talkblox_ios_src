//
//  SearchMediaVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 6/26/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
protocol SearchMediaVCDelegate {
    func didSearchMediaSelected(media:SearchResult)
    
}

class SearchMediaVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource ,UITextFieldDelegate{

    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet var activityIndicator:UIActivityIndicatorView!
    @IBOutlet var txtSearch:UITextField!
    
    var searchResults = [SearchResult]()
    
    var delegate:SearchMediaVCDelegate?
    
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad(){
        super.viewDidLoad()
        self.txtSearch.delegate = self
        txtSearch.returnKeyType = .search
        self.txtSearch.layer.cornerRadius = 10
        self.activityIndicator.isHidden = true
        
        self.txtSearch.setplaceHolderColor(color: .white)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Actions
    @IBAction func btnBackAction(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- UItextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtSearch {
            if textField.text != "" {
                self.serverCallForSearchResults()
                textField.resignFirstResponder()
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtSearch {
            if textField.text != "" {
                self.serverCallForSearchResults()
                textField.resignFirstResponder()
            }
        }
    }
    
    //MARK:- UICollectionViewDelegate and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchResultsCell", for: indexPath)
        let imgView = cell.viewWithTag(100) as! UIImageView
        let result = searchResults[indexPath.row]
        
       
        if result.gal_cat_id == 1{
            var urlstring =  WEB_URL.BaseURL + (result.gc_path ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
             imgView.kf.setImage(with: URL.init(string: urlstring), placeholder:#imageLiteral(resourceName: "blox_icon"), options: nil, progressBlock: nil, completionHandler: nil)
          
//            imgView.kf.setImage(with: URL.init(string: WEB_URL.BaseURL + (result.gc_path ?? "")) )
        }else {
            var urlstring =  WEB_URL.BaseURL + (result.gal_col_thumb ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            imgView.kf.setImage(with: URL.init(string:urlstring), placeholder:#imageLiteral(resourceName: "blox_icon"), options: nil, progressBlock: nil, completionHandler: nil)
            
//            imgView.kf.setImage(with: URL.init(string: WEB_URL.BaseURL + (result.gal_col_thumb ?? "")) )
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let result = searchResults[indexPath.row]
        self.delegate?.didSearchMediaSelected(media: result)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Server calls
    func serverCallForSearchResults(){
       
        let params:NSDictionary = ["searchname":self.txtSearch.text ?? ""]
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        NetworkManager.sharedInstance.executeService(WEB_URL.searchMedia, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            
            self.activityIndicator.isHidden = true
            
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let searchResult = [SearchResult].from(jsonArray: response?["result"] as! [JSON])
                    self.searchResults = searchResult!
                    self.collectionView.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }else if statusDic["ErrorCode"] as! Int == ErroCode.NoData{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: "No Data found.")
                    self.view.makeToast("No Data Found.", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
}
