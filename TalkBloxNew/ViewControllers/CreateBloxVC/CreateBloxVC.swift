//
//  CreateBloxVC.swift
//  TalkBlox
//
//  Created by Mac on 22/12/15.
//  Copyright © 2015 MobiWebTech. All rights reserved.
//

import Foundation
import UIKit
import CoreSpotlight
import MobileCoreServices
import AVFoundation
import AVKit
import QuartzCore
import JGProgressHUD
import SDWebImage
import IQKeyboardManagerSwift
import Kingfisher
import Photos
import Toast_Swift
import GiphyUISDK

/*protocol CreateBloxVCDelegate: class {
    func bloxDetails(_ resp: NSDictionary?, _ bool: Bool)
}*/

class CreateBloxVC: UIViewController, UITextViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,ColorPickerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SearchMediaVCDelegate,BloxBackgroundDelegate, videoDelegate
{
    //weak var delegate: CreateBloxVCDelegate?
    
    @IBOutlet weak var lblCaption: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var shouldShowSearchResults = false
    var searchClicked = false
    var isSearchEmpty = false

    var isVideoBloxSelected = false
    var audioThumbPath = ""
    var filteredArray : NSMutableArray = NSMutableArray()

    var detailArray : NSMutableArray = NSMutableArray()
    var allDetail : [String : AnyObject]!
   // var bloxId : NSString = ""
    var bloxType : NSString = ""

    @IBOutlet weak var layoutBottomMenu: NSLayoutConstraint!
    @IBOutlet weak var layoutTxtViewMessage: NSLayoutConstraint!
    @IBOutlet weak var layoutTxtViewMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var layoutViewSquareBloxWidth: NSLayoutConstraint!
    @IBOutlet weak var layoutViewSquareBloxHeight: NSLayoutConstraint!
    
    @IBOutlet weak var layoutTxtCaptionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var layoutTxtCaption: NSLayoutConstraint!
    
    @IBOutlet weak var imageLayout: NSLayoutConstraint!
    @IBOutlet var btnBottomControlArry : [UIButton]!
    
    @IBOutlet var txtViewMessage : UITextView!
    @IBOutlet var txtFieldCaption : UITextField!
    
    @IBOutlet var viewBottomOption : UIView!
    
    @IBOutlet var viewTextFormating : UIView!
    @IBOutlet var viewGallery : UIView!
    
    @IBOutlet var imgGallery : UIImageView!
    @IBOutlet var imgTextBgImage : UIImageView!
    
    @IBOutlet var collectionGallery : UICollectionView!
    var aryGalleryItem : NSMutableArray = NSMutableArray()
    
    @IBOutlet var tableOption : UITableView!
    var arryTableOption : NSMutableArray = NSMutableArray()
    var arrayTempGalleryList : NSMutableArray = NSMutableArray()
    var arryTableOptionAllData : NSMutableArray = NSMutableArray()
    
    var imagePicker: UIImagePickerController? = nil
    @IBOutlet var btnPlayVideo : UIButton!
    
    @IBOutlet var viewSquareBlox : UIView!
    
    //TODO:
    var bloxArrayIndex : NSInteger? = 0
    
    var isImageGallery : Bool = true
    
    var contentInsets : UIEdgeInsets = UIEdgeInsets()
    
    var strBloxType : NSString = "TextMessage"
    var soundID:NSString = ""
    var videoUrlPath : NSString = ""
    var isObserverAdded:Bool = false
    
    var textColor : String = ""//  UIColor(red: 0.0/255.0, green: 161.0/255.0, blue: 128.0/255.0, alpha: 1.0)
    
    //TODO:
    var backgroundColor : UIColor = UIColor.blue
    var strColorFor : NSString = ""
    var isGifs:Bool = false
    var gifsArray = ["tenor1","tenor2","tenor3","tenor4","tenor5"]
    @IBOutlet var sliderAnimationDuration : UISlider!
    @IBOutlet var colorCollectionView : UICollectionView!
    
    //****** font setting ******
    var arrayContainFontFamily : NSArray = NSArray()
    @IBOutlet var fontPickerView : UIPickerView!
    var fontSize : CGFloat = 90
    var strFontFamilyName: NSString = ""
    @IBOutlet var btnDone : UIButton!
    //**************************
    
    //***** Camera Options *****
    @IBOutlet var viewCameraOptions : UIView!
    @IBOutlet var btnOptionCamera : UIButton!
    @IBOutlet var btnOptionVideo : UIButton!
   
    //**************************
    
    var loading : JGProgressHUD = JGProgressHUD(style: .dark)
    var tableOffset : NSInteger = 0
    var strGalleryType : String = ""
    var strGalleryId : String = "0"
    var strMediaId : NSString = ""
    var strBgMediaId : NSString = ""
    var strBgImgMediaId : NSString = ""

    var arryGalleryCollection : NSMutableArray = NSMutableArray()
    var arryAllTableOption : NSMutableArray = NSMutableArray()
    
    //********
    var isVideoFromTalkBlox : Bool = false
    
    //**** Search controls *****
    @IBOutlet var txtSearch : UITextField!
    @IBOutlet var btnSearch : UIButton!
    @IBOutlet var viewSearch : UIView!
    var isSearchGallery : Bool = false
    var strSearchText : String = ""
    var strPreviousGalleryType : String = ""
    var isSearchFieldShifted : Bool = false
    
    //------ Load more Data -----
    @IBOutlet var vwFooter : UIView!
    @IBOutlet var btnLoadMore : UIButton!
    
    
    //------ Audio Stuff ------
    var audioUrlPath : String = ""
    var videoUrlPathToAdd : URL? = nil
    var imageVideoUrlPath : String = ""
    
    //--- Audio gallery data ---
    @IBOutlet var stopButton: UIButton!
    @IBOutlet var playButton: UIButton!
    var meterTimer:Timer? = nil
    var soundFileURL:URL? = nil
    @IBOutlet var viewRecorder : UIView!
    @IBOutlet var imgRecording : UIImageView!
    var animationTimer:Timer? = nil
    var incrSecond : NSInteger = 0
    @IBOutlet weak var viewRecorderBottomLayout: NSLayoutConstraint!
    @IBOutlet var btnCancelAudio: UIButton!
    @IBOutlet var btnUseAudio: UIButton!
    var strAudioURLPath: String = String()
    var videoPlayer :AVPlayer = AVPlayer()
    
    let aryMediaImageData : NSMutableArray = NSMutableArray()//img

    
    //--- Preview ---
    @IBOutlet var btnHome : UIButton!
    
    //--- Slider label value ---
    @IBOutlet var lblSliderValue : UILabel!
    
    var strTextBgType : String = ""
        
    //---- To check about button click ----
    var strGalleryToShow : String = ""
    
    //--- To check video is taken from preloaded gallery or from live camera
    var isVideoFromCamera : Bool = false
    
    let allowedCharacterSet = (CharacterSet(charactersIn:" ").inverted)
    var galleryImageData:Data?// = Data()
    //PopOver
    var popOver:Popover?
    
    var isNextBtn = Bool()               // Added 20Jan2021
    var isPrevBtn = Bool()               // Added 20Jan2021
//    var notAllowedNextFrame = Bool()     // Added 20Jan2021
    var isKeyboardeOpened = Bool()               // Added 20Jan2021
    var layoutBottomMenuHeight: CGFloat = 0.0
    
    var isGiphyCall = false
    
    var isLink = false
    
    //MARK: View Did Load
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sliderAnimationDuration.isContinuous = false
        
        let imgViewTitle: UIImageView = UIImageView(image: UIImage(named: "TalkbloxLogo"))
        imgViewTitle.tag = 123456
        imgViewTitle.frame = CGRect(x: self.view.frame.size.width / 2 - 60, y: 7, width: 130, height: 39)
        //self.view.addSubview(imgViewTitle)
        navigationController?.navigationBar.addSubview(imgViewTitle)
        
        self.configureSearchController()
        
        hideAllView()
        colorCollectionView.isHidden = true
        collectionGallery.isHidden = true
        layoutBottomMenu.constant = 0
        txtViewMessage.resignFirstResponder()
        viewTextFormating.isHidden = true
        txtViewMessage.isHidden = false
        imgGallery.isHidden = true
        txtFieldCaption.isHidden = true
        txtFieldCaption.resignFirstResponder()
        btnPlayVideo.isHidden = true
        viewSquareBlox.isHidden = false
        //txtViewMessage.becomeFirstResponder()
        imgTextBgImage.isHidden = false
        layoutBottomMenu.constant = 0
        self.viewGallery.isHidden = true
        txtFieldCaption.attributedPlaceholder = NSAttributedString(string: "Type your caption here",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: THEME_COLOR.themeColorBlue])
        
        //viewTextFormating.isHidden = true
        viewGallery.isHidden = true
        if txtFieldCaption.text == "" {
            lblCaption.isHidden = true
        } else {
            lblCaption.isHidden = false
            
        }
        
        txtViewMessage.autocorrectionType = UITextAutocorrectionType.no
        txtFieldCaption.autocorrectionType = UITextAutocorrectionType.no
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateBloxVC.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateBloxVC.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        backgroundColor = UIColor.white
        arrayContainFontFamily = UIFont.familyNames as NSArray
        fontSize = 60;
        strFontFamilyName = "Arial Rounded MT Bold"
        txtViewMessage.font = UIFont(name: strFontFamilyName as String, size: fontSize)
        
        if(bloxArrayIndex != nil)
        {
            loadSelectedBloxData()
        }
        btnLoadMore.layer.cornerRadius = 5.0
        self.loadColorList()
        
        btnOptionCamera.addTarget(self, action: #selector(CreateBloxVC.btnCameraAction(_:)), for: UIControl.Event.touchUpInside)
        btnOptionVideo.addTarget(self, action: #selector(CreateBloxVC.recordVideo(_:)), for: UIControl.Event.touchUpInside)
        
        viewSquareBlox.layer.borderColor = THEME_COLOR.themeColorBlue.cgColor
        viewSquareBlox.layer.borderWidth = 1.0
        layoutViewSquareBloxWidth.constant = viewSquareBlox.frame.size.height
        print("viewSquareBlox.frame.size.height\(viewSquareBlox.frame.size.height)")
        
        viewRecorderBottomLayout.constant = -150
        stopButton.layer.cornerRadius = stopButton.frame.size.height / 2
        playButton.layer.cornerRadius = playButton.frame.size.height / 2
        btnCancelAudio.layer.cornerRadius = btnCancelAudio.frame.size.height / 2
        btnUseAudio.layer.cornerRadius = btnUseAudio.frame.size.height / 2
        
        
        //var appDel = AppDelegate().appDelegate()
        if(APP_DELEGATE.isReplyMessage)
        {
            btnHome.isHidden = true
            self.navigationItem.hidesBackButton = true
        }
        else
        {
            btnHome.isHidden = false
            self.navigationItem.hidesBackButton = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        if !self.isGiphyCall {
                                            LoadingIndicatorView.hide()
//                                            self.loading.dismiss()
                                        }
                                      })
    }
  
    func reset() {
        hideAllView()
        colorCollectionView.isHidden = true
        collectionGallery.isHidden = true
        layoutBottomMenu.constant = 0
        txtViewMessage.resignFirstResponder()
        viewTextFormating.isHidden = true
        txtViewMessage.isHidden = false
        imgGallery.isHidden = true
        txtFieldCaption.isHidden = true
        txtFieldCaption.resignFirstResponder()
        btnPlayVideo.isHidden = true
        viewSquareBlox.isHidden = false
        //txtViewMessage.becomeFirstResponder()
        imgTextBgImage.isHidden = false
        layoutBottomMenu.constant = 0
        self.viewGallery.isHidden = true
        txtFieldCaption.attributedPlaceholder = NSAttributedString(string: "Type your caption here",
                                                                   attributes: [NSAttributedString.Key.foregroundColor: THEME_COLOR.themeColorBlue])
        //viewTextFormating.isHidden = true
        viewGallery.isHidden = true
        if txtFieldCaption.text == "" {
            lblCaption.isHidden = true
        } else {
            lblCaption.isHidden = false
            
        }
//        txtViewMessage.backgroundColor = UIColor.white
        
        viewSquareBlox.layer.borderColor = THEME_COLOR.themeColorBlue.cgColor
        viewSquareBlox.layer.borderWidth = 1.0
        layoutViewSquareBloxWidth.constant = viewSquareBlox.frame.size.height
        print("viewSquareBlox.frame.size.height\(viewSquareBlox.frame.size.height)")
        
        viewRecorderBottomLayout.constant = -150
        stopButton.layer.cornerRadius = stopButton.frame.size.height / 2
        playButton.layer.cornerRadius = playButton.frame.size.height / 2
        btnCancelAudio.layer.cornerRadius = btnCancelAudio.frame.size.height / 2
        btnUseAudio.layer.cornerRadius = btnUseAudio.frame.size.height / 2
    }
    @IBAction func btnInviteFriendAction(_ sender : UIButton)
    {
        let text2share = "Hi, please click on the link to install TalkBlox app: https://itunes.apple.com/us/app/talkblox/id938782906?ls=1&mt=8 and enjoy sharing images and videos in blox with your friends and relatives"
        let objects2Share = [text2share]
        let activityVC = ActivityViewController(activityItems: objects2Share, applicationActivities: nil)
        
        let excludeActivities = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.copyToPasteboard , UIActivity.ActivityType.mail, UIActivity.ActivityType.addToReadingList]
        
        activityVC.excludedActivityTypes = excludeActivities
        self.present(activityVC, animated: true, completion: nil)
    }
    
    //MARK: load Image/Video Gallery Content
    func loadGalleryData(){
        
        let params:NSDictionary = ["authentication_Token": AppSharedData.sharedInstance.getUser()!.authentication_token!]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.GetlistGalleries, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success == true {
                
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        
                        if(response?["galleries"] != nil)
                        {
                            if let ary : NSArray = response?["galleries"] as? NSArray
                            {
                                if(ary.count > 0){
                                    
                                    self.arryTableOptionAllData.removeAllObjects()
                                    self.arryTableOption.removeAllObjects()
                                    self.arrayTempGalleryList.removeAllObjects()
                                    
                                    for obj in ary
                                    {
                                        if let dict : NSDictionary = obj as? NSDictionary
                                        {
                                            if ((dict.value(forKey: "is_favourite")as! Bool))
                                            {
                                                self.arryTableOptionAllData.add(obj)
                                                self.arryTableOption.add(obj)
                                                self.arrayTempGalleryList.add(obj)
                                                
                                            }
                                            else //if(dict.value(forKey: "message") as? String != "From talkbox")
                                            {
                                                //self.arryTableOption.add(obj)
                                            }
                                        }
                                    }
                                    
                                    
                                }
                                else if(self.tableOffset > 0)
                                {
                                    self.tableOffset = self.arryTableOptionAllData.count
                                    //self.tableOffset = self.arryTableOption.count
                                    //self.tableOption.reloadData()
                                    let errorAlert = AppTheme().showAlert("No More Data to Show", errorTitle: "Gallery Alert")
                                    
                                    self.present(errorAlert, animated: true ,completion: nil)
                                    //self.btnLoadMore.userInteractionEnabled = false
                                }
                                else
                                {
                                    self.tableOffset = self.arryTableOptionAllData.count
                                    let errorAlert = AppTheme().showAlert("Gallery Data Not Found", errorTitle: "Gallery Alert")
                                    
                                    self.present(errorAlert, animated: true ,completion: nil)
                                }
                            }
                        }
                        
                    } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                        
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        })
    }
    
    //MARK: Load Selected Blox Data
    func loadSelectedBloxData()
    {
//        self.lblSliderValue.text =  "\(AppSharedData.sharedInstance.selectedBlox + 1)"
        let userDefaults = UserDefaults.standard
        if let key = userDefaults.object(forKey: "bloxArray")  as? Data
        {
            let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
            
            let array : NSMutableArray = arrayData! as! NSMutableArray
            
            if(array.count > bloxArrayIndex!)
            {
                //print(array[i])
                let i : NSInteger = bloxArrayIndex!
                if let bloxDict : NSDictionary = array[i] as? NSDictionary
                {
                    if let bloxType = bloxDict.object(forKey: "bloxType") as? NSString
                    {
                        //print(bloxDict.objectForKey("bloxDetails"))
                        let bloxDetail : NSDictionary = bloxDict.object(forKey: "bloxDetails") as! NSDictionary
                        let textButton : UIButton = self.view.viewWithTag(1001) as! UIButton
                      //  textButton.setImage(UIImage(named: "edit_text_icon"), for: UIControlState())
                        switch bloxType
                        {
                        case "TextMessage":
                            if let strMessage : NSString = bloxDetail.value(forKey: "message") as? NSString
                            {
                                let seletedButton : UIButton = self.view.viewWithTag(1001) as! UIButton
                                seletedButton.setImage(UIImage(named: "edit_text_icon"), for: UIControl.State())
                                txtViewMessage.text = strMessage as String
                                strBloxType = "TextMessage"
                                self.txtViewMessage.isHidden = false
                                self.imgGallery.isHidden = false
                              //  let fontColor = bloxDetail.value(forKey: "textColor") as! UIColor
                                if let strFontFName : String = bloxDetail.value(forKey: "fontName") as? String
                                {
                                    if let strSize : CGFloat = bloxDetail.value(forKey: "fontSize") as? CGFloat
                                    {
                                    txtViewMessage.font = UIFont(name: strFontFName, size: strSize)
                                        strFontFamilyName = strFontFName as NSString
                                        fontSize = strSize
                                    }
                                }
                                
                                if let txtColor : UIColor = bloxDetail.value(forKey: "textColor") as? UIColor
                                {
                                    print("text color is  \(txtColor)")
                                    txtViewMessage.textColor = txtColor
                                    //textColor = txtColor
                                }
                                // New condition for Bg Image and Bg Color
                                if ((bloxDetail.allKeys as NSArray).contains("backgroundImage"))
                                {
                                    if let txtBgColor : UIImage = UIImage(data: (bloxDetail.value(forKey: "backgroundImage") as? Data)!)
                                    {
                                        //backgroundColor = txtBgColor
                                        imgTextBgImage.image = txtBgColor// txtBgColor
                                        imgTextBgImage.isHidden = false
                                        self.strTextBgType = "BackgroundImage"
                                        //self.strGalleryType = "Background Image"
                                        self.txtViewMessage.backgroundColor = UIColor.clear
                                        //imgGallery.hidden = true
                                    }
                                }
                                else
                                {
                                    if let txtBgColor : UIColor = bloxDetail.value(forKey: "backgroundColor") as? UIColor
                                    {
                                        txtViewMessage.backgroundColor = txtBgColor
                                        backgroundColor = txtBgColor
                                        imgTextBgImage.isHidden = true
                                        strTextBgType = "BackgroundColor"
                                    }
                                }
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    sliderAnimationDuration.value = sliderValue
                                    self.lblSliderValue.text = "\(Int(sliderValue))"
                                }
                                
                                if let strCaptionValue : String = bloxDetail.value(forKey: "caption")  as? String
                                {
                                    txtFieldCaption.text = strCaptionValue
                                    if strCaptionValue.count > 0{
                                        txtFieldCaption.isHidden = false
                                    }
                                }
                                if let strAudioFile : String = bloxDetail.value(forKey: "audio") as? String
                                {
                                    audioUrlPath = strAudioFile
                                    
                                }
                                if let strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    imageVideoUrlPath = strVideoFile
                                    videoUrlPathToAdd = AppTheme().getFilePathURL(strVideoFile)
                                }
                                strMediaId = bloxDetail.value(forKey: "id") as? NSString ?? ""
                                soundID = bloxDetail.value(forKey: "soundID") as? NSString ?? ""
                            }
                            break
                            
                        case "Camera":
                            if let imageData : UIImage = UIImage(data: (bloxDetail.value(forKey: "image") as? Data)!)
                            {
                                let seletedButton : UIButton = self.view.viewWithTag(1002) as! UIButton
                                seletedButton.setImage(UIImage(named: "CameraButton_WIreBlue"), for: UIControl.State())
                                
                                strBloxType = "Camera"
                                strGalleryType = GalleryType.Image as String
                                imgGallery.image = imageData
                                imgGallery.isHidden = false
                                txtViewMessage.isHidden = true
                                imgGallery.contentMode = UIView.ContentMode.scaleAspectFit//scaleToFill
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    sliderAnimationDuration.value = sliderValue
                                    self.lblSliderValue.text = "\(Int(sliderValue))"
                                    
                                }
                                if let strCaptionValue : String = bloxDetail.value(forKey: "caption")  as? String
                                {
                                    txtFieldCaption.text = strCaptionValue
                                    if strCaptionValue.count > 0 {
                                        txtFieldCaption.isHidden = false
                                    }
                                }
                                if let strAudioFile : String = bloxDetail.value(forKey: "audio") as? String
                                {
                                    audioUrlPath = strAudioFile
                                }
                                if let strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    imageVideoUrlPath = strVideoFile
                                }
                                //btnShowGallery(UIButton())
                                btnCameraOption(UIButton())
                                strMediaId = bloxDetail.value(forKey: "id") as? NSString ?? ""
                                soundID = bloxDetail.value(forKey: "soundID") as? NSString ?? ""
                            }
                            break
                            
                        case  "GalleryImage":
                            if let imageData : UIImage = UIImage(data: (bloxDetail.value(forKey: "image") as? Data)!)
                            {
                                if let strCaption : NSString = bloxDetail.value(forKey: "caption") as? NSString
                                {
                                    txtFieldCaption.text = strCaption as String
                                    txtFieldCaption.isHidden = false
                                    lblCaption.text = txtFieldCaption.text
                                    lblCaption.isHidden = false
                                }
                                
                                let seletedButton : UIButton = self.view.viewWithTag(1003) as! UIButton
                                seletedButton.setImage(UIImage(named: "ImageGallery_WireBlue"), for: UIControl.State())
                                let data = bloxDetail.value(forKey: "image") as! Data
                                let isImageAnimated = isAnimatedImage(data)
                                print("isAnimated: \(isImageAnimated)")
                                if(isImageAnimated){
                                    isGifs = true
                                    imgGallery.image = UIImage.gifImageWithData(data)

                                }else{
                                     isGifs = false
                                imgGallery.image = imageData
                                }
                                galleryImageData = data
                                imgGallery.isHidden = false
                                isImageGallery = true
                                strBloxType = "GalleryImage"
                                strGalleryType = GalleryType.Image as String
                                txtViewMessage.isHidden = true
                                imgGallery.contentMode = UIView.ContentMode.scaleAspectFit//scaleToFill
                                btnShowGallery(UIButton())
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    sliderAnimationDuration.value = sliderValue
                                    self.lblSliderValue.text = "\(Int(sliderValue))"
                                }
                                if let strAudioFile : String = bloxDetail.value(forKey: "audio") as? String
                                {
                                    audioUrlPath = strAudioFile
                                }
                                if let strVideoFile : String = bloxDetail.value(forKey: "imageVideo") as? String
                                {
                                    imageVideoUrlPath = strVideoFile
                                }
                                if let strCaptionValue : String = bloxDetail.value(forKey: "caption")  as? String
                                {
                                    txtFieldCaption.text = strCaptionValue
                                    if strCaptionValue.count > 0 {
                                        txtFieldCaption.isHidden = false
                                    }
                                }
                                strMediaId = bloxDetail.value(forKey: "id") as? NSString ?? ""
                                soundID = bloxDetail.value(forKey: "soundID") as? NSString ?? ""
                            }
                            break
                            
                        case "Video":
                            if let imageData : UIImage = UIImage(data: (bloxDetail.value(forKey: "image") as? Data)!)
                            {
                                let seletedButton : UIButton = self.view.viewWithTag(1004) as! UIButton
                                seletedButton.setImage(UIImage(named: "VideoGallery_WIreBlac"), for: UIControl.State())
                                
                                strBloxType = "Video"
                                strGalleryType = GalleryType.Video as String
                                imgGallery.image = imageData
                                imgGallery.isHidden = false
                                txtViewMessage.isHidden = true
                                btnPlayVideo.isHidden = false
                                imgGallery.contentMode = UIView.ContentMode.scaleAspectFit//scaleToFill
                                if let strVideoPath : NSString = bloxDetail.value(forKey: "video") as? NSString
                                {
                                    videoUrlPath = strVideoPath
                                }
                                if let strCaptionValue : String = bloxDetail.value(forKey: "caption")  as? String
                                {
                                    txtFieldCaption.text = strCaptionValue
                                    if strCaptionValue.count > 0 {
                                        txtFieldCaption.isHidden = false
                                    }
                                }
                                
                                if let sliderValue : Float = bloxDetail.value(forKey: "animationDuration") as? Float
                                {
                                    sliderAnimationDuration.value = sliderValue
                                    self.lblSliderValue.text = "\(Int(sliderValue))"
                                    sliderAnimationDuration.isUserInteractionEnabled = false
                                }
                                
                                if (bloxDetail.allKeys as NSArray).contains("cameraVideo")
                                {
                                    btnCameraOption(UIButton())
                                }
                                else
                                {
                                    btnShowVideoGallery(UIButton())
                                }
                                strMediaId = bloxDetail.value(forKey: "id") as? NSString ?? ""
                                soundID = bloxDetail.value(forKey: "soundID") as? NSString ?? ""
                            }
                            break
                            
                        default:
                            break
                        }
                        
                        
                    }
                    else
                    {
                        print("No data found")
                    }
                }else {
                    self.strBloxType = "TextMessage"
                    self.txtViewMessage.isHidden = false
                    self.txtViewMessage.text = ""
                    self.imgGallery.isHidden = true
                    self.btnPlayVideo.isHidden = true
                }
            }
        }
        self.layoutBottomMenu.constant = 0
        self.viewGallery.isHidden = true
    }
    
    
    /*
    func handleTap (tap : UITapGestureRecognizer)
    {
        txtFieldCaption.resignFirstResponder()
        txtViewMessage.resignFirstResponder()
        if(txtViewMessage.isFirstResponder())
        {
        layoutBottomMenu.constant = 0
        }
    }
    */
    
    
    @IBAction func BtnHomeAction(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
//        tabBarController?.selectedIndex = 0
    }
    
    //MARK: Button Control Action
    @IBAction func BtnControlAction(_ sender : UIButton)
    {
      //  changeSelection()
        
        switch sender.tag
        {
        case 1001:
            self.showPopOverFrom(btn: sender)
            isVideoBloxSelected = false
            strBloxType = "TextMessage"
            btnShowTextControlAction(sender)
            
            break
        case 1002:
            isVideoBloxSelected = false

            strBloxType = "Camera"
            btnCameraOption(sender)
            self.showPopOverFrom(btn: sender)
            break
        case 1003:
          
            self.showPopOverFrom(btn: sender)
            
            break
        case 1004:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchMediaVC") as! SearchMediaVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            
            break
        case 1005:
            self.showPopOverFrom(btn: sender)
        
            break
        case 1006:
            isVideoBloxSelected = false
            btnDoneEditing(sender)
            break
        case 1007:
          /*  sender.setImage(UIImage(named: "CheckButton_WireBlueGreen"), for: UIControlState())
            isVideoBloxSelected = false

            btnDoneEditing(sender)*/
            break
        default:
            sender.setImage(UIImage(named: ""), for: UIControl.State())
            break
        }
        
//        if(sender.tag != 1001)
//        {
//            txtViewMessage.resignFirstResponder()
//        }
    }
    
   
    func changeSelection()
    {
        for sender in btnBottomControlArry
        {
            switch sender.tag
            {
            case 1001:
                sender.setImage(UIImage(named: "edit_text_icon"), for: UIControl.State())
                break
            case 1002:
                sender.setImage(UIImage(named: "Live-Shots_1"), for: UIControl.State())
                break
            case 1003:
                sender.setImage(UIImage(named: "Users-Image-Gallery_1"), for: UIControl.State())
                break
            case 1004:
                sender.setImage(UIImage(named: "VideoGallery_WIreBlac"), for: UIControl.State())
                break
            case 1005:
                sender.setImage(UIImage(named: "Sound-Clip-Gallery_1"), for: UIControl.State())
                break
            case 1006:
                sender.setImage(UIImage(named: "Caption_1"), for: UIControl.State())
                break
            case 1007:
                sender.setImage(UIImage(named: "CheckButton_WireBlueGreen"), for: UIControl.State())
                break
            default:
                sender.setImage(UIImage(named: ""), for: UIControl.State())
                break
            }
        }
    }
    
    //MARK:  1st Option text editing
      func btnShowTextControlAction (_ sender : UIButton)
    {
        //sender.resignFirstResponder()
        hideAllView()
        colorCollectionView.isHidden = true
        collectionGallery.isHidden = true
        layoutBottomMenu.constant = 0
        txtViewMessage.resignFirstResponder()
        viewTextFormating.isHidden = true
        txtViewMessage.isHidden = false
        imgGallery.isHidden = true
        txtFieldCaption.isHidden = true
        txtFieldCaption.resignFirstResponder()
        btnPlayVideo.isHidden = true
        viewSquareBlox.isHidden = false
        //txtViewMessage.becomeFirstResponder()
        imgTextBgImage.isHidden = false
    }

    //MARK:  2nd Option Camera/Video
    func btnCameraOption (_ sender:UIButton)
    {
        //**** Hiding all controls  Camera/Video
       // sender.resignFirstResponder()

        hideAllForCamera()
        imgGallery.isHidden = false
        imgTextBgImage.isHidden = true
        //self.layoutBottomMenu.constant = 0
        hideKeyboard()//self.view.endEditing(true)
        layoutBottomMenu.constant = 0
        
        callForCameraAuth()
        
    }
    
    func callForCameraAuth()
    {
    
        
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for:AVMediaType.video)
        
        //var authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(mediaType)
        if authStatus ==  AVAuthorizationStatus.authorized
        {
            // do your logic
            
        }
        else if authStatus == AVAuthorizationStatus.denied
        {
            // denied

            let alertController = UIAlertController(title: "Camera Access Alert", message: "Application is not allowed to access Camera. Do you want to allow access to camera", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Setting", style: UIAlertAction.Style.default){ (_) in
                
                switch UIDevice.current.systemVersion.compare("8.0.0", options: NSString.CompareOptions.numeric)
                {
                case .orderedSame, .orderedDescending:
                    UIApplication.shared.openURL(URL(string: (UIApplication.openSettingsURLString))!)
                case .orderedAscending: break
                    //Do Nothing.
                }
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            present(alertController, animated: true, completion: nil)
            
        }
        else if authStatus == AVAuthorizationStatus.restricted
        {
            // restricted, normally won't happen
            showAlert("Not allowed to access camera", strTitle: "Camera Access Alert")
        }
        else if authStatus == AVAuthorizationStatus.notDetermined
        {
            // not determined?!
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {(granted: Bool) -> Void in
                if granted
                {
                   print("Granted access to \(AVMediaType.video)" )
                }
                else {
                    print("Not granted access to \(AVMediaType.video)" )
                    
                }
            })
        }
        else
        {
            // impossible, unknown authorization status
        }

    }
    
    func callForLibraryAuth()
    {
        
        
        let authStatus = PHPhotoLibrary.authorizationStatus()
        
        //var authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(mediaType)
        if authStatus ==  .authorized
        {
            // do your logic
            
        }
        else if authStatus == .denied
        {
            // denied
            
            let alertController = UIAlertController(title: "Photo library Access Alert", message: "Application is not allowed to access Photo library. Do you want to allow access to photo library", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Setting", style: UIAlertAction.Style.default){ (_) in
                
                switch UIDevice.current.systemVersion.compare("8.0.0", options: NSString.CompareOptions.numeric)
                {
                case .orderedSame, .orderedDescending:
                    UIApplication.shared.openURL(URL(string: (UIApplication.openSettingsURLString))!)
                case .orderedAscending: break
                    //Do Nothing.
                }
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
            
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            present(alertController, animated: true, completion: nil)
            
        }
        else if authStatus == .restricted
        {
            // restricted, normally won't happen
            showAlert("Not allowed to access photo library.", strTitle: " Photo library Access Alert")
        }
        else if authStatus == .notDetermined
        {
            // not determined?!
            
            PHPhotoLibrary.requestAuthorization({ (status) in
                print(status)
            })
        }
        else
        {
            // impossible, unknown authorization status
        }
        
    }
    
    
    //MARK:  2nd Option (A) Camera
    @objc func btnCameraAction (_ sender : UIButton)
    {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus ==  AVAuthorizationStatus.authorized
        {
            // do your logic
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            {
                //**** Hiding all controls
                btnPlayVideo.isHidden = true
                strBloxType = "Camera"
                imagePicker =  UIImagePickerController()
                imagePicker?.delegate = self
                imagePicker?.sourceType =  .camera
               
                imagePicker?.allowsEditing = true
                present(imagePicker!, animated: true, completion: nil)
            }
            else
            {
                showAlert("Application cannot access the camera.", strTitle: "Camera inaccessable")
            }
        }
        else if authStatus == AVAuthorizationStatus.denied
        {
            callForCameraAuth()
        }

    }
    
    //MARK:  2nd Option (B) Video option
    @IBAction func recordVideo(_ sender: UIButton)
    {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
        let authStatus: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus ==  AVAuthorizationStatus.authorized
        {
            if (UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil
                {
                    strBloxType = "Video"
                    isVideoFromTalkBlox = false
                    imagePicker =  UIImagePickerController()
                    imagePicker?.delegate = self
                    
                    imagePicker?.sourceType = .camera
                    imagePicker?.mediaTypes = [kUTTypeMovie as String]
                    
                    imagePicker?.allowsEditing = false
                    
                    imagePicker?.videoMaximumDuration = 15.0
                    
                    present(imagePicker!, animated: true, completion: {})
                    isVideoFromCamera = true
                }
                else
                {
                    AppTheme().showAlert("Application cannot access the camera.", errorTitle: "Rear camera doesn't exist")
                }
            }
            else
            {
                showAlert("Application cannot access the camera.", strTitle: "Camera inaccessable")
            }
        }
        else if authStatus == AVAuthorizationStatus.denied
        {
            callForCameraAuth()
        }
        
    }
    
    @objc func btnPhotoLibraryAction(_ sender : UIButton)
    {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
        let authStatus = PHPhotoLibrary.authorizationStatus()
        if authStatus ==  .authorized
        {
            // do your logic
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
            {
                //**** Hiding all controls
                btnPlayVideo.isHidden = true
                strBloxType = "GalleryImage"
                imagePicker =  UIImagePickerController()
                imagePicker?.delegate = self
                imagePicker?.videoMaximumDuration = 15.00
                imagePicker?.sourceType =  .photoLibrary
                 imagePicker?.mediaTypes = ["public.image", "public.movie"]
                imagePicker?.allowsEditing = true
                present(imagePicker!, animated: true, completion: nil)
            }
            else
            {
                showAlert("Application cannot access the Photo Library.", strTitle: "Photo Library inaccessable")
            }
        }
        else
        {
            callForLibraryAuth()
        }
        
    }
    
    //MARK:- Video Delegate
    func selectedVideo(url: URL!, is_saved: Bool){
        if is_saved {
            
            let videoData = try? Data(contentsOf: url)
            //let videoData = NSData(contentsOfURL: NSURL(string: savePath)!)
            var squareVideoData : Data = Data()
            
            var dataPath = AppTheme().getDocumentFilePath("bloxVideo0.mp4")
            if((bloxArrayIndex ) != nil)
            {
                print("%@",bloxArrayIndex)
                let strIndex : String = "bloxVideo\(bloxArrayIndex).mp4"
                if strIndex != nil
                {
                   print(strIndex)
                }
               
                let videoName : String = String(format:"bloxVideo\(bloxArrayIndex).mp4")
                dataPath = AppTheme().getDocumentFilePath(videoName)
            }
            ((try? videoData?.write(to: URL(fileURLWithPath: dataPath), options: [])) as ()??)
            
            let playerItem : AVPlayerItem = AVPlayerItem(url: URL(fileURLWithPath: dataPath))
            let duration : CMTime = playerItem.duration
            let seconds : Float64 = CMTimeGetSeconds(duration)
            print(seconds)
            
            
            
//            let tempImage = info[UIImagePickerControllerMediaURL] as! NSURL!
//            let pathString = tempImage.relativePath
            
            let fileUrl:URL = URL(fileURLWithPath: dataPath)
            let asset:AVAsset = AVAsset(url: fileUrl)
            if( strBloxType == "GalleryImage") {
                                strBloxType = "Video"
                            }
           
            //added on 29Apr2021
            do {
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                DispatchQueue.main.async {
                    self.imgGallery.image = thumbnail
                    self.imgGallery.isHidden = false
                    self.txtViewMessage.isHidden = true
                }
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
            }
            
         
            
            
            //**** Distroying previous saved video ****
            let documentsPath: NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as NSString
            let killFile = FileManager.default
            if (killFile.isDeletableFile(atPath: documentsPath.appendingFormat("/xvideo.mov") as String)){
                do {
                    try killFile.removeItem(atPath: documentsPath.appendingFormat("/xvideo.mov") as String)
                }
                catch let error as NSError {
                    error.description
                    print(error.description)
                }
            }
            //************
            
            //let documentsPath: NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as NSString
            LoadingIndicatorView.show("Loading...")
//            self.loading.show(in: self.view)
            
            let exportPath: NSString = documentsPath.appendingFormat("/xvideo.mov")
            let exportUrl: URL = URL(fileURLWithPath: exportPath as String)
            
            
            let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)
            exporter!.videoComposition = self.squareVideoCompositionForAsset(asset: asset)//videoComposition
            exporter!.outputFileType = AVFileType.mov
            exporter!.outputURL = exportUrl
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                
                
           
            exporter!.exportAsynchronously(completionHandler: {
                
                //display video after export is complete, for example...
                let outputURL:URL = exporter!.outputURL!;
                
                squareVideoData = try! Data(contentsOf: outputURL)
                
                var dataPath = AppTheme().getDocumentFilePath("bloxVideo0.mp4")
                
                
                let asset : AVURLAsset = AVURLAsset(url: outputURL) as AVURLAsset
                let duration : CMTime = asset.duration
                //let rounded : Int = lround(CMTimeGetSeconds(duration));
                let rounded : Float = roundf(Float(CMTimeGetSeconds(duration)));
                print(rounded);
                DispatchQueue.main.async {
                    self.sliderAnimationDuration.setValue(rounded, animated: true)
                    self.sliderAnimationDuration.isUserInteractionEnabled = false
                    self.lblSliderValue.text = "\(Int(rounded))"
                }
                
                
                if((self.bloxArrayIndex ) != nil)
                {
                    print("%@",self.bloxArrayIndex)
                    let strIndex : String = "bloxVideo\(self.bloxArrayIndex).mp4"
                    if strIndex != nil
                    {
                        print(strIndex)
                    }
                    
                    let videoName : String = String(format:"bloxVideo\(self.bloxArrayIndex).mp4")
                    dataPath = AppTheme().getDocumentFilePath(videoName)
                    
                }
                try? squareVideoData.write(to: URL(fileURLWithPath: dataPath), options: [])
                
                //-- kill video in gallery to save memory ---
                AppTheme().killVideoFilesRecorded(url)
                //-----
                
               
                DispatchQueue.main.async(execute: {
                        // code here
                    self.imgGallery.image = self.generateThumnail(outputURL)
                    
                    self.imgGallery.isHidden = false
                    self.btnPlayVideo.isHidden = false
//                    self.loading.dismiss()
                    LoadingIndicatorView.hide()
                })
                
                 self.videoUrlPath = dataPath as NSString
                
                LoadingIndicatorView.show("Loading...")
                DispatchQueue.main.async {
                let imageData : Data = self.imgGallery.image!.jpegData(compressionQuality: 1.0)!
                
                self.aryMediaImageData.add(imageData)
                 let arrVideoData : NSMutableArray = NSMutableArray()//img
                
                    arrVideoData.add(videoData!)
                
                NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Video as String, arrDataToSend: arrVideoData, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
                    LoadingIndicatorView.hide()
//                    self.loading.dismiss()
                    if success == true {
                        
                       // let statusDic = response?["header"] as! NSDictionary
                        let detail = response?["result"] as! NSArray
                        if detail.count > 0 {
                            let dictDetail = detail[0] as! NSDictionary
                            self.strMediaId = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
                        }
                       
                        self.bloxType =  GalleryType.Video
                    }
                })
                }
            })
                //-- dispatch ends
            });
            imagePicker?.dismiss(animated: true, completion:
                {
                    // Anything you want to happen when the user saves an video
                    
                    if(self.layoutBottomMenu.constant == 0)
                    {
                        //self.imageLayout.constant = self.viewGallery.frame.origin.y - self.viewBottomOption.frame.origin.y
                    }
            })
            
        }
    }
    
    func squareVideoCompositionForAsset(asset: AVAsset) -> AVVideoComposition {
        let track = asset.tracks(withMediaType: AVMediaType.video)[0]
        let length = min(track.naturalSize.width, track.naturalSize.height)
        
        var transform = track.preferredTransform
        
        let size = track.naturalSize
//        let scale: CGFloat = (transform.a == -1 && transform.b == 0 && transform.c == 0 && transform.d == -1) ? -1 : 1 // check for inversion
        
        var scale = CGFloat()
        if (transform.a == 0 && transform.b == 1 && transform.c == -1 && transform.d == 0) {
            scale = -1
        }
        else if (transform.a == 0 && transform.b == -1 && transform.c == 1 && transform.d == 0) {
            scale = 1
        }
        else if (transform.a == 1 && transform.b == 0 && transform.c == 0 && transform.d == 1) {
            if(track.naturalSize.width>track.naturalSize.height){
            scale = -1
            }else{
                scale = 1
            }
        }
        else if (transform.a == -1 && transform.b == 0 && transform.c == 0 && transform.d == -1) {
            scale = 1
        }
        print(size)
        print(scale)
        print("length",length)
        let a = scale * (size.width - length) / 2
        let b = scale * -(size.height - length) / 2
        print(a)
        print(b)
        if(track.naturalSize.width>track.naturalSize.height){
        transform = transform.translatedBy(x: scale * (size.width - length) / 2, y: scale * -(size.height - length) / 2)
        }else{
            transform = transform.translatedBy(x: scale * (size.width - length) / 2, y: scale * -(size.height - length) / 2)
            
        }
        
        let transformer = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        transformer.setTransform(transform, at: CMTime.zero)
        
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRange(start: CMTime.zero, duration: CMTime.positiveInfinity)
        instruction.layerInstructions = [transformer]
        
        let composition = AVMutableVideoComposition()
        composition.frameDuration = CMTime(value: 1, timescale: 30)
        composition.renderSize = CGSize(width: length, height: length)
        composition.instructions = [instruction]
        
        return composition
    }
    
    
    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
//    {
//// Local variable inserted by Swift 4.2 migrator.
//let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
//
//        if let pickedVideo:URL = (info(UIImagePickerController.InfoKey.mediaURL) as? URL)
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
//        if let pickedVideo:URL = (info[UIImagePickerController.InfoKey.mediaURL] as? URL)
//        {
        if let pickedVideo:URL = info[.mediaURL] as? URL {
            let asset   = AVURLAsset.init(url: pickedVideo as URL)
        
        let mainstoryBoard = UIStoryboard(name:"Main", bundle: nil)
        let viewcontroller = mainstoryBoard.instantiateViewController(withIdentifier:"ViewController") as! ViewController
        viewcontroller.url = pickedVideo
        viewcontroller.asset = asset
        viewcontroller.delegate = self
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
        else
        {
            
        //imagePicker.dismissViewControllerAnimated(true, completion: nil)
//            imgGallery.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            
            if let cropRect = info[UIImagePickerController.InfoKey.cropRect] as? CGRect {
                if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                            let imageRef: CGImage = image.cgImage!.cropping(to: cropRect)!

                            let image1: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)

                            imgGallery.image = image1

                        }

                    }
//        imgGallery.image = info(UIImagePickerController.InfoKey.editedImage) as? UIImage
        imgGallery.isHidden = false
        txtViewMessage.isHidden = true
            btnPlayVideo.isHidden = true
            //layoutBottomMenu.constant = 0
            
//            let imageData : Data = imgGallery.image!.jpegData(compressionQuality: 1.0)!
            let imageData : Data = imgGallery.image!.pngData()!
            
            galleryImageData = imageData
          //  let params:NSDictionary = ["authentication_Token":AppSharedData.sharedInstance.getUser()!.authentication_token!,"fileType":GalleryType.Image,"name":"cameraImage","lf" : ""]
            aryMediaImageData.add(imageData)
            
            //LoadingIndicatorView.show("Loading...")
            LoadingIndicatorView.show("Loading...")
            
            NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Image as String, arrDataToSend: aryMediaImageData, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
                LoadingIndicatorView.hide()
//               self.loading.dismiss()
                
                if success == true {
                    let detail = response?["result"] as! NSArray
                    if detail.count > 0 {
                        let dictDetail = detail[0] as! NSDictionary
                        self.strMediaId = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
                    }
                    
                    self.bloxType =  GalleryType.Image
                    //self.delegate?.bloxDetails(detail, true)
                    // self.saveDictionaryInUserDefault(detail as! [String : AnyObject])
                   // self.saveDictionaryInUserDefault(detail as! [String : AnyObject])

                 /*   if statusDic["ErrorCode"] as! Int == ErroCode.Succes {
                        
                    }*/
                }
            })

        }
        imagePicker?.dismiss(animated: true, completion:
            {
            // Anything you want to happen when the user saves an video
                if(self.layoutBottomMenu.constant == 0)
                {
                    //self.imageLayout.constant = self.viewGallery.frame.origin.y - self.viewBottomOption.frame.origin.y
                }
        })
    }
    
    
    
    func videoCompositionInstructionForTrack(track: AVCompositionTrack, asset: AVAsset, standardSize:CGSize, atTime: CMTime) -> AVMutableVideoCompositionLayerInstruction {
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform: transform)
        
        var aspectFillRatio:CGFloat = 1
        if assetTrack.naturalSize.height < assetTrack.naturalSize.width {
            aspectFillRatio = standardSize.height / assetTrack.naturalSize.height
        }
        else {
            aspectFillRatio = standardSize.width / assetTrack.naturalSize.width
        }
        
        if assetInfo.isPortrait {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
            let posY = standardSize.height/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            
            instruction.setTransform(assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor), at: atTime)
            
        } else {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
            
            if assetInfo.orientation == .down {
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
            }
            
            instruction.setTransform(concat, at: atTime)
        }
        return instruction
    }
    
    func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoWasSavedSuccessfully(_ video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutableRawPointer)
    {
        print("Video saved")
        if let theError = error {
            print("An error happened while saving the video = \(theError)")
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                // What you want to happen
            })
        }
    }
    
    func previewImageForLocalVideo(_ url:URL) -> UIImage?
    {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        
        var time = asset.duration
        //If possible - take not the first frame (it could be completely black or white on camara's videos)
        time.value = min(time.value, 2)
        
        do
        {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return nil
        }
    }
    
    
    func generateThumnail(_ url : URL) -> UIImage
    {
         //-------
         let asset2:AVAsset = AVAsset(url: url)
         //-------
         let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset2)
         var time: CMTime = asset2.duration
         time.value = 0
         var frameImg : UIImage = UIImage()
         do {
         let imageRef =  try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
         
         frameImg = UIImage(cgImage: imageRef)
         
         print(frameImg.imageOrientation)
         
            
            DispatchQueue.main.async(execute: {
                            // code here
         self.imgGallery.image = frameImg
         //self.imgGallery.hidden = false
         //imgGallery.image = UIImage(named: "TalkBlox")
         //self.btnPlayVideo.hidden = false
                
            })
         }
         catch let error as NSError
         {
         print("Image generation failed with error \(error)")
         
         }
        
        //******
        return frameImg
    }
    
    
    
    @IBAction func playVideo(_ sender: AnyObject)
    {
        print("Play a video")
        
        // Find the video in the app's document directory
        var dataPath : String = ""
            dataPath = AppTheme().getDocumentFilePath("bloxVideo0.mp4")
        if(bloxArrayIndex != nil)
        {
            //let videoName : String = String(format:"bloxVideo%@.mp4" ,bloxArrayIndex)
             let videoName : String = "bloxVideo\(bloxArrayIndex).mp4"
            dataPath = AppTheme().getDocumentFilePath(videoName)
        }
//        }
        
         //dataPath = AppTheme().getDocumentFilePath("OutputAudio2.mp4")
        
        let videoAsset = (AVAsset(url: URL(fileURLWithPath: dataPath)))
        let playerItem = AVPlayerItem(asset: videoAsset)
        
        // Play the video
        let player = AVPlayer(playerItem: playerItem)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }

    
    
    //MARK:  3rd Option Image Gallery
    @objc func btnShowGallery (_ sender : UIButton)
    {
        strColorFor = ""
        // loadGalleryData()
        //sender.resignFirstResponder()
        
        hideKeyboard()
        hideAllView()
        self.layoutBottomMenu.constant = 210
        imgGallery.isHidden = false
        imgTextBgImage.isHidden = true
        txtViewMessage.resignFirstResponder()
        viewGallery.isHidden = false
//        txtViewMessage.isHidden = true
        tableOption.isHidden = false
        collectionGallery.isHidden = true
        txtFieldCaption.isHidden = true
        viewSquareBlox.isHidden = false
        
        strGalleryType = GalleryType.Image as String
        self.getAlbumListFrom(galleryType: "2")
        //  galleryFilterbyType()
        
    }
    
    //MARK:  4rd Option Video Gallery
    func btnShowVideoGallery (_ sender : UIButton)
    {
        strColorFor = ""
        //loadGalleryData()
        //   sender.resignFirstResponder()
        
        hideKeyboard()
        hideAllView()
        
        isVideoFromCamera = false
        
        if(videoUrlPath != "")
        {
            self.btnPlayVideo.isHidden = false
        }
        self.layoutBottomMenu.constant = 210
        imgGallery.isHidden = false
        imgTextBgImage.isHidden = true
        txtViewMessage.resignFirstResponder()
        viewGallery.isHidden = false
        txtViewMessage.isHidden = true
        tableOption.isHidden = false
        collectionGallery.isHidden = true
        txtFieldCaption.isHidden = true
        viewSquareBlox.isHidden = false
        
        strGalleryType = GalleryType.Video as String
        // strGalleryType = GalleryType.Image as String
        self.getAlbumListFrom(galleryType: "1")
        // galleryFilterbyType()
        
    }
    
    //MARK:  5th Option Audio Gallery
    func btnShowGifsGallery(_ sender : UIButton){
        strColorFor = ""
        sender.resignFirstResponder()
        
        self.hideKeyboard()
        self.hideAllView()
        self.layoutBottomMenu.constant = 210
        self.txtViewMessage.resignFirstResponder()
        self.viewGallery.isHidden = false
        self.imgGallery.isHidden = false
        self.tableOption.isHidden = true
        self.collectionGallery.isHidden = false
        
        self.strGalleryType = GalleryType.Gif as String
//        self.getAlbumListFrom(galleryType: "3")
    }
    
    
    func btnShowAudioGallery(_ sender : UIButton)
    {
        
        strColorFor = ""
        sender.resignFirstResponder()
        
        self.hideKeyboard()
        self.hideAllView()
        self.layoutBottomMenu.constant = 210
        self.txtViewMessage.resignFirstResponder()
        self.viewGallery.isHidden = false
        self.imgGallery.isHidden = false
        self.tableOption.isHidden = false
        self.collectionGallery.isHidden = true
        
        self.strGalleryType = GalleryType.Sound as String
        self.getAlbumListFrom(galleryType: "3")
        
    }
    
    //MARK:  6th Caption Option
    func showCaptionOption()
    {
        txtFieldCaption.isHidden = false
        lblCaption.isHidden = true
        txtFieldCaption.becomeFirstResponder()
     
        if(strBloxType == "TextMessage")
        {
            txtViewMessage.isHidden = false
            imgGallery.isHidden = true
        }
        else if(strBloxType != "TextMessage")
        {
            txtViewMessage.isHidden = true
            imgGallery.isHidden = false
            //btnPlayVideo.hidden = true
            //viewSquareBlox.hidden = true
            if(strBloxType == "Video")
            {
                //txtViewMessage.hidden = true
                //imgGallery.hidden = true
                btnPlayVideo.isHidden = false
                //viewSquareBlox.hidden = true
            }
        }
        
        if(isSearchFieldShifted)
        {
            var rectFrame : CGRect = viewSearch.frame
            rectFrame.origin.y += 80
            viewSearch.frame = rectFrame
            isSearchFieldShifted = false
        }
        //self.view.endEditing(true)
    }
    func screenShotMethod() {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
    }
    //MARK:  7th Option
    func btnDoneEditing(_ sender : UIButton) {
        USER_DEFAULT.set(false, forKey: NOTIF.IS_LOGIN)
        var details : [String : AnyObject] = [String : AnyObject]()

        let im1 = imgGallery.image
        switch strBloxType {
            case "TextMessage":                //client didn't required this
                if(txtViewMessage.text != "")// && txtViewMessage.text != "Type your message" )
                {
                    if(self.strFontFamilyName == "")
                    {
                        self.strFontFamilyName = "Arial Rounded MT Bold"
                    }
                    
                    let screenImage : UIImage = screenShot()
                    let imageData : Data = screenShot().pngData()!//.jpegData(compressionQuality: 1.0)!
                    let img = UIImage(data: imageData)
                    
                    var dataPath = AppTheme().getDocumentFilePath("screenShot0.png")
                    if((self.bloxArrayIndex ) != nil)
                    {
                        dataPath = AppTheme().getDocumentFilePath("screenShot\(bloxArrayIndex).png")
                    }
                    
                    try? imageData.write(to: URL(fileURLWithPath: dataPath), options: [])
                    
                    var strCaption : String = ""
                    if(txtFieldCaption.text != "")
                    {
                        strCaption = txtFieldCaption.text! as String
                    }
                    
                    //let strMessageText = "Test message"
                    
                    print("on save text color is  \(textColor)")
                    details = ["message": txtViewMessage.text as AnyObject, "textColor": txtViewMessage.textColor as! UIColor,  "fontName" : strFontFamilyName, "fontSize" : fontSize as AnyObject, "animationDuration":Int(sliderAnimationDuration.value) as AnyObject,"image": imageData as AnyObject,"caption" : strCaption as AnyObject,"id":strMediaId]//, "chat_text": strMessageText]
                    if(strTextBgType == "")
                    {
                        details.updateValue(UIColor.clear as AnyObject, forKey: "backgroundColor")
                        
                    }
                    else if(strTextBgType == "BackgroundColor")
                    {
                        details.updateValue(txtViewMessage.backgroundColor! as AnyObject, forKey: "backgroundColor")
                        
                       // details.updateValue(UIColor.white as AnyObject, forKey: "backgroundColor")
                    }
                    else if(strTextBgType == "BackgroundImage")
                    {           
                        if((imgTextBgImage.image) != nil)
                        {
                            let bgImageData : Data = imgTextBgImage.image!.jpegData(compressionQuality: 1.0)!
                            details.updateValue(bgImageData as AnyObject, forKey: "backgroundImage")
                            details.updateValue(strBgImgMediaId as AnyObject, forKey: "textBgMediaId")
                        }
                    }
                    details.updateValue(textColor as AnyObject, forKey: "textColor")
                        details.updateValue(self.audioUrlPath as AnyObject, forKey: "audio")   // on 22feb
                        details.updateValue(self.strBgMediaId as AnyObject, forKey: "bgAudioId")  // on 22feb
                        details.updateValue(self.soundID as AnyObject, forKey: "soundID")
                        self.saveDictionaryInUserDefault(details)
                } else {
                    if(imgGallery.image != nil)
                    {
                        let img = imgGallery.image
                        let imageData : Data = imgGallery.image!.jpegData(compressionQuality: 1.0)!
                        details = ["image": imageData as AnyObject, "caption" : txtFieldCaption.text as AnyObject, "audio":"" as AnyObject , "animationDuration":Int(sliderAnimationDuration.value) as AnyObject,"id":strMediaId as AnyObject,"fileType":bloxType as AnyObject]
                            details.updateValue(self.audioUrlPath as AnyObject, forKey: "audio")  // on 22feb
                            details.updateValue(self.strBgMediaId as AnyObject, forKey: "bgAudioId")  // on 22feb
                            details.updateValue(self.soundID as AnyObject, forKey: "soundID")
                            self.saveDictionaryInUserDefault(details)
//                        } // on 22feb
                    }
                    else if isNextBtn {
//                        isNextBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)    //Add on 3Feb
                        }
                    } else {
//                        isPrevBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)   //Add on 3Feb
                        }
                    }
                }
                break
            
            case "Camera":
                if(imgGallery.image != nil)
                {
                    let imageData : Data = imgGallery.image!.jpegData(compressionQuality: 1.0)!
                    details = ["image": imageData as AnyObject, "caption" : txtFieldCaption.text as AnyObject, "audio":"" as AnyObject , "animationDuration":Int(sliderAnimationDuration.value) as AnyObject,"id":strMediaId as AnyObject,"fileType":bloxType as AnyObject]
                    
                    
                    if(audioUrlPath != "")
                    {
                        
//                        self.loading.show(in: self.view)
                        LoadingIndicatorView.show("Loading...")
                        let priority = DispatchQueue.GlobalQueuePriority.default
                        DispatchQueue.global(priority: priority).async {
                            
                            // code here
                            let audioURL = AppTheme().getFilePathURL(self.audioUrlPath)
                            var movieURL : URL
                            if let strUrlCheck : String = self.videoUrlPathToAdd?.absoluteString
                            {
                                if(strUrlCheck == "") {
                                    movieURL = AppTheme().getFilePathURL("movieUrl0.mp4")
                                } else {
                                    movieURL = self.videoUrlPathToAdd!
                                }
                            } else {
                                movieURL = AppTheme().getFilePathURL("movieUrl0.mp4")
                            }
                            // AppTheme().getFilePathURL(data as! String)
                            var strTemp = "imageVideo0.mov"
                            var videoURL = AppTheme().getFilePathURL("imageVideo0.mov")
                            if((self.bloxArrayIndex ) != nil)
                            {
                                videoURL = AppTheme().getFilePathURL("imageVideo\(self.bloxArrayIndex).mov")
                                strTemp = "imageVideo\(self.bloxArrayIndex).mov"
                            }
                            //let videoURL = AppTheme().getFilePathURL("imageVideo\(self.bloxArrayIndex).mp4")
                            AppTheme().killFileOnPath(strTemp)
                            print(strTemp)
                            details.updateValue(self.audioUrlPath as AnyObject, forKey: "audio")
                            details.updateValue(self.strBgMediaId as AnyObject, forKey: "bgAudioId")
                             details.updateValue(self.soundID as AnyObject, forKey: "soundID")
                        //----- New code 26/05/2016 -----
                              DispatchQueue.main.async {
                            ImageMovie().build(outputSize: CGSize(width: 720, height: 720), photos: [self.imgGallery.image!,self.imgGallery.image!], completion: { (result, data) in
                                if(result == "success")
                                {
                                    if let url : URL = data as? URL
                                    {
                                        self.mergeAudio(audioURL, moviePathUrl: url, savePathUrl: videoURL, completion: { (result, data) in
                                            if(result == "success")
                                            {
                                                details.updateValue(strTemp as AnyObject, forKey: "imageVideo")
                                                let urlVideoDuration : URL = (data as? URL)!
                                                let asset : AVURLAsset = AVURLAsset(url: urlVideoDuration) as AVURLAsset
                                                let duration : CMTime = asset.duration
                                                let rounded : Int = lround(CMTimeGetSeconds(duration));
                                                print(rounded);
                                                details.updateValue(rounded as AnyObject, forKey: "animationDuration")
                                                self.saveDictionaryInUserDefault(details)
                                            }
                                        })
                                    }
                                }
                            })
                            }
                        }
                    } else {
                        details.updateValue(self.soundID as AnyObject, forKey: "soundID")
                        self.saveDictionaryInUserDefault(details)
                    }

                } else {
                    if isNextBtn {
//                        isNextBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)    //Add on 3Feb
                        }
                    } else {
//                        isPrevBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)   //Add on 3Feb
                        }
                    }
                }
            break
            
            case  "GalleryImage":
                if(imgGallery.image != nil)
                {
                    let im1 = imgGallery.image
                    var imageData = Data()
                    print(imgGallery.image.debugDescription)
                    print(imgGallery.image?.cgImage as Any)
                    print(imgGallery.image as Any)
                    print(imgGallery.debugDescription)
                    if let data = galleryImageData{
                        imageData = data
                    }else{
                         imageData = imgGallery.image!.jpegData(compressionQuality: 1.0)!
                    }
            
                    let img = UIImage(data: imageData)
                    details = ["image": imageData as AnyObject, "caption" : txtFieldCaption.text as AnyObject, "audio":"" as AnyObject , "animationDuration":Int(sliderAnimationDuration.value) as AnyObject , "id":strMediaId as AnyObject]
                 
                    //---- New Code 27052016 --- Added Audio finaly ---
                    
                    if(audioUrlPath != "")
                    {
                        LoadingIndicatorView.show("Loading...")
                        let priority = DispatchQueue.GlobalQueuePriority.default
                        DispatchQueue.global(priority: priority).async {
                            
                            // code here
                            let audioURL = AppTheme().getFilePathURL(self.audioUrlPath)
                            var movieURL : URL
                            if let strUrlCheck : String = self.videoUrlPathToAdd?.absoluteString
                            {
                                if(strUrlCheck == "")
                                {
                                    movieURL = AppTheme().getFilePathURL("movieUrl0.mp4")
                                }
                                else
                                {
                                    movieURL = self.videoUrlPathToAdd!
                                }
                            }
                            else
                            {
                                movieURL = AppTheme().getFilePathURL("movieUrl0.mp4")
                            }
                            // AppTheme().getFilePathURL(data as! String)
                            var strTemp = "imageVideo0.mov"
                            var videoURL = AppTheme().getFilePathURL("imageVideo0.mov")
                            if((self.bloxArrayIndex ) != nil)
                            {
                                videoURL = AppTheme().getFilePathURL("imageVideo\(self.bloxArrayIndex).mov")
                                strTemp = "imageVideo\(self.bloxArrayIndex).mov"
                            }
                            //let videoURL = AppTheme().getFilePathURL("imageVideo\(self.bloxArrayIndex).mp4")
                            AppTheme().killFileOnPath(strTemp)
                            print(strTemp)
                            details.updateValue(self.audioUrlPath as AnyObject, forKey: "audio")
                            details.updateValue(self.strBgMediaId as AnyObject, forKey: "bgAudioId")
                            details.updateValue(self.soundID as AnyObject, forKey: "soundID")

                            self.saveDictionaryInUserDefault(details) /// new code 19Jan2021
                        }
                        
                        
                    }
                    else
                    {
                        details.updateValue(self.audioUrlPath as AnyObject, forKey: "audio")
                        details.updateValue(self.strBgMediaId as AnyObject, forKey: "bgAudioId")
                        details.updateValue(self.soundID as AnyObject, forKey: "soundID")
                        self.saveDictionaryInUserDefault(details)
                    }
                    //-----------
                } else {
                    if isNextBtn {
//                        isNextBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)    //Add on 3Feb
                        }
                    } else {
//                        isPrevBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)   //Add on 3Feb
                        }
                    }
                }
            break
            
            case "Video":
                if(videoUrlPath != "")
                {
                    let imageData : Data = imgGallery.image!.jpegData(compressionQuality: 1.0)!
                    
                    var videoName : String = "bloxVideo0.mp4"
                    if(bloxArrayIndex != nil)
                    {
                        videoName = String(format:"bloxVideo\(bloxArrayIndex).mp4")
                    }
                    
                    let url : URL = AppTheme().getFilePathURL(videoName)
                    let asset : AVURLAsset = AVURLAsset(url: url) as AVURLAsset
                    let duration : CMTime = asset.duration
                    var rounded : Int = lround(CMTimeGetSeconds(duration));
                    print(rounded);
                    
                    if(rounded > 5)
                    {
                        rounded = 5
                    }
                    
                    let comOutputUrl : URL = AppTheme().getFilePathURL("rawVideo.mp4")
                    ImageMovie().compressVideo(url, outputURL: comOutputUrl, completion:
                        { (result, data) in
                        if(result == "success")
                        {
                            //-- file renaming 
                            do {
                                
                                AppTheme().killFileOnPath(videoName)
                                
                                let originPath = comOutputUrl
                                let destinationPath = url
                                try FileManager.default.moveItem(at: originPath, to: destinationPath)
                            } catch let error as NSError
                            {
                                print(error)
                            }
                        }
                    })
                    
                    
                    details = ["image": imageData as AnyObject, "caption" : txtFieldCaption.text as AnyObject, "video":videoName as AnyObject , "animationDuration":rounded as AnyObject,"id":strMediaId as AnyObject,"fileType":bloxType as AnyObject]
                    
                    if(isVideoFromCamera) {
                        
                        details.updateValue("cameraVideo" as AnyObject, forKey: "cameraVideo")
                    

                    }
                    details.updateValue(self.audioUrlPath as AnyObject, forKey: "audio") // added 11jan2021
                    details.updateValue(self.soundID as AnyObject, forKey: "soundID")  // added 15jan2021
                    
                    saveDictionaryInUserDefault(details)
                } else {
                    if isNextBtn {
//                        isNextBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)    //Add on 3Feb
                        }
                    } else {
//                        isPrevBtn = false
                        DispatchQueue.main.async {
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)   //Add on 3Feb
                        }
                    }
                }
            break
            
        default:
            var strError : String = ""
            if(txtFieldCaption.text != "")
            {
                strError = "Please add Text or Image/Video for caption" //"You have to add any data for caption"
            }
            else
            {
                strError = "You have not added anything for blox"
            }
            
            let alertController = UIAlertController(title: "Blox Alert", message: strError, preferredStyle: .alert)
            let updateAction = UIAlertAction(title: "Ok", style: .default)
            { (_) in
                //self.performSegueWithIdentifier("Home", sender: self)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            { (_) in }
            
            alertController.addAction(updateAction)
            alertController.addAction(cancelAction)
            
            alertController.view.backgroundColor = UIColor.white
            AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
            
            present(alertController, animated: true, completion: nil)
            if isNextBtn {
//                        isNextBtn = false
                DispatchQueue.main.async {
                    AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                    self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                    self.loadSelectedBloxData()   //Add on 22Jan
                    self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)    //Add on 3Feb
                }
            } else {
//                        isPrevBtn = false
                DispatchQueue.main.async {
                    AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                    self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                    self.loadSelectedBloxData()   //Add on 22Jan
                    self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)   //Add on 3Feb
                }
            }
            break
        }
        
        
        if(strBloxType == "" || details.count == 0)
        {
            if isPrevBtn {
                isPrevBtn = false
            } else if isNextBtn {
                isNextBtn = false
            } else {
                var strError : String = ""
                if(txtFieldCaption.text != "")
                {
                    strError = "Please add Text or Image/Video for caption" //"You have to add any data for caption"
                }
                else
                {
                    strError = "You have not added anything for blox"
                }
                
                let alertController = UIAlertController(title: "Blox Alert", message: strError, preferredStyle: .alert)
                
                let updateAction = UIAlertAction(title: "Ok", style: .default)
                { (_) in
                    //self.performSegueWithIdentifier("Home", sender: self)
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                    { (_) in }
                
                alertController.addAction(updateAction)
                alertController.addAction(cancelAction)
                
                alertController.view.backgroundColor = UIColor.white
                AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    func saveDictionaryInUserDefault(_ details : [String : AnyObject])
    {
        
        let img = UIImage(data: details["image"] as! Data)
        let dictBlox : NSMutableDictionary = NSMutableDictionary()
        if(details.count > 0)
        {
            let priority = DispatchQueue.GlobalQueuePriority.default
            DispatchQueue.global(priority: priority).async
            {
                
                //dictBlox = NSMutableDictionary(objects: [strBloxType, details], forKeys: ["bloxType","bloxDetails"])
                dictBlox.setObject(self.strBloxType, forKey: "bloxType" as NSCopying)
                dictBlox.setObject(details, forKey: "bloxDetails" as NSCopying)
                //print(dictBlox)
                let userDefaults = UserDefaults.standard
                if let key = userDefaults.object(forKey: "bloxArray")  as? Data
                {
                    let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                    
                    var array : NSMutableArray = arrayData! as! NSMutableArray
                    
                    if(array.count > 0)
                    {
                        if(self.bloxArrayIndex != nil)
                        {
                            if(array.count > self.bloxArrayIndex!)
                            {
                                array.replaceObject(at: self.bloxArrayIndex!, with: dictBlox)
                            }
                        }
                        else
                        {
                            array.replaceObject(at: 0, with: dictBlox)
                        }
                    }
                    else
                    {
                        if(array.count == 0)
                        {
                            let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
                            if(self.bloxArrayIndex != nil)
                            {
                                arr.replaceObject(at: self.bloxArrayIndex!, with: dictBlox)
                            }
                            else
                            {
                                arr.replaceObject(at: 0, with: dictBlox)
                            }
                            array = arr;
                            let data = NSKeyedArchiver.archivedData(withRootObject: array)
                            
                            userDefaults.set(data, forKey: "bloxArray")
                            userDefaults.synchronize()
                        }
                        else
                        {
                            array.replaceObject(at: 0, with: dictBlox)
                        }
                    }
                    
                    let data = NSKeyedArchiver.archivedData(withRootObject: array)
                    
                    userDefaults.set(data, forKey: "bloxArray")
                    userDefaults.synchronize()
                    
                    //                AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                    //                self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                    //                self.loadSelectedBloxData()   //Add on 22Jan
                }
                else
                {
                    //let array : NSMutableArray = NSMutableArray()
                    //array.addObject(dictBlox)
                    let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
                    arr.replaceObject(at: 0, with: dictBlox)
                    let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                    
                    userDefaults.set(data, forKey: "bloxArray")
                    userDefaults.synchronize()
                    
                }
                let when = DispatchTime.now() + 1.0
//                DispatchQueue.main.asyncAfter(deadline: when){  //Add on 31May
                    DispatchQueue.main.asyncAfter(deadline: when, execute: { //Add on 31May
//                DispatchQueue.main.async(execute: {
//                    self.loading.dismiss()
                    LoadingIndicatorView.hide()
                    //let appDel = AppDelegate().appDelegate()
                    if(APP_DELEGATE.isAppRunningFirstTime)
                    {    //TODO:Commented Code
                        //self.performSegue(withIdentifier: "Home", sender: self)
                        self.stop(UIButton())
                        self.backgroundColor = UIColor.white
                        self.txtViewMessage.text = ""
                        self.imgGallery.image = nil
                        self.videoUrlPath = ""
                        self.txtFieldCaption.text = ""
                        self.btnPlayVideo.isHidden = true
                        self.strMediaId = ""
                        self.imgTextBgImage.image = nil
                        self.imgTextBgImage.isHidden = true
                        self.audioUrlPath = ""
                        self.galleryImageData = nil
                        self.soundID = ""
                        
                        self.reset()
                        if self.isNextBtn {
                            self.isNextBtn = false
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)
                        } else if self.isPrevBtn {
                            self.isPrevBtn = false
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)
                        } else {
                            self.navigationController?.popViewController(animated: true)
                        }
                        APP_DELEGATE.isAppRunningFirstTime = false
                    }
                    else
                    {
                        self.stop(UIButton())
                        self.backgroundColor = UIColor.white
                        self.txtViewMessage.text = ""
                        self.imgGallery.image = nil
                        self.videoUrlPath = ""
                        self.txtFieldCaption.text = ""
                        self.btnPlayVideo.isHidden = true
                        self.strMediaId = ""
                        self.imgTextBgImage.image = nil
                        self.imgTextBgImage.isHidden = true
                        self.audioUrlPath = ""
                        self.galleryImageData = nil
                        self.soundID = ""
                        
                        
                        self.reset()
                        if self.isNextBtn {
                            self.isNextBtn = false
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox + 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)
                        } else if self.isPrevBtn {
                            self.isPrevBtn = false
                            AppSharedData.sharedInstance.selectedBlox =  AppSharedData.sharedInstance.selectedBlox - 1 //Add on 22Jan
                            self.bloxArrayIndex = AppSharedData.sharedInstance.selectedBlox  //Add on 22Jan
                            self.loadSelectedBloxData()   //Add on 22Jan
                            self.view.makeToast("Frame \(AppSharedData.sharedInstance.selectedBlox + 1)", duration: 2.0, position: .center)
                        } else {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                })
            }
        }
        else
        {
            var strError : String = ""
            if(txtFieldCaption.text != "")
            {
                strError = "Please add Text or Image/Video for caption" //"You have to add any data for caption"
            }
            else
            {
                strError = "You have not added anything for blox"
            }
            
            let alertController = UIAlertController(title: "Blox Alert", message: strError, preferredStyle: .alert)
            let updateAction = UIAlertAction(title: "Ok", style: .default)
            { (_) in
                //self.performSegueWithIdentifier("Home", sender: self)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            { (_) in }
            
            alertController.addAction(updateAction)
            alertController.addAction(cancelAction)
            
            alertController.view.backgroundColor = UIColor.white
            AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
            
            present(alertController, animated: true, completion: nil)
            
        }
        DispatchQueue.main.async(execute: {
            self.lblCaption.text = ""
        })
    }
  
    //MARK: Add audio to video file
    func mergeAudio(_ audioURL: URL, moviePathUrl: URL, savePathUrl: URL,completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        
        let composition = AVMutableComposition()
        let trackVideo:AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        let trackAudio:AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!
        
        let option = NSDictionary(object: true, forKey: "AVURLAssetPreferPreciseDurationAndTimingKey" as NSCopying)
        
        let sourceAsset = AVURLAsset(url: moviePathUrl, options: option as? [String : AnyObject])
        
        let audioAsset = AVURLAsset(url: audioURL, options: option as? [String : AnyObject])

        print("play duration: \(sourceAsset.duration)")
        
        print(sourceAsset)
        print("playable: \(sourceAsset.isPlayable)")
        print("exportable: \(sourceAsset.isExportable)")
        print("readable: \(sourceAsset.isReadable)")
        
        
        print("play duration audioAsset: \(audioAsset.duration)")
        
        print(sourceAsset)
        print("playable audioAsset: \(audioAsset.isPlayable)")
        print("exportable audioAsset: \(audioAsset.isExportable)")
        print("readable audioAsset: \(audioAsset.isReadable)")
        
        
        let tracks = sourceAsset.tracks(withMediaType: AVMediaType.video)
        let audios = audioAsset.tracks(withMediaType: AVMediaType.audio)
        
        if tracks.count > 0 {
            let assetTrack:AVAssetTrack = tracks[0] as AVAssetTrack
            let assetTrackAudio:AVAssetTrack = audios[0] as AVAssetTrack
            
            let videoDuration:CMTime = assetTrack.timeRange.duration
            let videoSeconds:Float64 = CMTimeGetSeconds(assetTrack.timeRange.duration)
            print(videoSeconds)
            
            let audioDuration:CMTime = assetTrackAudio.timeRange.duration
            let audioSeconds:Float64 = CMTimeGetSeconds(assetTrackAudio.timeRange.duration)
            print(audioSeconds)
            
            do{
                try
                    trackVideo.insertTimeRange(CMTimeRangeMake(start: CMTime.zero,duration: audioDuration), of: assetTrack, at: CMTime.zero)
            } catch let error as NSError {
                print("Failed to insert video/audio tracks!!!!\n")
                print(error.localizedDescription)
            }
            catch {
                print("Something went wrong!")
            }
            
            do{
                try
                    trackAudio.insertTimeRange(CMTimeRangeMake(start: CMTime.zero,duration: audioDuration), of: assetTrackAudio, at: CMTime.zero)
            } catch let error as NSError {
                print("Failed to insert video/audio tracks!!!!\n")
                print(error.localizedDescription)
            }
            catch {
                print("Something went wrong!")
            }
        }
        
        //--- new code ----
        
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        guard let documentDirectory: URL = urls.first else
        {
            fatalError("documentDir Error")
        }
        //changed code for compression -- to reuse replace savePathUrl by videoOutputURL
        //let videoOutputURL = documentDirectory.URLByAppendingPathComponent("rawVideo.mp4")
        
        if FileManager.default.fileExists(atPath: savePathUrl.path) {
            do {
                try FileManager.default.removeItem(atPath: savePathUrl.path)
            }
            catch
            {
                fatalError("Unable to delete file: \(error)")
            }
        }
        
        //-----
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetMediumQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = savePathUrl
        //self.tmpMovieURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true
        
        let duration = CMTimeGetSeconds(audioAsset.duration)
        print(duration)
        if (duration < 5.0) {
            print("sound is not long enough")
            return
        }
        // e.g. the first 30 seconds
        let startTime = CMTimeMake(value: 0, timescale: 1)
        let stopTime = CMTimeMake(value: 5,timescale: 1)
        let exportTimeRange = CMTimeRangeFromTimeToTime(start: startTime, end: stopTime)
        print(exportTimeRange)
        assetExport.timeRange = exportTimeRange
        print(assetExport.timeRange)
        
        assetExport.exportAsynchronously(completionHandler: {
            
            switch assetExport.status {
            case  .completed:
                //                export complete
                NSLog("Export Complete")
                
                print(savePathUrl)
                let sourceAsset = AVURLAsset(url: savePathUrl, options: option as? [String : AnyObject])
                print("play duration: \(sourceAsset.duration)")
                print("playable: \(sourceAsset.isPlayable)")
                print("exportable: \(sourceAsset.isExportable)")
                print("readable: \(sourceAsset.isReadable)")
                
            case .failed:
                NSLog("Export Failed")
                NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                //                export error (see exportSession.error)
                
            case .cancelled:
                NSLog("Export Failed")
                NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                //                export cancelled
                
            default:
                //do nothing
                break
            }
            
            print("converted video save here :- \(savePathUrl)")
            //ImageMovie().compressVideo(videoOutputURL, outputURL: savePathUrl)
//            ImageMovie().compressVideo(videoOutputURL, outputURL: savePathUrl, completion:
//                { (result, data) in
//                completion(result: "success", data: savePathUrl)
//            })
            completion("success", savePathUrl as AnyObject)
        })
        
    }
    
    //MARK:  TextField Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        hideAllView()
        layoutBottomMenu.constant = contentInsets.bottom
        
        if(textView == txtViewMessage)
        {
            txtFieldCaption.isHidden = true
        }
        // client didn't want this
        /*
        if(textView.text == "Type your message")
        {
            textView.text = ""
        }
        */
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        print("Editing finished")
    }
    //***** TextField Delegate Methods
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        if(textField == txtSearch)
        {
            IQKeyboardManager.shared.enableAutoToolbar = false

            self.view.bringSubviewToFront(viewSearch)
            var rectFrame : CGRect = viewSearch.frame
            rectFrame.origin.y -= 80
            viewSearch.frame = rectFrame
            isSearchFieldShifted = true
        }
        else
        {
            hideAllView()
        }
        layoutBottomMenu.constant = 210
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        //layoutBottomMenu.constant = 0
        txtFieldCaption.isHidden = true

        IQKeyboardManager.shared.enableAutoToolbar = true

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(isSearchFieldShifted)
        {
            var rectFrame : CGRect = viewSearch.frame
            rectFrame.origin.y += 80
            viewSearch.frame = rectFrame
            isSearchFieldShifted = false
        }
        self.view.endEditing(true)
        layoutBottomMenu.constant = 210
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == txtFieldCaption)
        {
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
        return newLength <= 30 // Bool
        }
        else 
        {
            return true
        }
    }
    
    var previousRect : CGRect  = CGRect.zero

    
    
    func textViewDidChange(_ textView: UITextView)
    {
            let pos = textView.endOfDocument
            let currentRect = textView.caretRect(for: pos)
            if(currentRect.origin.y > previousRect.origin.y)
            {
                numberOfLine += 1
                
            }
            previousRect = currentRect
    }

    //MARK:  Slider Method *****
    
    @IBAction func sliderValueChanged (_ slider : UISlider)
    {
        slider.setThumbImage(UIImage(named: "sliderCircle"), for: .highlighted)
        let rounded : Float = roundf(slider.value);
        slider.setValue(rounded, animated: true)
        lblSliderValue.text = "\(Int(rounded))"
        print(Int(slider.value))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = false
        txtViewMessage.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let width = self.view.frame.size.width
        let width2 = self.view.frame.size.height - 224 - 140
        if(width2 > width){
            layoutViewSquareBloxWidth.constant = self.view.frame.size.width
            layoutViewSquareBloxHeight.constant = self.view.frame.size.width
        }else{
            
            layoutViewSquareBloxHeight.constant = self.view.frame.size.height - 224 - 140
            layoutViewSquareBloxWidth.constant = self.view.frame.size.height - 224 - 140
        }
        print("viewSquareBlox.frame.size.width \(viewSquareBlox.frame.size.height),\(viewSquareBlox.frame.size.width), self.view.frame.size.height \(self.view.frame.size.height)")
//        let  rect = CGSize(width: 1, height: 1)
//        viewSquareBlox.frame = AVMakeRect(aspectRatio: rect, insideRect: viewSquareBlox.frame)
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        txtViewMessage.removeObserver(self, forKeyPath: "contentSize")
        if(isObserverAdded){
        videoPlayer.removeObserver(self, forKeyPath: "timeControlStatus")
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    /// Force the text in a UITextView to always center itself.
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
            let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
            if newStatus != oldStatus {
                DispatchQueue.main.async {[weak self] in
                    if newStatus == .playing || newStatus == .paused {
//                        self?.loading.dismiss()
                        LoadingIndicatorView.hide()
//                        self?.loaderView.isHidden = true
                    } else {
                    LoadingIndicatorView.show()
//                        self?.loading.show(in: self?.view)
//                        self?.loaderView.isHidden = false
                    }
                }
            }
        }else{
        let textView = object as! UITextView
        var topCorrect = (textView.bounds.size.height - textView.contentSize.height * textView.zoomScale) / 2
        topCorrect = topCorrect < 0.0 ? 0.0 : topCorrect;
        textView.contentInset.top = topCorrect
        }
    }
    
    //MARK:- Helper Methods
    func showPopOverFrom(btn:UIButton){
        var viewPopOver:UIView?
        if btn.tag == 1001{
            viewPopOver = self.getViewForEditText()
        }else if btn.tag == 1002{
            viewPopOver = self.getViewForCameraOptions()
        }else if btn.tag == 1003{
            viewPopOver = self.getViewForTBOptions()
        }else if btn.tag == 1005{
            viewPopOver = self.getViewForAddOptions()
        } else {
            viewPopOver = UINib(nibName: "TextEditPopUp", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView
        }
       
        let options = [
            .type(.up),
            .corner_Radius(2),
            .animationIn(0.3),
            .blackOverlayColor(UIColor.clear),
            .color(.black),
            .dismissOnBlackOverlayTap(true),
            .arrowSize(CGSize.init(width: 20, height: 20))
            ] as [PopoverOption]
      
        self.popOver = Popover(options: options, showHandler: nil, dismissHandler: nil)
        self.popOver?.show(viewPopOver!, fromView: btn)
        
    }
    func getViewForEditText() -> UIView  {
        let tmpView = UINib(nibName: "TextEditPopUp", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        let btnFontType = tmpView.viewWithTag(101) as! UIButton
        let btnColor = tmpView.viewWithTag(102) as! UIButton
        let btnSize = tmpView.viewWithTag(103) as! UIButton
        let btnBackGround = tmpView.viewWithTag(104) as! UIButton
        let btnLink = tmpView.viewWithTag(105) as! UIButton
        btnFontType.addTarget(self, action: #selector(btnTextControls(_:)), for: .touchUpInside)
        btnColor.addTarget(self, action: #selector(btnTextControls(_:)), for: .touchUpInside)
        btnSize.addTarget(self, action: #selector(btnTextControls(_:)), for: .touchUpInside)
        btnBackGround.addTarget(self, action: #selector(btnTextControls(_:)), for: .touchUpInside)
        btnLink.addTarget(self, action: #selector(btnTextControls(_:)), for: .touchUpInside)
        return tmpView
    }
    func getViewForCameraOptions() -> UIView  {
        let tmpView = UINib(nibName: "CameraOptionPopUP", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
         let btnPhoto = tmpView.viewWithTag(200) as! UIButton
         let btnVideo = tmpView.viewWithTag(201) as! UIButton
         let btnLibrary = tmpView.viewWithTag(202) as! UIButton
        btnPhoto.addTarget(self, action: #selector(btnCameraAction(_:)), for: .touchUpInside)
        btnVideo.addTarget(self, action: #selector(recordVideo(_:)), for: .touchUpInside)
        btnLibrary.addTarget(self, action: #selector(btnPhotoLibraryAction(_:)), for: .touchUpInside)
        return tmpView
    }
    func getViewForTBOptions() -> UIView  {
        let tmpView = UINib(nibName: "TBOptionsPopUp", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        let btnimage = tmpView.viewWithTag(300) as! UIButton
        let btnVideo = tmpView.viewWithTag(301) as! UIButton
        btnimage.addTarget(self, action: #selector(btnImageClicked(sender:)), for: .touchUpInside)
        btnVideo.addTarget(self, action: #selector(btnVideoClicked(sender:)), for: .touchUpInside)
        //   btnLibrary.addTarget(self, action: #selector(btnShowGallery(_:)), for: .touchUpInside)
        return tmpView
    }
    func getViewForAddOptions() -> UIView  {
        let tmpView = UINib(nibName: "AddOptionPopUP", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        let btnCaptions = tmpView.viewWithTag(400) as! UIButton
        let bntSoundClip = tmpView.viewWithTag(401) as! UIButton
        let bntGifs = tmpView.viewWithTag(402) as! UIButton
        let bntGiphy = tmpView.viewWithTag(403) as! UIButton
        btnCaptions.addTarget(self, action: #selector(btnCaptionClicked(sender:)), for: .touchUpInside)
        bntSoundClip.addTarget(self, action: #selector(btnSounClipClicked(sender:)), for: .touchUpInside)
          bntGifs.addTarget(self, action: #selector(btnGifClicked(sender:)), for: .touchUpInside)
//        bntGiphy.addTarget(self, action: #selector(btnGiphyClicked(sender:)), for: .touchUpInside)
        bntGiphy.addTarget(self, action: #selector(btnGiphyClicked), for: .touchUpInside)
        //   btnLibrary.addTarget(self, action: #selector(btnShowGallery(_:)), for: .touchUpInside)
        return tmpView
    }
    //MARK:- UIButton Actions
    @IBAction func btnNextAction(_ sender:Any) {
        if AppSharedData.sharedInstance.selectedBlox != 8 {
            isNextBtn = true// Added 20Jan2021
            isVideoBloxSelected = false   // Added 20Jan2021
            btnDoneEditing(sender as! UIButton)        // Added 20Jan2021
        }
    }
    @IBAction func btnPreviousAction(_ sender:Any) {
        if AppSharedData.sharedInstance.selectedBlox != 0 {
//            notAllowedNextFrame = false
            isPrevBtn = true   // Added 20Jan2021
            isVideoBloxSelected = false   // Added 20Jan2021
            btnDoneEditing(sender as! UIButton)        // Added 20Jan2021
        }
        
    }
    //MARK:- Selector Methods
    @objc func btnImageClicked(sender:UIButton)  {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
         isGifs = false
        isVideoBloxSelected = false
        isImageGallery = true
        strBloxType = "GalleryImage"
        strGalleryType = GalleryType.Image as String
        arryTableOptionAllData.removeAllObjects()
        arryTableOption.removeAllObjects()
        self.tableOption.reloadData()
        tableOffset = 0
       
         btnShowGallery(sender)
    }
    @objc func btnVideoClicked(sender:UIButton)  {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
         isGifs = false
        strBloxType = "Video"
        strGalleryType = GalleryType.Video as String
        arryTableOptionAllData.removeAllObjects()
        arryTableOption.removeAllObjects()
        self.tableOption.reloadData()
        tableOffset = 0
        btnShowVideoGallery(sender)
    }
    @objc func btnCaptionClicked(sender:UIButton)  {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
         isGifs = false
        isVideoBloxSelected = false
        showCaptionOption()
    }
    @objc func btnSounClipClicked(sender:UIButton)  {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
         isGifs = false
//        strBloxType = "Camera"
        if isVideoBloxSelected == false
        {
            if (imgGallery.image?.size.height == 0 && txtViewMessage.text == "")
            {
                showAlert("Please add Text or Image for adding audio", strTitle: "Audio alert")
            }
            else
            {
                isImageGallery = false
                strGalleryType = GalleryType.Sound as String
                arryTableOptionAllData.removeAllObjects()
                arryTableOption.removeAllObjects()
                self.tableOption.reloadData()
                tableOffset = 0
                btnShowAudioGallery(sender)
            }
        }
    }
    
    @objc func btnGifClicked(sender:UIButton)  {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
        strBloxType = "GalleryImage"
        isGifs = true
        self.collectionGallery.isHidden = false
        self.collectionGallery.reloadData()
        btnShowGifsGallery(sender)
        

    }
    
//    @objc func btnGiphyClicked(sender:UIButton)  {
    @objc func btnGiphyClicked()  {
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
        strBloxType = "GalleryImage"
        isGifs = true
//        self.collectionGallery.isHidden = false
//        self.collectionGallery.reloadData()
        
        strColorFor = ""
        
        self.hideKeyboard()
        self.hideAllView()
//        self.layoutBottomMenu.constant = 210
        self.txtViewMessage.resignFirstResponder()
//        self.viewGallery.isHidden = false
//        self.imgGallery.isHidden = false
        self.tableOption.isHidden = true
//        self.collectionGallery.isHidden = false
        self.strGalleryType = GalleryType.Gif as String
        
        let giphy = GiphyViewController()
        giphy.theme = GPHTheme(type: GPHThemeType.light)
        giphy.mediaTypeConfig = GPHContentType.defaultSetting
        GiphyViewController.trayHeightMultiplier = 0.7
        giphy.showConfirmationScreen = false
        giphy.shouldLocalizeSearch = true
        giphy.delegate = self
        giphy.dimBackground = true
        giphy.modalPresentationStyle = .overCurrentContext
        
//        if let contentType = self.selectedContentType {
//            giphy.selectedContentType = contentType
//        }
//        if let user = self.showMoreByUser {
//            giphy.showMoreByUser = user
//        }
        
        present(giphy, animated: true, completion: nil)
    }
    // ****** Enter means end editing hide keyboard ********
    
    var numberOfLine : NSInteger = 1
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
 
        
        
        
        //var labelsize: CGSize
        var textFromTextView: String = textView.text

        textFromTextView = String(format: "%@%@", textFromTextView,text)

        for i in 1 ..< numberOfLine
        {
            textFromTextView = String(format: "%@\(i)", textFromTextView,text)
        }
        
        //labelsize = text.sizeWithFont(textView.font, constrainedToSize: CGSizeMake(280, 2000.0), lineBreakMode: NSLineBreakMode.ByWordWrapping)// UILineBreakMode.WordWrap)
        
        let constraintRect = CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = textFromTextView.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: textView.font!], context: nil)

        print(boundingBox.height)
      
        print(textView.frame.size.height)
        
        if boundingBox.height < textView.frame.size.height
        {
            //show text with 300 height
            return true
        }
        else
        {
            //show full text
            if(fontSize == 90)
            {
                self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 60)
                self.fontSize = 60
            }
            else if(fontSize == 60)
            {
            self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 30)
            self.fontSize = 30
            }
            else if(fontSize == 30)
            {
            self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 18)
            self.fontSize = 18
            }
            return false
        }
        
        
        /*
        labelsize =
           CGSize boundingBox = [textView.font boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        var boundingBox: CGSize = textView.font.boundingRectWithSize(constraint, options: .UsesLineFragmentOrigin, attributes: attributes, context: nil).size

            
        .boundingRectWithSize(CGSizeMake(width, 2000), options:NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: tempView.font], context: context).size as CGSize
        */
        /*
        NSLog("%f", labelsize.height)
        if labelsize.height > 300 {
            //show text with 300 height
        }
        else {
            //show full text
        }
        */
        
        
        /*
        CGSize labelsize;
        NSString *text=@"your text";
        [textView setFont:[UIFont fontWithName:@"Helvetica"size:14]];
        labelsize=[text sizeWithFont:textView.font constrainedToSize:CGSizeMake(280, 2000.0) lineBreakMode:UILineBreakModeWordWrap];
        NSLog(@"%f",labelsize.height);
        if(labelsize.height>300)
        {
            //show text with 300 height
        }
        else
        {
            //show full text
        }
        */
        
    }
    
    
    //MARK: Keyboard Hide Show Methods
    @objc func keyboardWillShow(_ notification: Notification)
    {
        isKeyboardeOpened = true
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            {
                print(contentInsets)
                
//                layoutBottomMenu.constant += contentInsets.bottom
            }
            else
            {
                // no UIKeyboardFrameBeginUserInfoKey entry in userInfo
            }
        }
        else
        {
            // no userInfo dictionary in notification
        }
        
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        //
        isKeyboardeOpened = false
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            {
                txtFieldCaption.isHidden = true
                if txtFieldCaption.text != "" {
                    lblCaption.text = txtFieldCaption.text
                    lblCaption.isHidden = false
                }else{
                    lblCaption.isHidden = true

                }
                hideKeyboard()
               
                layoutBottomMenu.constant = 0
//                contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                // ...
                print(contentInsets)
                
                
                
                //layoutBottomMenu.constant -= contentInsets.bottom
            }
            else
            {
                // no UIKeyboardFrameBeginUserInfoKey entry in userInfo
            }
        }
        else
        {
            // no userInfo dictionary in notification
        }

    }
    
    //MARK: Text view options
    @IBAction func btnTextControls (_ sender : UIButton)
    {
        
        if self.popOver != nil {
            self.popOver?.dismiss()
        }
        
        self.txtViewMessage.isHidden = false
        self.imgGallery.isHidden = true
        
        switch sender.tag
        {
        case 101:
            fontPickerView.isHidden = false
            fontPickerView.reloadAllComponents()
            fontPickerView.selectRow(arrayContainFontFamily.index(of: strFontFamilyName), inComponent: 0, animated: true)
            
            btnDone.isHidden = false
            break
            
        case 102:
            strColorFor = "Text"
            //self.performSegue(withIdentifier: "showColorPicker", sender: self)
            self.viewTextFormating.isHidden = false
            colorCollectionView.isHidden = false
            break
            
        case 103:
            let alertController = UIAlertController(title: "Select Font Size", message: "", preferredStyle: .actionSheet)
            let extrLargeAction = UIAlertAction(title: "Extra Large", style: .default)
            { (_) in
                if(self.strFontFamilyName == "")
                {
                    self.strFontFamilyName = "Arial Rounded MT Bold"
                }
                self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 90)
                self.fontSize = 90
                //self.txtViewMessage.scrollIndicatorInsets = UIEdgeInsetsMake(-10,0,10,0)
                
            }
            let largeAction = UIAlertAction(title: "Large", style: .default)
                { (_) in
                    if(self.strFontFamilyName == "")
                    {
                        self.strFontFamilyName = "Arial Rounded MT Bold"
                    }
                    self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 60)
                    self.fontSize = 60
                    //self.txtViewMessage.scrollIndicatorInsets = UIEdgeInsetsMake(-10,0,10,0)

            }
            let mediumAction = UIAlertAction(title: "Medium", style: .default)
                { (_) in
                    if(self.strFontFamilyName == "")
                    {
                        self.strFontFamilyName = "Arial Rounded MT Bold"
                    }
                    self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 30)
               self.fontSize = 30
            }
            let smallAction = UIAlertAction(title: "Small", style: .default)
                { (_) in
                    if(self.strFontFamilyName == "")
                    {
                        self.strFontFamilyName = "Arial Rounded MT Bold"
                    }
                    self.txtViewMessage.font = UIFont(name: self.strFontFamilyName as String, size: 18)
                    self.fontSize = 18
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                { (_) in }
            alertController.addAction(extrLargeAction)
            alertController.addAction(largeAction)
            alertController.addAction(mediumAction)
            alertController.addAction(smallAction)
            alertController.addAction(cancelAction)
            
            alertController.view.backgroundColor = UIColor.white
            //AppTheme().drawBorder(alertController.view, color: AppTheme().themeColorBlue, borderWidth: 3.0, cornerRadius: 12.0)
            
            present(alertController, animated: true, completion: nil)
            
            break
            
        case 104:
            //New condition for Bg Color and Bg Image
            let alertController = UIAlertController(title: "Select Background Type", message: "", preferredStyle: .actionSheet)
           let largeAction = UIAlertAction(title: "Image", style: .default)
            { (_) in
                
                self.strColorFor = "BackgroundImage"
                self.strTextBgType = "BackgroundImage"
                self.strGalleryType = GalleryType.Backgroundimage as String
                self.txtViewMessage.backgroundColor = UIColor.clear
                self.arryTableOptionAllData.removeAllObjects()
                self.arryTableOption.removeAllObjects()
             //   self.tableOption.reloadData()
              //  self.tableOffset = 0
               // self.loadGalleryData()
                self.viewGallery.isHidden = true
                self.viewTextFormating.isHidden = true
                
                self.strGalleryType = GalleryType.Backgroundimage as String
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxBackgroundVC") as! BloxBackgroundVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.modalTransitionStyle = .crossDissolve
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                
                
             //   self.galleryFilterbyType()
                
            }
            let mediumAction = UIAlertAction(title: "Color", style: .default)
            { (_) in
                self.strTextBgType = "BackgroundColor"
                self.strColorFor = "BackgroundColor"
//                self.txtViewMessage.backgroundColor = UIColor.white
                self.viewTextFormating.isHidden = false
                self.colorCollectionView.isHidden = false
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            { (_) in }
            
           alertController.addAction(largeAction)
            alertController.addAction(mediumAction)
            alertController.addAction(cancelAction)
            alertController.view.backgroundColor = UIColor.white
            present(alertController, animated: true, completion: nil)
            /*
            imgGallery.image = screenShot()
            imgGallery.hidden = false
            txtViewMessage.hidden = true
            */
            //self.performSegueWithIdentifier("showColorPicker", sender: self)
            //colorCollectionView.hidden = false
            
            // new code for showing image for backgound instead of color
            break
        case 105:
            isLink = true
            let clickedHexColor = "4BC1FF"//"1476FF" //self.colorList[indexPath.row]
            
            // convert hex color to UI Color
            let clickedUIColor = Utilities.shared.convertHexToUIColor(hexColor: clickedHexColor)
            
//            if(strColorFor == "Text")
//            {
                self.viewTextFormating.isHidden = true
                txtViewMessage.textColor = clickedUIColor
                textColor = clickedHexColor
//            }
        default:
            break
        }
    }
    
    func colorPickerDidColorSelected(selectedUIColor: UIColor, selectedHexColor: String)
    {
        if(strColorFor == "Text")
        {
            txtViewMessage.textColor = selectedUIColor
            textColor = selectedHexColor
          
        }
//        else if(strColorFor == "Background")
//        {
//            txtViewMessage.backgroundColor = selectedUIColor
//            backgroundColor = selectedUIColor
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showColorPicker")
        {
            let objColorPicker : ColorPickerViewController = segue.destination as! ColorPickerViewController
            objColorPicker.colorPickerDelegate = self
        }
    }
    
    func hideAllForCamera()
    {
        viewTextFormating.isHidden = true
        viewGallery.isHidden = true
        txtFieldCaption.isHidden = true
        viewSquareBlox.isHidden = false
    }
    
    func hideAllView()
    {
        btnPlayVideo.isHidden = true
//        if isKeyboardeOpened {
//            layoutBottomMenu.constant = layoutBottomMenuHeight
//            print("layoutBottomMenuHeight ",layoutBottomMenuHeight)
//            print("viewBottomOptionFrame ",viewBottomOption.frame)
//        } else {
//            layoutBottomMenu.constant = 210
//            print("viewBottomOptionFrame ",viewBottomOption.frame)
//        }
        //imageLayout.constant = 70
        viewTextFormating.isHidden = true
        viewGallery.isHidden = true
        viewCameraOptions.isHidden = true
        //viewSquareBlox.hidden = true
        if(isSearchFieldShifted)
        {
            var rectFrame : CGRect = searchBar.frame
            rectFrame.origin.y += 80
            searchBar.frame = rectFrame
            isSearchFieldShifted = false
            //txtSearch.resignFirstResponder()
        }
        //self.view.endEditing(true)
    }
    
    func hideKeyboard()
    {
        txtFieldCaption.resignFirstResponder()
        txtViewMessage.resignFirstResponder()
        if(isSearchFieldShifted)
        {
            var rectFrame : CGRect = searchBar.frame
            rectFrame.origin.y += 80
            searchBar.frame = rectFrame
            isSearchFieldShifted = false
            txtSearch.resignFirstResponder()
        }
        //self.view.endEditing(true)
    }
    
    
    
    
    //MARK: Table Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if shouldShowSearchResults {
            return filteredArray.count
        }else{
            return arryTableOption.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        /*
        if let dict : NSDictionary = arryTableOption.objectAtIndex(indexPath.row) as? NSDictionary
        {
            if(dict.valueForKey("message") as? String == "From talkbox")
            {
                if ((dict.valueForKey("is_active")as! Bool))
                {
                    return 30
                }
                else { return 40 }
            }
            else { return 40 }
        }
        else
        { return 30 }
        */
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath)
        
        //print(arryTableOption.objectAtIndex(indexPath.row))
        cell.textLabel?.text = arryTableOption.object(at: indexPath.row) as? String
        cell.textLabel?.textAlignment = NSTextAlignment.center
        cell.textLabel?.textColor = AppTheme().themeColorBlue
        cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        if shouldShowSearchResults {
            if let dict : NSDictionary = filteredArray.object(at: indexPath.row) as? NSDictionary {
                if(dict.value(forKey: "message") as? String == "From talkbox")
                {
                    
                    if ((dict.value(forKey: "is_active")as! Bool))
                    {
                        cell.textLabel?.text = dict.value(forKey: "name") as? String
                        cell.textLabel?.text!.capitalized
                        cell.textLabel?.textAlignment = NSTextAlignment.center
                        cell.detailTextLabel?.text = ""
                        cell.detailTextLabel?.isHidden = true
                        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
                    }
                    else
                    {
                        cell.textLabel?.text = dict.value(forKey: "name") as? String
                        cell.textLabel?.text!.capitalized
                        cell.detailTextLabel?.text = "Not Active"
                        cell.detailTextLabel?.isHidden = false
                        cell.textLabel?.textAlignment = NSTextAlignment.center
                        cell.detailTextLabel?.textAlignment = NSTextAlignment.center
                    }
                }
                else
                {
                    print("In else")
                    cell.textLabel?.text = dict.value(forKey: "name") as? String
                    cell.textLabel?.text!.capitalized
                    cell.textLabel?.textAlignment = NSTextAlignment.center
                    cell.detailTextLabel?.text = ""
                    cell.detailTextLabel?.isHidden = true
                    
                }

            }
        }
        
        else {
            
            
        if let dict : NSDictionary = arryTableOption.object(at: indexPath.row) as? NSDictionary
        {
            if(dict.value(forKey: "message") as? String == "From talkbox")
            {
            
            if ((dict.value(forKey: "is_active")as! Bool))
            {
                cell.textLabel?.text = dict.value(forKey: "name") as? String
                cell.textLabel?.text!.capitalized
                cell.textLabel?.textAlignment = NSTextAlignment.center
                cell.detailTextLabel?.text = ""
                cell.detailTextLabel?.isHidden = true
                cell.detailTextLabel?.textAlignment = NSTextAlignment.center
            }
            else
            {
                cell.textLabel?.text = dict.value(forKey: "name") as? String
                cell.textLabel?.text!.capitalized
                cell.detailTextLabel?.text = "Not Active"
                cell.detailTextLabel?.isHidden = false
                cell.textLabel?.textAlignment = NSTextAlignment.center
                cell.detailTextLabel?.textAlignment = NSTextAlignment.center
            }
            }
            else if  dict.value(forKey: "gal_alb_name") as? String != nil{
                cell.textLabel?.text = dict.value(forKey: "gal_alb_name") as? String
                cell.textLabel?.text!.capitalized
                cell.textLabel?.textAlignment = NSTextAlignment.center
                cell.detailTextLabel?.text = ""
                cell.detailTextLabel?.isHidden = true
            }else  {
                print("In else")
                cell.textLabel?.text = dict.value(forKey: "gal_cat_name") as? String
                cell.textLabel?.text!.capitalized
                cell.textLabel?.textAlignment = NSTextAlignment.center
                cell.detailTextLabel?.text = ""
                cell.detailTextLabel?.isHidden = true
          
                /*  cell.textLabel?.text = dict.value(forKey: "name") as? String
                cell.textLabel?.text!.capitalized
                cell.textLabel?.textAlignment = NSTextAlignment.center
                cell.detailTextLabel?.text = ""
                cell.detailTextLabel?.isHidden = true*/
                
            }
            
            
        }
    }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if shouldShowSearchResults {
            if let dict: NSDictionary = filteredArray.object(at: indexPath.row) as? NSDictionary {
                if(dict.value(forKey: "message") as? String == "From talkbox")
                {
                    if ((dict.value(forKey: "is_active")as! Bool))
                    {
                        //print(dict)
                        if let strId: NSInteger = dict.value(forKey: "id") as? NSInteger
                        {
                            strGalleryId = String(strId)
                            arryGalleryCollection.removeAllObjects()
                            
                            //-- Adding last selected gallery to user default
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(strGalleryId, forKey: strGalleryType)
                            userDefaults.synchronize()
                            //-----
                            //collectionGallery.hidden = false
                            
                            loadCollectionData()
                        }
                    }
                    else
                    {
                        let errorAlert = AppTheme().showAlert("Gallery is not Active", errorTitle: "Alert")
                        
                        self.present(errorAlert, animated: true, completion: nil)
                    }
                } else {
                    //print(dict)
                    
                    let strId = dict.value(forKey: "id")! as! String
                    let mediaId = Int(strId)!
                    
                    strGalleryId = String(mediaId)
                    arryGalleryCollection.removeAllObjects()
                    
                    //collectionGallery.hidden = false
                    //-- Adding last selected gallery to user default
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(strGalleryId, forKey: strGalleryType)
                    userDefaults.synchronize()
                    //-----
                    loadCollectionData()
                    
                }
                
            }
        }
        else {
            if let dict: NSDictionary = arryTableOption.object(at: indexPath.row) as? NSDictionary
            {
                
                if(dict.value(forKey: "message") as? String == "From talkbox")
                {
                    if ((dict.value(forKey: "is_active")as! Bool))
                    {
                        //print(dict)
                        if let strId: NSInteger = dict.value(forKey: "id") as? NSInteger
                        {
                            strGalleryId = String(strId)
                            arryGalleryCollection.removeAllObjects()
                            
                            //-- Adding last selected gallery to user default
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(strGalleryId, forKey: strGalleryType)
                            userDefaults.synchronize()
                            //-----
                            //collectionGallery.hidden = false
                            
                            loadCollectionData()
                        }
                    }
                    else
                    {
                        let errorAlert = AppTheme().showAlert("Gallery is not Active", errorTitle: "Alert")
                        
                        self.present(errorAlert, animated: true, completion: nil)
                    }
                } else {
                    //print(dict)
                    if dict.value(forKey: "gal_cat_id") as? Int != nil {
                        let categoryID = dict.value(forKey: "gal_cat_id")! as! Int
                        self.getImagesFromCategory(categoryID)
                    } else if dict.value(forKey: "gal_alb_id") as? Int != nil {
                        let albumId = dict.value(forKey: "gal_alb_id")! as! Int
                        
                        if strGalleryType == GalleryType.Image as String {
                            self.getCategoryFrom(galleryType: "2", albumID: "\(albumId)")
                        } else if strGalleryType == GalleryType.Video as String {
                            self.getCategoryFrom(galleryType: "1", albumID: "\(albumId)")
                        } else {
                            self.getCategoryFrom(galleryType: "3", albumID: "\(albumId)")
                        }
                        
                    }
                }
                
            }
        }
    }

    @IBAction func BtnLoadMoreData(_ sender : UIButton)
    {
        loadGalleryData()
    }

    
    func loadCollectionData(){
        
        let params:NSDictionary = ["authentication_Token":AppSharedData.sharedInstance.getUser()!.authentication_token!,"galleryID": strGalleryId]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.GetGalleryDetail, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    
                    if(response?["medias"] != nil){
                        
                        if let ary : NSArray = response?["medias"] as? NSArray
                        {
                            print(ary)
                            if(ary.count > 0)
                            {
                                self.collectionGallery.isHidden = false
                                self.arryGalleryCollection = ary.mutableCopy() as! NSMutableArray
//                                self.isGifs = false
                                self.collectionGallery.reloadData()
                            }
                            else
                            {
                                let errorAlert = AppTheme().showAlert("Gallery is empty", errorTitle: "Gallery Alert")
                                
                                self.present(errorAlert, animated: true ,completion: nil)
                                self.collectionGallery.isHidden = true
                            }
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                        }
                        else
                        {
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                        }
                    }
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
                
            }
        })
}
    
    //MARK: Collection View Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(collectionView == colorCollectionView)
        {
            return colorList.count
        }
        else
        {
            if(isGifs){
                return gifsArray.count
            }else{
        //print(aryGalleryItem.count + 1)
        return arryGalleryCollection.count//aryGalleryItem.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if(collectionView == colorCollectionView)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath)
            
            // set cell background color from given color list
            cell.backgroundColor = Utilities.shared.convertHexToUIColor(hexColor: self.colorList[indexPath.row])
            
            // return cell
            return cell
        }
        else
        {
             if(isGifs){
                 let cell: GalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
                let imageName = gifsArray[indexPath.row]
//                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: imageName, withExtension: "gif")!)
//                galleryImageData = imageData ?? Data()
                cell.imgItem.image =  UIImage.gifImageWithName(imageName)
                return cell
             }else{
            let cell: GalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
            //cell.imgItem.image = aryGalleryItem.objectAtIndex(indexPath.row) as? UIImage
            //
            cell.imgItem.image = UIImage(named: "Talkblox.png")
            if let dict: NSDictionary = arryGalleryCollection.object(at: indexPath.row) as? NSDictionary
            {
                
                if(strGalleryType == GalleryType.Image as String)
                {
                    if var strDefaultUrl: String = dict.value(forKey: "gc_path") as? String
                    {
                        strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                        if(strDefaultUrl == "")
                        {
                            cell.imgItem.image = UIImage(named: "SingleBlox")
                        }
                        else
                        {
                   
                            cell.imgItem.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultUrl)!, placeholder: UIImage(named: "SingleBlox"), options: nil)
                        }
                        if let str: NSString = dict.value(forKey: "url") as? NSString
                        {
                            // cell.lblTittleText.text = str.lastPathComponent
                            //cell.lblTittleText.isHidden = false
                        }
                    }
                } else if (strGalleryType == GalleryType.Video as String) {
                    if var strDefaultUrl: String = dict.value(forKey: "gc_path") as? String{
                        strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                       if(strDefaultUrl == "")
                       {
                           cell.imgItem.image = UIImage(named: "SingleBlox")
                       }
                       else
                       {
                        cell.imgItem.image = AppTheme().getImageFromUrl(strDefaultUrl)
                       }
                    }
                    else if var strDefaultUrl: String = dict.value(forKey: "gal_col_thumb") as? String
                    {
                         strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                        if(strDefaultUrl == "")
                        {
                            cell.imgItem.image = UIImage(named: "SingleBlox")
                        }
                        else
                        {
                            cell.imgItem.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultUrl)!, placeholder: UIImage(named: "SingleBlox"), options: nil)
                        }
                        if let str: NSString = dict.value(forKey: "url") as? NSString
                        {
                            // cell.lblTittleText.text = str.lastPathComponent
                            //cell.lblTittleText.isHidden = false
                        }
                    }
                } else if (strGalleryType == GalleryType.Sound as String) {
                    if var strDefaultUrl: String = dict.value(forKey: "gal_col_thumb") as? String
                    {
                         strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                        if(strDefaultUrl == "")
                        {
                            cell.imgItem.image = UIImage(named: "SingleBlox")
                        }
                        else
                        {
                            cell.imgItem.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultUrl)!, placeholder: UIImage(named: "SingleBlox"), options: nil)
                        }
                        if let str: NSString = dict.value(forKey: "url") as? NSString
                        {
                            // cell.lblTittleText.text = str.lastPathComponent
                            //cell.lblTittleText.isHidden = false
                        }
                    }
                }
                
            }
            cell.imgItem.contentMode = UIView.ContentMode.scaleAspectFill
            cell.imgItem.layer.borderColor = AppTheme().themeColorBlue.cgColor
            cell.imgItem.layer.borderWidth = 1.0
            return cell
        }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if(collectionView == colorCollectionView)
        {
            let clickedHexColor = self.colorList[indexPath.row]
            
            // convert hex color to UI Color
            let clickedUIColor = Utilities.shared.convertHexToUIColor(hexColor: clickedHexColor)
            
            if(strColorFor == "Text")
            {
                self.viewTextFormating.isHidden = true
                txtViewMessage.textColor = clickedUIColor
                textColor = clickedHexColor
            }
            else if(strColorFor == "BackgroundColor")
            {
                self.viewTextFormating.isHidden = true
                txtViewMessage.backgroundColor = clickedUIColor
                backgroundColor = clickedUIColor
            }
            
            // close color picker view
            self.closeColorPicker()
        }
        else
        {
            if(isGifs){
                let imageName = gifsArray[indexPath.row]
              imgGallery.image = UIImage.gifImageWithName(imageName)
                
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: imageName, withExtension: "gif")!)
                galleryImageData = imageData ?? Data()
                 txtViewMessage.isHidden = true
                 isVideoBloxSelected = false
                
                aryMediaImageData.removeAllObjects()   // 1Mar2021
                aryMediaImageData.add(galleryImageData)
                
                //LoadingIndicatorView.show("Loading...")
                LoadingIndicatorView.show("Loading...")
                
                NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Gif as String, arrDataToSend: aryMediaImageData, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
                    LoadingIndicatorView.hide()
//                   self.loading.dismiss()
                    
                    if success == true {
                        let detail = response?["result"] as! NSArray
                        if detail.count > 0 {
                            let dictDetail = detail[0] as! NSDictionary
                            self.strMediaId = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
                        }
                        
                        self.bloxType =  GalleryType.Gif
                        //self.delegate?.bloxDetails(detail, true)
                        // self.saveDictionaryInUserDefault(detail as! [String : AnyObject])
                       // self.saveDictionaryInUserDefault(detail as! [String : AnyObject])

                     /*   if statusDic["ErrorCode"] as! Int == ErroCode.Succes {
                            
                        }*/
                    }
                })
                
            }else{
            if(strColorFor == "BackgroundImage")
            {
                
                if let dict: NSDictionary = arryGalleryCollection.object(at: indexPath.row) as? NSDictionary
                {
                    let cell: GalleryCell = collectionView.cellForItem(at: indexPath) as! GalleryCell
                    if var strDefaultUrl: String = dict.value(forKey: "gc_path") as? String
                    {
                        // strMediaId = dict.value(forKey: "id") as! String
                        
                        strGalleryType = dict.value(forKey: "mediaType") as! String
                        strBgImgMediaId = dict.value(forKey: "gc_id") as! String as NSString
                        
                        if(strGalleryType == GalleryType.Backgroundimage as String)
                        {
                            if(strDefaultUrl != "")
                            {
                                 strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                                loading.dismiss()
                                
                                let urlStr: NSString = strDefaultUrl.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)! as NSString
                                let imgURL = URL(string: WEB_URL.BaseURL + (urlStr as String) as String)
                                
                                imgTextBgImage.kf.setImage(with: imgURL, placeholder: cell.imgItem.image, options: nil)
                                imgTextBgImage.isHidden = false
                                txtViewMessage.backgroundColor = UIColor.clear
                                //txtViewMessage.backgroundColor = UIColor(patternImage: imgTemp.image!)
                                //imgGallery.kf_setImageWithURL(NSURL(string: strDefaultUrl)!, placeholderImage: cell.imgItem.image)
                            }
                            else
                            {
                                loading.dismiss()
                                imgTextBgImage.image = cell.imgItem.image
                                imgTextBgImage.isHidden = false
                                txtViewMessage.backgroundColor = UIColor.clear
                                //txtViewMessage.backgroundColor = UIColor(patternImage: cell.imgItem.image!)
                                //imgGallery.image = cell.imgItem.image
                            }
                        }
                    }
                }
            }
            else
            {
                if let dict: NSDictionary = arryGalleryCollection.object(at: indexPath.row) as? NSDictionary
                {
                    let cell: GalleryCell = collectionView.cellForItem(at: indexPath) as! GalleryCell
                    if var strDefaultUrl: String = dict.value(forKey: "gc_path") as? String
                    {
                         strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                        if(strGalleryType == GalleryType.Image as String)
                        {
                            strMediaId = "\(dict.value(forKey: "gc_id") as! Int)" as NSString
                            if let gallaryType = dict.value(forKey: "mediaType") as? String {
                                strGalleryType = gallaryType
                            }
                            
                            // strMediaId = dict.value(forKey: "id") as! String as NSString
                            isVideoBloxSelected = false
                            
                            let im1 = cell.imgItem.image
                            let im2 = imgGallery.image
                            
                            if(strDefaultUrl != "")
                            {
                                loading.dismiss()
                                
                                imgGallery.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultUrl)!, placeholder: cell.imgItem.image, options: nil)
                                imgGallery.image = cell.imgItem.image
                            }
                            else
                            {
                                loading.dismiss()
                                imgGallery.image = cell.imgItem.image
                            }
                            txtViewMessage.isHidden = true
                        }
                        else if(strGalleryType == GalleryType.Video as String)
                        {
                            strMediaId = "\(dict.value(forKey: "gc_id") as! Int)" as NSString
                            if let gallaryType = dict.value(forKey: "mediaType") as? String {
                                strGalleryType = gallaryType
                            }
                            isVideoBloxSelected = true
                            if(strDefaultUrl != "")
                            {
                                 strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                                if let strDefaultImageUrl: String = dict.value(forKey: "gal_col_thumb") as? String
                                {
                                    
                                    loading.dismiss()
                                    
                                    imgGallery.kf.setImage(with: URL(string: strDefaultImageUrl)!, placeholder: cell.imgItem.image, options: nil)
                                    
                                }
                                else
                                {
                                    loading.dismiss()
                                    imgGallery.image = cell.imgItem.image
                                }
                                
                                isVideoFromTalkBlox = true
                                
//                                self.loading.show(in: self.view)
                                LoadingIndicatorView.show("Loading...")
                                downloadSelectedVideo(WEB_URL.BaseURL + strDefaultUrl)
                                
                            }
                            else
                            {
                                loading.dismiss()
                                imgGallery.image = cell.imgItem.image
                            }
                            txtViewMessage.isHidden = true
                        }
                        else if(strGalleryType == GalleryType.Sound as String)
                        {
                            isVideoBloxSelected = false
                            
                            print("sound file selected")
                              strBgMediaId = "\(dict.value(forKey: "gc_id") as! Int)" as NSString
                            if var strDefaultImageUrl: String = (dict.value(forKey: "gal_col_thumb") as? String) {
                                strDefaultImageUrl = strDefaultImageUrl.replacingOccurrences(of: " ", with: "%20")
                                self.audioThumbPath = strDefaultImageUrl
                            }
                            if(strDefaultUrl != "")
                            {
//                                self.loading.show(in: self.view)
                                LoadingIndicatorView.show("Loading...")
                                //downloadSelectedAudio(strDefaultUrl)
                                //strMediaId = dict.value(forKey: "id") as! String
                                
                                var pathComponent = "bloxAudio0.caf"
                                if((self.bloxArrayIndex) != nil)
                                {
                                    let strIndex: String = "bloxAudio\(self.bloxArrayIndex ?? 0).caf"
                                    if strIndex != nil
                                    {
                                        print(strIndex)
                                        
                                        pathComponent = String(format: "bloxAudio\(String(describing: self.bloxArrayIndex)).caf")
                                    }
                                }
//                                self.trim(strDefaultUrl: WEB_URL.BaseURL + strDefaultUrl)
                                
                                AppTheme().callGetServiceForDownload((WEB_URL.BaseURL + strDefaultUrl), fileNameToBeSaved: pathComponent, completion: { (result, data) in
                                    
                                    if(result == "success")
                                    {
                                        print("video saved \(pathComponent)")
                                        
//                                        self.loading.dismiss()
                                        LoadingIndicatorView.hide()
                                        
                                     //   AppTheme().getFilePathURL(self.audioUrlPath)
                                         self.audioUrlPath = pathComponent
                                        self.strAudioURLPath = (WEB_URL.BaseURL + strDefaultUrl)
                                       
                                        self.incrSecond = 0
                                        
                                        if((self.meterTimer) != nil)
                                        {
                                            self.meterTimer?.invalidate()
                                        }
                                        if((self.animationTimer) != nil)
                                        {
                                            self.animationTimer?.invalidate()
                                        }
                                        
                                        self.viewRecorder.isHidden = false
                                        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
                                            
                                            self.viewRecorderBottomLayout.constant = 0
                                            self.view
                                                .layoutIfNeeded()
                                            
                                        })
                                        { (finished) -> Void in
                                            LoadingIndicatorView.show()
                                            self.playAudio(self.strAudioURLPath)
                                            self.incrSecond = 0
                                          //  self.playAudio(strDefaultUrl)
                                        }
                                    }
                                    else
                                    {
//                                        self.loading.dismiss()
                                        LoadingIndicatorView.hide()
                                        self.showAlert(data as! String, strTitle: "Error")
                                    }
                                    
                                    
                                })
                                
                            }
                        }
                    }
                    let im3 = cell.imgItem.image
                    let im4 = imgGallery.image
                }
                imgGallery.contentMode = UIView.ContentMode.scaleAspectFit//scaleToFill
                imgGallery.isHidden = false
                
                
            }
        }
        }

    }
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadSelectedAudio(_ strDefaultUrl:String)
    {
        var pathComponent = "bloxAudio0.caf"
        print("sound file url found")
        //print("self.bloxArrayIndex\(self.bloxArrayIndex)")
        if((self.bloxArrayIndex ) != nil)
        {
            print("%@",self.bloxArrayIndex)
            let strIndex : String = "bloxAudio\(self.bloxArrayIndex).caf"
//            if img != nil
//            {
                print(strIndex)
//            }
            pathComponent = String(format:"bloxAudio\(self.bloxArrayIndex).caf")
        }
            print("sound file path created \(pathComponent)")
            
            //AppTheme().killFileOnPath(pathComponent)
            AppTheme().callGetServiceForDownload(strDefaultUrl, fileNameToBeSaved: pathComponent, completion: { (result, data) in
//                self.loading.dismiss()
                LoadingIndicatorView.hide()
                if(result == "success")
                {
                    self.audioUrlPath = data as! String
                }
                else
                {
                    print(result)
                    print(data)
                    self.showAlert(data as! String, strTitle: "Error")

                }
            })
    }
    
    
    func trim(strDefaultUrl:String) {
        let url =  URL(string: strDefaultUrl)
        let asset = AVAsset(url: url!)
            exportAsset(asset, fileName: "trimmed.m4a")
      
      
    }
    
    func exportAsset(_ asset: AVAsset, fileName: String) {
        print("\(#function)")
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
        print("saving to \(trimmedSoundFileURL.absoluteString)")
        
        if FileManager.default.fileExists(atPath: trimmedSoundFileURL.absoluteString) {
            print("sound exists, removing \(trimmedSoundFileURL.absoluteString)")
            do {
                if try trimmedSoundFileURL.checkResourceIsReachable() {
                    print("is reachable")
                }
                
                try FileManager.default.removeItem(atPath: trimmedSoundFileURL.absoluteString)
            } catch {
                print("could not remove \(trimmedSoundFileURL)")
                print(error.localizedDescription)
            }
            
        }
        
        print("creating export session for \(asset)")
        
        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) {
            exporter.outputFileType = AVFileType.m4a
            exporter.outputURL = trimmedSoundFileURL
            
            let duration = CMTimeGetSeconds(asset.duration)
            if duration < 5.0 {
                print("sound is not long enough")
                return
            }
            // e.g. the first 5 seconds
            let startTime = CMTimeMake(value: 0, timescale: 1)
            let stopTime = CMTimeMake(value: 5, timescale: 1)
            exporter.timeRange = CMTimeRangeFromTimeToTime(start: startTime, end: stopTime)
            
            // do it
            exporter.exportAsynchronously(completionHandler: {
                print("export complete \(exporter.status)")
                
                switch exporter.status {
                case  AVAssetExportSessionStatus.failed:
                    
                    if let e = exporter.error {
                        print("export failed \(e)")
                    }
                    
                case AVAssetExportSessionStatus.cancelled:
                    print("export cancelled \(String(describing: exporter.error))")
                default:
                    print("export complete")
                    print(trimmedSoundFileURL)
                    print(trimmedSoundFileURL.absoluteString)
                }
            })
        } else {
            print("cannot create AVAssetExportSession for asset \(asset)")
        }
        
    }
    
    
    
    
    
    func downloadSelectedVideo(_ strURL : String)
    {
        var pathComponent = "bloxVideo0.mp4"
        if((self.bloxArrayIndex ) != nil)
        {
            print("%@",self.bloxArrayIndex)
        let strIndex : String = "bloxVideo\(self.bloxArrayIndex).mp4"
//            {
                print(strIndex)
//            }
            pathComponent = String(format:"bloxVideo\(self.bloxArrayIndex).mp4")
        }
        
        //---- 12/11/16 New method getting crash in iOS 10 with old method
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
        {
            // do some task
                AppTheme().callGetServiceForDownload(strURL, fileNameToBeSaved: pathComponent,
                                                     completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async
                        {
                            // update some UI
                            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                            let localPath: URL = directoryURL.appendingPathComponent(data as! String)
                            
                            self.videoUrlPath = String(describing: localPath) as NSString
                            //self.showAlert("Downloaded file to route:- \(localPath)", strTitle: "Video Download Alert")
                            let urlOfVideo : URL = localPath
                            let asset : AVURLAsset = AVURLAsset(url: urlOfVideo) as AVURLAsset
                            let duration : CMTime = asset.duration
                            //let rounded : Int = lround(CMTimeGetSeconds(duration));
                            let rounded : Float = roundf(Float(CMTimeGetSeconds(duration)));
                            print(rounded);
                            self.sliderAnimationDuration.setValue(rounded, animated: true)
                            self.sliderAnimationDuration.isUserInteractionEnabled = false
                            
                            if(Int(rounded) > 5)
                            {
                                self.lblSliderValue.text = "5"
                            }
                            else
                            {
                                self.lblSliderValue.text = "\(Int(rounded))"
                            }
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                            self.btnPlayVideo.isHidden = false
                            
                            self.imgGallery.image = self.previewImageForLocalVideo(urlOfVideo)
                        }
                        
                    }
                    else
                    {
//                        self.loading.dismiss()
                        LoadingIndicatorView.hide()
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
            
        }
        //----
        
    }

    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        
        self.present(errorAlert, animated: true ,completion: nil)
    }

    //MARK: Color Methods
    //*********** color methods **************
    
    var colorList = [String]() {
        didSet {
          
            self.colorCollectionView.reloadData()
        }
    }
    
    func loadColorList(){
        
        // create path for Colors.plist resource file.
        let colorFilePath = Bundle.main.path(forResource: "Colors", ofType: "plist")
        
        // save piist file array content to NSArray object
        let colorNSArray = NSArray(contentsOfFile: colorFilePath!)
        
        // Cast NSArray to string array.
        self.colorList = colorNSArray as! [String]
    }
    
    
    func closeColorPicker()
    {
        colorCollectionView.isHidden = true
    }
    
    //MARK: Table search methods
    @IBAction func BtnSearchGallery(_ sender : UIButton)
    {
        if(txtSearch.text != "")
        {
            isSearchGallery = true
            tableOffset = 0
            strSearchText = (txtSearch.text)!.trimmingCharacters(
                in: CharacterSet.whitespacesAndNewlines
            )
           // loadGalleryData()
        }
    }

    
    //MARK: Font Family
    //******* Font Family ********
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayContainFontFamily.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
       return arrayContainFontFamily.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        let label : UILabel = UILabel(frame: CGRect(x: 5, y: 0, width: pickerView.frame.size.width-7, height: 44))
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont(name: arrayContainFontFamily.object(at: row) as! String, size: 18)
        label.text = arrayContainFontFamily.object(at: row) as? String
        return label;

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        strFontFamilyName = arrayContainFontFamily.object(at: row) as! String as NSString
        txtViewMessage.font = UIFont(name: strFontFamilyName as String, size: fontSize)
       //fontPickerView.hidden = true
       // lblTextShow.font = [UIFont fontWithName:strFontFamilyName size:fontSizeIndexValue];
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    @IBAction func btnDoneWithPicker()
    {
        fontPickerView.isHidden = true
        btnDone.isHidden = true
    }
    
    
    func screenShot() -> UIImage
    {
        // not working perfectly
        
        var frameRect : CGRect = viewSquareBlox.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        let color = txtViewMessage.tintColor
        txtViewMessage.tintColor = UIColor.clear
        txtViewMessage.resignFirstResponder()
        
        
        viewSquareBlox.layer.borderColor = UIColor.clear.cgColor
//        UIGraphicsBeginImageContextWithOptions(viewSquareBlox.bounds.size, viewSquareBlox.isOpaque, 0.0)
        
        UIGraphicsBeginImageContext(viewSquareBlox.bounds.size)  ///added 22/07/21  start
            guard let context = UIGraphicsGetCurrentContext() else { return UIImage() }

            UIColor.clear.set()
            context.fill(viewSquareBlox.bounds)

        viewSquareBlox.isOpaque = false
        viewSquareBlox.layer.isOpaque = false
        viewSquareBlox.backgroundColor = UIColor.clear
        viewSquareBlox.layer.backgroundColor = UIColor.clear.cgColor

        viewSquareBlox.layer.render(in: context)   ///added 22/07/21  end
        
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
//        viewSquareBlox.layer.render(in: UIGraphicsGetCurrentContext()!) //commented on 22/07/21
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        viewSquareBlox.layer.borderColor = AppTheme().themeColorBlue.cgColor
        
        //*****
        

        txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
    
    
    //MARK: Audio player methods
    
    func playAudio(_ strVideoURL : String)//, videoImage : UIImage)
    {
       
        let playerItem = AVPlayerItem(url: URL(string: strVideoURL)!)
        
        if((videoPlayer.currentItem) != nil)
        {
            self.videoPlayer.replaceCurrentItem(with: playerItem)
        }
        else
        {
            self.videoPlayer = AVPlayer(playerItem: playerItem)
        }
        self.videoPlayer.play()
        isObserverAdded = true
         videoPlayer.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)

      
        //-- for animation ---
        self.animationTimer = Timer.scheduledTimer(timeInterval: 0.3,
                                                                     target:self,
                                                                     selector:#selector(CreateBloxVC.animateRecordImage),
                                                                     userInfo:nil,
                                                                     repeats:true)
        let delayInSeconds = 45.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        //----
        NotificationCenter.default.addObserver(self, selector: #selector(CreateBloxVC.playerDidFinishPlaying(_:)),
                                                         name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)
        
        
    }
    
   
    
    @objc func playerDidFinishPlaying(_ note: Notification)
    {
        print("Video Finished")
        self.videoPlayer.pause()
        //btnHideAction(UIButton())
        stop(UIButton())
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)
        
    }
    //MARK:- SearchMediaVC Delegates
    func didSearchMediaSelected(media: SearchResult) {
        if var strDefaultUrl: String = media.gc_path
        {
                strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
            
            if(media.gal_cat_id == 1)
            {
                strBloxType = "GalleryImage"
                strGalleryType = GalleryType.Image as String
                hideKeyboard()
                hideAllView()
                imgGallery.isHidden = false
                imgTextBgImage.isHidden = true
                txtViewMessage.resignFirstResponder()
                viewGallery.isHidden = true
                txtViewMessage.isHidden = true
                tableOption.isHidden = true
                collectionGallery.isHidden = true
                txtFieldCaption.isHidden = true
                viewSquareBlox.isHidden = false
                self.layoutBottomMenu.constant = 0
            
                strMediaId = "\(media.gal_id)" as NSString
              /*  if let gallaryType = dict.value(forKey: "mediaType") as? String {
                    strGalleryType = gallaryType
                }*/
                
                // strMediaId = dict.value(forKey: "id") as! String as NSString
                isVideoBloxSelected = false
                print("______")
                print(strDefaultUrl)
                if(strDefaultUrl != "")
                {
                    loading.dismiss()
                    
                    imgGallery.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultUrl)!, placeholder: #imageLiteral(resourceName: "blox_icon"), options: nil)
                   
                    
                }
                else
                {
                    loading.dismiss()
                   // imgGallery.image = cell.imgItem.image
                }
                txtViewMessage.isHidden = true
            }
            else if(media.gal_cat_id == 2)
            {
                strBloxType = "Video"
                 strGalleryType = GalleryType.Video as String
                hideKeyboard()
                hideAllView()
                imgGallery.isHidden = false
                self.layoutBottomMenu.constant = 0
                imgTextBgImage.isHidden = true
                txtViewMessage.resignFirstResponder()
                viewGallery.isHidden = true
                txtViewMessage.isHidden = true
                tableOption.isHidden = true
                collectionGallery.isHidden = true
                txtFieldCaption.isHidden = true
                viewSquareBlox.isHidden = false
                strMediaId = "\(media.gal_id)" as NSString
               /* if let gallaryType = dict.value(forKey: "mediaType") as? String {
                    strGalleryType = gallaryType
                }*/
                isVideoBloxSelected = true
                if(strDefaultUrl != "")
                {
                    if var strDefaultImageUrl: String = media.gal_col_thumb
                    {
                        strDefaultImageUrl = strDefaultImageUrl.replacingOccurrences(of: " ", with: "%20")
                        loading.dismiss()
                        
                        imgGallery.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultImageUrl)!, placeholder: #imageLiteral(resourceName: "blox_icon"), options: nil)
                        
                    }
                    else
                    {
                        loading.dismiss()
                      //  imgGallery.image = cell.imgItem.image
                    }
                    
                    isVideoFromTalkBlox = true
                    
//                    self.loading.show(in: self.view)
                    LoadingIndicatorView.show("Loading...")
                    downloadSelectedVideo(WEB_URL.BaseURL + strDefaultUrl)
                    
                }
                else
                {
                    loading.dismiss()
                    //imgGallery.image = cell.imgItem.image
                }
                txtViewMessage.isHidden = true
            }
            else if(media.gal_cat_id == 3)
            {
                strGalleryType = GalleryType.Sound as String
                self.hideKeyboard()
                self.hideAllView()
                self.layoutBottomMenu.constant = 0
                self.txtViewMessage.resignFirstResponder()
                self.viewGallery.isHidden = true
                self.imgGallery.isHidden = false
                self.tableOption.isHidden = false
                self.collectionGallery.isHidden = true
                
                isVideoBloxSelected = false
                
                print("sound file selected")
                strBgMediaId = "\(media.gal_id)" as NSString
                self.soundID = strBgMediaId
                if var strDefaultImageUrl: String = media.gal_col_thumb{//(dict.value(forKey: "gal_col_thumb") as? String) {
                    strDefaultImageUrl = strDefaultImageUrl.replacingOccurrences(of: " ", with: "%20")
                    self.audioThumbPath = strDefaultImageUrl
                }
                if(strDefaultUrl != "")
                {
//                    self.loading.show(in: self.view)
                    LoadingIndicatorView.show("Loading...")
                    //downloadSelectedAudio(strDefaultUrl)
                    //strMediaId = dict.value(forKey: "id") as! String
                    
                    var pathComponent = "bloxAudio0.caf"
                    if((self.bloxArrayIndex) != nil)
                    {
                        let strIndex: String = "bloxAudio\(String(describing: self.bloxArrayIndex)).caf"
//                        {
                            print(strIndex)
                            
                            pathComponent = String(format: "bloxAudio\(String(describing: self.bloxArrayIndex)).caf")
//                        }
                    }
                    
                    AppTheme().callGetServiceForDownload((WEB_URL.BaseURL + strDefaultUrl), fileNameToBeSaved: pathComponent, completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            print("video saved \(pathComponent)")
                            
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                            
                            self.strAudioURLPath = (WEB_URL.BaseURL + strDefaultUrl)
                            self.audioUrlPath = pathComponent
                            self.incrSecond = 0
                            
                            
                            if((self.meterTimer) != nil)
                            {
                                self.meterTimer?.invalidate()
                            }
                            if((self.animationTimer) != nil)
                            {
                                self.animationTimer?.invalidate()
                            }
                            
                            self.viewRecorder.isHidden = false
                            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
                                
                                self.viewRecorderBottomLayout.constant = 0
                                self.view
                                    .layoutIfNeeded()
                                
                            })
                            { (finished) -> Void in
                                self.playAudio(strDefaultUrl)
                            }
                        }
                        else
                        {
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
//                           self.loading.dismiss()
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                        
                    })
                    
                }
            }
        }
    }
    
    //MARK:- BloxBackground Delgates
    func didBackgroundImageSelectedForBlox(_ image:String, mediaId:NSString) {
//        if var strDefaultUrl: String = image.bgi_name
//        {
             let strDefaultUrl = image.replacingOccurrences(of: " ", with: "%20")
            // strMediaId = dict.value(forKey: "id") as! String
            
           // strGalleryType = dict.value(forKey: "mediaType") as! String
            strBgImgMediaId =  mediaId
            
            if(strGalleryType == GalleryType.Backgroundimage as String)
            {
                if(strDefaultUrl != "")
                {
                    loading.dismiss()
                    
                    let urlStr: NSString = strDefaultUrl.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)! as NSString
                    let imgURL = URL(string: WEB_URL.BaseURL + (urlStr as String) as String)
                    
                    imgTextBgImage.kf.setImage(with: imgURL, placeholder: nil, options: nil)
                    imgTextBgImage.isHidden = false
                    txtViewMessage.backgroundColor = UIColor.clear
                    //txtViewMessage.backgroundColor = UIColor(patternImage: imgTemp.image!)
                    //imgGallery.kf_setImageWithURL(NSURL(string: strDefaultUrl)!, placeholderImage: cell.imgItem.image)
                }
                else
                {
                    loading.dismiss()
                   // imgTextBgImage.image = cell.imgItem.image
                    imgTextBgImage.isHidden = false
                    txtViewMessage.backgroundColor = UIColor.clear
                    //txtViewMessage.backgroundColor = UIColor(patternImage: cell.imgItem.image!)
                    //imgGallery.image = cell.imgItem.image
                }
            }
//        }
    }
    
    //MARK: Button Action
    @IBAction func btnHomeAction(_ sender: UIButton)
    {
         stop(UIButton())
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func play(_ sender: UIButton)
    {
        
        if((videoPlayer.currentItem) != nil)
        {
       self.videoPlayer.play()
        }else{
            playAudio(strAudioURLPath)
        }
        incrSecond = 0
    }
    
    @IBAction func stop(_ sender: UIButton)
    {
        if((videoPlayer.currentItem) != nil)
    {
        self.videoPlayer.pause()
        }
        
        if((meterTimer) != nil)
        {
            meterTimer?.invalidate()
            incrSecond = 0
        }
        if((animationTimer) != nil)
        {
            self.animationTimer?.invalidate()
        }
    }
    
    
    @IBAction func btnSaveAudioAction (_ sender : UIButton)
    {
        btnHideAction(UIButton())
        stop(UIButton())
        //self.loading.showInView(self.view)
        loading.dismiss()
        var img : UIImage = UIImage()
        
        
        if(strBloxType == "GalleryImage" || strBloxType == "Camera")
        {
            img = imgGallery.image!
        }
        else if(strBloxType == "TextMessage")
        {
            img = screenShot()
        }
        self.soundID = strBgMediaId
        self.sliderAnimationDuration.setValue(5.0, animated: true)
        print(img.size.height)
        print(imgGallery.image)
        
        if self.txtViewMessage.text == "" {
            if imgGallery.image == nil {
                hideKeyboard()
                hideAllView()
                imgGallery.isHidden = false
                imgTextBgImage.isHidden = true
                txtViewMessage.resignFirstResponder()
                viewGallery.isHidden = false
                txtViewMessage.isHidden = true
                tableOption.isHidden = false
                collectionGallery.isHidden = true
                txtFieldCaption.isHidden = true
                viewSquareBlox.isHidden = false
                var urlString = WEB_URL.BaseURL + self.audioThumbPath
                       urlString = urlString.replacingOccurrences(of: " ", with: "%20")
                imgGallery.kf.setImage(with: URL.init(string: urlString), placeholder: UIImage(named: "SingleBlox"), options: nil, progressBlock: nil, completionHandler: nil)
                LoadingIndicatorView.show("Loading...")
                
                let imageData : Data = self.imgGallery.image!.jpegData(compressionQuality: 1.0)!
                strBloxType = "Camera"
                self.aryMediaImageData.add(imageData)
                
                
//                let url = URL(string: self.strAudioURLPath)
//
//                let audioData = try? Data(contentsOf:url!)
//                self.aryMediaImageData.add(audioData!)
                
                
                NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Image as String, arrDataToSend: self.aryMediaImageData, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
                    LoadingIndicatorView.hide()
//                    self.loading.dismiss()
                    if success == true {
                        
                        // let statusDic = response?["header"] as! NSDictionary
                        let detail = response?["result"] as! NSArray
                        if detail.count > 0 {
                            let dictDetail = detail[0] as! NSDictionary
                            self.strMediaId = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
                        }
                        
                        self.bloxType =  GalleryType.Image
                        //self.delegate?.bloxDetails(detail, true)
                        
                        /* if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                         
                         }*/
                    }
                })
            }
        }
    }
    
    @IBAction func btnHideAction (_ sender : UIButton)
    {
        self.videoPlayer.pause()
        /*
         if((meterTimer) != nil)
         {
         meterTimer.invalidate()
         incrSecond = 0
         }
         */
        if((animationTimer) != nil)
        {
            self.animationTimer?.invalidate()
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations:
            {
                self.viewRecorderBottomLayout.constant = -150
                self.view.layoutIfNeeded()
            }, completion: nil)
        
    }
    
    //MARK: ---- Animating Red round Image for playing audio ----
    @objc func animateRecordImage()
    {
        //put at initialising
        //imgRecording.transform = CGAffineTransformMakeScale(0.5, 0.5)
        let duration = 0.5
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.2, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
            
            self.imgRecording.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
            
            })
        { (finished) -> Void in
            UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.2, options: UIView.AnimationOptions.curveLinear, animations: { () -> Void in
                
                self.imgRecording.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
                })
            { (finished) -> Void in
                
            }
        }
    }

    
    
    /////  end  /////
    
    func audioVideoMerging(_ audioURL: URL, moviePathUrl: URL, savePathUrl: URL ,completion: @escaping (_ result: String, _ data : AnyObject) -> Void)
    {
        let fileNamePath = "audio.caf"
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let oldappSettingsPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(fileNamePath).absoluteString
        let audioUrl = URL(fileURLWithPath: oldappSettingsPath)
        let fileNamePath1 = "output.mp4"
        var paths1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory1 = paths1[0]
        let oldappSettingsPath1 = URL(fileURLWithPath: documentsDirectory1).appendingPathComponent(fileNamePath1).absoluteString
        print("oldpath=\(oldappSettingsPath)")
        let videoUrl = URL(fileURLWithPath: oldappSettingsPath1)
        
        
//        if avPlayer.duration > 0.00000
//        {
            print("SOMEDATA     IS THERE ")
            let audioAsset = AVURLAsset(url: audioURL, options: nil)
            let videoAsset = AVURLAsset(url: moviePathUrl, options: nil)
            
            let mixComposition = AVMutableComposition()
            print("audio =\(audioAsset)")
        //-- Audio thingy
        let compositionCommentaryTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            do {
                (try compositionCommentaryTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: audioAsset.duration), of: audioAsset.tracks(withMediaType: AVMediaType.audio)[0], at: CMTime.zero))
            }
            catch let error
            {
                print(error)
            }
        //-- Video thingy
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
            do
            {
                (try compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: AVMediaType.video)[0], at: CMTime.zero))
            }
            catch let error
            {
                print(error)
            }
        
            let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetPassthrough)!
            let videoName = "export.mov"
            let exportPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(videoName).absoluteString
            //let exportUrl = NSURL.fileURLWithPath(exportPath)
        //new code
            let exportUrl = savePathUrl
        
            if FileManager.default.fileExists(atPath: exportPath) {
                do {
                    (try FileManager.default.removeItem(atPath: exportPath))
                }
                catch let error {
                    print(error)
                }
            }
        assetExport.outputFileType = AVFileType.mp4// "com.apple.quicktime-movie"
            print("file type \(assetExport.outputFileType)")
            assetExport.outputURL = exportUrl
            assetExport.shouldOptimizeForNetworkUse = true
            assetExport.exportAsynchronously(completionHandler: {() -> Void in
                
                
                switch assetExport.status {
                case  .completed:
                    //                export complete
                    NSLog("Export Complete")
                    
                    print(savePathUrl)
                    
                case .failed:
                    NSLog("Export Failed")
                    NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                    //                export error (see exportSession.error)
                    
                case .cancelled:
                    NSLog("Export Failed")
                    NSLog("ExportSessionError: %@", assetExport.error!.localizedDescription)
                    //                export cancelled
                    
                default:
                    //do nothing
                    break
                }
                
                print("converted video save here :- \(savePathUrl)")
                completion("success", savePathUrl as AnyObject)
                
            })
        }

//    }
    
    //MARK:- Server Calls
    func getImagesFromCategory(_ categoryID: Int) {
        
        let params: NSDictionary = ["gal_cat_id": categoryID]
        LoadingIndicatorView.show("Loading...")
        
        NetworkManager.sharedInstance.executeService(WEB_URL.getCollection, postParameters: params, completionHandler: { (success: Bool, response: NSDictionary?) in
            LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes {
                    
                    if(response?["result"] != nil) {
                        
                        if let ary: NSArray = response?["result"] as? NSArray
                        {
                            print(ary)
                            if(ary.count > 0)
                            {
                                //   self.tableOption.isHidden = true
                                self.collectionGallery.isHidden = false
                                self.arryGalleryCollection = ary.mutableCopy() as! NSMutableArray
//                                isGifs = false
                                self.collectionGallery.reloadData()
                            }
                            else
                            {
                                let errorAlert = AppTheme().showAlert("Gallery is empty", errorTitle: "Gallery Alert")
                                
                                self.present(errorAlert, animated: true, completion: nil)
                                self.collectionGallery.isHidden = true
                            }
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                        }
                        else
                        {
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                        }
                    }
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail {
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
                
            }
        })
    }
    
    func getAlbumListFrom(galleryType:String){
        LoadingIndicatorView.show("Loading...")
        let param = ["gt_type":galleryType]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getAlbumList, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success {
                print("Get Album Response is ----->>>>>>> \(response!)")
                if let statusDic = response?["header"] as? NSDictionary{
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        if let result = response?["result"] as? NSArray {
                            for dictAlbum in result {
                                let dictAlbum:NSDictionary = dictAlbum as! NSDictionary
                                self.arryTableOption.add(dictAlbum)
                                self.tableOption.reloadData()
                            }
                        }
                    }
                }else {
                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
                }
            }
        }
    }
    func getCategoryFrom(galleryType:String,albumID:String){
        LoadingIndicatorView.show("Loading...")
        let param = ["gt_type":galleryType,"gal_alb_id":albumID]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getCategory, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success {
                print("Get Category Response is ----->>>>>>> \(response!)")
                if let statusDic = response?["header"] as? NSDictionary{
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        if let result = response?["result"] as? NSArray {
                            self.arryTableOption.removeAllObjects()
                            for dictAlbum in result {
                                let dictAlbum:NSDictionary = dictAlbum as! NSDictionary
                                self.arryTableOption.add(dictAlbum)
                                self.tableOption.reloadData()
                            }
                        }
                    }
                }else {
                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
                }
            }
        }
    }
    
    //MARK:- METHODS
    
    func uploadGiphyImage(gifUrl: String){
//        LoadingIndicatorView.show("Loading...")
//        let imageName = gifsArray[indexPath.row]
//        imgGallery.kf.setImage(with: URL.init(string: gifUrl ))
        
//        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: imageName, withExtension: "gif")!)
        let imageData = try? Data(contentsOf: URL(string: gifUrl)!)
        galleryImageData = imageData ?? Data()
         txtViewMessage.isHidden = true
         isVideoBloxSelected = false
//        let img = UIImage.gifImageWithURL(gifUrl)
        imgGallery.image = UIImage.gifImageWithURL(gifUrl)//gifImageWithData(imageData!)//(data: imageData!)
        imgGallery.isHidden = false
        
        aryMediaImageData.removeAllObjects()   // 1Mar2021
        aryMediaImageData.add(galleryImageData!)
        
        //LoadingIndicatorView.show("Loading...")
        
        NetworkManager.sharedInstance.executeServiceWithMultipart(WEB_URL.UploadFile, fileType: GalleryType.Gif as String, arrDataToSend: aryMediaImageData, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
//                   self.loading.dismiss()
            
            if success == true {
                let detail = response?["result"] as! NSArray
                if detail.count > 0 {
                    let dictDetail = detail[0] as! NSDictionary
                    self.strMediaId = "\(dictDetail.object(forKey: "id") as! Int)" as NSString
//                    let mediaurl = dictDetail.object(forKey: "mediaurl") as! String
//                    self.imgGallery.image = UIImage.gifImageWithURL(mediaurl)
                }
                
                self.bloxType =  GalleryType.Gif
                //self.delegate?.bloxDetails(detail, true)
                // self.saveDictionaryInUserDefault(detail as! [String : AnyObject])
               // self.saveDictionaryInUserDefault(detail as! [String : AnyObject])

             /*   if statusDic["ErrorCode"] as! Int == ErroCode.Succes {
                    
                }*/
            } else {
                self.galleryImageData = nil
                self.txtViewMessage.isHidden = false
                self.imgGallery.image = nil
                self.aryMediaImageData.removeAllObjects()
                self.strMediaId = ""
            }
            self.isGiphyCall = false
        })
        
    }
    func galleryFilterbyType(){
        
        
        strPreviousGalleryType = strGalleryType
        let arrFilteredGal:NSMutableArray = NSMutableArray()
        for galObj in arrayTempGalleryList {
            let galDic:NSDictionary = galObj as! NSDictionary
            if galDic.value(forKey: "gallery_type") as! String == strGalleryType {
                arrFilteredGal.add(galDic)
            }
        }
        if arrFilteredGal.count > 0 {
            arryTableOption.removeAllObjects()
            arryTableOption = arrFilteredGal.mutableCopy() as! NSMutableArray
            print("my filtered arr \(arryTableOption)")
            self.tableOption.reloadData()
        }
    }

    func configureSearchController() {
        
        UITextField.appearance().tintColor = .red
        
        searchBar.delegate = self
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
      //  IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        self.view.bringSubviewToFront(searchBar)
        var rectFrame : CGRect = searchBar.frame
        rectFrame.origin.y -= 80
        searchBar.frame = rectFrame
        isSearchFieldShifted = true
        if filteredArray.count == 0 {
            shouldShowSearchResults = false
        }else {
            shouldShowSearchResults = true
        }
        layoutBottomMenu.constant = 210

    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
       // IQKeyboardManager.sharedManager().enableAutoToolbar = true
        if searchClicked {
            shouldShowSearchResults = true;
            searchBar.resignFirstResponder()

        }else {
        shouldShowSearchResults = false;
        searchBar.resignFirstResponder()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        searchBar.text = ""
       // tableViewHeightConstraint.constant = 406
        filteredArray.removeAllObjects()
        
        tableOption.reloadData()
        searchBar.resignFirstResponder()
        
    }
    
    @available(iOS 8.0, *)
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //let searchString = searchController.searchBar.text
        
        let resultPredicate = NSPredicate(format: "name BEGINSWITH[c] %@", searchText)
        let data = arryTableOption.filtered(using: resultPredicate)
        
        if(data.count > 0)
        {
            filteredArray.removeAllObjects()
            filteredArray.addObjects(from: data)
           // adjustTableViewHeight()
        }
        
        if(filteredArray.count == 0 || searchText == ""){
            shouldShowSearchResults = false;
           // tableViewHeightConstraint.constant = 406
            isSearchEmpty = true
            filteredArray.removeAllObjects()

            tableOption.reloadData()
           // searchBar.resignFirstResponder()
        } else {
            isSearchEmpty = false
            shouldShowSearchResults = true;
        }
        
        // Reload the tableview.
       // tableOption.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       // IQKeyboardManager.sharedManager().enable = false
        if isSearchEmpty {
            searchClicked = false
        } else {
        searchClicked = true
        }
        tableOption.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func cropFrame(videoAsset:AVAsset, animation:Bool) -> Void {
        var insertTime = CMTime.zero
        var arrayLayerInstructions:[AVMutableVideoCompositionLayerInstruction] = []
        var outputSize = CGSize.init(width: 0, height: 0)
        
        // Determine video output size
        let videoTrack = videoAsset.tracks(withMediaType: AVMediaType.video)[0]
        
        let assetInfo = orientationFromTransform(transform: videoTrack.preferredTransform)
        
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait == true {
            videoSize.width = videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        
        if videoSize.height > outputSize.height {
            outputSize = videoSize
        }
        
        let defaultSize = CGSize(width: 720, height: 720) // Default video size
        
        if outputSize.width == 0 || outputSize.height == 0 {
            outputSize = defaultSize
        }
        
        // Silence sound (in case of video has no sound track)
        let silenceURL = Bundle.main.url(forResource: "silence", withExtension: "mp3")
        let silenceAsset = AVAsset(url:silenceURL!)
        let silenceSoundTrack = silenceAsset.tracks(withMediaType: AVMediaType.audio).first
        
        // Init composition
        let mixComposition = AVMutableComposition.init()
        
        
        // Get audio track
        var audioTrack:AVAssetTrack?
        if videoAsset.tracks(withMediaType: AVMediaType.audio).count > 0 {
            audioTrack = videoAsset.tracks(withMediaType: AVMediaType.audio).first
        }
        else {
            audioTrack = silenceSoundTrack
        }
        
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        do {
            let startTime = CMTime.zero
            let duration = videoAsset.duration
            
            // Add video track to video composition at specific time
            try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: startTime, duration: duration),
                                                       of: videoTrack,
                                                       at: insertTime)
            
            // Add audio track to audio composition at specific time
            if let audioTrack = audioTrack {
                try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: startTime, duration: duration),
                                                           of: audioTrack,
                                                           at: insertTime)
            }
            
            // Add instruction for video track
            let layerInstruction = videoCompositionInstructionForTrack(track: videoCompositionTrack!,
                                                                       asset: videoAsset,
                                                                       standardSize: outputSize,
                                                                       atTime: insertTime)
            // Hide video track before changing to new track
            let endTime = CMTimeAdd(insertTime, duration)
            //let finalTimer = CMTimeAdd(CMTime(seconds: 5, preferredTimescale: videoAsset.duration.timescale), CMTime(seconds: 5, preferredTimescale: videoAsset.duration.timescale))
            
            //Kalpesh crop video frames
            if animation {
                let timeScale = videoAsset.duration.timescale
                let durationAnimation = CMTime.init(seconds: 1, preferredTimescale: timeScale)
                layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: CMTimeRange.init(start: endTime, duration: durationAnimation))
                
                //**********======== CROP YOUR VIDEO FRAME HERE MANUALLY ========**********
                
                layerInstruction.setCropRectangle(CGRect(x: 0, y: 0, width: videoTrack.naturalSize.width, height: 300.0), at: startTime)
            } else {
                layerInstruction.setOpacity(0, at: endTime)
            }
            arrayLayerInstructions.append(layerInstruction)
            
            // Increase the insert time
            insertTime = CMTimeAdd(insertTime, duration)
        }
        catch {
            print("Load track error")
        }
        
        
        // Main video composition instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: insertTime)
        mainInstruction.layerInstructions = arrayLayerInstructions
        
        // Main video composition
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mainComposition.renderSize = outputSize
        
        // Export to file
        let path = NSTemporaryDirectory().appending("mergedVideo.mp4")
        let exportURL = URL.init(fileURLWithPath: path)
        
        
        // Init exporter
        let exporter = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = exportURL
        exporter?.outputFileType = AVFileType.mp4
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.videoComposition = mainComposition
        
        // Do export
        exporter?.exportAsynchronously(completionHandler: {
            
        })
    }
    
    func isAnimatedImage(_ imageData: Data) -> Bool {
        if let source = CGImageSourceCreateWithData(imageData as CFData, nil) {
            let count = CGImageSourceGetCount(source)
            return count > 1
        }
        return false
    }
  

    
}

extension CreateBloxVC: GiphyDelegate {
    func didSearch(for term: String) {
        print("your user made a search! ", term)
    }
    
    func didSelectMedia(giphyViewController: GiphyViewController, media: GPHMedia) {
//        self.selectedContentType = giphyViewController.selectedContentType
        LoadingIndicatorView.show("Loading...")
        giphyViewController.dismiss(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            let gif_URL = media.url(rendition: .fixedWidth, fileType: .gif)
//            let mediaView = GPHMediaView()
//            mediaView.media = media
//            self.imgGallery.addSubview(mediaView)
//            self.txtViewMessage.isHidden = true
            self.isGiphyCall = true
            self.uploadGiphyImage(gifUrl: gif_URL!)
        })
        GPHCache.shared.clear()
    }
    
    func didDismiss(controller: GiphyViewController?) {
        GPHCache.shared.clear()
    }
}

extension CreateBloxVC: GPHMediaViewDelegate {
    func didPressMoreByUser(_ user: String) {
//        showMoreByUser = user
        btnGiphyClicked()
    }
}

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

extension AVAsset {
    
    
    var g_size: CGSize {
        return tracks(withMediaType: AVMediaType.video).first?.naturalSize ?? .zero
    }
    
    
    var g_orientation: UIInterfaceOrientation {
        
        guard let transform = tracks(withMediaType: AVMediaType.video).first?.preferredTransform else {
            return .portrait
        }
        
        switch (transform.tx, transform.ty) {
        case (0, 0):
            return .landscapeRight
        case (g_size.width, g_size.height):
            return .landscapeLeft
        case (0, g_size.width):
            return .portraitUpsideDown
        default:
            return .portrait
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
