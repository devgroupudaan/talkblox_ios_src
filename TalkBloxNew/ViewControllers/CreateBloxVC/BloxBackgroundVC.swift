//
//  BloxBackgroundVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 6/29/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss

protocol BloxBackgroundDelegate {
    func didBackgroundImageSelectedForBlox(_ image:String, mediaId:NSString)
}

class BloxBackgroundVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet var collectionview:UICollectionView!
    @IBOutlet var activityIndicator:UIActivityIndicatorView!
    var arrImages = [BackGroundData]()
    var delegate:BloxBackgroundDelegate?
    var arryGalleryCollection : NSMutableArray = NSMutableArray()
   
    //MARK:- UIViewControler Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serverCallForGetBackgroundImages()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIButtons Actions
    @IBAction func btnBackAction(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UICollectionview Delegate and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return arrImages.count
        return arryGalleryCollection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BloxBackgroundCell", for: indexPath)
        let imgView = cell.viewWithTag(100) as! UIImageView
//        let data = arrImages[indexPath.row]
//        var urlstring =  WEB_URL.BaseURL + data.bgi_name!
//        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
//        imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        
        if let dict: NSDictionary = arryGalleryCollection.object(at: indexPath.row) as? NSDictionary
        {
                if var strDefaultUrl: String = dict.value(forKey: "gc_path") as? String
                {
                    strDefaultUrl = strDefaultUrl.replacingOccurrences(of: " ", with: "%20")
                    if(strDefaultUrl == "")
                    {
                        imgView.image = UIImage(named: "SingleBlox")
                    }
                    else
                    {
               
                        imgView.kf.setImage(with: URL(string: WEB_URL.BaseURL + strDefaultUrl)!, placeholder: UIImage(named: "SingleBlox"), options: nil)
                    }
                    if let str: NSString = dict.value(forKey: "url") as? NSString
                    {
                        // cell.lblTittleText.text = str.lastPathComponent
                        //cell.lblTittleText.isHidden = false
                    }
                }
            }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageData = arryGalleryCollection.object(at: indexPath.row) as? NSDictionary
        let image = imageData!.value(forKey: "gc_path") as? String
        let strMediaId = "\(imageData!.value(forKey: "gc_id") as! Int)" as NSString
        self.delegate?.didBackgroundImageSelectedForBlox(image!, mediaId: strMediaId)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Server Calls
//    func serverCallForGetBackgroundImages() {
//        LoadingIndicatorView.show("Loading...")
//        let param = ["type":"image" ]
//        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getBackGround, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
//            if success {
//                print("Get Album Response is ----->>>>>>> \(response!)")
//                if let statusDic = response?["header"] as? NSDictionary{
//                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                        if let result = response?["result"] as? NSArray {
//                            for dictAlbum in result {
//                                self.arrImages = [BackGroundData].from(jsonArray: result as! [JSON])!
//                               self.collectionview.reloadData()
//                            }
//                        }
//                    }
//                }else {
//                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
//                }
//            }
//        }
//    }
    
    func serverCallForGetBackgroundImages() {
        LoadingIndicatorView.show("Loading...")
//        let param = ["type":"image" ]
        let param = ["gal_cat_id" : "1" ]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getCollection, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success {
                print("Get Album Response is ----->>>>>>> \(response!)")
                if let statusDic = response?["header"] as? NSDictionary{
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        if let result = response?["result"] as? NSArray {
                            if(result.count > 0)
                            {
                                self.arryGalleryCollection = result.mutableCopy() as! NSMutableArray
                                self.collectionview.reloadData()
                            }
                            else
                            {
                                let errorAlert = AppTheme().showAlert("Gallery is empty", errorTitle: "Gallery Alert")
                                self.present(errorAlert, animated: true, completion: nil)
                            }
                        }
                        else
                        {
//                            self.loading.dismiss()
                            LoadingIndicatorView.hide()
                        }
                    }
                }else {
                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
                }
            }
        }
    }

}
