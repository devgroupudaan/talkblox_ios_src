//
//  NotificationViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 01/06/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss

class notificationCell: UITableViewCell {
    @IBOutlet weak var user_img: DesignableImageView!
    @IBOutlet weak var notStr: UILabel!
    @IBOutlet weak var notSubject: UILabel!
    @IBOutlet weak var time: UILabel!
    
}

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var notTable: UITableView!
    
    var pageNO = 1
    var arrNotifs = [notificationItem]()
    var currentLbl = UILabel()
    
    var messageID = 0
    var chatID = 0
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    
    var inputImageBgID: String!
    var inputSoundBgID: String!
    
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var dictUserMessageData : NSMutableDictionary!
    
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    
    var inbloxData:Inblox?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notTable.delegate = self
        notTable.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageNO = 1
        self.serverCallForGetNotifications()
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
//        guard let text = bottomLbl.text else { return }
        let notif = arrNotifs[gesture.view!.tag]
        let boldText = notif.senderName
        let completeText = (notif.senderName ?? "") + " " + (notif.title ?? "")
        let boldTextRange = (completeText as NSString).range(of: boldText!)
        if gesture.didTapAttributedTextInLabel(label: gesture.view as! UILabel, inRange: boldTextRange) {
            print("Bold Text tapped")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.user_id = notif.userid
            vc.isSelfProfile = false
            vc.previewKind = "Bloxcast"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- UITableViewDelegates And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationCell
        cell.tag = indexPath.row
        let notif = arrNotifs[indexPath.row]
        let boldTextStr = notif.senderName ?? ""
        let normalText = " " + (notif.title ?? "")
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14)]
        let attributedString = NSMutableAttributedString(string: boldTextStr, attributes:attrs1)
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)]
        let normalString = NSMutableAttributedString(string: normalText, attributes:attrs2)
//        let normalString = NSMutableAttributedString(string:normalText)
        attributedString.append(normalString)
        
        cell.notStr.attributedText = attributedString
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        cell.notStr.tag = indexPath.row
        cell.notStr.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture2.numberOfTouchesRequired = 1
        cell.user_img.tag = indexPath.row
        cell.user_img.addGestureRecognizer(tapGesture2)
        
        var urlstring = WEB_URL.BaseURL + (notif.user_image ?? "")
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        cell.user_img.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.notSubject.text = notif.subject
        
        let notifDate = notif.created_at
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let notif_date = formatter.date(from: notifDate!)
        cell.time.text = notif_date?.timeAgo()
        
        return cell
        
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notif = arrNotifs[indexPath.row]
        let type = notif.type
        switch type {
        case "rating":
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//            vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
//            vc.isSelfProfile = true
//            self.navigationController?.pushViewController(vc, animated: true)
        
            self.messageID = notif.message_id ?? 0
            self.chatID = notif.chat_id ?? 0
//            self.bloxSubject = notif.subject!
            
            let user = AppSharedData.sharedInstance.currentUser
           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
            self.Blox_user_image = user?.user_image ?? ""
            self.bloxcastUsername = user?.displayName  ?? ""
            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
//            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = user?.userid ?? 0//notif.userid!
//            self.bloxRating = notif.rating ?? 0.0
        case "comment":
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//            vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
//            vc.isSelfProfile = true
//            self.navigationController?.pushViewController(vc, animated: true)
        
            self.messageID = notif.message_id ?? 0
            self.chatID = notif.chat_id ?? 0
//            self.bloxSubject = notif.subject!
            
            let user = AppSharedData.sharedInstance.currentUser
           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
            self.Blox_user_image = user?.user_image ?? ""
            self.bloxcastUsername = user?.displayName  ?? ""
            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
//            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = user?.userid ?? 0
        
        case "follow":
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.user_id = notif.userid
            vc.isSelfProfile = false
            self.navigationController?.pushViewController(vc, animated: true)
        case "sendbloxcastToUser":
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxCastViewController") as! BloxCastViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//            if let tabBarController = self.view.window?.rootViewController?.children[0] as? UITabBarController {
//                tabBarController.selectedIndex = 3
//            }
            
//            let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
//            if navController != nil{
//                for controller in navController!.viewControllers as Array {
//                    if controller.isKind(of: TabViewController.classForCoder()) {
//                        navController!.popToViewController(controller, animated: true)
//                        (controller as! TabViewController).selectedIndex = 3
//                        break
//                    }
//                }
//            }
        
            self.messageID = notif.message_id ?? 0
            self.chatID = notif.chat_id ?? 0
//            self.bloxSubject = notif.subject!
            
            let user = AppSharedData.sharedInstance.currentUser
           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
            self.Blox_user_image = user?.user_image ?? ""
            self.bloxcastUsername = user?.displayName  ?? ""
            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
//            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = user?.userid ?? 0
        case "sendbloxcastToSubscriber":
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxCastViewController") as! BloxCastViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//            if let tabBarController = self.view.window?.rootViewController?.children[0] as? UITabBarController {
//                tabBarController.selectedIndex = 3
//            }
//            self.popToHome()
//            self.tabBarController?.selectedIndex = 3
        
//            let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
//            if navController != nil{
//                for controller in navController!.viewControllers as Array {
//                    if controller.isKind(of: TabViewController.classForCoder()) {
//                        navController!.popToViewController(controller, animated: true)
//                        (controller as! TabViewController).selectedIndex = 3
//                        break
//                    }
//                }
//            }
        
            self.messageID = notif.message_id ?? 0
            self.chatID = notif.chat_id ?? 0
//            self.bloxSubject = notif.subject!
            
            let user = AppSharedData.sharedInstance.currentUser
           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
            self.Blox_user_image = user?.user_image ?? ""
            self.bloxcastUsername = user?.displayName  ?? ""
            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
//            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = user?.userid ?? 0
        default:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if indexPath.row == (pageNO * 10) - 1 {
                pageNO = pageNO + 1
                self.serverCallForGetNotifications()
        }
    }
    // MARk: - Server Calls
    
    func serverCallForGetNotifications(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d&page=%d&limit=10", AppSharedData.sharedInstance.currentUser?.userid ?? 0, pageNO)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.notificationLogs, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [notificationItem].from(jsonArray: response?["result"] as! [JSON])
                    
                    if self.pageNO == 1 {
                        self.arrNotifs = inbloxData!
                    } else {
                        self.arrNotifs.append(contentsOf: inbloxData!)
                    }
//                    self.arrNotifs = self.arrNotifs.reversed()
                    self.notTable.reloadData()
                } else{
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCallForPreview()  {
        let params:NSDictionary = ["message_id":messageID,"chat_id":chatID]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.preview, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            //            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let dict : NSDictionary = response {
                        
                        print("\n\ndictionary for message all urls :", dict, "\n\n")
                        self.generateDictionaryForPreview(dict.mutableCopy() as! NSMutableDictionary )
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
   
    
    //MARK:- Helper Methods
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        self.present(errorAlert, animated: true ,completion: nil)
    }
    func generateDictionaryForPreview(_ dictMessageData : NSMutableDictionary)
    {
//        if let imageBg = dictMessageData.value(forKey: "imageBg")
        if let imageBg = dictMessageData.value(forKey: "chatImage") // changed on 28Dec2020
        {
            inputImageBgID = imageBg as! String
        }
        if let chatSubject = dictMessageData.value(forKey: "chatSubject") // changed on 28Dec2020
        {
            bloxSubject = chatSubject as! String
        }
        if let imageBg = dictMessageData.value(forKey: "chatSound") // changed on 28Dec2020
        {
            inputSoundBgID = imageBg as! String
        }
        if let contactId = dictMessageData.value(forKey: "contact_id")
        {
            dictMessagePreivewData.setValue(contactId, forKey: "contact_id")
        }
        if let messageRecList = dictMessageData.value(forKey: "message_receiver_list")
        {
            dictMessagePreivewData.setValue(messageRecList, forKey: "message_receiver_list")
        }
        if let messageUserId = dictMessageData.value(forKey: "message_user_id")
        {
            dictMessagePreivewData.setValue(messageUserId, forKey: "message_user_id")
        }
        if let messageUserName = dictMessageData.value(forKey: "message_user_name")
        {
            dictMessagePreivewData.setValue(messageUserName, forKey: "message_user_name")
        }
        
        if(messageParentId != 0)
        {
            dictMessagePreivewData.setValue(messageParentId, forKey: "parentId")
        }
        if(strMessageUserName != "")
        {
            dictMessagePreivewData.setValue(strMessageUserName, forKey: "messageUserName")
        }
        
        dictMessagePreivewData.setValue(strMessageId, forKey: "id")
        
        dictMessagePreivewData.setValue("Inbox", forKey: "MessagePreviewType")
        
        
        //-- Code to download BG bubble image ---
        
        if(previewBubbleImageURL != "")
        {
          
            previewBubbleImageURL = previewBubbleImageURL.replacingOccurrences(of: " ", with: "%20")
            var imageData : UIImage = UIImage()
            let img : UIImage = AppTheme().getImageFromUrl(previewBubbleImageURL)
            if img != nil
            {
                imageData = img
            }
            else
            {
                previewBubbleImageURL = ""
            }
            if(imageData.size.height > 0)
            {
                bubblePreviewImage = imageData
            }
            else
            {
                previewBubbleImageURL = ""
            }
        }
        //----
        
        /////new logic for bg Music
        if var strBgURL : String = dictMessageData.value(forKey: "chatSound") as? String
        {
              strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
            //AppTheme().killFileOnPath("previewBg.caf")
            
            //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
            AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                
                if(result == "success")
                {
                    self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
                else
                {
                    self.showAlert(data as! String, strTitle: "Error")
                }
                
            })
        }
        
        
        if let aryBgMedia : NSArray = dictMessageData.value(forKey: "bgMedia") as? NSArray
        {
            if let dictBgMedia : NSDictionary = aryBgMedia.object(at: 0) as? NSDictionary
            {
                if var strBgURL : String = dictBgMedia.value(forKey: "mediaUrl") as? String
                {
                      strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
                    //AppTheme().killFileOnPath("previewBg.caf")
                    
                    //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                            if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                            {
                                if aryMedia.count > 0
                                {
                                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                                    self.processResponce()
                                }
                            }
                        }
                        else
                        {
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                    })
                }
                else
                {
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
            }
            else
            {
                if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                {
                    if aryMedia.count > 0
                    {
                        self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                        self.processResponce()
                    }
                }
            }
        }
        else
        {
            if let aryMedia : NSArray =  dictMessageData.value(forKey: "frames") as? NSArray
            {
                if aryMedia.count > 0
                {
                    let result: NSArray = aryMedia.sorted(by: {(int1, int2)  -> Bool in
                        return ((int1 as! NSDictionary).value(forKey: "frameNo") as! Int) < ((int2 as! NSDictionary).value(forKey: "frameNo") as! Int) // It sorted the values and return to the mySortedArray
                    }) as NSArray
                    self.aryMessageMedia = result.mutableCopy() as! NSMutableArray
                    self.processResponce()
                }
            }
        }
        
        
    }
    
    var aryCountIncr : NSInteger = 0
    func processResponce()
    {
        if(aryMessageMedia.count > aryCountIncr) {
            
            if let dictMedia : NSDictionary = aryMessageMedia.object(at: aryCountIncr) as? NSDictionary
            {
                //bloxDetails
                dictBloxDetails = NSMutableDictionary()
                //var strBloxType : String = String()
                if let strCaption : String = dictMedia.value(forKey: "caption") as? String
                {
                    dictBloxDetails.setValue(strCaption, forKey: "caption")
                }
                else
                {
                    dictBloxDetails.setValue("", forKey: "caption")
                }
                
                if var soundURL : String = dictMedia.value(forKey: "soundURL") as? String
                {
                        soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    dictBloxDetails.setValue(soundURL, forKey: "soundURL")
                }
                
                
                if let duration : NSNumber = dictMedia.value(forKey: "animationDuration") as? NSNumber
                {
                    dictBloxDetails.setValue(duration, forKey: "animationDuration")
                }
                else
                {
                    dictBloxDetails.setValue(1.0, forKey: "animationDuration")
                }
                
                if let strType : String = dictMedia.value(forKey: "mediaType") as? String
                {
                    
                    if(strType == GalleryType.Text as String)
                    {
                        strMessageBloxType = GalleryType.Text as String
                        
                        if let txtMessage : String = dictMedia.value(forKey: "text") as? String
                        {
                            self.txtMessage = txtMessage
                            dictBloxDetails.setValue(txtMessage, forKey: "message")
                        }
                        if let textColor : String = dictMedia.value(forKey: "textColor") as? String
                        {
                            self.txtColor = textColor
                            dictBloxDetails.setValue(textColor, forKey: "textColor")
                        }
                        if let txtSize : String = dictMedia.value(forKey: "textSize") as? String
                        {
                            guard let n = NumberFormatter().number(from: txtSize) else { return }
                            self.txtSize = CGFloat(n)
                            dictBloxDetails.setValue(txtSize, forKey: "fontSize")
                        }
                        if let txtStyle : String = dictMedia.value(forKey: "textStyle") as? String
                        {
                            self.txtStyle = txtStyle
                            dictBloxDetails.setValue(txtStyle, forKey: "fontName")
                        }
                        if let textBgMediaId : NSInteger = dictMedia.value(forKey: "textBgMediaId") as? NSInteger
                        {
                            dictBloxDetails.setValue(textBgMediaId, forKey: "textBgMediaId")
                        }
                        loadTextMessage()
                        
                    }
                    
                    
                    if(strType == GalleryType.Image as String)
                    {
                        strMessageBloxType = GalleryType.Image as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                    else if(strType == GalleryType.Video as String)
                    {
                        strMessageBloxType = GalleryType.Video as String
                        /*
                         if let strURL : String = dictMedia.valueForKey("thumb_url") as? String
                         {
                         if let img : UIImage = AppTheme().getImageFromUrl(strURL)
                         {
                         let data : NSData = UIImagePNGRepresentation(img)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         }
                         else
                         {
                         let data : NSData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         */
                        if var strVideoURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                             strVideoURL = strVideoURL.replacingOccurrences(of: " ", with: "%20")
                            //AppTheme().killFileOnPath("previewFile\(i).mp4")
                            //dictBloxDetails.setValue("previewFile\(i).mp4", forKey: "video")
                            var strImageThumbURL : String = String()
                            if var strURL : String = dictMedia.value(forKey: "thumbUrl") as? String {
                                   strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                                strImageThumbURL = strURL
                            }
                            loadVideoDataForURL(strVideoURL, fileName: "previewFile\(aryCountIncr).mp4", strURL: strImageThumbURL)
                            /*
                             AppTheme().callGetServiceForDownload(strVideoURL, fileNameToBeSaved: "previewFile\(i).mp4", completion: { (result, data) in
                             if(result == "success")
                             {
                             self.dictBloxDetails.setValue(data, forKey: "video")
                             }
                             })
                             */
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                        
                    }else if(strType == GalleryType.Sound as String){
                        strMessageBloxType = GalleryType.Sound as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
//                            self.playAudio(strURL)
//                            if var strMp3URL : String = dictMedia.value(forKey: "soundURL") as? String{
//                                strMp3URL = strMp3URL.replacingOccurrences(of: " ", with: "%20")
//                                self.playAudio(strMp3URL)
//                            }
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    } else if(strType == GalleryType.Gif as String){
                        strMessageBloxType = GalleryType.Gif as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            dictBloxDetails.setValue(strURL, forKey: "image")
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
                            
                            aryMessagePreivewData.add(dictTemp)
                            aryCountIncr += 1
                            processResponce()
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                }
                
            }
        }
        else
        {
            aryCountIncr = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
            vc.aryUserMessageData = self.aryMessagePreivewData
            vc.dictUserMessageData = self.dictUserMessageData
            vc.inbloxData = self.inbloxData
            vc.chatId = self.chatID
            vc.previewKind = "Bloxcast"
            vc.isSentByMe = self.isSentByMe
            vc.inputImageBgID = self.inputImageBgID
//            vc.bgImageName = self.bgImageName
            vc.strBgAudioPath = self.inputSoundBgID
            vc.bloxSubject = self.bloxSubject
            vc.user_name = self.bloxcastUsername
            vc.user_image = self.Blox_user_image
            vc.bloxDate = self.BloxDate
//            vc.ratingFromBlox = self.bloxRating
            vc.userID_for_profile = self.userID_for_profile
            //    vc.imgViewBackground.image = self.imgBackground.image
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            //self.perform(#selector(MessageConversationVC.allSetUpWithPreviewData), with: nil, afterDelay: 1.0)
        }
        
    }
    
    
    
    func loadTextMessage() {
        
        dictBloxDetails.setValue(UIColor.white, forKey: "backgroundColor")
        let imageData : Data = screenShot().jpegData(compressionQuality: 0.8)!//UIImageJPEGRepresentation(screenShot(), 0.8)!
        
        dictBloxDetails.setValue(imageData, forKey: "image")
        
        let dictTemp : NSMutableDictionary = NSMutableDictionary()
        dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
        dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
        
        aryMessagePreivewData.add(dictTemp)
        aryCountIncr += 1
        processResponce()
    }
    
    func loadImageFromURL(_ strURL:String)
    {
        var imageData : Data = Data()
        
        let fileName = URL(fileURLWithPath: strURL).pathExtension
        if fileName == "gif"{
            let urlNew:String = WEB_URL.BaseURL + strURL//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let imageData1 = try? Data(contentsOf: URL(string: urlNew)!)
            {
                let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
                print("isAnimated: \(isImageAnimated)")
                imageData = imageData1
            } else {
                
            }
        } else {
            let img : UIImage = AppTheme().getImageFromUrl(WEB_URL.BaseURL + strURL)
            //        if let img : UIImage = AppTheme().getImageFromUrl("http://103.76.248.170:3001/" + strURL)
            if img != nil
            {
                imageData = img.pngData()! // UIImagePNGRepresentation(img)! //
            } else {
                imageData = UIImage(named: "SingleBlox")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)! //
            }
        }
        let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
        print("isAnimated: \(isImageAnimated)")
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
            
            aryMessagePreivewData.add(dictTemp)
            aryCountIncr += 1
            processResponce()
        }
    }
    
    func loadVideoDataForURL(_ strVideoURL:String, fileName: String, strURL : String)
    {
        var imageData : Data = Data()
//        if let img : UIImage = AppTheme().getImageFromUrl(strURL)  //commented on 25Jan
        let img : UIImage = AppTheme().getImageFromUrl(strVideoURL)
        if img != nil
        {
            imageData = img.pngData()! // UIImagePNGRepresentation(img)!//
        }
        else
        {
//            imageData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!  //commented on 25Jan
            imageData = UIImage(named: "signup_background")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "signup_background")!)!//
        }
        
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
        }
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
            {
                // do some task
                let url = WEB_URL.BaseURL + strVideoURL//"http://103.76.248.170:3001/"
//            let url = "http://103.76.248.170:3001/" + strVideoURL
                AppTheme().callGetServiceForDownload(url, fileNameToBeSaved: fileName, completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async {
                            // update some UI
                            self.dictBloxDetails.setValue(data, forKey: "video")
                            //  self.dictBloxDetails.setValue(64, forKey: "id")
                            
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(self.dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue("Video", forKey: "bloxType")
                            
                            self.aryMessagePreivewData.add(dictTemp)
                            self.aryCountIncr += 1
                            self.processResponce()
                        }
                    } else {
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
        }
    }
    func screenShot() -> UIImage {
        var DynamicView=UILabel(frame: CGRect(x:68, y:0, width:240, height:240))
        DynamicView.backgroundColor=UIColor.white
        //DynamicView.layer.cornerRadius=25
        //DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
        DynamicView.text = self.txtMessage
        DynamicView.textAlignment = NSTextAlignment.center
        DynamicView.font = UIFont(name:self.txtStyle, size: self.txtSize)
        let clr = Utilities.shared.convertHexToUIColor(hexColor: self.txtColor)
        DynamicView.textColor = clr
        //DynamicView.font = UIFont.systemFont(ofSize: self.txtSize)
        // DynamicView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DynamicView.lineBreakMode = .byWordWrapping
        DynamicView.numberOfLines = 0
        
        var frameRect : CGRect = DynamicView.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        //var textView=UITextView(frame: CGRect(x:68, y:0, width:240, height:240))
        
        
        DynamicView.layer.borderColor = UIColor.clear.cgColor
        UIGraphicsBeginImageContextWithOptions(DynamicView.bounds.size, DynamicView.isOpaque, 0.0)
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        DynamicView.layer.borderColor = AppTheme().themeColorBlue.cgColor
        // txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
    
//    func convertHexToUIColor(hexColor : String) -> UIColor {
//        
//        // define character set (include whitespace, newline character etc.)
//        let characterSet = CharacterSet.whitespacesAndNewlines as CharacterSet
//        
//        //trim unnecessary character set from string
//        var colorString : String = hexColor.trimmingCharacters(in: characterSet)
//        
//        // convert to uppercase
//        colorString = colorString.uppercased()
//        
//        //if # found at start then remove it.
//        if colorString.hasPrefix("#") {
//            let index = colorString.index(colorString.startIndex, offsetBy: 1)
//            colorString =  String(colorString[..<index])
//        }
//        
//        if colorString.count != 6 {
//            return UIColor.black
//        }
//        
//        // split R,G,B component
//        var rgbValue: UInt32 = 0
//        Scanner(string:colorString).scanHexInt32(&rgbValue)
//        let valueRed    = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
//        let valueGreen  = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
//        let valueBlue   = CGFloat(rgbValue & 0x0000FF) / 255.0
//        let valueAlpha  = CGFloat(1.0)
//        
//        // return UIColor
//        return UIColor(red: valueRed, green: valueGreen, blue: valueBlue, alpha: valueAlpha)
//    }
    
//    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
//        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
//        let imageRef = image.cgImage
//
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
//        let context = UIGraphicsGetCurrentContext()
//
//        // Set the quality level to use when rescaling
//        context!.interpolationQuality = CGInterpolationQuality.high
//        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
//
//        context!.concatenate(flipVertical)
//        // Draw into the context; this scales the image
//        context!.draw(imageRef!, in: newRect)
//
//        let newImageRef = context!.makeImage()! as CGImage
//        let newImage = UIImage(cgImage: newImageRef)
//
//        // Get the resized image from the context and a UIImage
//        UIGraphicsEndImageContext()
//
//        return newImage
//    }

}

