//
//  ResetPasswordVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 9/11/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ResetPasswordVC: UIViewController {

    @IBOutlet var txtOldPassword:UITextField!
    @IBOutlet var txtNewPassword:UITextField!
    @IBOutlet var txtConfirmPassword:UITextField!
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    //MARK:- Helper Methods
    func isValidData() -> Bool {
        var msg = ""
        if self.txtOldPassword.text == "" {
            msg = "Please enter password."
        }else if self.txtNewPassword.text == "" {
            msg = "Please enter new password."
        }else if self.txtNewPassword.text != self.txtConfirmPassword.text {
            msg = "Pawword and confirm password must be same."
        }
        
        if msg == "" {
            return true
        }else {
//            AppSharedData.sharedInstance.showErrorAlert(message: msg)
            self.view.makeToast(msg, duration: 2.0, position: .bottom)
            return false
        }
    }
    //MARK:- UIButton Actions
    @IBAction func btnBackAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnUpdateAction(_ sender:Any) {
        if self.isValidData() {
            self.serverCallForResetPassword()
        }
    }
    @IBAction func btnCancelAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Sever calls
    func serverCallForResetPassword()  {
        LoadingIndicatorView.show()
        let param = ["userid":AppSharedData.sharedInstance.getUser()?.id ?? "","password":self.txtOldPassword.text ?? "", "newpassword":self.txtNewPassword.text ?? "","cnfpassword":self.txtConfirmPassword.text ?? ""]
        NetworkManager.sharedInstance.executeService(WEB_URL.resetPassword, postParameters: param as NSDictionary) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                    AppSharedData.sharedInstance.showErrorAlert(message: "Password reset successfully.")
//                    self.popToHome()
                    let when = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: when){
                        self.navigationController?.popViewController(animated: true)
                    }
                }else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
        
    }
}
