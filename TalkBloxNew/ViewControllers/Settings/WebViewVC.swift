//
//  WebViewVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 8/21/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController,WKUIDelegate,WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet var lblTitle:UILabel!
    
    var strTitle = ""
    var url = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = strTitle
        
//        let request = URLRequest.init(url: URL.init(string: url)!)
//        webView.loadRequest(request)
        
        webView!.uiDelegate = self
        webView!.navigationDelegate = self
        let Url = URL(string: url)!
        webView!.load(URLRequest(url: Url))
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        LoadingIndicatorView.show()
//    }
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        DispatchQueue.main.async {
//             LoadingIndicatorView.hide()
//        }
//    }
    
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        DispatchQueue.main.async {
//            LoadingIndicatorView.hide()
//        }
//    }
    
    //new wkwebview methods
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        LoadingIndicatorView.show()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DispatchQueue.main.async {
            LoadingIndicatorView.hide()
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DispatchQueue.main.async {
            LoadingIndicatorView.hide()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

