//
//  ProfileViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 06/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import Cosmos

class bcCell: UICollectionViewCell{
    
    @IBOutlet weak var blox_image: DesignableImageView!
    @IBOutlet weak var blox_subject: UILabel!
    @IBOutlet weak var views_count: UILabel!
    @IBOutlet weak var blox_ratings: CosmosView!
}

class fTableCell: UITableViewCell {
    @IBOutlet weak var user_img: DesignableImageView!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var follower: UILabel!
    @IBOutlet weak var bloxcast: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    
}

class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var followBtn: DesignableButton!
    @IBOutlet weak var user_intro: UILabel!
    @IBOutlet weak var user_img: DesignableImageView!
    @IBOutlet weak var editProfile_width: NSLayoutConstraint!
    
    @IBOutlet weak var following_count: UILabel!
    @IBOutlet weak var following_label: UILabel!
    @IBOutlet weak var followers_count: UILabel!
    @IBOutlet weak var followers_label: UILabel!
    @IBOutlet weak var bloxcast_count: UILabel!
    @IBOutlet weak var bloxcast_label: UILabel!
    @IBOutlet weak var views_count: UILabel!
    
    @IBOutlet weak var bcCollectionView: UICollectionView!
    
    @IBOutlet weak var collection_view: UIView!
    @IBOutlet weak var tblView: UIView!
    @IBOutlet weak var fTableview: UITableView!
    
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var dictUserMessageData : NSMutableDictionary!
    
    var inputImageBgID: String!
    var inputSoundBgID: String!
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    
    var messageID = 0
    var chatID = 0
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    
    var arrConversations = [BloxCast]()
    var arrUsers = [user]()
    var user_id: Int!
    var isSelfProfile: Bool!
    var previewKind = ""
    var pageNO = 1
    var followers_pageNo = 1
    var following_pageNo = 1
    var tableType = ""
    
    var userfollowedbyme = [Int]()
    
    var bloxRating: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bcCollectionView.delegate = self
        bcCollectionView.dataSource = self
        
        fTableview.delegate = self
        fTableview.dataSource = self
        // Do any additional setup after loading the view.
        bloxcast_label.font = UIFont.boldSystemFont(ofSize: 16)
        bloxcast_count.font = UIFont.boldSystemFont(ofSize: 16)
        following_label.font = UIFont.systemFont(ofSize: 14)
        following_count.font = UIFont.systemFont(ofSize: 14)
        followers_label.font = UIFont.systemFont(ofSize: 14)
        followers_count.font = UIFont.systemFont(ofSize: 14)
        self.serverCallForGetMyBloxCast()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isSelfProfile {
            followBtn.isHidden = true
            editProfile_width.constant = 100
            isSentByMe = true
            self.view.layoutIfNeeded()
        } else {
            followBtn.isHidden = false
            editProfile_width.constant = 0
            isSentByMe = false
            self.view.layoutIfNeeded()
        }
        self.serverCallForGetProfileDetail()
        self.serverCallForLoginUserFollowing()
//        self.serverCallForGetBloxCast()
//        self.populateData()
    }

    
    //MARK:- Helper Methods
//    func populateData() {
//        if previewKind == "Bloxcast" {
//
//        } else if previewKind == "Preview" {
//            let user = AppSharedData.sharedInstance.currentUser
//           self.nameLbl.text = user?.displayName  ?? ""
//           self.user_intro.text = user?.description ?? ""
//           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
//           self.user_img.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "TalkBlox"), options: nil, progressBlock: nil, completionHandler: nil)
//        } else if previewKind == "InBlox"{
//
//        }
//
//    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func followUser(_ sender: UIButton) {
        if sender.title(for: .normal) == "Unfollow" {
            self.serverCallForUnSubscribe(USERID: user_id)
        } else {
            self.serverCallForSubscribe(USERID: user_id)
        }
    }
    @IBAction func editProfile(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func bloxcast_tapped(_ sender: UIButton) {
        collection_view.isHidden = false
        tblView.isHidden = true
        bloxcast_label.font = UIFont.boldSystemFont(ofSize: 16)
        bloxcast_count.font = UIFont.boldSystemFont(ofSize: 16)
        following_label.font = UIFont.systemFont(ofSize: 14)
        following_count.font = UIFont.systemFont(ofSize: 14)
        followers_label.font = UIFont.systemFont(ofSize: 14)
        followers_count.font = UIFont.systemFont(ofSize: 14)
        
        self.serverCallForGetMyBloxCast()
    }
    @IBAction func following_tapped(_ sender: UIButton) {
        collection_view.isHidden = true
        tblView.isHidden = false
        tableType = "following"
        bloxcast_label.font = UIFont.systemFont(ofSize: 14)
        bloxcast_count.font = UIFont.systemFont(ofSize: 14)
        following_label.font = UIFont.boldSystemFont(ofSize: 16)
        following_count.font = UIFont.boldSystemFont(ofSize: 16)
        followers_label.font = UIFont.systemFont(ofSize: 14)
        followers_count.font = UIFont.systemFont(ofSize: 14)
        self.serverCallForMyFollowing(pageNo: following_pageNo)
    }
    @IBAction func followers_tapped(_ sender: UIButton) {
        collection_view.isHidden = true
        tblView.isHidden = false
        tableType = "followers"
        bloxcast_label.font = UIFont.systemFont(ofSize: 14)
        bloxcast_count.font = UIFont.systemFont(ofSize: 14)
        following_label.font = UIFont.systemFont(ofSize: 14)
        following_count.font = UIFont.systemFont(ofSize: 14)
        followers_label.font = UIFont.boldSystemFont(ofSize: 16)
        followers_count.font = UIFont.boldSystemFont(ofSize: 16)
        self.serverCallForMySubscribers(pageNo: followers_pageNo)
    }
    @IBAction func followBtnTapped(_ sender: UIButton) {
        if sender.title(for: .normal) == "Unfollow" {
            self.serverCallForUnSubscribe(USERID: sender.tag)
        } else {
            self.serverCallForSubscribe(USERID: sender.tag)
        }
    }
    
    //MARK:- UITableViewDelegates And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fTableCell", for: indexPath) as! fTableCell
        cell.tag = indexPath.row
        let user = arrUsers[indexPath.row]
        
        var urlstring: String = WEB_URL.BaseURL + user.user_image
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        cell.user_img.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.user_name.text = user.displayName
        cell.following.text = (user.following).formattedWithSeparator
        cell.follower.text = (Int(user.subscribers))?.formattedWithSeparator
        cell.bloxcast.text = (user.bloxcastCount).formattedWithSeparator
        
        cell.followBtn.tag = user.userid
        if userfollowedbyme.contains(user.userid){
            cell.followBtn.cornerRadius = 4.0
            cell.followBtn.borderWidth = 0.0
            cell.followBtn.borderColor = .clear
            cell.followBtn.backgroundColor = #colorLiteral(red: 0.5758395791, green: 0.1376600266, blue: 0.1044845805, alpha: 1)
            cell.followBtn.setTitle("Unfollow", for: .normal)
        } else {
            cell.followBtn.cornerRadius = 4.0
            cell.followBtn.borderWidth = 1.0
            cell.followBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.followBtn.backgroundColor = .clear
            cell.followBtn.setTitle("Follow", for: .normal)
        }
        return cell
        
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 105
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableType == "following" {
            if indexPath.row == (following_pageNo * 10) - 1 {
                following_pageNo = following_pageNo + 1
                self.serverCallForMyFollowing(pageNo: following_pageNo)
            }
        } else {
            if indexPath.row == (followers_pageNo * 10) - 1 {
                followers_pageNo = followers_pageNo + 1
                self.serverCallForMySubscribers(pageNo: followers_pageNo)
            }
        }
    }
    
    //MARK:-  Collectionview Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrConversations.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bcCell", for: indexPath) as! bcCell
        let conversation = arrConversations[indexPath.row]
        
        var urlstring = WEB_URL.BaseURL + (conversation.mediaUrl ?? "")
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        if urlstring.hasSuffix(".mp4") || urlstring.hasSuffix(".gif") {
//            let img : UIImage = AppTheme().getImageFromUrl(urlstring)
//            cell.blox_image.image = img
            
            var img: UIImage!
            cell.blox_image.image = nil
            DispatchQueue.global(qos: .userInitiated).async {
                img  = AppTheme().getImageFromUrl(urlstring)
                DispatchQueue.main.async {
                    cell.blox_image.image = img
                }
            }
        } else {
        cell.blox_image.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        cell.views_count.text = (conversation.views)!.formattedWithSeparator + " views"
        cell.blox_subject.text = conversation.subject
        cell.blox_ratings.rating = conversation.rating!
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: (screenWidth-60)/2 , height: (screenWidth-60)/2);
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == (pageNO * 10) - 1 {
            pageNO = pageNO + 1
            self.serverCallForGetMyBloxCast()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let conversation = arrConversations[indexPath.row]
        self.aryMessagePreivewData.removeAllObjects()
        self.chatID = conversation.chat_id ?? 0
        self.bloxSubject = conversation.subject!
        self.Blox_user_image = conversation.user_image!
        self.bloxcastUsername = conversation.displayName!
        self.BloxDate =  Utilities.shared.getChangedDate(date: conversation.created_at!)
        self.aryMessagePreivewData.removeAllObjects()
//        self.serverCallForPreview(message: conversation)
//        self.chatID = conversation.chat_id ?? 0
        self.userID_for_profile = conversation.userid!
        self.bloxRating = conversation.rating!
        
    }
    
    //MARK:- Server Calls
    func serverCallForGetProfileDetail(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":user_id ?? ""]
        NetworkManager.sharedInstance.executeService(WEB_URL.getProfile, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let userData = [UserDetail].from(jsonArray: response?["result"] as! [JSON])
                    let currentUser = userData?[0]
                    DispatchQueue.main.async {
                        self.nameLbl.text = currentUser?.displayName  ?? ""
                        self.user_intro.text = currentUser?.description ?? ""
                        let imgURL = WEB_URL.BaseURL + (currentUser?.user_image ?? "")
                        self.user_img.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "TalkBlox"), options: nil, progressBlock: nil, completionHandler: nil)
                        self.following_count.text = (currentUser?.following)!.formattedWithSeparator //String((currentUser?.following)!)
                        self.followers_count.text = Int((currentUser?.subscribers)!)?.formattedWithSeparator //String((currentUser?.subscribers)!)
                        self.bloxcast_count.text = (currentUser?.myBloxCast_Count)!.formattedWithSeparator //String((currentUser?.myBloxCast_Count)!)
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCallForGetBloxCast(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d", user_id)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.seeBloxCastWithRating, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [BloxCast].from(jsonArray: response?["Result"] as! [JSON])
                    
                    self.arrConversations = inbloxData!
                    
                    self.bcCollectionView.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCallForGetMyBloxCast(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d&page=%d&limit=10", user_id, pageNO)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.myBloxCastList, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [BloxCast].from(jsonArray: response?["Result"] as! [JSON])
                    
                    if self.pageNO == 1 {
                        self.arrConversations = inbloxData!
                    } else {
                        self.arrConversations.append(contentsOf: inbloxData!)
                    }
                    
                    self.bcCollectionView.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCallForSubscribe(USERID: Int){
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "name": AppSharedData.sharedInstance.currentUser?.displayName ?? "","subscriberData":[USERID]]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.subscribe, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        self.followBtn.setTitle("Unfollow", for: .normal)
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                        self.fTableview.reloadData()
                        self.serverCallForLoginUserFollowing()
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                        
                        
                    }else if statusDic["ErrorCode"] as! Int == 401 {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                    else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    } else {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        })
    }
    func serverCallForUnSubscribe(USERID: Int){
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0),"unSubscribeId": USERID]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.unsubscribe, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        self.followBtn.setTitle("Follow", for: .normal)
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                        self.fTableview.reloadData()
                        self.serverCallForLoginUserFollowing()
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                        
                    }else if statusDic["ErrorCode"] as! Int == 401 {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                    else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    } else {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        })
    }
    
    func serverCallForPreview(message:BloxCast)  {
        let params:NSDictionary = ["message_id":messageID,"chat_id":message.chat_id ?? 0]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.preview, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            //            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let dict : NSDictionary = response {
                        
                        print("\n\ndictionary for message all urls :", dict, "\n\n")
                        self.generateDictionaryForPreview(dict.mutableCopy() as! NSMutableDictionary )
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCallForMySubscribers(pageNo: Int){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(user_id),"page":pageNo, "limit": 10]
        let url = WEB_URL.BaseURL + String(format: "api/v1/mySubscribeList") as NSString
        NetworkManager.sharedInstance.executeServiceFor(url: url, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    let userlist = response?["user"] as! NSArray
                    if pageNo == 1 {
                        self.arrUsers.removeAll()
                    }
                    for users in userlist{
                        let currentUser = users as! NSDictionary
                        let usr = user(userid: (currentUser["userid"] as! NSNumber).intValue, user_image: currentUser["user_image"] as! String, displayName: currentUser["displayName"] as! String, description: currentUser["description"] as! String, subscribers: currentUser["subscribers"] as! String, phoneNo: currentUser["phoneNo"] as! String, phone_code: currentUser["phone_code"] as! String, user_added_on: currentUser["user_added_on"] as! String, following: currentUser["following"] as! Int, bloxcastCount: currentUser["bloxcastCount"] as! Int)
                        self.arrUsers.append(usr)
                    }
                    self.fTableview.reloadData()
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    
    func serverCallForMyFollowing(pageNo: Int){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(user_id),"page":pageNo, "limit": 10]
        let url = WEB_URL.BaseURL + String(format: "api/v1/myFollowingList") as NSString
        NetworkManager.sharedInstance.executeServiceFor(url: url, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    let userlist = response?["user"] as! NSArray
                    if pageNo == 1 {
                        self.arrUsers.removeAll()
                    }
                    for users in userlist{
                        let currentUser = users as! NSDictionary
                        let usr = user(userid: (currentUser["userid"] as! NSNumber).intValue, user_image: currentUser["user_image"] as! String, displayName: currentUser["displayName"] as! String, description: currentUser["description"] as! String, subscribers: currentUser["subscribers"] as! String, phoneNo: currentUser["phoneNo"] as! String, phone_code: currentUser["phone_code"] as! String, user_added_on: currentUser["user_added_on"] as! String, following: currentUser["following"] as! Int, bloxcastCount: currentUser["bloxcastCount"] as! Int)
                        self.arrUsers.append(usr)
                    }
                    self.fTableview.reloadData()
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    func serverCallForLoginUserFollowing(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0),"page":1, "limit": 1000]
        let url = WEB_URL.BaseURL + String(format: "api/v1/myFollowingList") as NSString
        NetworkManager.sharedInstance.executeServiceFor(url: url, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    self.userfollowedbyme.removeAll()
                    let userlist = response?["user"] as! NSArray
                    for users in userlist{
                        let currentUser = users as! NSDictionary
                        let usr = (currentUser["userid"] as! NSNumber).intValue
                        self.userfollowedbyme.append(usr)
                    }
                    if self.userfollowedbyme.contains(self.user_id){
                        self.followBtn.borderWidth = 0.0
                        self.followBtn.borderColor = .clear
                        self.followBtn.backgroundColor = #colorLiteral(red: 0.5758395791, green: 0.1376600266, blue: 0.1044845805, alpha: 1)
                        self.followBtn.setTitle("Unfollow", for: .normal)
                    } else {
                        self.followBtn.borderWidth = 1.0
                        self.followBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                        self.followBtn.backgroundColor = .clear
                        self.followBtn.setTitle("Follow", for: .normal)
                    }
                    self.fTableview.reloadData()
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    
    //MARK:- Helper Methods
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        self.present(errorAlert, animated: true ,completion: nil)
    }
    func generateDictionaryForPreview(_ dictMessageData : NSMutableDictionary)
    {
//        if let imageBg = dictMessageData.value(forKey: "imageBg")
        if let imageBg = dictMessageData.value(forKey: "chatImage") // changed on 28Dec2020
        {
            inputImageBgID = imageBg as! String
        }
        if let imageBg = dictMessageData.value(forKey: "chatSound") // changed on 28Dec2020
        {
            inputSoundBgID = imageBg as! String
        }
        if let contactId = dictMessageData.value(forKey: "contact_id")
        {
            dictMessagePreivewData.setValue(contactId, forKey: "contact_id")
        }
        if let messageRecList = dictMessageData.value(forKey: "message_receiver_list")
        {
            dictMessagePreivewData.setValue(messageRecList, forKey: "message_receiver_list")
        }
        if let messageUserId = dictMessageData.value(forKey: "message_user_id")
        {
            dictMessagePreivewData.setValue(messageUserId, forKey: "message_user_id")
        }
        if let messageUserName = dictMessageData.value(forKey: "message_user_name")
        {
            dictMessagePreivewData.setValue(messageUserName, forKey: "message_user_name")
        }
        
        if(messageParentId != 0)
        {
            dictMessagePreivewData.setValue(messageParentId, forKey: "parentId")
        }
        if(strMessageUserName != "")
        {
            dictMessagePreivewData.setValue(strMessageUserName, forKey: "messageUserName")
        }
        
        dictMessagePreivewData.setValue(strMessageId, forKey: "id")
        
        dictMessagePreivewData.setValue("Inbox", forKey: "MessagePreviewType")
        
        
        //-- Code to download BG bubble image ---
        
        if(previewBubbleImageURL != "")
        {
          
            previewBubbleImageURL = previewBubbleImageURL.replacingOccurrences(of: " ", with: "%20")
            var imageData : UIImage = UIImage()
            let img : UIImage = AppTheme().getImageFromUrl(previewBubbleImageURL)
            if img != nil
            {
                imageData = img
            }
            else
            {
                previewBubbleImageURL = ""
            }
            if(imageData.size.height > 0)
            {
                bubblePreviewImage = imageData
            }
            else
            {
                previewBubbleImageURL = ""
            }
        }
        //----
        
        /////new logic for bg Music
        if var strBgURL : String = dictMessageData.value(forKey: "chatSound") as? String
        {
              strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
            //AppTheme().killFileOnPath("previewBg.caf")
            
            //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
            AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                
                if(result == "success")
                {
                    self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
                else
                {
                    self.showAlert(data as! String, strTitle: "Error")
                }
                
            })
        }
        
        
        if let aryBgMedia : NSArray = dictMessageData.value(forKey: "bgMedia") as? NSArray
        {
            if let dictBgMedia : NSDictionary = aryBgMedia.object(at: 0) as? NSDictionary
            {
                if var strBgURL : String = dictBgMedia.value(forKey: "mediaUrl") as? String
                {
                      strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
                    //AppTheme().killFileOnPath("previewBg.caf")
                    
                    //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                            if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                            {
                                if aryMedia.count > 0
                                {
                                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                                    self.processResponce()
                                }
                            }
                        }
                        else
                        {
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                    })
                }
                else
                {
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
            }
            else
            {
                if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                {
                    if aryMedia.count > 0
                    {
                        self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                        self.processResponce()
                    }
                }
            }
        }
        else
        {
            if let aryMedia : NSArray =  dictMessageData.value(forKey: "frames") as? NSArray
            {
                if aryMedia.count > 0
                {
                    let result: NSArray = aryMedia.sorted(by: {(int1, int2)  -> Bool in
                        return ((int1 as! NSDictionary).value(forKey: "frameNo") as! Int) < ((int2 as! NSDictionary).value(forKey: "frameNo") as! Int) // It sorted the values and return to the mySortedArray
                    }) as NSArray
                    self.aryMessageMedia = result.mutableCopy() as! NSMutableArray
                    self.processResponce()
                }
            }
        }
        
        
    }
    
    var aryCountIncr : NSInteger = 0
    func processResponce()
    {
        if(aryMessageMedia.count > aryCountIncr) {
            
            if let dictMedia : NSDictionary = aryMessageMedia.object(at: aryCountIncr) as? NSDictionary
            {
                //bloxDetails
                dictBloxDetails = NSMutableDictionary()
                //var strBloxType : String = String()
                if let strCaption : String = dictMedia.value(forKey: "caption") as? String
                {
                    dictBloxDetails.setValue(strCaption, forKey: "caption")
                }
                else
                {
                    dictBloxDetails.setValue("", forKey: "caption")
                }
                
                if var soundURL : String = dictMedia.value(forKey: "soundURL") as? String
                {
                        soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    dictBloxDetails.setValue(soundURL, forKey: "soundURL")
                }
                
                
                if let duration : NSNumber = dictMedia.value(forKey: "animationDuration") as? NSNumber
                {
                    dictBloxDetails.setValue(duration, forKey: "animationDuration")
                }
                else
                {
                    dictBloxDetails.setValue(1.0, forKey: "animationDuration")
                }
                
                if let strType : String = dictMedia.value(forKey: "mediaType") as? String
                {
                    
                    if(strType == GalleryType.Text as String)
                    {
                        strMessageBloxType = GalleryType.Text as String
                        
                        if let txtMessage : String = dictMedia.value(forKey: "text") as? String
                        {
                            self.txtMessage = txtMessage
                            dictBloxDetails.setValue(txtMessage, forKey: "message")
                        }
                        if let textColor : String = dictMedia.value(forKey: "textColor") as? String
                        {
                            self.txtColor = textColor
                            dictBloxDetails.setValue(textColor, forKey: "textColor")
                        }
                        if let txtSize : String = dictMedia.value(forKey: "textSize") as? String
                        {
                            guard let n = NumberFormatter().number(from: txtSize) else { return }
                            self.txtSize = CGFloat(n)
                            dictBloxDetails.setValue(txtSize, forKey: "fontSize")
                        }
                        if let txtStyle : String = dictMedia.value(forKey: "textStyle") as? String
                        {
                            self.txtStyle = txtStyle
                            dictBloxDetails.setValue(txtStyle, forKey: "fontName")
                        }
                        if let textBgMediaId : NSInteger = dictMedia.value(forKey: "textBgMediaId") as? NSInteger
                        {
                            dictBloxDetails.setValue(textBgMediaId, forKey: "textBgMediaId")
                        }
                        loadTextMessage()
                        
                    }
                    
                    
                    if(strType == GalleryType.Image as String)
                    {
                        strMessageBloxType = GalleryType.Image as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                    else if(strType == GalleryType.Video as String)
                    {
                        strMessageBloxType = GalleryType.Video as String
                        /*
                         if let strURL : String = dictMedia.valueForKey("thumb_url") as? String
                         {
                         if let img : UIImage = AppTheme().getImageFromUrl(strURL)
                         {
                         let data : NSData = UIImagePNGRepresentation(img)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         }
                         else
                         {
                         let data : NSData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         */
                        if var strVideoURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                             strVideoURL = strVideoURL.replacingOccurrences(of: " ", with: "%20")
                            //AppTheme().killFileOnPath("previewFile\(i).mp4")
                            //dictBloxDetails.setValue("previewFile\(i).mp4", forKey: "video")
                            var strImageThumbURL : String = String()
                            if var strURL : String = dictMedia.value(forKey: "thumbUrl") as? String {
                                   strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                                strImageThumbURL = strURL
                            }
                            loadVideoDataForURL(strVideoURL, fileName: "previewFile\(aryCountIncr).mp4", strURL: strImageThumbURL)
                            /*
                             AppTheme().callGetServiceForDownload(strVideoURL, fileNameToBeSaved: "previewFile\(i).mp4", completion: { (result, data) in
                             if(result == "success")
                             {
                             self.dictBloxDetails.setValue(data, forKey: "video")
                             }
                             })
                             */
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                        
                    }else if(strType == GalleryType.Sound as String){
                        strMessageBloxType = GalleryType.Sound as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
//                            self.playAudio(strURL)
//                            if var strMp3URL : String = dictMedia.value(forKey: "soundURL") as? String{
//                                strMp3URL = strMp3URL.replacingOccurrences(of: " ", with: "%20")
//                                self.playAudio(strMp3URL)
//                            }
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    } else if(strType == GalleryType.Gif as String){
                        strMessageBloxType = GalleryType.Gif as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            dictBloxDetails.setValue(strURL, forKey: "image")
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
                            
                            aryMessagePreivewData.add(dictTemp)
                            aryCountIncr += 1
                            processResponce()
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                }
                
            }
        }
        else
        {
            aryCountIncr = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
            vc.aryUserMessageData = self.aryMessagePreivewData
            vc.dictUserMessageData = self.dictUserMessageData
//            vc.inbloxData = self.inbloxData
            vc.chatId = self.chatID
            vc.previewKind = "Bloxcast"
            vc.isSentByMe = self.isSentByMe
            vc.inputImageBgID = self.inputImageBgID
//            vc.bgImageName = self.bgImageName
            vc.strBgAudioPath = self.inputSoundBgID
            vc.bloxSubject = self.bloxSubject
            vc.user_name = self.bloxcastUsername
            vc.user_image = self.Blox_user_image
            vc.bloxDate = self.BloxDate
            vc.userID_for_profile = self.userID_for_profile
            //    vc.imgViewBackground.image = self.imgBackground.image
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            //self.perform(#selector(MessageConversationVC.allSetUpWithPreviewData), with: nil, afterDelay: 1.0)
        }
        
    }
    
    
    
    func loadTextMessage() {
        
        dictBloxDetails.setValue(UIColor.white, forKey: "backgroundColor")
        let imageData : Data = screenShot().jpegData(compressionQuality: 0.8)!//UIImageJPEGRepresentation(screenShot(), 0.8)!
        
        dictBloxDetails.setValue(imageData, forKey: "image")
        
        let dictTemp : NSMutableDictionary = NSMutableDictionary()
        dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
        dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
        
        aryMessagePreivewData.add(dictTemp)
        aryCountIncr += 1
        processResponce()
    }
    
    func loadImageFromURL(_ strURL:String)
    {
        var imageData : Data = Data()
        
        let fileName = URL(fileURLWithPath: strURL).pathExtension
        if fileName == "gif"{
            let urlNew:String = WEB_URL.BaseURL + strURL//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let imageData1 = try? Data(contentsOf: URL(string: urlNew)!)
            {
                let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
                print("isAnimated: \(isImageAnimated)")
                imageData = imageData1
            } else {
                
            }
        } else {
            let img : UIImage = AppTheme().getImageFromUrl(WEB_URL.BaseURL + strURL)
            //        if let img : UIImage = AppTheme().getImageFromUrl("http://103.76.248.170:3001/" + strURL)
            if img != nil
            {
                imageData = img.pngData()! // UIImagePNGRepresentation(img)! //
            } else {
                imageData = UIImage(named: "SingleBlox")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)! //
            }
        }
        let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
        print("isAnimated: \(isImageAnimated)")
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
            
            aryMessagePreivewData.add(dictTemp)
            aryCountIncr += 1
            processResponce()
        }
    }
    
    func loadVideoDataForURL(_ strVideoURL:String, fileName: String, strURL : String)
    {
        var imageData : Data = Data()
//        if let img : UIImage = AppTheme().getImageFromUrl(strURL)  //commented on 25Jan
        let img : UIImage = AppTheme().getImageFromUrl(strVideoURL)
        if img != nil
        {
            imageData = img.pngData()! // UIImagePNGRepresentation(img)!//
        }
        else
        {
//            imageData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!  //commented on 25Jan
            imageData = UIImage(named: "signup_background")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "signup_background")!)!//
        }
        
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
        }
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
            {
                // do some task
                let url = WEB_URL.BaseURL + strVideoURL//"http://103.76.248.170:3001/"
//            let url = "http://103.76.248.170:3001/" + strVideoURL
                AppTheme().callGetServiceForDownload(url, fileNameToBeSaved: fileName, completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async {
                            // update some UI
                            self.dictBloxDetails.setValue(data, forKey: "video")
                            //  self.dictBloxDetails.setValue(64, forKey: "id")
                            
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(self.dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue("Video", forKey: "bloxType")
                            
                            self.aryMessagePreivewData.add(dictTemp)
                            self.aryCountIncr += 1
                            self.processResponce()
                        }
                    } else {
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
        }
    }
    func screenShot() -> UIImage {
        var DynamicView=UILabel(frame: CGRect(x:68, y:0, width:240, height:240))
        DynamicView.backgroundColor=UIColor.white
        //DynamicView.layer.cornerRadius=25
        //DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
        DynamicView.text = self.txtMessage
        DynamicView.textAlignment = NSTextAlignment.center
        DynamicView.font = UIFont(name:self.txtStyle, size: self.txtSize)
        let clr = Utilities.shared.convertHexToUIColor(hexColor: self.txtColor)
        DynamicView.textColor = clr
        //DynamicView.font = UIFont.systemFont(ofSize: self.txtSize)
        // DynamicView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DynamicView.lineBreakMode = .byWordWrapping
        DynamicView.numberOfLines = 0
        
        var frameRect : CGRect = DynamicView.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        //var textView=UITextView(frame: CGRect(x:68, y:0, width:240, height:240))
        
        
        DynamicView.layer.borderColor = UIColor.clear.cgColor
        UIGraphicsBeginImageContextWithOptions(DynamicView.bounds.size, DynamicView.isOpaque, 0.0)
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        DynamicView.layer.borderColor = AppTheme().themeColorBlue.cgColor
        // txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
    
//    func convertHexToUIColor(hexColor : String) -> UIColor {
//        
//        // define character set (include whitespace, newline character etc.)
//        let characterSet = CharacterSet.whitespacesAndNewlines as CharacterSet
//        
//        //trim unnecessary character set from string
//        var colorString : String = hexColor.trimmingCharacters(in: characterSet)
//        
//        // convert to uppercase
//        colorString = colorString.uppercased()
//        
//        //if # found at start then remove it.
//        if colorString.hasPrefix("#") {
//            let index = colorString.index(colorString.startIndex, offsetBy: 1)
//            colorString =  String(colorString[..<index])
//        }
//        
//        if colorString.count != 6 {
//            return UIColor.black
//        }
//        
//        // split R,G,B component
//        var rgbValue: UInt32 = 0
//        Scanner(string:colorString).scanHexInt32(&rgbValue)
//        let valueRed    = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
//        let valueGreen  = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
//        let valueBlue   = CGFloat(rgbValue & 0x0000FF) / 255.0
//        let valueAlpha  = CGFloat(1.0)
//        
//        // return UIColor
//        return UIColor(red: valueRed, green: valueGreen, blue: valueBlue, alpha: valueAlpha)
//    }
    
//    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
//        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
//        let imageRef = image.cgImage
//
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
//        let context = UIGraphicsGetCurrentContext()
//
//        // Set the quality level to use when rescaling
//        context!.interpolationQuality = CGInterpolationQuality.high
//        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
//
//        context!.concatenate(flipVertical)
//        // Draw into the context; this scales the image
//        context!.draw(imageRef!, in: newRect)
//
//        let newImageRef = context!.makeImage()! as CGImage
//        let newImage = UIImage(cgImage: newImageRef)
//
//        // Get the resized image from the context and a UIImage
//        UIGraphicsEndImageContext()
//
//        return newImage
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
