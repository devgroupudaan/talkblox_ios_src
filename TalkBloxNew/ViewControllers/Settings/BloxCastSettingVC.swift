//
//  BloxCastSettingVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/23/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit

class BloxCastSettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tblView:UITableView!
    
    var arrBloxCasts = [String]()
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
         arrBloxCasts.append("BloxCasts")
        arrBloxCasts.append("My Contacts")
        arrBloxCasts.append("Contacts of My Contacts")
        arrBloxCasts.append("Everyone")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIButton Actions
    @IBAction func btnHomeAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK;- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBloxCasts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BloxCastCell")
        let imgRadio = cell?.viewWithTag(100) as! UIImageView
        let lblOption = cell?.viewWithTag(101) as! UILabel
        let swtStatus = cell?.viewWithTag(102) as! UISwitch
        lblOption.text = arrBloxCasts[indexPath.row]
        if indexPath.row == 0 {
            swtStatus.isHidden = false
            imgRadio.isHidden = true
        }else {
            swtStatus.isHidden = true
            imgRadio.isHidden = false
        }
        if selectedIndex == indexPath.row {
            imgRadio.image = #imageLiteral(resourceName: "check_radio")
        }else {
            imgRadio.image = #imageLiteral(resourceName: "uncheck_radio")
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            selectedIndex = indexPath.row
            self.tblView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
