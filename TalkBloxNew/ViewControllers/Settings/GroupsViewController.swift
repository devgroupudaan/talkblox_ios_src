//
//  GroupsViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 23/06/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import Contacts
import Gloss
import ContactsUI
import IQKeyboardManagerSwift

class groupCell: UITableViewCell{
    @IBOutlet weak var groupImg: UIImageView!
    @IBOutlet weak var groupName: UILabel!
    
}
class sContactsCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var cName: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
}

class selectedContactCell: UITableViewCell {
    @IBOutlet weak var contact_name: UILabel!
    
}

class GroupsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var groupTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var contactsSearchBar: UISearchBar!
    @IBOutlet weak var addGroupPopUp: UIView!
    
    @IBOutlet weak var contactsPickerView: UIView!
    @IBOutlet weak var contactsTbl: UITableView!
    @IBOutlet weak var groupName: UITextField!
    @IBOutlet weak var selectedContactsTbl: UITableView!
    
    var searchActive : Bool = false
    var filteredData : NSMutableArray = NSMutableArray()
    
    var groups = [Group]()
    var searchResults = [Group]()
    
    var arrContacts = [String]()
    var arrContactNames = [String]()

    let contactStore = CNContactStore()
    var contactSearchActive : Bool = false
    var contacts = [CNContact]()
    var searchContactsResults = [CNContact]()
//    var selectedContacts = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        searchBar.backgroundColor = .clear
//        searchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            searchBar.backgroundImage = UIImage()
            searchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        contactsSearchBar.delegate = self
        contactsSearchBar.backgroundColor = .clear
//        contactsSearchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            contactsSearchBar.backgroundImage = UIImage()
            contactsSearchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        groupTable.delegate = self
        groupTable.dataSource = self
        
        contactsTbl.delegate = self
        contactsTbl.dataSource = self
        
        selectedContactsTbl.delegate = self
        selectedContactsTbl.dataSource = self
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        serverCallForGetGroups()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.searchBar.resignFirstResponder()
        self.contactsSearchBar.resignFirstResponder()
        self.groupName.resignFirstResponder()
    }
    
    func fetchContacts (){
        self.contacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                    }
                    
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result

            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTbl.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contactsPickerBack(_ sender: UIButton) {
        contactsPickerView.isHidden = true
        arrContacts.removeAll()
        contactsSearchBar.resignFirstResponder()
        contactsSearchBar.text = ""
    }
    @IBAction func contactPickerNext(_ sender: UIButton) {
        if arrContacts.count == 0{
//            AppSharedData.sharedInstance.showErrorAlert(message: "No user selected.")
            self.view.makeToast("No user selected", duration: 2.0, position: .bottom)
        } else {
            self.addGroupPopUp.isHidden = false
        }
        contactsSearchBar.resignFirstResponder()
        selectedContactsTbl.reloadData()
    }
    @IBAction func addGroup(_ sender: UIButton) {
        self.fetchContacts()
        self.contactsPickerView.isHidden = false
        self.arrContacts.removeAll()
    }
    @IBAction func confirmAddGroup(_ sender: UIButton) {
        serverCallForCreateGroup()
    }
    @IBAction func cancelAddGroup(_ sender: UIButton) {
        self.contactsPickerView.isHidden = true
        self.addGroupPopUp.isHidden = true
        self.arrContacts.removeAll()
    }
    
    @IBAction func selectContacts(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(contactSearchActive)
            {
                if searchContactsResults[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (searchContactsResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                            arrContactNames.remove(at: index)
                        }
                    }
//                    let currentContactname = searchContactsResults[sender.tag].givenName
//                    for (index,contactName) in arrContactNames.enumerated(){
//                        if contactName == currentContactname {
//                            arrContactNames.remove(at: index)
//                        }
//                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                            arrContactNames.remove(at: index)
                        }
                    }
//                    let currentContactname = contacts[sender.tag].givenName
//                    for (index,contactName) in arrContactNames.enumerated(){
//                        if contactName == currentContactname {
//                            arrContactNames.remove(at: index)
//                        }
//                    }
                }
            }
        } else {
            sender.isSelected = true
            if(contactSearchActive)
            {
                if searchContactsResults[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (searchContactsResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                        let currentContactname = searchContactsResults[sender.tag].givenName
                        arrContactNames.append(currentContactname)
                    }
//                    let currentContactname = searchContactsResults[sender.tag].givenName
//                    if !arrContactNames.contains(currentContactname){
//                        arrContactNames.append(currentContactname)
//                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    
                    let currentContactStr = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                        let currentContactname = contacts[sender.tag].givenName
                        arrContactNames.append(currentContactname)
                    }
//                    let currentContactname = contacts[sender.tag].givenName
//                    if !arrContactNames.contains(currentContactname){
//                        arrContactNames.append(currentContactname)
//                    }
                }
            }
        }
    }
    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == groupTable {
            if(searchActive) {
                return searchResults.count
            } else {
                return groups.count
            }
        } else if tableView == contactsTbl {
            if(contactSearchActive) {
                return searchContactsResults.count
            } else {
                return contacts.count
            }
        } else {
            return arrContacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == groupTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "groupCell") as! groupCell
            if(searchActive) {
                cell.groupName.text =  searchResults[indexPath.row].group_name
            } else {
                cell.groupName.text =  groups[indexPath.row].group_name
            }
            return cell
        } else if tableView == contactsTbl {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sContactsCell") as! sContactsCell
            if(contactSearchActive) {
                cell.cName.text =  "\(searchContactsResults[indexPath.row].givenName) \(searchContactsResults[indexPath.row].familyName)"
                let currentContact = (searchContactsResults[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
            } else {
                cell.cName.text =  "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
                let currentContact = (contacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
            }
            cell.checkBtn.tag = indexPath.row
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectedContactCell") as! selectedContactCell
            cell.contact_name.text =  arrContactNames[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == groupTable {
            let groupID = groups[indexPath.row].group_id
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "GroupMembersViewController") as! GroupMembersViewController
            vc.groupID = groupID
            vc.groupNameStr = groups[indexPath.row].group_name!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//
//        } else if editingStyle == .insert {
//
//        }
//    }
//    func tableView(_ tableView: UITableView,
//                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//    {
//        let editAction = UIContextualAction(style: .normal, title:  "Edit", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//            print("edit")
//            self.selectedContact = self.contacts[indexPath.row].mutableCopy() as? CNMutableContact
//            self.nameFld.text = self.selectedContact.givenName
//            self.familyNameFld.text = self.selectedContact.familyName
//            self.phoneFld.text = self.selectedContact.phoneNumbers.first?.value.stringValue
//            self.emailFld.text = "\(self.selectedContact.emailAddresses.first?.value ?? "")"
//            self.isEditContact = true
//            self.addBtn.setTitle("SAVE", for: .normal)
//            self.addContactsView.isHidden = false
//
//            success(true)
//        })
//        editAction.backgroundColor = .blue
//
//        return UISwipeActionsConfiguration(actions: [editAction])
//    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let groupID = groups[indexPath.row].group_id
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("delete")

            let deletAlert = UIAlertController(title: "Are you sure?", message: "This will delete from your account.", preferredStyle: UIAlertController.Style.alert)

            deletAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                self.serverCallForDeleteGroup(groupID: groupID!)
            }))

            deletAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
                deletAlert.dismiss(animated: true, completion: nil)
            }))

            self.present(deletAlert, animated: true, completion: nil)
            success(true)

        })
        deleteAction.backgroundColor = .red

        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            searchActive = false;
            searchBar.resignFirstResponder()
        } else {
            contactSearchActive = false;
            contactsSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            searchActive = false;
            groupTable.reloadData()
            searchBar.resignFirstResponder()
        } else {
            contactSearchActive = false;
            contactsTbl.reloadData()
            contactsSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            searchActive = false;
            searchBar.resignFirstResponder()
        } else {
            contactSearchActive = false;
            contactsSearchBar.resignFirstResponder()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == self.searchBar {
            searchResults = groups.filter{($0.group_name!.lowercased().contains(searchText.lowercased()))}
            
            if(searchText.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.groupTable.reloadData()
        } else {
            searchContactsResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
            
            if(searchText.count == 0){
                contactSearchActive = false;
            } else {
                contactSearchActive = true;
            }
            self.contactsTbl.reloadData()
        }
    }
    
    //MARK:- Api Calls
    
    func serverCallForGetGroups(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d", AppSharedData.sharedInstance.currentUser?.userid ?? 0)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.listGroup, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [Group].from(jsonArray: response?["group"] as! [JSON])
                    
                    self.groups = inbloxData!
                    
                    self.groupTable.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCallForCreateGroup(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "group_name": groupName.text!, "group_member_id": arrContacts]
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.createGroup, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    DispatchQueue.main.async{
                        self.contactsPickerView.isHidden = true
                        self.addGroupPopUp.isHidden = true
                        self.serverCallForGetGroups()
                    }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    func serverCallForDeleteGroup(groupID:Int){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "group_id": groupID]
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.deleteGroup, methodType: .delete, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
//                    UIAlertController.showMessageDialog(onController: self, withTitle: "Info", withMessage: "Group is removed from your account.")
//                    let when = DispatchTime.now() + 2
//                    DispatchQueue.main.asyncAfter(deadline: when){
                        DispatchQueue.main.async{
                            self.serverCallForGetGroups()
                        }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
