//
//  GroupMembersViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 01/07/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import Contacts
import Gloss
import ContactsUI
import IQKeyboardManagerSwift

class memberCell: UITableViewCell{
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var memberName: UILabel!
    
}
class ContactsCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var cName: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
}

class GroupMembersViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var groupMemberTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var contactsSearchBar: UISearchBar!
    
    @IBOutlet weak var contactsPickerView: UIView!
    @IBOutlet weak var contactsTbl: UITableView!
    
    var groupID: Int!
    var groupNameStr: String = ""
    var searchActive : Bool = false
    var filteredData : NSMutableArray = NSMutableArray()
    
    var groupMembers = [GroupMember]()
    var searchResults = [GroupMember]()
    
    var arrContacts = [String]()
    var arrContactNames = [String]()

    let contactStore = CNContactStore()
    var contactSearchActive : Bool = false
    var contacts = [CNContact]()
    var searchContactsResults = [CNContact]()
    
//    var selectedContacts = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        searchBar.backgroundColor = .clear
//        searchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            searchBar.backgroundImage = UIImage()
            searchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        contactsSearchBar.delegate = self
        contactsSearchBar.backgroundColor = .clear
//        contactsSearchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            contactsSearchBar.backgroundImage = UIImage()
            contactsSearchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        groupMemberTable.delegate = self
        groupMemberTable.dataSource = self
        
        contactsTbl.delegate = self
        contactsTbl.dataSource = self
        
        groupName.text = groupNameStr
        // Do any additional setup after loading the view.
        fetchContacts()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        serverCallForGetGroupMembers()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.searchBar.resignFirstResponder()
        self.contactsSearchBar.resignFirstResponder()
    }
    
    func fetchContacts (){
        self.contacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                    }
                    
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result

            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when){
                self.groupMemberTable.reloadData()
                self.contactsTbl.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contactsPickerBack(_ sender: UIButton) {
        contactsPickerView.isHidden = true
        arrContacts.removeAll()
        contactsSearchBar.resignFirstResponder()
        contactsSearchBar.text = ""
    }
    @IBAction func contactPickerNext(_ sender: UIButton) {
        if arrContacts.count == 0{
//            AppSharedData.sharedInstance.showErrorAlert(message: "No user selected.")
            self.view.makeToast("No user selected", duration: 2.0, position: .bottom)
        } else {
//            self.addGroupPopUp.isHidden = false
            serverCallForAddMemberToGroup()
        }
        contactsSearchBar.resignFirstResponder()
    }
    @IBAction func addMembers(_ sender: UIButton) {
//        self.fetchContacts()
        self.contactsPickerView.isHidden = false
        self.arrContacts.removeAll()
    }
    
    @IBAction func selectContacts(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(searchActive)
            {
                if searchContactsResults[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (searchContactsResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                    let currentContactname = searchContactsResults[sender.tag].givenName
                    for (index,contactName) in arrContactNames.enumerated(){
                        if contactName == currentContactname {
                            arrContactNames.remove(at: index)
                        }
                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                    let currentContactname = contacts[sender.tag].givenName
                    for (index,contactName) in arrContactNames.enumerated(){
                        if contactName == currentContactname {
                            arrContactNames.remove(at: index)
                        }
                    }
                }
            }
        } else {
            sender.isSelected = true
            if(searchActive)
            {
                if searchContactsResults[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (searchContactsResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                    let currentContactname = searchContactsResults[sender.tag].givenName
                    if !arrContactNames.contains(currentContactname){
                        arrContactNames.append(currentContactname)
                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    
                    let currentContactStr = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                    let currentContactname = contacts[sender.tag].givenName
                    if !arrContactNames.contains(currentContactname){
                        arrContactNames.append(currentContactname)
                    }
                }
            }
        }
    }
    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == groupMemberTable {
            if(searchActive) {
                return searchResults.count
            } else {
                return groupMembers.count
            }
        } else {
            if(contactSearchActive) {
                return searchContactsResults.count
            } else {
                return contacts.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == groupMemberTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell") as! memberCell
            var member_name = ""
            var member_phone = ""
            if(searchActive) {
                member_name =  searchResults[indexPath.row].displayName ?? ""
                member_phone =  searchResults[indexPath.row].phoneNo ?? ""
            } else {
                member_name =  groupMembers[indexPath.row].displayName ?? ""
                member_phone =  groupMembers[indexPath.row].phoneNo ?? ""
            }
            if member_name == "" {
                for contact in contacts {
                    var contactNO = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    contactNO = String(contactNO.suffix(10))
                    if contactNO == member_phone {
                        member_name = "\(contact.givenName) \(contact.familyName)"
                    }
                }
            }
            cell.memberName.text = member_name
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell") as! ContactsCell
            if(searchActive) {
                cell.cName.text =  "\(searchContactsResults[indexPath.row].givenName) \(searchContactsResults[indexPath.row].familyName)"
                let currentContact = (searchContactsResults[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
            } else {
                cell.cName.text =  "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
                let currentContact = (contacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
            }
            cell.checkBtn.tag = indexPath.row
            return cell
        }
    }
        func tableView(_ tableView: UITableView,
                       trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
            
            var member_id: Int!
            if(searchActive) {
                member_id =  searchResults[indexPath.row].member_id
            } else {
                member_id =  groupMembers[indexPath.row].member_id
            }
            let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                print("delete")
    
                let deletAlert = UIAlertController(title: "Are you sure?", message: "This member will be removed from this group.", preferredStyle: UIAlertController.Style.alert)
    
                deletAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                      print("Handle Ok logic here")
                    self.serverCallForDeleteMember(memberID: member_id)
    
                }))
    
                deletAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                      print("Handle Cancel Logic here")
                    deletAlert.dismiss(animated: true, completion: nil)
                }))
    
                self.present(deletAlert, animated: true, completion: nil)
                success(true)
    
            })
            deleteAction.backgroundColor = .red
    
            return UISwipeActionsConfiguration(actions: [deleteAction])
        }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == contactsTbl {
//
//            if(contactSearchActive) {
//                selectedContacts.append(searchContactsResults[indexPath.row].givenName)
//            } else {
//                selectedContacts.append(contacts[indexPath.row].givenName)
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            searchActive = false;
            searchBar.resignFirstResponder()
        } else {
            contactSearchActive = false;
            contactsSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            searchActive = false;
            groupMemberTable.reloadData()
            searchBar.resignFirstResponder()
        } else {
            contactSearchActive = false;
            contactsTbl.reloadData()
            contactsSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBar {
            searchActive = false;
            searchBar.resignFirstResponder()
        } else {
            contactSearchActive = false;
            contactsSearchBar.resignFirstResponder()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == self.searchBar {
            searchResults = groupMembers.filter{($0.displayName!.lowercased().contains(searchText.lowercased()))}
            
            if(searchText.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.groupMemberTable.reloadData()
        } else {
            searchContactsResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
            
            if(searchText.count == 0){
                contactSearchActive = false;
            } else {
                contactSearchActive = true;
            }
            self.contactsTbl.reloadData()
        }
    }
    
    //MARK:- Api Calls
    
    func serverCallForGetGroupMembers(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "group_id=%d", groupID)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.getMembersOfGroup, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [GroupMember].from(jsonArray: response?["group"] as! [JSON])
                    
                    self.groupMembers.removeAll()
                    var members:[GroupMember]  = inbloxData!
//                    self.groupMembers = inbloxData!
                    for member in members {
                        var name = ""
                        var image = ""
                        if member.user_image == "" || member.user_image == nil {
                            image = ""
                        }
                        if member.displayName == "" || member.displayName == nil{
                            for contact in self.contacts {
                                var contactNO = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                                contactNO = String(contactNO.suffix(10))
                                if contactNO == member.phoneNo {
                                    name = "\(contact.givenName) \(contact.familyName)"
                                }
                            }
                        } else {
                            name  = member.displayName!
                        }
                        let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id! ])
                        self.groupMembers.append(mem!)
                    }
                    
                    self.groupMemberTable.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCallForAddMemberToGroup(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["group_id": self.groupID,
                                   "userid": String(AppSharedData.sharedInstance.currentUser?.userid ?? 0),
                                   "group_member_id": self.arrContacts]
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.addUserInGroup, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
//                    UIAlertController.showMessageDialog(onController: self, withTitle: "Info", withMessage: "Members are added.")
//                    let when = DispatchTime.now() + 2
//                    DispatchQueue.main.asyncAfter(deadline: when){
//                        self.searchBar.text = ""
//                        self.searchBar.resignFirstResponder()
//                        self.serverCallForGetGroupMembers()
//                    }
                    DispatchQueue.main.async{
                        self.searchBar.text = ""
                        self.searchBar.resignFirstResponder()
                        self.serverCallForGetGroupMembers()
                        self.contactsPickerView.isHidden = true
                    }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    
    func serverCallForDeleteMember(memberID: Int){
            LoadingIndicatorView.show("Loading...")
            let params:NSDictionary = ["group_id": self.groupID,
                                       "removeId": memberID,
                                       "userid": AppSharedData.sharedInstance.currentUser?.userid ?? 0]
            NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.removeUserFromGroup, methodType: .delete, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
                LoadingIndicatorView.hide()
                if success == true {
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        print(response!)
                            DispatchQueue.main.async {
                                self.searchBar.text = ""
                                self.searchBar.resignFirstResponder()
                                self.serverCallForGetGroupMembers()
                            }
                    } else {
//                        let msg = statusDic["ErrorMessage"] as! String
//                        AppSharedData.sharedInstance.showErrorAlert(message: msg)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
