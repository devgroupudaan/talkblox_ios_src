//
//  EraseAccountVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/23/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import JGProgressHUD
class EraseAccountVC: UIViewController,UITextFieldDelegate {

    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtpassword:UITextField!
    @IBOutlet var txtConfirmPassword:UITextField!
    @IBOutlet var txtFeedback:UITextField!
     var loading : JGProgressHUD = JGProgressHUD(style: .dark)
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMobile.delegate = self
        txtpassword.delegate = self
        txtConfirmPassword.delegate = self
        txtFeedback.delegate = self
        self.txtMobile.setplaceHolderColor(color: .white)
        self.txtpassword.setplaceHolderColor(color: .white)
        self.txtConfirmPassword.setplaceHolderColor(color: .white)
        self.txtFeedback.setplaceHolderColor(color: .white)
        self.txtMobile.text = AppSharedData.sharedInstance.getUser()?.phone_number ?? ""
        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        txtMobile.resignFirstResponder()
        txtpassword.resignFirstResponder()
        txtConfirmPassword.resignFirstResponder()
        txtFeedback.resignFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK;- Helper Methods
    func isValidData() -> Bool {
        var msg = ""
        if self.txtMobile.text == "" {
            msg = "Please enter mobile Number."
        }else if self.txtpassword.text == "" {
            msg = "Please enter password."
        }else if self.txtpassword.text != self.txtConfirmPassword.text {
            msg = "Password and confirm password must be same."
        }else if self.txtFeedback.text == "" {
            msg = "Please enter feedback."
        }
        if msg == ""{
            return true
        }else {
//            AppSharedData.sharedInstance.showErrorAlert(message: msg)
            self.view.makeToast(msg, duration: 2.0, position: .bottom)
            return false
        }
    }
    //MARK:- UIButton Actions
    @IBAction func btnHomeAction(_ sender:Any) {
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEraseAccountAction(_ sender:Any) {
        if self.isValidData() {
            self.serverCallForDeleteAccount()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    //MARK:- Server Calls
    func serverCallForDeleteAccount(){
        
        let params: NSDictionary = ["userid": AppSharedData.sharedInstance.getUser()?.id ?? "","reqtype":4,"phoneNo":self.txtMobile.text ?? "","password":self.txtpassword.text ?? "","feedback":self.txtFeedback.text ?? ""]
        LoadingIndicatorView.show("Loading...")
        
        NetworkManager.sharedInstance.executeService(WEB_URL.eraseAccount, postParameters: params, completionHandler: { (success: Bool, response: NSDictionary?) in
            LoadingIndicatorView.hide()
//            self.loading.dismiss()
            if success == true {
                
                let statusDic = response?["header"] as! NSDictionary
                
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes {
                    AppSharedData.sharedInstance.logout()
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail {
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }else if statusDic["ErrorCode"] as! Int == ErroCode.NoData {
//                    AppSharedData.sharedInstance.showErrorAlert(message:"No data Found")
                    self.view.makeToast("No Data Found.", duration: 2.0, position: .bottom)
                }
            }
        })
    }
}
