//
//  HelpAndSupportVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/23/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss

class HelpAndSupportVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tblView:UITableView!
    var arrSettings = [[String:Any]]()

    var selectedRow: Int!
    
    var faqURL = ""
    var privacyURL = ""
    var contactUSURL = ""
    
    
     //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.populateData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK;- Helper Methods
    func populateData() {
        let dict1 = ["icon":#imageLiteral(resourceName: "FAQ"),"title":"FAQ"] as [String : Any]
        let dict2 = ["icon":#imageLiteral(resourceName: "privacyPolicy"),"title":"Privacy Policy"] as [String : Any]
        let dict3 = ["icon":#imageLiteral(resourceName: "contactUs"),"title":"Contact Us"] as [String : Any]
        self.arrSettings.append(dict1)
        self.arrSettings.append(dict2)
        self.arrSettings.append(dict3)
        self.getpagesURL()
    }
    
    //MARK:- Button Actions
    @IBAction func btnHomeAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UItableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingCell")
        let imgIcon = cell?.viewWithTag(101) as! UIImageView
        let lblTitle = cell?.viewWithTag(102) as! UILabel
        
        let alphaView = cell?.viewWithTag(105)!
        if indexPath.row == selectedRow {
            alphaView?.alpha = 0.6
        } else {
            alphaView?.alpha = 0.4
        }
        
        let dict = arrSettings[indexPath.row]
        imgIcon.image = dict["icon"] as? UIImage
        lblTitle.text = dict["title"] as? String
        return cell!
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        let selectedCell = tableView.cellForRow(at: indexPath)
        let alphaView = selectedCell?.viewWithTag(105)!
        alphaView?.alpha = 0.6
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        if indexPath.row == 0 {
            vc.strTitle = "FAQ"
            vc.url = faqURL
        }else if indexPath.row == 1 {
            vc.strTitle = "Privacy Policy"
            vc.url = privacyURL
        }else {
            vc.strTitle = "Contact US"
            vc.url = contactUSURL
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK:- server Calls
    func getpagesURL(){
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.getPages, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
            
            LoadingIndicatorView.hide()
            
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let pageData = [Page].from(jsonArray: response?["result"] as! [JSON])
                    let privacyPolicy = pageData?.filter({$0.pagename == "privacypolicy"})
                    self.privacyURL = privacyPolicy![0].url ?? ""
                    let faq = pageData?.filter({$0.pagename == "faq"})
                    self.faqURL = faq![0].url ?? ""
                    let contactUS = pageData?.filter({$0.pagename == "contact"})
                    self.contactUSURL = contactUS![0].url ?? ""
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
