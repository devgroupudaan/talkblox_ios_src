//
//  EditProfileVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/24/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Gloss

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var txtFullName:UITextField!
    @IBOutlet var txtUserName:UITextField!
    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtDescription:UITextField!
    @IBOutlet var imguser:UIImageView!
    let imagePicker = UIImagePickerController()
    var arrImages = NSMutableArray()
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.populateData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
    }
    //MARK:- Helper Methods
    func populateData(){
        let user = AppSharedData.sharedInstance.currentUser
        self.txtMobile.text = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
        self.txtFullName.text = user?.displayName ?? ""
        self.txtUserName.text = user?.username ?? ""
        self.txtDescription.text = user?.description ?? ""
        let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
        self.imguser.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func isValidData() -> Bool {
        var msg = ""
        if self.txtFullName.text == ""{
            msg = "Please enter full name."
        }else if self.txtUserName.text == ""{
            msg = "Please enter username."
        }
        
        if msg == ""{
            return true
        }else {
//            AppSharedData.sharedInstance.showErrorAlert(message: msg)
            self.view.makeToast(msg, duration: 2.0, position: .bottom)
            return false
        }
    }
    func showActionSheet(){
        let actionSheet = UIAlertController.init(title: "", message: "Please select one of the following", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func showPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- UIImagePickerControlloller Delegates
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.imguser.image = image
            let imageData = image.pngData()
            self.serverCallForUpdateProfilePhoto(imageData!)
            
        }
        self.dismiss(animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//// Local variable inserted by Swift 4.2 migrator.
////let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
////
////        if let image = infoconvertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage) as? UIImage {
////            self.imguser.image = image
////            let imageData = image.pngData()
////            self.serverCallForUpdateProfilePhoto(imageData!)
////
////        }
//
//        self.dismiss(animated: true, completion: nil)
//    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UIButton Actions
    @IBAction func btnResetpasswordAction(_ sender:Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnUpdateAction(_ sender:Any){
        if self.isValidData(){
           self.serverCallForUpdateProfile()
        }
    }
    @IBAction func btnChangePhotoAction(_ sender:Any){
       self.showActionSheet()
    }
    @IBAction func btnHomeAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Server Calls
    func serverCallForUpdateProfile(){
        
        let user = AppSharedData.sharedInstance.getUser()
        let param = [
            "authentication_Token": AppSharedData.sharedInstance.accessToken,
            "userId": AppSharedData.sharedInstance.getUser()?.id ?? "",
            "user": [
                "address": "",
                "age_range": "",
                "birth_month": "",
                "city": "",
                "country": "",
                "current_password": "",
                "description": self.txtDescription.text ?? "",
                "display_name": self.txtFullName.text ?? "",
                "gender":"",
                "state": "",
                "username": self.txtUserName.text ?? "",
                "zip_code": ""
            ]
            ] as [String : Any]
        
        LoadingIndicatorView.show("Loading...")
        
        NetworkManager.sharedInstance.apiCallForUpdateProfileWith(parameters: param ) { (success:Bool, httpStatus:Int, response:NSDictionary?) in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
            
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    DispatchQueue.main.async {
//                        self.popToHome()
//                        AppSharedData.sharedInstance.showErrorAlert(message: "Profile Updated Successfully.")
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    DispatchQueue.main.async {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                    
                }
            }
        }
        
        

       /* NetworkManager.sharedInstance.executeServiceForJSONEncoding(WEB_URL.updateProfile, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, httpStatus:Int, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    AppSharedData.sharedInstance.showErrorAlert(message: "Profile Updated Successfully.")
                    self.popToHome()
                    
                    
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                }
            }
        }*/
        
        
        /*NetworkManager.sharedInstance.executeService(WEB_URL.updateProfile, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    AppSharedData.sharedInstance.showErrorAlert(message: "Profile Updated Successfully.")
                   self.popToHome()
                    
                    
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                }
            }
        })*/
    }
    
    func serverCallForUpdateProfilePhoto(_ imageData:Data){
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.uploadImageWith(url: WEB_URL.updatePhoto as String, userID: AppSharedData.sharedInstance.getUser()?.id ?? "", imageData: imageData) { (success) in
            DispatchQueue.main.async {
                 LoadingIndicatorView.hide()
            }
            
            if success {
                DispatchQueue.main.async {
                    self.serverCallForGetProfileDetail()
//                  AppSharedData.sharedInstance.showErrorAlert(message: "Profile Updated Successfully.")
                }
                
            }else {
//                AppSharedData.sharedInstance.showErrorAlert(message: "Unable To update Photo")
                self.view.makeToast("Unable to update photo.", duration: 2.0, position: .bottom)
            }
        }
        
    }
    
    func serverCallForGetProfileDetail(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":AppSharedData.sharedInstance.getUser()?.id ?? ""]
        NetworkManager.sharedInstance.executeService(WEB_URL.getProfile, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let userData = [UserDetail].from(jsonArray: response?["result"] as! [JSON])
                    AppSharedData.sharedInstance.currentUser = userData?[0]
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
