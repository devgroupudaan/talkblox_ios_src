//
//  PrivacySettingsVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/23/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit

class PrivacySettingsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var tblView:UITableView!
  
    var arrPrivacyOptions = [String]()
    var selectedIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrPrivacyOptions.append("My Contacts")
        arrPrivacyOptions.append("Contacts of My Contacts")
        arrPrivacyOptions.append("Everyone")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- UIButton Actions
    @IBAction func btnHomeAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK;- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPrivacyOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyOptionCell")
        let imgRadio = cell?.viewWithTag(100) as! UIImageView
        let lblOption = cell?.viewWithTag(101) as! UILabel
        lblOption.text = arrPrivacyOptions[indexPath.row]
        if selectedIndex == indexPath.row {
            imgRadio.image = #imageLiteral(resourceName: "check_radio")
        }else {
            imgRadio.image = #imageLiteral(resourceName: "uncheck_radio")
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.tblView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}
