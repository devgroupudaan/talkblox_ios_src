//
//  SettingsVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/19/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import ContactsUI

class SettingsVC: UIViewController,UITableViewDelegate,UITableViewDataSource, CNContactPickerDelegate {

    @IBOutlet var imgUser:UIImageView!
    @IBOutlet var lblUserName:UILabel!
    @IBOutlet var lbluserDesc:UILabel!
    @IBOutlet var tblView:UITableView!
    
    @IBOutlet weak var accountSettingBG: DesignableView!
    @IBOutlet weak var accountSettingIcon: UIImageView!
    @IBOutlet weak var notSettingBG: DesignableView!
    @IBOutlet weak var notSettingIcon: UIImageView!
    @IBOutlet weak var userSettingBG: DesignableView!
    @IBOutlet weak var userSettingIcon: UIImageView!
    @IBOutlet weak var helpBG: DesignableView!
    @IBOutlet weak var helpIcon: UIImageView!
    @IBOutlet weak var inviteBG: DesignableView!
    @IBOutlet weak var inviteIcon: UIImageView!
    
    var arrSettings = [[String:Any]]()
    var arrContacts = NSMutableArray()
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.populateData()
        tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK:- Helper Methods
    func populateData() {
        let dict1 = ["icon":#imageLiteral(resourceName: "key_icon"),"title":"Accounts Settings","desc":"Erase Account"] as [String : Any]
        let dict2 = ["icon":#imageLiteral(resourceName: "notification_icon"),"title":"Notification Settings","desc":"Blox Messages"] as [String : Any]
      //  let dict3 = ["icon":#imageLiteral(resourceName: "BloxCast_icon"),"title":"BloxCast Settings","desc":"BloxCast"] as [String : Any]
        let dict4 = ["icon":#imageLiteral(resourceName: "userSetting_icon"),"title":"User Settings","desc":"Manage Contacts, Manager Subscribers"] as [String : Any]
        let dict5 = ["icon":#imageLiteral(resourceName: "help_icon"),"title":"Help","desc":"FAQs, Privacy Policy, Contact us"] as [String : Any]
        let dict6 = ["icon":#imageLiteral(resourceName: "inviteFriend_icon"),"title":"Invite Friends","desc":"Invite Your Friends"] as [String : Any]
        self.arrSettings.append(dict1)
        self.arrSettings.append(dict2)
     //   self.arrSettings.append(dict3)
        self.arrSettings.append(dict4)
        self.arrSettings.append(dict5)
        self.arrSettings.append(dict6)
         let user = AppSharedData.sharedInstance.currentUser
        self.lblUserName.text = user?.displayName  ?? ""
        self.lbluserDesc.text = user?.description ?? ""
        let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
        self.imgUser.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func showContactPicker()  {
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
    }
    //MARK:- ContactPicker Delegates
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        contacts.forEach { contact in
            for number in contact.phoneNumbers {
                let tmpNumber =   number.value
                let phoneNumber = tmpNumber.value(forKey: "digits") as! String
                print("number is = \(phoneNumber)")
                arrContacts.add(phoneNumber)
            }
        }
        // self.callSendMessage()
        self.sendInvites()
    }
    //MARK:- Button Actions
    @IBAction func btnProfileAction(_ sender:Any) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
        vc.isSelfProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnHomeAction(_ sender:Any) {
        self.popToHome()
    }
    @IBAction func btnLogoutAction(_ sender:Any){
        let alert = UIAlertController.init(title: "TalkBlox", message: "Are you sure you want to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "NO", style: .cancel, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction.init(title: "YES", style: .default, handler: { (action) in
            let userDefaults = UserDefaults.standard
            if let key = userDefaults.object(forKey: "bloxArray")  as? Data
            {
                let arrayData = NSKeyedUnarchiver.unarchiveObject(with: key)
                let array : NSMutableArray = arrayData! as! NSMutableArray
                array.removeAllObjects()
                
                let arr: NSMutableArray = NSMutableArray(objects: "","","","","","","","","")
                let data = NSKeyedArchiver.archivedData(withRootObject: arr)
                
                if(AppTheme().killFileOnPath("backgroundAudio.caf") == "File Killed"){
                    print("file deleted")
                }
                
                userDefaults.removeObject(forKey: "bloxBubbleAudio")
                userDefaults.set(data, forKey: "bloxArray")
                userDefaults.synchronize()
            }
            AppSharedData.sharedInstance.logout()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func accountSettingTapped(_ sender: UIButton) {
        accountSettingBG.alpha = 0.6
        accountSettingIcon.image = #imageLiteral(resourceName: "key_icon")
        notSettingBG.alpha = 0.4
        notSettingIcon.image = #imageLiteral(resourceName: "notification_icon_white")
        userSettingBG.alpha = 0.4
        userSettingIcon.image = #imageLiteral(resourceName: "userSetting_icon_white")
        helpBG.alpha = 0.4
        helpIcon.image = #imageLiteral(resourceName: "help_icon_white")
        inviteBG.alpha = 0.4
        inviteIcon.image = #imageLiteral(resourceName: "inviteFriend_icon_white")
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingsVC") as! AccountSettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func notificationSettingTapped(_ sender: UIButton) {
        accountSettingBG.alpha = 0.4
        accountSettingIcon.image = #imageLiteral(resourceName: "key_icon_white")
        notSettingBG.alpha = 0.6
        notSettingIcon.image = #imageLiteral(resourceName: "notification_icon")
        userSettingBG.alpha = 0.4
        userSettingIcon.image = #imageLiteral(resourceName: "userSetting_icon_white")
        helpBG.alpha = 0.4
        helpIcon.image = #imageLiteral(resourceName: "help_icon_white")
        inviteBG.alpha = 0.4
        inviteIcon.image = #imageLiteral(resourceName: "inviteFriend_icon_white")
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingsVC") as! NotificationSettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func userSettingTpped(_ sender: UIButton) {
        accountSettingBG.alpha = 0.4
        accountSettingIcon.image = #imageLiteral(resourceName: "key_icon_white")
        notSettingBG.alpha = 0.4
        notSettingIcon.image = #imageLiteral(resourceName: "notification_icon_white")
        userSettingBG.alpha = 0.6
        userSettingIcon.image = #imageLiteral(resourceName: "userSetting_icon")
        helpBG.alpha = 0.4
        helpIcon.image = #imageLiteral(resourceName: "help_icon_white")
        inviteBG.alpha = 0.4
        inviteIcon.image = #imageLiteral(resourceName: "inviteFriend_icon_white")
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserSettingVC") as! UserSettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func helpTapped(_ sender: UIButton) {
        accountSettingBG.alpha = 0.4
        accountSettingIcon.image = #imageLiteral(resourceName: "key_icon_white")
        notSettingBG.alpha = 0.4
        notSettingIcon.image = #imageLiteral(resourceName: "notification_icon_white")
        userSettingBG.alpha = 0.4
        userSettingIcon.image = #imageLiteral(resourceName: "userSetting_icon_white")
        helpBG.alpha = 0.6
        helpIcon.image = #imageLiteral(resourceName: "help_icon")
        inviteBG.alpha = 0.4
        inviteIcon.image = #imageLiteral(resourceName: "inviteFriend_icon_white")
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpAndSupportVC") as! HelpAndSupportVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func inviteFriendTapped(_ sender: UIButton) {
        accountSettingBG.alpha = 0.4
        accountSettingIcon.image = #imageLiteral(resourceName: "key_icon_white")
        notSettingBG.alpha = 0.4
        notSettingIcon.image = #imageLiteral(resourceName: "notification_icon_white")
        userSettingBG.alpha = 0.4
        userSettingIcon.image = #imageLiteral(resourceName: "userSetting_icon_white")
        helpBG.alpha = 0.4
        helpIcon.image = #imageLiteral(resourceName: "help_icon_white")
        inviteBG.alpha = 0.6
        inviteIcon.image = #imageLiteral(resourceName: "inviteFriend_icon")
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InviteContactsViewController") as! InviteContactsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- UItableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell")
        let lblLine =  cell?.viewWithTag(100) as! UILabel
        let imgIcon = cell?.viewWithTag(101) as! UIImageView
        let lblTitle = cell?.viewWithTag(102) as! UILabel
        let lblDesc = cell?.viewWithTag(103) as! UILabel
       
        let dict = arrSettings[indexPath.row]
        imgIcon.image = dict["icon"] as? UIImage
        lblTitle.text = dict["title"] as? String
        lblDesc.text = dict["desc"] as? String
       
        if indexPath.row == self.arrSettings.count - 1 {
              lblLine.isHidden = false
        }else {
             lblLine.isHidden = true
        }
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingsVC") as! AccountSettingsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingsVC") as! NotificationSettingsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2 {
            /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxCastSettingVC") as! BloxCastSettingVC
            self.navigationController?.pushViewController(vc, animated: true)*/
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserSettingVC") as! UserSettingVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 3 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpAndSupportVC") as! HelpAndSupportVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4 {
//            self.showContactPicker()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InviteContactsViewController") as! InviteContactsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func sendInvites() {
//        let parameter : [String : AnyObject] =
//            ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"contactIds":arrContacts as AnyObject]
        let parameter : [String : AnyObject] =
            ["contactIds":arrContacts as AnyObject]
        print(parameter)
        
        NetworkManager.sharedInstance.executeService(WEB_URL.sendinvite, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
            if success {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                    self.popToHome()
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: "User Invited.")
                    self.view.makeToast("User Invited.", duration: 2.0, position: .bottom)
                }
            }
            print(response!)
        })
    }
}
