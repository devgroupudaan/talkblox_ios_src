//
//  InviteContactsViewController.swift
//  TalkBloxNew
//
//  Created by Mahendra Lariya on 19/12/20.
//  Copyright © 2020 iOSDeveloper. All rights reserved.
//

import UIKit
import Contacts
import IQKeyboardManagerSwift

class inviteContactsCell:UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
}

class InviteContactsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var contactsTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive : Bool = false
    var filteredData : NSMutableArray = NSMutableArray()
    
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    var searchResults = [CNContact]()
    var arrContacts = [String]()
    var selectedContact:CNMutableContact!
    var isEditContact = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
//        searchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            searchBar.backgroundImage = UIImage()
            searchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        contactsTable.delegate = self
        contactsTable.dataSource = self
        
        fetchContacts()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    
    func fetchContacts (){
        self.contacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                    }
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result

            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTable.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    
    func sendInvites() {
//        let parameter : [String : AnyObject] =
//            ["authentication_Token":AppSharedData.sharedInstance.accessToken as AnyObject,"contactIds":arrContacts as AnyObject]
        let parameter : [String : AnyObject] =
            ["contactIds":arrContacts as AnyObject]
        print(parameter)
        
        NetworkManager.sharedInstance.executeService(WEB_URL.sendinvite, postParameters: parameter as NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
            if success {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                    self.popToHome()
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: "User Invited.")
                    self.view.makeToast("User Invited.", duration: 2.0, position: .bottom)
                    let when = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: when){
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            print(response!)
            self.searchBar.resignFirstResponder()
        })
    }
    @IBAction func sendInvites(_ sender: UIButton) {
        if arrContacts.count > 0 {
            self.sendInvites()
        } else {
//            AppSharedData.sharedInstance.showErrorAlert(message: "Please select contacts to send invites.")
            self.view.makeToast("Please select contacts to send invites.", duration: 2.0, position: .bottom)
        }
        searchBar.resignFirstResponder()
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addContact(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(searchActive)
            {
                if searchResults[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (searchResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                }
            }
        } else {
            sender.isSelected = true
            if(searchActive)
            {
                if searchResults[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (searchResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
            }
        }
    }
    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive)
        {
            return searchResults.count
        }
        else
        {
            return contacts.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inviteContactsCell") as! inviteContactsCell
        if(searchActive) {
            cell.contactName.text =  "\(searchResults[indexPath.row].givenName) \(searchResults[indexPath.row].familyName)"
            let currentContact = (searchResults[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
            if currentContact != nil {
                if arrContacts.contains(currentContact) {
                    cell.checkBtn.isSelected = true
                } else {
                    cell.checkBtn.isSelected = false
                }
            } else {
                cell.checkBtn.isSelected = false
            }
        } else {
            cell.contactName.text =  "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
            let currentContact = (contacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
            if currentContact != nil {
                if arrContacts.contains(currentContact) {
                    cell.checkBtn.isSelected = true
                } else {
                    cell.checkBtn.isSelected = false
                }
            } else {
                cell.checkBtn.isSelected = false
            }
        }
        cell.checkBtn.tag = indexPath.row
        return cell
    }
    
    
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
//        contactsTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        contactsTable.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//            let resultPredicate = NSPredicate(format: "name contains[cd] %@", searchText)
//            let data = aryTalkBloxContacts.filteredArrayUsingPredicate(resultPredicate)
//
//        searchResults = contacts.filter{($0.givenName.prefix(searchText.count) == searchText)}
        searchResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
//            if(data.count > 0)
//            {
//                filteredData.removeAllObjects()
//                filteredData.addObjectsFromArray(data)
//            }
            
            

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.contactsTable.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
