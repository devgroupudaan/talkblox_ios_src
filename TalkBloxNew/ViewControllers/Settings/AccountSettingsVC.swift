//
//  AccountSettingsVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/23/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit

class AccountSettingsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tblView:UITableView!
    
    var arrSettings = [[String:Any]]()
    var selectedRow: Int!
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.populateData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK;- Helper Methods
    func populateData() {
        let dict1 = ["icon":#imageLiteral(resourceName: "lock_green"),"title":"Privacy Settings"] as [String : Any]
        let dict2 = ["icon":#imageLiteral(resourceName: "delete_green"),"title":"Erase My Account"] as [String : Any]
       // self.arrSettings.append(dict1)
        self.arrSettings.append(dict2)
    }
   
    //MARK:- Button Actions
    @IBAction func btnHomeAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UItableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingCell")
        let imgIcon = cell?.viewWithTag(101) as! UIImageView
        let lblTitle = cell?.viewWithTag(102) as! UILabel
        
        let alphaView = cell?.viewWithTag(105)!
        if indexPath.row == selectedRow {
            alphaView?.alpha = 0.6
        } else {
            alphaView?.alpha = 0.4
        }
        
        let dict = arrSettings[indexPath.row]
        imgIcon.image = dict["icon"] as? UIImage
        lblTitle.text = dict["title"] as? String
       
        return cell!
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        let selectedCell = tableView.cellForRow(at: indexPath)
        let alphaView = selectedCell?.viewWithTag(105)!
        alphaView?.alpha = 0.6
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EraseAccountVC") as! EraseAccountVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
