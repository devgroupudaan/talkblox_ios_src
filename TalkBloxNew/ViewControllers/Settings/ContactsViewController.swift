//
//  ContactsViewController.swift
//  TalkBloxNew
//
//  Created by Mahendra Lariya on 03/12/20.
//  Copyright © 2020 iOSDeveloper. All rights reserved.
//

import UIKit
import Contacts
import IQKeyboardManagerSwift

class contactsCell:UITableViewCell {
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var contactName: UILabel!
}

class ContactsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var contactsTable: UITableView!
    @IBOutlet weak var addContactsView: UIView!
    @IBOutlet weak var nameFld: UITextField!
    @IBOutlet weak var familyNameFld: UITextField!
    @IBOutlet weak var phoneFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    var filteredData : NSMutableArray = NSMutableArray()
    
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    var searchResults = [CNContact]()
    var selectedContact:CNMutableContact!
    var isEditContact = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
//        searchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            searchBar.backgroundImage = UIImage()
            searchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        contactsTable.delegate = self
        contactsTable.dataSource = self

        addBtn.layer.cornerRadius = 25.0
        addBtn.layer.borderWidth = 1.0
        addBtn.layer.borderColor = #colorLiteral(red: 0.2135244906, green: 0.4920310974, blue: 0.7494547963, alpha: 1).cgColor
        
        cancelBtn.layer.cornerRadius = 25.0
        cancelBtn.layer.borderWidth = 1.0
        cancelBtn.layer.borderColor = #colorLiteral(red: 0.2135244906, green: 0.4920310974, blue: 0.7494547963, alpha: 1).cgColor
        
        fetchContacts()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    func fetchContacts (){
        self.contacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                self.contacts.append(contact)
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                    }
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result

            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTable.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        nameFld.resignFirstResponder()
        familyNameFld.resignFirstResponder()
        phoneFld.resignFirstResponder()
        emailFld.resignFirstResponder()
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addContacts(_ sender: UIButton) {
        nameFld.text = ""
        familyNameFld.text = ""
        phoneFld.text = ""
        emailFld.text = ""
        addContactsView.isHidden = false
    }
    @IBAction func addContactOk(_ sender: UIButton) {
        if isEditContact {
            isEditContact = false
            self.selectedContact.givenName = nameFld.text!
            self.selectedContact.familyName = familyNameFld.text!
            let phone = CNLabeledValue(
                label:CNLabelPhoneNumberMain,
                value:CNPhoneNumber(stringValue:phoneFld.text!))
            self.selectedContact.phoneNumbers = [phone]
            let WorkEmail = CNLabeledValue(label:CNLabelWork, value: emailFld.text! as NSString)
            self.selectedContact.emailAddresses = [WorkEmail]
            let saveRequest = CNSaveRequest()
            saveRequest.update(selectedContact)
            try? contactStore.execute(saveRequest)
            self.addContactsView.isHidden = true
            self.addBtn.setTitle("ADD", for: .normal)
        } else {
            let contact = CNMutableContact()
            contact.givenName = nameFld.text!
            contact.familyName = familyNameFld.text!
            let phone = CNLabeledValue(
                label:CNLabelPhoneNumberMain,
                value:CNPhoneNumber(stringValue:phoneFld.text!))
            
            contact.phoneNumbers = [phone]
            let WorkEmail = CNLabeledValue(label:CNLabelWork, value: emailFld.text! as NSString)
            contact.emailAddresses = [WorkEmail]
            
            let saveRequest = CNSaveRequest()
            saveRequest.add(contact, toContainerWithIdentifier: nil)
            try? contactStore.execute(saveRequest)
            addContactsView.isHidden = true
        }
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            self.fetchContacts()
        }
    }
    @IBAction func cancelAddContact(_ sender: UIButton) {
        print("cancel")
        addContactsView.isHidden = true
    }
    
    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive)
        {
            return searchResults.count
        }
        else
        {
            return contacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCell") as! contactsCell
        if(searchActive) {
            cell.contactName.text =  "\(searchResults[indexPath.row].givenName) \(searchResults[indexPath.row].familyName)"
        } else {
            cell.contactName.text =  "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//
//        } else if editingStyle == .insert {
//
//        }
//    }
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let editAction = UIContextualAction(style: .normal, title:  "Edit", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("edit")
            self.selectedContact = self.contacts[indexPath.row].mutableCopy() as? CNMutableContact
            self.nameFld.text = self.selectedContact.givenName
            self.familyNameFld.text = self.selectedContact.familyName
            self.phoneFld.text = self.selectedContact.phoneNumbers.first?.value.stringValue
            self.emailFld.text = "\(self.selectedContact.emailAddresses.first?.value ?? "")"
            self.isEditContact = true
            self.addBtn.setTitle("SAVE", for: .normal)
            self.addContactsView.isHidden = false

            success(true)
        })
        editAction.backgroundColor = .blue
        
        return UISwipeActionsConfiguration(actions: [editAction])
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("delete")
            
            let deletAlert = UIAlertController(title: "Are you sure?", message: "This contact will be deleted from your device.", preferredStyle: UIAlertController.Style.alert)

            deletAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                let selectedContact:CNMutableContact = self.contacts[indexPath.row].mutableCopy() as! CNMutableContact
                let req = CNSaveRequest()
                req.delete(selectedContact)
                try? self.contactStore.execute(req)
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when){
                    self.fetchContacts()
                }
                
            }))

            deletAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
                deletAlert.dismiss(animated: true, completion: nil)
            }))

            self.present(deletAlert, animated: true, completion: nil)
            success(true)
            
        })
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        contactsTable.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//            let resultPredicate = NSPredicate(format: "name contains[cd] %@", searchText)
//            let data = aryTalkBloxContacts.filteredArrayUsingPredicate(resultPredicate)
//
//        searchResults = contacts.filter{($0.givenName.prefix(searchText.count) == searchText)}
        searchResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
//            if(data.count > 0)
//            {
//                filteredData.removeAllObjects()
//                filteredData.addObjectsFromArray(data)
//            }
            
            

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.contactsTable.reloadData()
    }
    /*ZD
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
