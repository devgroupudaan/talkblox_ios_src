//
//  BloxcastSubscriptionViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 07/04/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import Cosmos


struct user {
    var userid: Int
    var user_image: String
    var displayName: String
    var description: String
    var subscribers: String
    var phoneNo: String
    var phone_code: String
    var user_added_on: String
    var following: Int
    var bloxcastCount: Int
}

class subsciptionCell: UITableViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subscribers: UILabel!
    @IBOutlet weak var star_rating: CosmosView!
    @IBOutlet weak var checkBtn: UIButton!
    
    override public func prepareForReuse() {
       // Ensures the reused cosmos view is as good as new
        star_rating.prepareForReuse()
     }
}

class BloxcastSubscriptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var subsciptionTable: UITableView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoImgView: UIImageView!
    @IBOutlet weak var infoName: UILabel!
    @IBOutlet weak var infoSubscriberCount: UILabel!
    @IBOutlet weak var infoDescription: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    //    var arrInblox = [Inblox]()
    var arrInblox = [user]()
    var searchResults = [user]()
    var selectedUserIDs = [String]()
    
    var selectedInfoIndex: Int? = nil
    var selectedIndex: Int? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
//        searchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            searchBar.backgroundImage = UIImage()
            searchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        subsciptionTable.delegate = self
        subsciptionTable.dataSource = self
        
//        serverCallForGetInBlox()
        serverCallForAllUsers()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func hideInfoView(_ sender: UIButton) {
        infoView.isHidden = true
    }
    
    @objc func ImgTapped(_ tapGesture : UITapGestureRecognizer){
        self.infoView.isHidden = true
        selectedInfoIndex = tapGesture.view?.tag
        
        let indexPath = NSIndexPath(item: selectedInfoIndex!, section: 0)
        let rectOfCellInTableView = subsciptionTable.rectForRow(at: indexPath as IndexPath)
        let rectOfCellInSuperview = subsciptionTable.convert(rectOfCellInTableView, to: subsciptionTable.superview)
        print("Y of Cell is: \(rectOfCellInSuperview.origin.y)")
        
        
        
        let inblox = self.arrInblox[self.selectedInfoIndex!]
        self.infoName.text = inblox.displayName
        var urlstring = WEB_URL.BaseURL + inblox.user_image
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        self.infoImgView.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        self.infoDescription.text = inblox.description
        
        let when = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: when){
            var yPosition = rectOfCellInSuperview.origin.y
            let bottomPosition = yPosition + rectOfCellInSuperview.height
            if bottomPosition > UIScreen.main.bounds.height {
                yPosition = UIScreen.main.bounds.height - rectOfCellInSuperview.height - 60
            }
            self.infoView.center = CGPoint(x: self.infoView.center.x, y: (yPosition + 60))
            self.infoView.isHidden = false
        }
    }
    @IBAction func checkBtnTapped(_ sender: UIButton) {
        if !sender.isSelected {
            sender.isSelected = true
            selectedIndex = sender.tag
            var inblox: user!
            if(searchActive) {
                inblox = searchResults[sender.tag]
            } else {
                inblox = arrInblox[sender.tag]
            }
            let user_id = String(inblox.userid)
            if !selectedUserIDs.contains(user_id){
                selectedUserIDs.append(user_id)
                debugPrint("selectedUserIDs:",selectedUserIDs)
            }
        } else {
            sender.isSelected = false
            selectedIndex = nil
            var inblox: user!
            if(searchActive) {
                inblox = searchResults[sender.tag]
            } else {
                inblox = arrInblox[sender.tag]
            }
            let user_id = String(inblox.userid)
            if selectedUserIDs.contains(user_id){
                selectedUserIDs = selectedUserIDs.filter{$0 != user_id}
                debugPrint("_selectedUserIDs:",selectedUserIDs)
            }
        }
        self.subsciptionTable.reloadData()
    }
    @IBAction func subscribe(_ sender: UIButton) {
        if selectedUserIDs.count == 0 {
//            AppSharedData.sharedInstance.showErrorAlert(message: "Please select users to subscribe.")
            self.view.makeToast("Please select users to unsubscribe.", duration: 2.0, position: .bottom)
        } else {
            serverCallForSubscribe()
        }
    }
    
    // MARK: - Tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return searchResults.count
        } else {
            return arrInblox.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subsciptionCell", for: indexPath) as! subsciptionCell
        cell.imgView.tag = indexPath.row
        cell.checkBtn.tag = indexPath.row
        
        var inblox: user!
        if(searchActive) {
            inblox = searchResults[indexPath.row]
        } else {
            inblox = arrInblox[indexPath.row]
        }
        
        cell.name.text = inblox.displayName
        var urlstring = WEB_URL.BaseURL + inblox.user_image
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        cell.imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BloxcastSubscriptionViewController.ImgTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        cell.imgView.addGestureRecognizer(tapGesture)
        let user_id = String(inblox.userid)
        if selectedUserIDs.contains(user_id) {
            cell.checkBtn.isSelected = true
        } else {
            cell.checkBtn.isSelected = false
        }
        cell.subscribers.text = (Int(inblox!.subscribers) ?? 0)!.formattedWithSeparator + " subscribers"
        cell.star_rating.rating = Double.random(in: 0...5)///Int.random(in: 0...5)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        subsciptionTable.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//            let resultPredicate = NSPredicate(format: "name contains[cd] %@", searchText)
//            let data = aryTalkBloxContacts.filteredArrayUsingPredicate(resultPredicate)
//
//        searchResults = contacts.filter{($0.givenName.prefix(searchText.count) == searchText)}
        searchResults = arrInblox.filter{($0.displayName.lowercased().contains(searchText.lowercased()))}
//            if(data.count > 0)
//            {
//                filteredData.removeAllObjects()
//                filteredData.addObjectsFromArray(data)
//            }
            
            

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.subsciptionTable.reloadData()
    }
    
    //MARK:- server Calls
    
//    func serverCallForGetInBlox(){
//
//        let mobile  = "\(USER_DEFAULT.value(forKey: NOTIF.PHONE_CODE) ?? "")\(USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) ?? "")"
//        let params:NSDictionary = ["sender_id":AppSharedData.sharedInstance.currentUser?.userid ?? 0]
//
//
//        LoadingIndicatorView.show("Loading...")
//        NetworkManager.sharedInstance.executeService(WEB_URL.getMessages, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
//
//            LoadingIndicatorView.hide()
//
//            if success == true {
//                if let statusDic = response?["header"] as? NSDictionary {
//                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                        let inbloxData = [Inblox].from(jsonArray: response?["result"] as! [JSON])
//                        let result = inbloxData!.sorted(by: {
//                            (first: Inblox, second: Inblox) -> Bool in
//                            let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                            let dateAString = first.message_date
//                            let dateBString = second.message_date
//                            let dateA = dateFormatter.date(from: dateAString!)
//                            let dateB = dateFormatter.date(from: dateBString!)
//                            if dateA == nil || dateB == nil {
//                                return true
//                            }
//                            return dateA!.compare(dateB!) == .orderedAscending
//                        })
//                        self.arrInblox = result
//                        self.subsciptionTable.reloadSections(IndexSet(integer: 0), with: .none)
////                        if self.selectedIndex != nil {
////                            if let index = self.arrInblox.firstIndex(where: { $0.message_id == self.messageID }){
////                                self.selectedIndex = index
////                            }
//////                            let indexPath = NSIndexPath(item: self.selectedIndex!, section: 0)
//////                            self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
////                            self.isOpened = true
////                            let inblox = self.arrInblox[self.selectedIndex!]
////                            self.messageID = inblox.message_id ?? 0
////                            self.isBackFromPreview = true
////                            self.serverCallForGetConversation()
////                        } else {
////                            LoadingIndicatorView.hide()
////                            if self.arrInblox.count > 0 {
////                                let indexPath = NSIndexPath(item: self.arrInblox.count - 1, section: 0)
////                                self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
////                            }
////                        }
//                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
//
//
//                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                    }
//                }
//            }
//        })
//    }
    
    func serverCallForAllUsers(){
        LoadingIndicatorView.show("Loading...")
        let url = NSString(format: "\(WEB_URL.allUsersListForSubscribe)?limit=%d&userid=%d&searchByName=%@" as NSString, 100,AppSharedData.sharedInstance.currentUser?.userid ?? 0,"" )//
        NetworkManager.sharedInstance.executeServiceFor(url: url, methodType: .get, postParameters: nil) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
//                    self.showOTPAlert()
                    let userlist = response?["user"] as! NSArray
                    for users in userlist{
                        let currentUser = users as! NSDictionary
                        let usr = user(userid: (currentUser["userid"] as! NSNumber).intValue, user_image: currentUser["user_image"] as! String, displayName: currentUser["displayName"] as! String, description: currentUser["description"] as! String, subscribers: currentUser["subscribers"] as! String, phoneNo: currentUser["phoneNo"] as! String, phone_code: currentUser["phone_code"] as! String, user_added_on: currentUser["user_added_on"] as! String, following: currentUser["following"] as! Int , bloxcastCount: currentUser["bloxcastCount"] as! Int)
                        self.arrInblox.append(usr)
                    }
                    self.subsciptionTable.reloadData()
                }else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    
    func serverCallForSubscribe(){
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "name": AppSharedData.sharedInstance.currentUser?.displayName ?? "", "subscriberData":selectedUserIDs]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.subscribe, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            
            if success == true {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                        self.selectedUserIDs.removeAll()
                        self.subsciptionTable.reloadData()
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                        
                        
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
