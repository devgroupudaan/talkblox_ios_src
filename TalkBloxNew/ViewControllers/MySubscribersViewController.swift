//
//  MySubscribersViewController.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 07/04/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit

class MySubscriberCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subscribers: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
}

class MySubscribersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    var arrInblox = [user]()
    var searchResults = [user]()
    var unSubscribeId = String()
    var pageNo = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
//        searchBar.tintColor = .clear
        if #available(iOS 13.0, *) {
            searchBar.backgroundImage = UIImage()
            searchBar.searchTextField.backgroundColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        tableview.delegate = self
        tableview.dataSource = self
        
        serverCallForAllUsers(pageNo: pageNo)
        // Do any additional setup after loading the view.
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func removeSubscriber(_ sender: UIButton) {
        let deletAlert = UIAlertController(title: "Are you sure?", message: "This will removed the subcriber completely..", preferredStyle: UIAlertController.Style.alert)

        deletAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
            DispatchQueue.main.async {
//              AppSharedData.sharedInstance.showErrorAlert(message: "Coming soon.")
                var inblox: user!
                if(self.searchActive) {
                    inblox = self.searchResults[sender.tag]
                } else {
                    inblox = self.arrInblox[sender.tag]
                }
                self.unSubscribeId = String(inblox.userid)
                self.serverCallForUnsubscribeUser()
            }
            
        }))

        deletAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
            deletAlert.dismiss(animated: true, completion: nil)
        }))

        self.present(deletAlert, animated: true, completion: nil)
    }
    
    // MARK: - Tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return searchResults.count
        } else {
            return arrInblox.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MySubscriberCell", for: indexPath) as! MySubscriberCell
        cell.removeBtn.tag = indexPath.row
        
        var inblox: user!
        if(searchActive) {
            inblox = searchResults[indexPath.row]
        } else {
            inblox = arrInblox[indexPath.row]
        }
        
        cell.name.text = inblox.displayName
        cell.subscribers.text = (Int(inblox!.subscribers) ?? 0)!.formattedWithSeparator + " subscribers"
        var urlstring = WEB_URL.BaseURL + inblox.user_image
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        cell.imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (pageNo * 10) - 1 {
            pageNo = pageNo + 1
            self.serverCallForAllUsers(pageNo: pageNo)
        }
    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        tableview.reloadData()
        searchBar.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchResults = arrInblox.filter{($0.displayName.lowercased().contains(searchText.lowercased()))}
        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableview.reloadData()
    }

    //MARK:- server Calls
    func serverCallForAllUsers(pageNo: Int){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0)]
        let url = WEB_URL.BaseURL + String(format: "api/v1/mySubscribeList?page=%d&limit=10", pageNo) as NSString
        NetworkManager.sharedInstance.executeServiceFor(url: url, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    let userlist = response?["user"] as! NSArray
                    if pageNo == 0 {
                        self.arrInblox.removeAll()
                    }
                    for users in userlist{
                        let currentUser = users as! NSDictionary
                        let usr = user(userid: (currentUser["userid"] as! NSNumber).intValue, user_image: currentUser["user_image"] as! String, displayName: currentUser["displayName"] as! String, description: currentUser["description"] as! String, subscribers: currentUser["subscribers"] as! String, phoneNo: currentUser["phoneNo"] as! String, phone_code: currentUser["phone_code"] as! String, user_added_on: currentUser["user_added_on"] as! String, following: currentUser["following"] as! Int, bloxcastCount: currentUser["bloxcastCount"] as! Int)
                        self.arrInblox.append(usr)
                    }
                    self.tableview.reloadData()
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    func serverCallForUnsubscribeUser(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "unSubscribeId":unSubscribeId]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.mySubscribeList, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
//                    AppSharedData.sharedInstance.showErrorAlert(message: "User Unsubscribed.")
                    self.view.makeToast("User Unsubscribed.", duration: 2.0, position: .bottom)
                    self.tableview.reloadData()
                }else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
