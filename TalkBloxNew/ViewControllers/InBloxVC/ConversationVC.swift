//
//  ConversationVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/27/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import AVFoundation
import AVKit
import Kingfisher
import ContactsUI
import Toast_Swift

class SentBloxCell: UITableViewCell {
    @IBOutlet private weak var messageContainerView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var senderImg: DesignableImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
}
class ReceiveBloxCell: UITableViewCell {
    @IBOutlet private weak var messageContainerView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var senderImg: DesignableImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
}

class memberlistCell: UITableViewCell {
    @IBOutlet weak var memberImg: DesignableImageView!
    @IBOutlet weak var mShortName: UILabel!
    @IBOutlet weak var member_name: UILabel!
    @IBOutlet weak var bloxcount: UILabel!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
}

class ConversationVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UISearchBarDelegate {

    @IBOutlet var tblView:UITableView!
    
    @IBOutlet weak var contactSelectionView: DesignableView!
    @IBOutlet weak var selectionCountLbl: UILabel!
    @IBOutlet weak var selectionSearchBar: UISearchBar!
    @IBOutlet weak var selectedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedCollectionView: UICollectionView!
    @IBOutlet weak var contactsTblView: UITableView!
    
    @IBOutlet weak var titleImg: DesignableImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var editGroupBtn: UIButton!
    @IBOutlet weak var editGroupView: DesignableView!
    @IBOutlet weak var groupImage: DesignableImageView!
    @IBOutlet weak var groupName: UITextField!
    @IBOutlet weak var memberTblView: UITableView!
    @IBOutlet weak var groupimgBtn: UIButton!
    @IBOutlet weak var saveGroupChangeBtn: UIButton!
    @IBOutlet weak var addParticipantHeight: NSLayoutConstraint!
    @IBOutlet weak var leaveGroupPopUp: UIView!
    
    var arrConversations = [Conversation]()
    var messageID = 0
   
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    //--- Delete Messsage ---
    var aryDeleteIds : NSMutableArray = NSMutableArray()
    var deleteMessageId : NSInteger!
    var inbloxData:Inblox?
    
    //--- Send Text Message Data
    var allDataArray : [String : AnyObject]!
    
    //----- Code for showing message from sent and recieve list ----
    var dictUserMessageData : NSMutableDictionary!
    
    
    //--- Preview message with server bubble image sent by user ---
    var previewBubbleImage : String = ""
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var chatID = 0
    var aryMessageListServerOrder : NSMutableArray = NSMutableArray()
    
    var inputImageBgID: String!
    
    var videoPlayer :AVPlayer = AVPlayer()
    
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    
    var inputSoundBgID: String!
    
    var groupID: String!
    var conversationType: String!
    var groupMembers = [GroupMember]()
    
    var arrContacts = [String]()
    var contactIds : [String : AnyObject]!
    let contactStore = CNContactStore()
    var contactsSearchActive : Bool = false
    var contacts = [CNContact]()
    var searchResults = [CNContact]()
    var selectedContacts = [CNContact]()
    
    var TB_members = [GroupMember]()
    var nonTB_members = [GroupMember]()
    
    let imagePicker = UIImagePickerController()
    var groupImageData = Data()
    
    var isGroupAdmin = false
    
    var contactStr: String!
    
    var titleName: String!
    var titleImage: String!
    var AdminID: Int = 0
    var NewEvent = false
    
    var bloxIDsArr = [bloxIds]()
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        memberTblView.delegate = self
        memberTblView.dataSource = self
        
        contactsTblView.delegate = self
        contactsTblView.dataSource = self
        selectedCollectionView.delegate = self
        selectedCollectionView.dataSource = self
//        self.serverCallForGetConversation()
        
        if conversationType == "group" {
            editGroupBtn.isHidden = false
        } else {
            editGroupBtn.isHidden = true
        }
        // Do any additional setup after loading the view.
        self.fetchContacts()

        selectionSearchBar.delegate = self
        selectionSearchBar.backgroundColor = .white
        selectionSearchBar.backgroundImage = UIImage()
        
        titleLbl.text = titleName
        var urlstring = WEB_URL.BaseURL + titleImage
        if conversationType == "group" {
            urlstring = "https://api.talkblox.info/groups/" + titleImage
        }
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        
        titleImg.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "Group-pic"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serverCallForGetConversation()
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
                                      })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIButton Actions
    @IBAction func btnbackAction(_ sender:Any){
        NotificationCenter.default.post(name: Notification.Name("backfromConversationVC"), object: NewEvent)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendBloxtoGroup(_ sender: UIButton) {
        APP_DELEGATE.isNewChat = true
        NewEvent = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
        vc.inbloxData = self.inbloxData
        var arr : [Int] = []
        if conversationType == "group" {
            let gID = Int(self.groupID)
            arr.append(gID!)
            vc.arrGroupIds = arr
        } else {
            vc.arrContacts = [contactStr]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func editGroup(_ sender: UIButton) {
//        serverCallForGetGroupMembers()
        self.serverCallForGroupDetails()
        editGroupView.isHidden = false
    }
    @IBAction func cancelEditGroup(_ sender: UIButton) {
        editGroupView.isHidden = true
        groupName.resignFirstResponder()
        selectionSearchBar.resignFirstResponder()
    }
    @IBAction func updateGroup(_ sender: UIButton) {
        if isGroupAdmin {
            self.serverCallForUpdateGroup()
        } else {
            leaveGroupPopUp.isHidden = false
        }
    }
    @IBAction func addGroupImage(_ sender: UIButton) {
        self.showActionSheet()
    }
    @IBAction func removeMember(_ sender: UIButton) {
        var member_id: Int!
        let number = sender.tag
        let numberStr = String(number)
        var section: Int!
        var index: Int!
        if numberStr.count == 3 {
            section = number/100
            index = number%100
        } else if numberStr.count == 2 {
            section = number/10
            index = number%10
        } else{
            section = 0
            index = number
        }
        if section == 1 {
            member_id = nonTB_members[index].member_id
        } else {
            member_id = TB_members[index].member_id
        }
//        if(searchActive) {
//            member_id =  searchResults[sender.tag].member_id
//        } else {
//            member_id =  groupMembers[sender.tag].member_id
//        }
        
        self.serverCallForDeleteMember(memberID: member_id)
    }
    @IBAction func addParticipants(_ sender: UIButton) {
        self.fetchContacts()
        self.arrContacts.removeAll()
        selectedViewHeight.constant = 0
        contactSelectionView.isHidden = false
    }
    
    @IBAction func cancelSelection(_ sender: UIButton) {
        contactSelectionView.isHidden = true
        selectedContacts.removeAll()
        selectedCollectionView.reloadData()
        selectionSearchBar.resignFirstResponder()
        groupName.resignFirstResponder()
    }
    @IBAction func selectionNext(_ sender: UIButton) {
        serverCallForAddMemberToGroup()
    }
    @IBAction func selectContacts(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(contactsSearchActive)
            {
                if searchResults[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (searchResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                }
                for (index,contact) in selectedContacts.enumerated(){
                    if contact == searchResults[sender.tag] {
                        selectedContacts.remove(at: index)
                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                }
                for (index,contact) in selectedContacts.enumerated(){
                    if contact == contacts[sender.tag] {
                        selectedContacts.remove(at: index)
                    }
                }
            }
        } else {
            sender.isSelected = true
            if(contactsSearchActive)
            {
                if searchResults[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (searchResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
                if !selectedContacts.contains(searchResults[sender.tag]){
                    selectedContacts.append(searchResults[sender.tag])
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
                if !selectedContacts.contains(contacts[sender.tag]){
                    selectedContacts.append(contacts[sender.tag])
                }
            }
        }
            selectedCollectionView.reloadData()
            let H = selectedCollectionView.collectionViewLayout.collectionViewContentSize.height
            selectedViewHeight.constant = H
            self.view.layoutIfNeeded()
            selectedCollectionView.layoutIfNeeded()
    }
    @IBAction func deSelectContacts(_ sender: UIButton) {
        selectedContacts.remove(at: sender.tag)
        arrContacts.remove(at: sender.tag)
        selectedCollectionView.reloadData()
        let H = selectedCollectionView.collectionViewLayout.collectionViewContentSize.height
        selectedViewHeight.constant = H
        self.view.layoutIfNeeded()
        selectedCollectionView.layoutIfNeeded()
        contactsTblView.reloadData()
    }
    @IBAction func leaveGroupYes(_ sender: UIButton) {
        serverCallForLeaveGroup()
    }
    @IBAction func leaveGroupCancel(_ sender: UIButton) {
        leaveGroupPopUp.isHidden = true
    }
    
    
    func fetchContacts (){
        self.contacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey,
            CNContactImageDataKey,
            CNContactImageDataAvailableKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                    }
                    
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result

            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTblView.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    
    func showActionSheet(){
        let actionSheet = UIAlertController.init(title: "", message: "Please select one of the following", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func showPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        selectionSearchBar.resignFirstResponder()
        groupName.resignFirstResponder()
    }
    
    //MARK:- UIImagePickerControlloller Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.groupImage.image = image
//            self.groupImgBtn.setImage(nil, for: .normal)
            groupImageData = image.pngData()!
            self.serverCallForUploadGroupImage(groupID: groupID)
            
        }
        self.dismiss(animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//// Local variable inserted by Swift 4.2 migrator.
////let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
////
////        if let image = infoconvertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage) as? UIImage {
////            self.imguser.image = image
////            let imageData = image.pngData()
////            self.serverCallForUpdateProfilePhoto(imageData!)
////
////        }
//
//        self.dismiss(animated: true, completion: nil)
//    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return selectedContacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectedContactsCell", for: indexPath) as! selectedContactsCell
        cell.contactName.text = "\(selectedContacts[indexPath.row].givenName) \(selectedContacts[indexPath.row].familyName)"
    let myName = "\(selectedContacts[indexPath.row].givenName) \(selectedContacts[indexPath.row].familyName)"
    let nameArr = myName.split(separator: " ")
    if nameArr.count == 1 {
        let firstLetter = String(nameArr[0])
        cell.shortName.text = String(Array(firstLetter)[0])
    } else if nameArr.count == 2 {
        let firstLetter = String(nameArr[0])
        let secondLetter: String = String(nameArr[1])
        cell.shortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
    } else {
        cell.shortName.text = ""
    }
        let numberStr = (selectedContacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
        cell.contactNo.text = numberStr.toPhoneNumber()
    if selectedContacts[indexPath.row].imageDataAvailable {
        let image = UIImage(data: selectedContacts[indexPath.row].imageData!)
        cell.ImgView.image = image
        cell.shortName.isHidden = true
    } else {
        cell.ImgView.image = nil
        cell.shortName.isHidden = false
    }
    cell.closeBtn.tag = indexPath.row
        return cell
    
}
    
    //MARK:- UITableViewDelegates And DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == memberTblView{
            return 2
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == memberTblView {
            switch section {
            case 1:
                return nonTB_members.count
            default:
                return TB_members.count
            }
        } else if tableView == contactsTblView{
            if(contactsSearchActive) {
                return searchResults.count
            } else {
                return contacts.count
            }
        } else {
        return self.arrConversations.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == memberTblView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "memberlistCell") as! memberlistCell
            if indexPath.section == 1 {
                cell.followerCount.isHidden = true
                let member = nonTB_members[indexPath.row]
                cell.member_name.text = member.displayName
                if member.displayName == "" || member.displayName == nil{
                    cell.member_name.text = "Unknown"
                }
                let currentContact = nonTB_members[indexPath.row].phoneNo
                cell.bloxcount.text = currentContact!.toPhoneNumber()
                if nonTB_members[indexPath.row].user_image!.count > 0 {
                    var urlstring = WEB_URL.BaseURL + (member.user_image ?? "")
                    urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
                    cell.memberImg.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
                    cell.mShortName.isHidden = true
                } else {
                    let myName = member.displayName
                    let nameArr = myName!.split(separator: " ")
                    if nameArr.count == 1 {
                        let firstLetter = String(nameArr[0])
                        cell.mShortName.text = String(Array(firstLetter)[0])
                    } else if nameArr.count == 2 {
                        let firstLetter = String(nameArr[0])
                        let secondLetter: String = String(nameArr[1])
                        cell.mShortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                    } else {
                        cell.mShortName.isHidden = true
                        cell.memberImg.image = #imageLiteral(resourceName: "place_holder_user")
                    }
//                    cell.mShortName.isHidden = false
                }
            } else {
                cell.followerCount.isHidden = false
                let member = TB_members[indexPath.row]
                if AdminID == member.userid {
                    cell.member_name.text = member.displayName! + " (Admin)"
                } else {
                    cell.member_name.text = member.displayName
                }
                let currentContact = TB_members[indexPath.row].phoneNo
                cell.bloxcount.text = currentContact!.toPhoneNumber()
                cell.followerCount.isHidden = true
                if TB_members[indexPath.row].user_image!.count > 0 {
                    var urlstring = WEB_URL.BaseURL + (member.user_image ?? "")
                    urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
                    cell.memberImg.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
                    cell.mShortName.isHidden = true
                } else {
                    let myName = member.displayName
                    let nameArr = myName!.split(separator: " ")
                    if nameArr.count == 1 {
                        let firstLetter = String(nameArr[0])
                        cell.mShortName.text = String(Array(firstLetter)[0])
                    } else if nameArr.count == 2 {
                        let firstLetter = String(nameArr[0])
                        let secondLetter: String = String(nameArr[1])
                        cell.mShortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                    } else {
                        cell.mShortName.text = ""
                    }
                    cell.mShortName.isHidden = false
                }
            }
            cell.minusBtn.tag = Int("\(indexPath.section)\(indexPath.row)")!
            if !isGroupAdmin {
                cell.minusBtn.isHidden = true
            }
            return cell
        } else if tableView == contactsTblView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selContactCell") as! selContactCell
            if(contactsSearchActive) {
                cell.cName.text =  "\(searchResults[indexPath.row].givenName) \(searchResults[indexPath.row].familyName)"
                let myName = "\(searchResults[indexPath.row].givenName) \(searchResults[indexPath.row].familyName)"
                let nameArr = myName.split(separator: " ")
                if nameArr.count == 1 {
                    let firstLetter = String(nameArr[0])
                    cell.sName.text = String(Array(firstLetter)[0])
                } else if nameArr.count == 2 {
                    let firstLetter = String(nameArr[0])
                    let secondLetter: String = String(nameArr[1])
                    cell.sName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                } else {
                    cell.sName.text = ""
                }
                let currentContact = String(((searchResults[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String).suffix(10))
                var isMember = false
                for cntct in groupMembers {
                    let numb = cntct.phoneNo?.suffix(10)
                    if numb! == currentContact {
                        isMember = true
                    }
                }
                if searchResults[indexPath.row].imageDataAvailable {
                    let image = UIImage(data: searchResults[indexPath.row].imageData!)
                    cell.cImage.image = image
                    cell.sName.isHidden = true
                } else {
                    cell.cImage.image = nil
                    cell.sName.isHidden = false
                }
                if currentContact != nil {
                    if isMember {
                        cell.checkBtn.isSelected = true
                        cell.checkBtn.isUserInteractionEnabled = false
                    }else if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                        cell.checkBtn.isUserInteractionEnabled = true
                    } else {
                        cell.checkBtn.isSelected = false
                        cell.checkBtn.isUserInteractionEnabled = true
                    }
                } else {
                    cell.checkBtn.isSelected = false
                    cell.checkBtn.isUserInteractionEnabled = true
                }
            
            } else {
                cell.cName.text =  "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
                let myName = "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
                let nameArr = myName.split(separator: " ")
                if nameArr.count == 1 {
                    let firstLetter = String(nameArr[0])
                    cell.sName.text = String(Array(firstLetter)[0])
                } else if nameArr.count == 2 {
                    let firstLetter = String(nameArr[0])
                    let secondLetter: String = String(nameArr[1])
                    cell.sName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                } else {
                    cell.sName.text = ""
                }
                if contacts[indexPath.row].imageDataAvailable {
                    let image = UIImage(data: contacts[indexPath.row].imageData!)
                    cell.cImage.image = image
                    cell.sName.isHidden = true
                } else {
                    cell.cImage.image = nil
                    cell.sName.isHidden = false
                }
                let currentContact = String(((contacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String).suffix(10))
                var isMember = false
                for cntct in groupMembers {
                    let numb = cntct.phoneNo?.suffix(10)
                    if numb! == currentContact {
                        isMember = true
                    }
                }
                if currentContact != nil {
                    if isMember {
                        cell.checkBtn.isSelected = true
                        cell.checkBtn.isUserInteractionEnabled = false
                    }else if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                        cell.checkBtn.isUserInteractionEnabled = true
                    } else {
                        cell.checkBtn.isSelected = false
                        cell.checkBtn.isUserInteractionEnabled = true
                    }
                } else {
                    cell.checkBtn.isSelected = false
                    cell.checkBtn.isUserInteractionEnabled = true
                }
            }
            cell.checkBtn.tag = indexPath.row
            
            return cell
        }
        else {
        let conversation = arrConversations[indexPath.row]
        if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SentBloxCell") as! SentBloxCell
            
            var urlstring = WEB_URL.BaseURL + (conversation.gc_path ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            cell.imgUser.tag = indexPath.row
            if urlstring.hasSuffix(".mp4") || urlstring.hasSuffix(".gif"){
                var img: UIImage!
                DispatchQueue.global(qos: .userInitiated).async {
                    img  = AppTheme().getImageFromUrl(urlstring)
                    DispatchQueue.main.async {
                        cell.imgUser.image = img
                    }
                }
            } else {
                cell.imgUser.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            var senderImgStr = WEB_URL.BaseURL + (conversation.setthumb ?? "")
            senderImgStr = senderImgStr.replacingOccurrences(of: " ", with: "%20")
            cell.senderImg.kf.setImage(with: URL.init(string: senderImgStr), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            
            cell.lblDate.text = conversation.text_added ?? ""
            if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
                cell.lblUsername.text = "Sent by Me"
            }else {
                cell.lblUsername.text = conversation.sendername ?? ""
            }
            cell.lblSubject.text = conversation.chatSubject ?? ""
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = #colorLiteral(red: 0.07890754369, green: 0.3148461237, blue: 0.7081929449, alpha: 1)
            cell.selectedBackgroundView = bgColorView
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveBloxCell") as! ReceiveBloxCell
            
            var urlstring = WEB_URL.BaseURL + (conversation.gc_path ?? "")//"http://103.76.248.170:3001/"
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            cell.imgUser.tag = indexPath.row
            if urlstring.hasSuffix(".mp4") || urlstring.hasSuffix(".gif") {
                var img: UIImage!
                DispatchQueue.global(qos: .userInitiated).async {
                    img  = AppTheme().getImageFromUrl(urlstring)
                    DispatchQueue.main.async {
                        cell.imgUser.image = img
                    }
                }
            } else {
                cell.imgUser.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            
            var senderImgStr = WEB_URL.BaseURL + (conversation.setthumb ?? "")
            senderImgStr = senderImgStr.replacingOccurrences(of: " ", with: "%20")
            cell.senderImg.kf.setImage(with: URL.init(string: senderImgStr), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
            
            cell.lblDate.text = conversation.text_added ?? ""
            if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
                cell.lblUsername.text = "Sent by Me"
            }else {
                cell.lblUsername.text = conversation.sendername ?? ""
            }
            cell.lblSubject.text = conversation.chatSubject ?? ""
            let bgColorView = UIView()
            bgColorView.backgroundColor = #colorLiteral(red: 0.07890754369, green: 0.3148461237, blue: 0.7081929449, alpha: 1)
            cell.selectedBackgroundView = bgColorView
            return cell
        }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == memberTblView {
            return 80
        } else if tableView == contactsTblView {
            return 80
        } else {
            return 100
        }
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if tableView == memberTblView {
//            if section == 1 {
//                return "Participants List (members)"
//            } else {
//                return "Participants List (non-members)"
//            }
//        } else {
//             return ""
//        }
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == memberTblView {
            let view = UIView()
            let label = UILabel()
            let label2 = UILabel()
            label.text = "       Participants List"
            if section == 1 {
                label2.text = " (non-members)"
            } else {
                label2.text = " (members)"
            }
            label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            label.font = UIFont.boldSystemFont(ofSize: 16)
            label2.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            label2.font = UIFont.boldSystemFont(ofSize: 12)
            let stackView   = UIStackView()
            stackView.axis  = NSLayoutConstraint.Axis.horizontal
            stackView.distribution  = UIStackView.Distribution.equalSpacing
            stackView.alignment = UIStackView.Alignment.center
            stackView.spacing   = 5.0
            
            stackView.addArrangedSubview(label)
            stackView.addArrangedSubview(label2)
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.frame = CGRect(x: 20, y: 0, width: view.frame.width - 40 , height: view.frame.height)
            view.addSubview(stackView)
            view.backgroundColor = .white
            return view
        } else {
            let view = UIView()
            view.backgroundColor = .clear
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == memberTblView {
            return 30
        } else {
            return 0.01
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblView {
            let conversation = arrConversations[indexPath.row]
            self.aryMessagePreivewData.removeAllObjects()
            self.chatID = conversation.chat_id ?? 0
            if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
                self.isSentByMe = true
                
            }else {
                self.isSentByMe = false
            }
            self.aryMessagePreivewData.removeAllObjects()
            if conversation.chatSubject != nil {
                self.bloxSubject = conversation.chatSubject!
            }
            self.BloxDate = conversation.text_added!
            self.bloxcastUsername = conversation.sendername!
            self.Blox_user_image = conversation.setthumb!
            
            self.chatID = conversation.chat_id ?? 0
            self.messageID = conversation.message_id ?? 0
            
            self.serverCallForPreview(message: conversation)
            //        let inblox = arrInblox[selectedIndex!]
            //        inbloxData = inblox
            
            if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
                self.isSentByMe = true
                
            }else {
                self.isSentByMe = false
                self.userID_for_profile = conversation.sent_by ?? 0
            }
        }
    }
    
    //MARK:- Helper Methods
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        
        self.present(errorAlert, animated: true ,completion: nil)
    }
    
    func generateDictionaryForPreview(_ dictMessageData : NSMutableDictionary)
    {
//        if let imageBg = dictMessageData.value(forKey: "imageBg")
        if let imageBg = dictMessageData.value(forKey: "chatImage") // changed on 28Dec2020
        {
            inputImageBgID = imageBg as! String
        }
        if let imageBg = dictMessageData.value(forKey: "chatSound") // changed on 28Dec2020
        {
            inputSoundBgID = imageBg as! String
        }
        if let contactId = dictMessageData.value(forKey: "contact_id")
        {
            dictMessagePreivewData.setValue(contactId, forKey: "contact_id")
        }
        if let messageRecList = dictMessageData.value(forKey: "message_receiver_list")
        {
            dictMessagePreivewData.setValue(messageRecList, forKey: "message_receiver_list")
        }
        if let messageUserId = dictMessageData.value(forKey: "message_user_id")
        {
            dictMessagePreivewData.setValue(messageUserId, forKey: "message_user_id")
        }
        if let messageUserName = dictMessageData.value(forKey: "message_user_name")
        {
            dictMessagePreivewData.setValue(messageUserName, forKey: "message_user_name")
        }
        
        if(messageParentId != 0)
        {
            dictMessagePreivewData.setValue(messageParentId, forKey: "parentId")
        }
        if(strMessageUserName != "")
        {
            dictMessagePreivewData.setValue(strMessageUserName, forKey: "messageUserName")
        }
        
        dictMessagePreivewData.setValue(strMessageId, forKey: "id")
        
        dictMessagePreivewData.setValue("Inbox", forKey: "MessagePreviewType")
        
        
        //-- Code to download BG bubble image ---
        
        if(previewBubbleImageURL != "")
        {
          
            previewBubbleImageURL = previewBubbleImageURL.replacingOccurrences(of: " ", with: "%20")
            var imageData : UIImage = UIImage()
            let img : UIImage = AppTheme().getImageFromUrl(previewBubbleImageURL)
            if img != nil
            {
                imageData = img
            }
            else
            {
                previewBubbleImageURL = ""
            }
            if(imageData.size.height > 0)
            {
                bubblePreviewImage = imageData
            }
            else
            {
                previewBubbleImageURL = ""
            }
        }
        //----
        
        
        if let aryBgMedia : NSArray = dictMessageData.value(forKey: "bgMedia") as? NSArray
        {
            if let dictBgMedia : NSDictionary = aryBgMedia.object(at: 0) as? NSDictionary
            {
                if var strBgURL : String = dictBgMedia.value(forKey: "mediaUrl") as? String
                {
                      strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
                    //AppTheme().killFileOnPath("previewBg.caf")
                    
                    //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                            if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                            {
                                if aryMedia.count > 0
                                {
                                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                                    self.processResponce()
                                }
                            }
                        }
                        else
                        {
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                    })
                }
                else
                {
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
            }
            else
            {
                if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                {
                    if aryMedia.count > 0
                    {
                        self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                        self.processResponce()
                    }
                }
            }
        }
        else
        {
            if let aryMedia : NSArray =  dictMessageData.value(forKey: "frames") as? NSArray
            {
                if aryMedia.count > 0
                {
                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                    self.processResponce()
                }
            }
        }
        
        
    }
    
    var aryCountIncr : NSInteger = 0
    func processResponce()
    {
        if(aryMessageMedia.count > aryCountIncr) {
            
            if let dictMedia : NSDictionary = aryMessageMedia.object(at: aryCountIncr) as? NSDictionary
            {
                //bloxDetails
                dictBloxDetails = NSMutableDictionary()
                //var strBloxType : String = String()
                if let strCaption : String = dictMedia.value(forKey: "caption") as? String
                {
                    dictBloxDetails.setValue(strCaption, forKey: "caption")
                }
                else
                {
                    dictBloxDetails.setValue("", forKey: "caption")
                }
                
                if var soundURL : String = dictMedia.value(forKey: "soundURL") as? String
                {
                        soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    dictBloxDetails.setValue(soundURL, forKey: "soundURL")
                }
                
                
                if let duration : NSNumber = dictMedia.value(forKey: "time_interval") as? NSNumber
                {
                    dictBloxDetails.setValue(duration, forKey: "animationDuration")
                }
                else
                {
                    dictBloxDetails.setValue(1.0, forKey: "animationDuration")
                }
                
                if let strType : String = dictMedia.value(forKey: "mediaType") as? String
                {
                    
                    if(strType == GalleryType.Text as String)
                    {
                        strMessageBloxType = GalleryType.Text as String
                        
                        if let txtMessage : String = dictMedia.value(forKey: "text") as? String
                        {
                            self.txtMessage = txtMessage
                            dictBloxDetails.setValue(txtMessage, forKey: "message")
                        }
                        if let textColor : String = dictMedia.value(forKey: "textColor") as? String
                        {
                            self.txtColor = textColor
                            dictBloxDetails.setValue(textColor, forKey: "textColor")
                        }
                        if let txtSize : String = dictMedia.value(forKey: "textSize") as? String
                        {
                            guard let n = NumberFormatter().number(from: txtSize) else { return }
                            self.txtSize = CGFloat(n)
                            dictBloxDetails.setValue(txtSize, forKey: "fontSize")
                        }
                        if let txtStyle : String = dictMedia.value(forKey: "textStyle") as? String
                        {
                            self.txtStyle = txtStyle
                            dictBloxDetails.setValue(txtStyle, forKey: "fontName")
                        }
                        if let textBgMediaId : NSInteger = dictMedia.value(forKey: "textBgMediaId") as? NSInteger
                        {
                            dictBloxDetails.setValue(textBgMediaId, forKey: "textBgMediaId")
                        }
                        loadTextMessage()
                        
                    }
                    
                    
                    if(strType == GalleryType.Image as String)
                    {
                        strMessageBloxType = GalleryType.Image as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                    else if(strType == GalleryType.Video as String)
                    {
                        strMessageBloxType = GalleryType.Video as String
                        /*
                         if let strURL : String = dictMedia.valueForKey("thumb_url") as? String
                         {
                         if let img : UIImage = AppTheme().getImageFromUrl(strURL)
                         {
                         let data : NSData = UIImagePNGRepresentation(img)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         }
                         else
                         {
                         let data : NSData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         */
                        if var strVideoURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                             strVideoURL = strVideoURL.replacingOccurrences(of: " ", with: "%20")
                            //AppTheme().killFileOnPath("previewFile\(i).mp4")
                            //dictBloxDetails.setValue("previewFile\(i).mp4", forKey: "video")
                            var strImageThumbURL : String = String()
                            if var strURL : String = dictMedia.value(forKey: "thumbUrl") as? String {
                                   strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                                strImageThumbURL = strURL
                            }
                            loadVideoDataForURL(strVideoURL, fileName: "previewFile\(aryCountIncr).mp4", strURL: strImageThumbURL)
                            /*
                             AppTheme().callGetServiceForDownload(strVideoURL, fileNameToBeSaved: "previewFile\(i).mp4", completion: { (result, data) in
                             if(result == "success")
                             {
                             self.dictBloxDetails.setValue(data, forKey: "video")
                             }
                             })
                             */
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                        
                    }else if(strType == GalleryType.Sound as String){
                        strMessageBloxType = GalleryType.Sound as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                            self.playAudio(strURL)
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    } else if(strType == GalleryType.Gif as String){
                        strMessageBloxType = GalleryType.Gif as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            dictBloxDetails.setValue(strURL, forKey: "image")
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
                            
                            aryMessagePreivewData.add(dictTemp)
                            aryCountIncr += 1
                            processResponce()
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                }
                
            }
        }
        else
        {
            aryCountIncr = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
            vc.aryUserMessageData = self.aryMessagePreivewData
            vc.dictUserMessageData = self.dictUserMessageData
            vc.inbloxData = self.inbloxData
            vc.chatId = self.chatID
            vc.isSentByMe = self.isSentByMe
            vc.userID_for_profile = self.userID_for_profile
            vc.previewKind = "InBlox"
            vc.inputImageBgID = self.inputImageBgID
//            vc.bgImageName = self.bgImageName
//            vc.strBgAudioPath = self.inputSoundBgID
            //    vc.imgViewBackground.image = self.imgBackground.image
            
            vc.bloxSubject = self.bloxSubject
            vc.user_name = self.bloxcastUsername
            vc.user_image = self.Blox_user_image
            vc.bloxDate = self.BloxDate
            self.navigationController?.pushViewController(vc, animated: true)
            
            //self.perform(#selector(MessageConversationVC.allSetUpWithPreviewData), with: nil, afterDelay: 1.0)
        }
        
    }
    
    //MARK: Audio player methods
    
    func playAudio(_ strVideoURL : String)//, videoImage : UIImage)
    {
       
        let playerItem = AVPlayerItem(url: URL(string: strVideoURL)!)
        
        if((videoPlayer.currentItem) != nil)
        {
            self.videoPlayer.replaceCurrentItem(with: playerItem)
        }
        else
        {
            self.videoPlayer = AVPlayer(playerItem: playerItem)
        }
        self.videoPlayer.play()
//        isObserverAdded = true
         videoPlayer.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: nil)

      
        //-- for animation ---
//        self.animationTimer = Timer.scheduledTimer(timeInterval: 0.3,
//                                                                     target:self,
//                                                                     selector:#selector(CreateBloxVC.animateRecordImage),
//                                                                     userInfo:nil,
//                                                                     repeats:true)
        let delayInSeconds = 45.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
//                       execute: {
//                        self.stop(UIButton())
//                        //self.audioRecorder!.stop()
//        })
      
        
        /*
         self.meterTimer = NSTimer.scheduledTimerWithTimeInterval(1.0,
         target:self,
         selector:"updateAudioMeter:",
         userInfo:nil,
         repeats:true)
         */
        //----
        NotificationCenter.default.addObserver(self, selector: #selector(CreateBloxVC.playerDidFinishPlaying(_:)),
                                                         name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)
        
        
    }
    
   
    
    @objc func playerDidFinishPlaying(_ note: Notification)
    {
        print("Video Finished")
        self.videoPlayer.pause()
        //btnHideAction(UIButton())
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)
        
    }
    
    func loadTextMessage() {
        
        //dictBloxDetails.setValue("Ret", forKey: "message")
        dictBloxDetails.setValue(UIColor.white, forKey: "backgroundColor")
        let imageData : Data = screenShot().jpegData(compressionQuality: 0.8)!
        
        // dictBloxDetails.setValue(UIColor.black, forKey: "textColor")
        //  dictBloxDetails.setValue("", forKey: "id")
        
        //        var imageData : Data = Data()
        //
        //        imageData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
        dictBloxDetails.setValue(imageData, forKey: "image")
        
        
        //  dictBloxDetails.setValue(AppSharedData.sharedInstance.imageData, forKey: "image")
        
        let dictTemp : NSMutableDictionary = NSMutableDictionary()
        dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
        dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
        
        aryMessagePreivewData.add(dictTemp)
        aryCountIncr += 1
        processResponce()
    }
    
    func loadImageFromURL(_ strURL:String)
    {
        var imageData : Data = Data()
        
        let fileName = URL(fileURLWithPath: strURL).pathExtension
        if fileName == "gif"{
//            var image : UIImage = UIImage(named: "preview_layer")!
            let urlNew:String = WEB_URL.BaseURL + strURL//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let imageData1 = try? Data(contentsOf: URL(string: urlNew)!)
            {
                let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
                print("isAnimated: \(isImageAnimated)")
                imageData = imageData1
            }
            else
            {
                
            }
        } else {
             let img : UIImage = AppTheme().getImageFromUrl(WEB_URL.BaseURL + strURL)
            //        if let img : UIImage = AppTheme().getImageFromUrl("http://103.76.248.170:3001/" + strURL)
             if img != nil
            {
                imageData = img.pngData()!
            }
            else
            {
                imageData = UIImage(named: "SingleBlox")!.pngData()!
            }
        }
        let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
        print("isAnimated: \(isImageAnimated)")
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
            
            aryMessagePreivewData.add(dictTemp)
            aryCountIncr += 1
            processResponce()
        }
    }
  
    func loadVideoDataForURL(_ strVideoURL:String, fileName: String, strURL : String)
    {
        var imageData : Data = Data()
//        let img : UIImage = AppTheme().getImageFromUrl(strURL)
        let img : UIImage = AppTheme().getImageFromUrl(strVideoURL) // 12Aug21
        if img != nil
        {
            imageData = img.pngData()!
        }
        else
        {
            imageData = UIImage(named: "SingleBlox")!.pngData()!
        }
        
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
        }
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
            {
                // do some task
                let url = WEB_URL.BaseURL + strVideoURL//"http://103.76.248.170:3001/"
//            let url = "http://103.76.248.170:3001/" + strVideoURL
                AppTheme().callGetServiceForDownload(url, fileNameToBeSaved: fileName, completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async {
                            // update some UI
                            self.dictBloxDetails.setValue(data, forKey: "video")
                            //  self.dictBloxDetails.setValue(64, forKey: "id")
                            
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(self.dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue("Video", forKey: "bloxType")
                            
                            self.aryMessagePreivewData.add(dictTemp)
                            self.aryCountIncr += 1
                            self.processResponce()
                        }
                        
                    }
                    else
                    {
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
        }
    }
    
    func screenShot() -> UIImage {
        var DynamicView=UILabel(frame: CGRect(x:68, y:0, width:240, height:240))
        DynamicView.backgroundColor=UIColor.white
        //DynamicView.layer.cornerRadius=25
        //DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
        DynamicView.text = self.txtMessage
        DynamicView.textAlignment = NSTextAlignment.center
        DynamicView.font = UIFont(name:self.txtStyle, size: self.txtSize)
        let clr = Utilities.shared.convertHexToUIColor(hexColor: self.txtColor)
        DynamicView.textColor = clr
        //DynamicView.font = UIFont.systemFont(ofSize: self.txtSize)
        // DynamicView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DynamicView.lineBreakMode = .byWordWrapping
        DynamicView.numberOfLines = 0
        
        var frameRect : CGRect = DynamicView.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        //var textView=UITextView(frame: CGRect(x:68, y:0, width:240, height:240))
        
        
        DynamicView.layer.borderColor = UIColor.clear.cgColor
        UIGraphicsBeginImageContextWithOptions(DynamicView.bounds.size, DynamicView.isOpaque, 0.0)
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        DynamicView.layer.borderColor = AppTheme().themeColorBlue.cgColor
        // txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
//    func convertHexToUIColor(hexColor : String) -> UIColor {
//        
//        // define character set (include whitespace, newline character etc.)
//        let characterSet = CharacterSet.whitespacesAndNewlines as CharacterSet
//        
//        //trim unnecessary character set from string
//        var colorString : String = hexColor.trimmingCharacters(in: characterSet)
//        
//        // convert to uppercase
//        colorString = colorString.uppercased()
//        
//        //if # found at start then remove it.
//        if colorString.hasPrefix("#") {
//            let index = colorString.index(colorString.startIndex, offsetBy: 1)
//            colorString =  String(colorString[..<index])
//        }
//        
//        if colorString.count != 6 {
//            return UIColor.black
//        }
//        
//        // split R,G,B component
//        var rgbValue: UInt32 = 0
//        Scanner(string:colorString).scanHexInt32(&rgbValue)
//        let valueRed    = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
//        let valueGreen  = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
//        let valueBlue   = CGFloat(rgbValue & 0x0000FF) / 255.0
//        let valueAlpha  = CGFloat(1.0)
//        
//        // return UIColor
//        return UIColor(red: valueRed, green: valueGreen, blue: valueBlue, alpha: valueAlpha)
//    }
    
//    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
//        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
//        let imageRef = image.cgImage
//        
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
//        let context = UIGraphicsGetCurrentContext()
//        
//        // Set the quality level to use when rescaling
//        context!.interpolationQuality = CGInterpolationQuality.high
//        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
//        
//        context!.concatenate(flipVertical)
//        // Draw into the context; this scales the image
//        context!.draw(imageRef!, in: newRect)
//        
//        let newImageRef = context!.makeImage()! as CGImage
//        let newImage = UIImage(cgImage: newImageRef)
//        
//        // Get the resized image from the context and a UIImage
//        UIGraphicsEndImageContext()
//        
//        return newImage
//    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
//        contactsTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == selectionSearchBar {
            contactsSearchActive = false;
            selectionSearchBar.resignFirstResponder()
        } else {
            contactsSearchActive = false;
            selectionSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == selectionSearchBar {
            contactsSearchActive = false
            contactsTblView.reloadData()
            selectionSearchBar.resignFirstResponder()
        } else {
            contactsSearchActive = false;
            contactsTblView.reloadData()
            selectionSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == selectionSearchBar{
            contactsSearchActive = false
            selectionSearchBar.resignFirstResponder()
        } else {
            contactsSearchActive = false;
            selectionSearchBar.resignFirstResponder()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == selectionSearchBar{
            searchResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
            if(searchText.count == 0){
                contactsSearchActive = false;
            } else {
                contactsSearchActive = true;
            }
            self.contactsTblView.reloadData()
        } else {
            searchResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
            
            if(searchText.count == 0){
                contactsSearchActive = false;
            } else {
                contactsSearchActive = true;
            }
            self.contactsTblView.reloadData()
        }
    }

    
    //MARK:- Server Calls
    func serverCallForGetConversation(){
        let params:NSDictionary = ["message_id":messageID]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.getConversation, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
             DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [Conversation].from(jsonArray: response?["result"] as! [JSON])
                    let result = inbloxData!.sorted(by: {
                        (first: Conversation, second: Conversation) -> Bool in
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"

                        let dateAString = first.text_added
                        let dateBString = second.text_added
                        let dateA = dateFormatter.date(from: dateAString!)
                        let dateB = dateFormatter.date(from: dateBString!)
                        if dateA == nil || dateB == nil {
                            return true
                        }
                        return dateA!.compare(dateB!) == .orderedAscending
                    })
                    self.arrConversations = result
                    for item in self.arrConversations {
                        let IDs = bloxIds(chat_id: item.chat_id!, message_id: item.message_id!)
                        self.bloxIDsArr.append(IDs)
                    }
                    self.tblView.reloadData()
                    if self.arrConversations.count > 0 {
                        let indexPath = NSIndexPath(item: self.arrConversations.count - 1, section: 0)
                        self.tblView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    } else {
//                        AppSharedData.sharedInstance.showErrorAlert(message: "No conversation yet.")
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
    func serverCallForPreview(message:Conversation)  {
        let params:NSDictionary = ["message_id":messageID,"chat_id":message.chat_id ?? 0]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.preview, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let dict : NSDictionary = response {
                        
                        print("\n\ndictionary for message all urls :", dict, "\n\n")
                        self.generateDictionaryForPreview(dict.mutableCopy() as! NSMutableDictionary )
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCallForGetGroupMembers(){
        LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "group_id=%@", groupID)
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.getMembersOfGroup, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
             DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [GroupMember].from(jsonArray: response?["group"] as! [JSON])
                    
                    self.groupMembers.removeAll()
                    var members:[GroupMember]  = inbloxData!
                    for member in members {
                        var name = ""
                        var image = ""
                        if member.user_image == "" || member.user_image == nil {
                            image = ""
                        }
                        if member.member_status == 1 {
                            name  = member.displayName!
                            let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id! ])
                            self.TB_members.append(mem!)
                        } else {
                            name  = member.displayName!
                            if member.displayName == "" || member.displayName == nil{
                                for contact in self.contacts {
                                    let contactNO = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                                    if contactNO == member.phoneNo {
                                        name = "\(contact.givenName) \(contact.familyName)"
                                    }
                                }
                            }
                                let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id! ])
                                self.nonTB_members.append(mem!)
                        }
                        
                        let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id! ])
                        self.groupMembers.append(mem!)
                    }
                    
                    self.memberTblView.reloadData()
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as? String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCallForGroupDetails(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["group_id": groupID!]
        let when = DispatchTime.now() + 10
        DispatchQueue.main.asyncAfter(deadline: when){
            LoadingIndicatorView.hide()
        }
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.getGroupDetail, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
             DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    let result = response?["result"] as! NSDictionary
                    DispatchQueue.main.async{
                    if let groupNameStr = result["group_name"] as? String {
                        self.groupName.text = groupNameStr
                    }
                    if let adminID = result["userid"] as? Int {
                        self.AdminID = adminID
                        if adminID == AppSharedData.sharedInstance.currentUser?.userid {
                            self.isGroupAdmin = true
                            self.groupName.isUserInteractionEnabled = true
                            self.groupimgBtn.isUserInteractionEnabled = true
//                            self.saveGroupChangeBtn.isHidden = false
                            self.saveGroupChangeBtn.setTitle("Save", for: .normal)
                            self.addParticipantHeight.constant = 50
                        } else {
                            self.isGroupAdmin = false
                            self.groupName.isUserInteractionEnabled = false
                            self.groupimgBtn.isUserInteractionEnabled = false
//                            self.saveGroupChangeBtn.isHidden = true
                            self.saveGroupChangeBtn.setTitle("Leave", for: .normal)
                            self.addParticipantHeight.constant = 0
                        }
                    }
                    if (result["group_image"] as! String).count >  0{
                        var urlstring = "https://api.talkblox.info/groups/" + (result["group_image"] as! String)
                        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
                        self.groupImage.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "Group-pic2"), options: nil, progressBlock: nil, completionHandler: nil)
                        self.groupimgBtn.setImage(nil, for: .normal)
                    }
                    
                    let membersArr = [GroupMember].from(jsonArray: result["members"] as! [JSON])
                    self.groupMembers.removeAll()
                    self.TB_members.removeAll()
                    self.nonTB_members.removeAll()
                    var members:[GroupMember]  = membersArr!
                    for member in members {
                        var name = ""
                        var image = ""
                        if member.user_image == "" || member.user_image == nil {
                            image = ""
                        } else {
                            image = member.user_image!
                        }
                        if member.member_status == 1 {
                            name  = member.displayName!
                            let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id!, "userid":member.userid,"member_status": member.member_status])
                            if member.userid != AppSharedData.sharedInstance.currentUser?.userid {
                                self.TB_members.append(mem!)
                            }
                        } else {
                            name  = member.displayName!
                            if member.displayName == "" || member.displayName == nil{
                                for contact in self.contacts {
                                    var contactNO = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                                    contactNO = String(contactNO.suffix(10))
                                    if contactNO == member.phoneNo {
                                        name = "\(contact.givenName) \(contact.familyName)"
                                    }
                                }
                            }
                                let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id!, "userid":member.userid,"member_status": member.member_status ])
                                self.nonTB_members.append(mem!)
                        }
                        
                        let mem = GroupMember.init(json: ["phoneNo":member.phoneNo!,"displayName":name,"user_image":image,"member_id": member.member_id!, "userid":member.userid,"member_status": member.member_status ])
                        self.groupMembers.append(mem!)
                    }
                    
                    self.memberTblView.reloadData()
                    }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    func serverCallForAddMemberToGroup(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["group_id": groupID!, "userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "group_member_id": arrContacts]
        
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.addUserInGroup, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
             DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    DispatchQueue.main.async{
                        self.arrContacts.removeAll()
                        self.selectedContacts.removeAll()
                        self.contactSelectionView.isHidden = true
//                        self.serverCallForGetGroupMembers()
                        self.serverCallForGroupDetails()
                    }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
    func serverCallForUploadGroupImage(groupID: String){
        let params:NSDictionary = ["group_id": groupID]
        NetworkManager.sharedInstance.executeServiceWithMultipartWithImageName(WEB_URL.uploadGroupImage, fileName: "group_image", arrDataToSend: [groupImageData], postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
         DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
        
        if success == true {
            let statusDic = response?["header"] as! NSDictionary
            if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                print(response!)
                DispatchQueue.main.async{
                    self.titleImg.image = UIImage(data: self.groupImageData)
                    self.groupimgBtn.setImage(nil, for: .normal)
                }
//                AppSharedData.sharedInstance.showErrorAlert(message: "Added image to group.")
            } else {
//                let msg = statusDic["ErrorMessage"] as! String
//                AppSharedData.sharedInstance.showErrorAlert(message: msg)
                self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
            }
        }
    })
}
    func serverCallForUpdateGroup(){
            LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["group_id": self.groupID!,
                                   "group_name": self.groupName.text!,
                                       "userid": AppSharedData.sharedInstance.currentUser?.userid ?? 0]
            NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.updateGroup, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
                 DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
                if success == true {
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        print(response!)
                            DispatchQueue.main.async {
                                self.editGroupView.isHidden = true
                                self.titleLbl.text = self.groupName.text
                                self.groupName.resignFirstResponder()
                            }
                    } else {
//                        let msg = statusDic["ErrorMessage"] as! String
//                        AppSharedData.sharedInstance.showErrorAlert(message: msg)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        }
    func serverCallForDeleteMember(memberID: Int){
            LoadingIndicatorView.show("Loading...")
            let params:NSDictionary = ["group_id": self.groupID,
                                       "removeId": memberID,
                                       "userid": AppSharedData.sharedInstance.currentUser?.userid ?? 0]
            NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.removeUserFromGroup, methodType: .delete, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
                 DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
                if success == true {
                    let statusDic = response?["header"] as! NSDictionary
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        print(response!)
                            DispatchQueue.main.async {
                                self.serverCallForGroupDetails()
                            }
                    } else {
//                        let msg = statusDic["ErrorMessage"] as! String
//                        AppSharedData.sharedInstance.showErrorAlert(message: msg)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }
            }
        }
    func serverCallForLeaveGroup(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "group_id": self.groupID]
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.exitFromGroup, methodType: .delete, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
             DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                        DispatchQueue.main.async{
                            self.leaveGroupPopUp.isHidden = true
                            self.NewEvent = true
                            NotificationCenter.default.post(name: Notification.Name("backfromConversationVC"), object: self.NewEvent)
                            self.navigationController?.popViewController(animated: true)
                        }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }
}
