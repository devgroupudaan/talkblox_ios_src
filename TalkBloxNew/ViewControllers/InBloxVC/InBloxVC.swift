//
//  InBloxVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/4/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import AVFoundation
import AVKit
import ContactsUI

class selContactCell: UITableViewCell {
    @IBOutlet weak var cImage: DesignableImageView!
    @IBOutlet weak var sName: UILabel!
    @IBOutlet weak var cName: UILabel!
    @IBOutlet weak var cPhone: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
}
class selectedContactsCell: UICollectionViewCell {
    @IBOutlet weak var ImgView: DesignableImageView!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    
    @IBOutlet weak var closeBtn: UIButton!
}

//protocol MessageCellDelegate: class {
//    func selectedConversation(message: Conversation)
//}
class MessageCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var allMessageBtn: UIButton!
    
    
    var arrConversations = [Conversation]()
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var isSentByMe = false
    var chatID = 0
    var selectedSubIndex:Int? = nil
//    var delegate:MessageCellDelegate?
    
    func configure(with conversationArr: [Conversation]){
        arrConversations = conversationArr
        if selectedSubIndex != nil {
        }
        let fView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0))
        fView.backgroundColor = .clear
    }
//    @objc func openPreview(sender: UIButton) {
//        let conversation = arrConversations[sender.tag]
//        self.aryMessagePreivewData.removeAllObjects()
//        self.chatID = conversation.chat_id ?? 0
//        if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
//           self.isSentByMe = true
//
//        }else {
//            self.isSentByMe = false
//        }
////        delegate?.selectedConversation(message: conversation)
//    }
}

class InBloxVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var tableView:UITableView!
    
    @IBOutlet weak var inblox_searchBar: UISearchBar!
    
    @IBOutlet weak var contactSelectionView: DesignableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var selectionCountLbl: UILabel!
    @IBOutlet weak var selectionSearchBar: UISearchBar!
    @IBOutlet weak var selectedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedCollectionView: UICollectionView!
    @IBOutlet weak var contactsTblView: UITableView!
    
    @IBOutlet weak var nameGroupPopUp: DesignableView!
    @IBOutlet weak var groupMemberCountLbl: UILabel!
    @IBOutlet weak var groupImg: DesignableImageView!
    @IBOutlet weak var groupName: UITextField!
    @IBOutlet weak var selectedCollectionView2: UICollectionView!
    @IBOutlet weak var selectedViewHeight2: NSLayoutConstraint!
    @IBOutlet weak var groupImgBtn: UIButton!
    @IBOutlet weak var contactSelectionNextBtn: UIButton!
    
    @IBOutlet weak var notificationLbl: DesignableLabel!
    @IBOutlet weak var myImage: DesignableImageView!
    @IBOutlet weak var newGroupBtn: UIButton!
    @IBOutlet weak var myShortName: UILabel!
    
    var arrConversations = [Conversation]()
     var arrInblox = [Inblox]()
    var messageID = 0
//    var selectedIndex:Int? = nil
//    var selectedMessageID = ""
    
    var inputImageBgID: String!
    var inputSoundBgID: String!
    
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    
    var inbloxData:Inblox?
    
    //----- Code for showing message from sent and recieve list ----
    var dictUserMessageData : NSMutableDictionary!
    
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var chatID = 0
    
    var videoPlayer :AVPlayer = AVPlayer()
    var addObserver = false
    
    var isBackFromPreview = Bool()
//    var timeObserver =
    
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    
    var selectedMessageID: Int!
    
    var arrContacts = [String]()
    var contactIds : [String : AnyObject]!
    let contactStore = CNContactStore()
    var contactsSearchActive : Bool = false
    var contacts = [CNContact]()
    var searchResults = [CNContact]()
    var selectedContacts = [CNContact]()
    
    var inbloxSearchActive : Bool = false
    var inbloxSearchResults = [Inblox]()
    
    var selectionType = ""
    
    let imagePicker = UIImagePickerController()
    var groupImageData = Data()
    var isFromConversationVC = false
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        contactsTblView.delegate = self
        contactsTblView.dataSource = self
        selectionSearchBar.delegate = self
        selectionSearchBar.backgroundColor = .white
//        selectionSearchBar.tintColor = .white
        selectionSearchBar.backgroundImage = UIImage()
        
        inblox_searchBar.delegate = self
        inblox_searchBar.backgroundColor = .clear
//        inblox_searchBar.tintColor = .white
        
        if #available(iOS 13.0, *) {
            inblox_searchBar.backgroundImage = UIImage()
            inblox_searchBar.searchTextField.backgroundColor = #colorLiteral(red: 0.2055262625, green: 0.6016036272, blue: 0.8644766808, alpha: 1)
            inblox_searchBar.searchTextField.tintColor = .white
            inblox_searchBar.searchTextField.leftView?.tintColor = .white
            inblox_searchBar.searchTextField.rightView?.tintColor = .white
            inblox_searchBar.searchTextField.setplaceHolderColor(color: .white)
            inblox_searchBar.searchTextField.textColor = .white
        } else {
            // Fallback on earlier versions
        }
        
        selectedCollectionView.delegate = self
        selectedCollectionView.dataSource = self
        selectedCollectionView2.delegate = self
        selectedCollectionView2.dataSource = self
        
        let gestureProfile1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchtoProfile(_:)))
        gestureProfile1.numberOfTapsRequired = 1
        myImage.addGestureRecognizer(gestureProfile1)
        
        let gestureProfile2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchtoProfile(_:)))
        gestureProfile2.numberOfTapsRequired = 1
        myShortName.addGestureRecognizer(gestureProfile2)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.messagereceived(notification:)), name: NSNotification.Name(rawValue: "backfromConversationVC"), object: nil)
//         self.serverCallForGetInBlox()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFromConversationVC {
            self.serverCallForGetInBlox()
        } else {
            isFromConversationVC = false
        }
        tabBarController?.tabBar.isHidden = false
        
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
                                      })
        
        let user = AppSharedData.sharedInstance.currentUser
        DispatchQueue.main.async{
        
        let myName = user?.displayName  ?? ""
        let nameArr = myName.split(separator: " ")
        if nameArr.count == 1 {
            let firstLetter = String(nameArr[0])
            self.myShortName.text = String(Array(firstLetter)[0])
        } else if nameArr.count == 2 {
            let firstLetter = String(nameArr[0])
            let secondLetter: String = String(nameArr[1])
            self.myShortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
        } else {
            self.myShortName.text = ""
        }
       let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
       self.myImage.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    @objc func messagereceived(notification: Notification) {
        isFromConversationVC = true
        let message = notification.object
        let newEvent = message as? Bool
        print(newEvent!)
        if newEvent! {
            self.serverCallForGetInBlox()
        }
    }
    
    @objc func switchtoProfile(_ sender: UITapGestureRecognizer){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
        vc.isSelfProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func bell_icon_tapped(_ sender: UIButton) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.pushViewController(VC!, animated: false)
    }
    
    @IBAction func newChatTapped(_ sender: UIButton) {
        contactSelectionNextBtn.isHidden = true
        selectionType = "newChat"
        self.fetchContacts()
        self.arrContacts.removeAll()
        selectedViewHeight.constant = 0
        contactSelectionView.isHidden = false
        titleLbl.text = "New Chat"
    }
    @IBAction func newGroupTapped(_ sender: UIButton) {
        
        sender.setImage(#imageLiteral(resourceName: "icons8-people-1"), for: .highlighted)
//
//        let when = DispatchTime.now() + 1.5
//        DispatchQueue.main.asyncAfter(deadline: when){
//            sender.setImage(#imageLiteral(resourceName: "Group-pic"), for: .normal)
//        }
        
        contactSelectionNextBtn.isHidden = false
        selectionType = "newGroup"
        self.fetchContacts()
        self.arrContacts.removeAll()
        selectedViewHeight.constant = 0
        contactSelectionView.isHidden = false
        titleLbl.text = "Add Participants"
    }
    @IBAction func cancelSelection(_ sender: UIButton) {
        contactSelectionView.isHidden = true
        selectedContacts.removeAll()
        contactsTblView.reloadData()
        selectedCollectionView.reloadData()
        selectionSearchBar.resignFirstResponder()
    }
    @IBAction func selectionNext(_ sender: UIButton) {
//        if selectionType == "newChat" {
//            APP_DELEGATE.isNewChat = true
//            self.arrContacts.removeAll()
//            self.contactSelectionView.isHidden = true
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
//            vc.inbloxData = self.inbloxData
//            vc.arrContacts = self.arrContacts
//            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
        if selectedContacts.count == 0  {
            self.view.makeToast("No Contact selected.", duration: 2.0, position: .bottom)
        } else{
            nameGroupPopUp.isHidden = false
    }
//        }
    }
    @IBAction func selectContacts(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            if(contactsSearchActive)
            {
                if searchResults[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (searchResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                }
                for (index,contact) in selectedContacts.enumerated(){
                    if contact == searchResults[sender.tag] {
                        selectedContacts.remove(at: index)
                    }
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    var currentContact = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    currentContact = String(currentContact.suffix(10))
                    for (index,contact) in arrContacts.enumerated(){
                        if contact == currentContact {
                            arrContacts.remove(at: index)
                        }
                    }
                }
                for (index,contact) in selectedContacts.enumerated(){
                    if contact == contacts[sender.tag] {
                        selectedContacts.remove(at: index)
                    }
                }
            }
        } else {
            sender.isSelected = true
            if(contactsSearchActive)
            {
                if searchResults[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (searchResults[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
                if !selectedContacts.contains(searchResults[sender.tag]){
                    selectedContacts.append(searchResults[sender.tag])
                }
            } else {
                if contacts[sender.tag].phoneNumbers.count > 0 {
                    let currentContactStr = (contacts[sender.tag].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    var currentContact = String()
                    if currentContactStr.count > 10 {
                        currentContact = String(currentContactStr.suffix(10))
                    } else {
                        currentContact = currentContactStr
                    }
                    if !arrContacts.contains(currentContact){
                        arrContacts.append(currentContact)
                    }
                }
                if !selectedContacts.contains(contacts[sender.tag]){
                    selectedContacts.append(contacts[sender.tag])
                }
            }
        }
//        if selectedContacts.count > 0 {
//            selectedViewHeight.constant = 120
//            self.view.layoutIfNeeded()
//            selectedCollectionView.layoutIfNeeded()
//            selectedCollectionView.reloadData()
//        }
        if selectionType == "newGroup" {
            selectedCollectionView.reloadData()
            selectedCollectionView2.reloadData()
            let H = selectedCollectionView.collectionViewLayout.collectionViewContentSize.height
            selectedViewHeight.constant = H
            selectedViewHeight2.constant = H
            self.view.layoutIfNeeded()
            selectedCollectionView.layoutIfNeeded()
        }
    }
    @IBAction func deSelectContacts(_ sender: UIButton) {
        if selectionType == "newGroup" {
            selectedContacts.remove(at: sender.tag)
            arrContacts.remove(at: sender.tag)
            selectedCollectionView.reloadData()
            selectedCollectionView2.reloadData()
            let H = selectedCollectionView.collectionViewLayout.collectionViewContentSize.height
            selectedViewHeight.constant = H
            selectedViewHeight2.constant = H
            self.view.layoutIfNeeded()
            selectedCollectionView.layoutIfNeeded()
            contactsTblView.reloadData()
        }
    }
    
    @IBAction func newBlox(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchContacts (){
        self.contacts.removeAll()
        let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactPhoneNumbersKey,
                        CNContactEmailAddressesKey,
            CNContactImageDataKey,
            CNContactImageDataAvailableKey
                ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                    (contact, stop) in
                // Array containing all unified contacts from everywhere
                let selfNo = USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) as? String
                if contact.phoneNumbers.count > 0{
                    let phone = (contact.phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                    if phone != nil && phone != selfNo && phone.count > 0{
                        self.contacts.append(contact)
                    }
                }
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
//                        print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue), email: \(contact.emailAddresses)")
                    }
                    
                }
            }
            // sort by name given
                let result = contacts.sorted(by: {
                    (firt: CNContact, second: CNContact) -> Bool in firt.givenName < second.givenName
                })
            self.contacts = result

            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                self.contactsTblView.reloadSections([0], with: .automatic)
            }
//            print(contacts)
        } catch {
            print("unable to fetch contacts")
        }
    }
    
    @IBAction func nameGroupCancel(_ sender: UIButton) {
        nameGroupPopUp.isHidden = true
        groupName.text = ""
        groupImg.image = nil
    }
    @IBAction func addGroupImage(_ sender: UIButton) {
        self.showActionSheet()
    }
    @IBAction func updateGroup(_ sender: UIButton) {
    }
    @IBAction func createGroup(_ sender: UIButton) {
        serverCallForCreateGroup()
    }
    func showActionSheet(){
        let actionSheet = UIAlertController.init(title: "", message: "Please select one of the following", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func showPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        inblox_searchBar.resignFirstResponder()
        selectionSearchBar.resignFirstResponder()
        groupName.resignFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIImagePickerControlloller Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.groupImg.image = image
            self.groupImgBtn.setImage(nil, for: .normal)
            groupImageData = image.pngData()!
//            self.serverCallForUpdateProfilePhoto(imageData!)
            
        }
        self.dismiss(animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//// Local variable inserted by Swift 4.2 migrator.
////let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
////
////        if let image = infoconvertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage) as? UIImage {
////            self.imguser.image = image
////            let imageData = image.pngData()
////            self.serverCallForUpdateProfilePhoto(imageData!)
////
////        }
//
//        self.dismiss(animated: true, completion: nil)
//    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return selectedContacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectedContactsCell", for: indexPath) as! selectedContactsCell
            cell.contactName.text = "\(selectedContacts[indexPath.row].givenName) \(selectedContacts[indexPath.row].familyName)"
        let myName = "\(selectedContacts[indexPath.row].givenName) \(selectedContacts[indexPath.row].familyName)"
        let nameArr = myName.split(separator: " ")
        if nameArr.count == 1 {
            let firstLetter = String(nameArr[0])
            cell.shortName.text = String(Array(firstLetter)[0])
        } else if nameArr.count == 2 {
            let firstLetter = String(nameArr[0])
            let secondLetter: String = String(nameArr[1])
            cell.shortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
        } else {
            cell.shortName.text = ""
        }
        
        let numberStr = (selectedContacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
        cell.contactNo.text = numberStr.toPhoneNumber()
        if selectedContacts[indexPath.row].imageDataAvailable {
            let image = UIImage(data: selectedContacts[indexPath.row].imageData!)
            cell.ImgView.image = image
            cell.shortName.isHidden = true
        } else {
            cell.ImgView.image = nil
            cell.shortName.isHidden = false
        }
        
        cell.closeBtn.tag = indexPath.row
            return cell
        
    }
    
    //MARK:- UITableView Delegates and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == contactsTblView {
            if(contactsSearchActive) {
                return searchResults.count
            } else {
                return contacts.count
            }
        } else {
            if (inbloxSearchActive){
                return inbloxSearchResults.count
            } else {
                return arrInblox.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == contactsTblView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selContactCell") as! selContactCell
            if(contactsSearchActive) {
                cell.cName.text =  "\(searchResults[indexPath.row].givenName) \(searchResults[indexPath.row].familyName)"
                let myName = "\(searchResults[indexPath.row].givenName) \(searchResults[indexPath.row].familyName)"
                let nameArr = myName.split(separator: " ")
                if nameArr.count == 1 {
                    let firstLetter = String(nameArr[0])
                    cell.sName.text = String(Array(firstLetter)[0])
                } else if nameArr.count == 2 {
                    let firstLetter = String(nameArr[0])
                    let secondLetter: String = String(nameArr[1])
                    cell.sName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                } else {
                    cell.sName.text = ""
                }
                if searchResults[indexPath.row].imageDataAvailable {
                    let image = UIImage(data: searchResults[indexPath.row].imageData!)
                    cell.cImage.image = image
                    cell.sName.isHidden = true
                } else {
                    cell.cImage.image = nil
                    cell.sName.isHidden = false
                }
                let currentContact = String(((searchResults[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String).suffix(10))
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
            
            } else {
                cell.cName.text =  "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
                let myName = "\(contacts[indexPath.row].givenName) \(contacts[indexPath.row].familyName)"
                let nameArr = myName.split(separator: " ")
                if nameArr.count == 1 {
                    let firstLetter = String(nameArr[0])
                    cell.sName.text = String(Array(firstLetter)[0])
                } else if nameArr.count == 2 {
                    let firstLetter = String(nameArr[0])
                    let secondLetter: String = String(nameArr[1])
                    cell.sName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
                } else {
                    cell.sName.text = ""
                }
                if contacts[indexPath.row].imageDataAvailable {
                    let image = UIImage(data: contacts[indexPath.row].imageData!)
                    cell.cImage.image = image
                    cell.sName.isHidden = true
                } else {
                    cell.cImage.image = nil
                    cell.sName.isHidden = false
                }
                let currentContact = String(((contacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String).suffix(10))
                if currentContact != nil {
                    if arrContacts.contains(currentContact) {
                        cell.checkBtn.isSelected = true
                    } else {
                        cell.checkBtn.isSelected = false
                    }
                } else {
                    cell.checkBtn.isSelected = false
                }
            }
            cell.checkBtn.tag = indexPath.row
            if selectionType == "newChat" {
                cell.checkBtn.isHidden = true
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell") as! MessageCell
            let imgUser = cell.viewWithTag(100) as! UIImageView
            let lblMessageDate = cell.viewWithTag(101) as! UILabel
            let lblUsernName = cell.viewWithTag(102) as! UILabel
            let lblPersonCount = cell.viewWithTag(103) as! UILabel
            
            var inblox: Inblox!
            if (inbloxSearchActive) {
                inblox = inbloxSearchResults[indexPath.row]
            } else {
                inblox  = arrInblox[indexPath.row]
            }
            lblMessageDate.text = inblox.message_date ?? ""
            if "\(inblox.user_ids ?? "")" == AppSharedData.sharedInstance.getUser()?.id {
                lblUsernName.text = inblox.display_name ?? ""
                var urlstring = WEB_URL.BaseURL + (inblox.display_image ?? "")
                urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
                imgUser.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "Single--pic1"), options: nil, progressBlock: nil, completionHandler: nil)
                if inblox.total_users == 1 {
                    lblPersonCount.text = "You and \(inblox.display_name ?? "")"
                } else {
                    lblPersonCount.text = "You and \(inblox.total_users ?? 0) others"
                }
            } else {
                lblUsernName.text = inblox.display_name_2 ?? ""
                var urlstring = WEB_URL.BaseURL + (inblox.display_image_2 ?? "")
                if inblox.message_type == 1{
                    urlstring = "https://api.talkblox.info/groups/" + (inblox.display_image_2 ?? "")
                    urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
                    imgUser.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "Group-pic2"), options: nil, progressBlock: nil, completionHandler: nil)
                } else {
                    urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
                    imgUser.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "Single--pic1"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                if inblox.total_users == 1 {
                    lblPersonCount.text = "You and \(inblox.display_name_2 ?? "")"
                } else {
//                    lblPersonCount.text = "You and \(inblox.total_users ?? 0) others"
                    lblPersonCount.text = "You and other members"
                }
            }
            cell.allMessageBtn.tag = indexPath.row
//            cell.bgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            if inblox.message_id == selectedMessageID {
                cell.bgView.alpha = 0.3
            } else {
                cell.bgView.alpha = 0.1
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == contactsTblView {
             return 80
        } else {
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == contactsTblView {
            if selectionType == "newChat" {
                self.arrContacts.removeAll()
                if(contactsSearchActive) {
                    if searchResults[indexPath.row].phoneNumbers.count > 0 {
                        let currentContactStr = (searchResults[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        var currentContact = String()
                        if currentContactStr.count > 10 {
                            currentContact = String(currentContactStr.suffix(10))
                        } else {
                            currentContact = currentContactStr
                        }
                        if !arrContacts.contains(currentContact){
                            arrContacts.append(currentContact)
                        }
                    }
                } else {
                    if contacts[indexPath.row].phoneNumbers.count > 0 {
                        let currentContactStr = (contacts[indexPath.row].phoneNumbers.first?.value as! CNPhoneNumber).value(forKey: "digits") as! String
                        var currentContact = String()
                        if currentContactStr.count > 10 {
                            currentContact = String(currentContactStr.suffix(10))
                        } else {
                            currentContact = currentContactStr
                        }
                        if !arrContacts.contains(currentContact){
                            arrContacts.append(currentContact)
                        }
                }
                }
                APP_DELEGATE.isNewChat = true
                self.contactSelectionView.isHidden = true
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TalkBoxHomeVC") as! TalkBoxHomeVC
                vc.inbloxData = self.inbloxData
                vc.arrContacts = self.arrContacts
                self.navigationController?.pushViewController(vc, animated: true)
                self.selectionSearchBar.resignFirstResponder()
            }
        } else {
//        let inblox = arrInblox[indexPath.row]
            var inblox: Inblox!
            if (inbloxSearchActive) {
                inblox = inbloxSearchResults[indexPath.row]
            } else {
                inblox  = arrInblox[indexPath.row]
            }
            selectedMessageID = inblox.message_id
            
            
            
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConversationVC") as! ConversationVC
        vc.messageID = inblox.message_id ?? 0
        vc.inbloxData = inblox
            if inblox.message_type == 1 {
                vc.conversationType = "group"
                vc.groupID = inblox.user_ids
                vc.titleName = inblox.display_name_2 ?? ""
                vc.titleImage = inblox.display_image_2 ?? ""
            } else {
                vc.conversationType = "single"
                if "\(inblox.user_ids ?? "")" == AppSharedData.sharedInstance.getUser()?.id {
                    vc.titleName = inblox.display_name ?? ""
                    vc.titleImage = inblox.display_image ?? ""
                } else {
                    vc.titleName = inblox.display_name_2 ?? ""
                    vc.titleImage = inblox.display_image_2 ?? ""
                }
            }
            if "\(inblox.user_ids ?? "")" == AppSharedData.sharedInstance.getUser()?.id {
                vc.contactStr = inblox.user_phone_1
            } else {
                vc.contactStr = inblox.user_phone_2
            }
            
        self.navigationController?.pushViewController(vc, animated: true)
        }
        inblox_searchBar.resignFirstResponder()
    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! MessageCell
//        cell.bgView.alpha = 0.1
//    }
//    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
//        let cell = tableView.cellForRow(at: indexPath) as! MessageCell
//        cell.bgView.alpha = 0.1
//        return indexPath
//    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if tableView == contactsTblView {
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! MessageCell
            cell.bgView.alpha = 0.3
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when){
                tableView.reloadSections([0], with: .none)
            }
        }
    }
//    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! MessageCell
//        cell.bgView.alpha = 0.1
//    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
//        contactsTable.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == inblox_searchBar {
            inbloxSearchActive = false;
            inblox_searchBar.resignFirstResponder()
        } else {
            contactsSearchActive = false;
            selectionSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == inblox_searchBar {
            inbloxSearchActive = false
            tableView.reloadData()
            inblox_searchBar.resignFirstResponder()
        } else {
            contactsSearchActive = false;
            contactsTblView.reloadData()
            selectionSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == inblox_searchBar{
            inbloxSearchActive = false
            inblox_searchBar.resignFirstResponder()
        } else {
            contactsSearchActive = false;
            selectionSearchBar.resignFirstResponder()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == inblox_searchBar{
            inbloxSearchResults = arrInblox.filter{
                if "\($0.user_ids ?? "")" == AppSharedData.sharedInstance.getUser()?.id {
                    return ($0.display_name?.lowercased().contains(searchText.lowercased()))!
                } else {
                    return ($0.display_name_2?.lowercased().contains(searchText.lowercased()))!
                }
            }
            if(searchText.count == 0){
                inbloxSearchActive = false;
            } else {
                inbloxSearchActive = true;
            }
            self.tableView.reloadData()
        } else {
         
            searchResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
//            searchResults = contacts.filter{($0.givenName.lowercased().contains(searchText.lowercased()))||($0.familyName.lowercased().contains(searchText.lowercased())) || (("\($0.givenName.lowercased()) \($0.familyName.lowercased())").contains(searchText.lowercased()))}
            
            if(searchText.count == 0){
                contactsSearchActive = false;
            } else {
                contactsSearchActive = true;
            }
            self.contactsTblView.reloadData()
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnHomeAction(_ sender:Any){
        self.popToHome()
    }
    

    //MARK:- server Calls
    func serverCallForGetInBlox(){
        
        let params:NSDictionary = ["sender_id":AppSharedData.sharedInstance.currentUser?.userid ?? 0,"limit": 50,
                                   "page":1]
        
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.getMessages, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            
//            LoadingIndicatorView.hide()
           
            if success == true {
                if let statusDic = response?["header"] as? NSDictionary {
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        let inbloxData = [Inblox].from(jsonArray: response?["result"] as! [JSON])
                        let result = inbloxData!.sorted(by: {
                            (first: Inblox, second: Inblox) -> Bool in
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"

                            let dateAString = first.message_date
                            let dateBString = second.message_date
                            let dateA = dateFormatter.date(from: dateAString!)
                            let dateB = dateFormatter.date(from: dateBString!)
                            if dateA == nil || dateB == nil {
                                return true
                            }
                            return dateA!.compare(dateB!) == .orderedDescending
                        })
                        self.arrInblox = result
                        self.tableView.reloadData() //reloadSections(IndexSet(integer: 0), with: .top)
                        DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
                            
//                            if self.arrInblox.count > 0 {
//                                let indexPath = NSIndexPath(item: 0, section: 0)
//                                self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
//                            }
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                        print("failes")
                         DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
                        
                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                         DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
                    }
                    else {
                        print("failes")
                    }
                } else {
                    if let statusDic = response?["result"] as? NSDictionary {
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                         DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
                    }
                }
            }
        })
    }
    

    
    func serverCallForCreateGroup(){
        LoadingIndicatorView.show("Loading...")
        let params:NSDictionary = ["userid":String(AppSharedData.sharedInstance.currentUser?.userid ?? 0), "group_name": groupName.text!, "group_member_id": arrContacts]
        NetworkManager.sharedInstance.executeServiceFor(url:  WEB_URL.createGroup, methodType: .post, postParameters: params as? [String : Any]) { (success:Bool, response:NSDictionary?) in
             DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    DispatchQueue.main.async{
                        self.arrContacts.removeAll()
                        self.selectedContacts.removeAll()
                        self.contactSelectionView.isHidden = true
                        self.nameGroupPopUp.isHidden = true
                        let group_id = (response?["group_id"] as! NSNumber).stringValue
                        self.serverCallForUploadGroupImage(groupID: group_id)
                        self.serverCallForGetInBlox()
                        self.selectionSearchBar.resignFirstResponder()
                        self.selectedCollectionView.reloadData()
                        self.selectedCollectionView2.reloadData()
                        self.contactsTblView.reloadData()
                    }
                } else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            } else {
                print("failed")
            }
        }
    }
    
    func serverCallForUploadGroupImage(groupID: String){
        let params:NSDictionary = ["group_id": groupID]
        NetworkManager.sharedInstance.executeServiceWithMultipartWithImageName(WEB_URL.uploadGroupImage, fileName: "group_image", arrDataToSend: [groupImageData], postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
         DispatchQueue.main.async {
                            LoadingIndicatorView.hide()
                        }
        
        if success == true {
            let statusDic = response?["header"] as! NSDictionary
            if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                print(response!)

            } else {
//                let msg = statusDic["ErrorMessage"] as! String
//                AppSharedData.sharedInstance.showErrorAlert(message: msg)
                self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
            }
        }
    })
}
}
