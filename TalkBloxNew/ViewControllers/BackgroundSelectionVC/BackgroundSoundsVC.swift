//
//  BackgroundSoundsVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 6/21/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import AVKit

protocol BackGroundSoundVCDelegates {
    func didSoundTrackSelected(sound:BackGroundMusic)
}

class BackgroundSoundCell: UITableViewCell {
    @IBOutlet weak var btnPlay: UIButton!
    
}
class BackgroundSoundsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var tableView:UITableView!
    var arrImages = [BackGroundMusic]()
    var videoPlayer :AVPlayer = AVPlayer()
    var animationTimer:Timer? = nil
    var delegate:BackGroundSoundVCDelegates?
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serverCallForGetSoundTracks()
         // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
                                      })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Selector Methods
    @objc func playClicked(_ btn:UIButton){
        let cell = btn.superview?.superview as! UITableViewCell
        let indexpath = self.tableView.indexPath(for: cell)
        let data = arrImages[(indexpath?.row)!]
        
    }
    
    //MARK:- Helper Methods
    //MARK: Audio player methods
    
    func playAudio(_ strVideoURL : String)//, videoImage : UIImage)
    {
       
        let playerItem = AVPlayerItem(url: URL(string: strVideoURL)!)
        self.videoPlayer = AVPlayer(playerItem: playerItem)
        if((videoPlayer.currentItem) != nil)
        {
            self.videoPlayer.replaceCurrentItem(with: playerItem)
        }
        else
        {
            self.videoPlayer = AVPlayer(playerItem: playerItem)
        }
        self.videoPlayer.play()
        //-- for animation ---
   /*     self.animationTimer = Timer.scheduledTimer(timeInterval: 0.3,
                                                   target:self,
                                                   selector:#selector(CreateBloxVC.animateRecordImage),
                                                   userInfo:nil,
                                                   repeats:true)
        let delayInSeconds = 45.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)*/
       /* DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        self.stop(UIButton())
                                        //self.audioRecorder!.stop()
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateBloxVC.playerDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem)*/
        
        
    }
    //MARK:- Button Actions
    @IBAction func btnPlayAction(_ btn:UIButton){
        if btn.isSelected {
            self.videoPlayer.pause()
            for cell in tableView.visibleCells as! [BackgroundSoundCell] {
                if cell.btnPlay.tag == btn.tag {
                    btn.isSelected = false
                } else {
                    cell.btnPlay.isSelected = false
                }
            }
        } else {
            for cell in tableView.visibleCells as! [BackgroundSoundCell] {
                if cell.btnPlay.tag == btn.tag {
                    btn.isSelected = true
                } else {
                    cell.btnPlay.isSelected = false
                }
            }
            btn.isSelected = true
            LoadingIndicatorView.show("Loading...")
            let cell = btn.superview?.superview as! UITableViewCell
            let indexpath = self.tableView.indexPath(for: cell)
            let sound = arrImages[(indexpath?.row)!]
            print("sound file selected")
            //   if(strDefaultUrl != "")
            //  {
            let pathComponent = "bgAudio\(indexpath?.row ?? 0).caf"
            var urlstring = (WEB_URL.BaseURL + sound.bgm_name!)
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
            AppTheme().callGetServiceForDownload(urlstring, fileNameToBeSaved: pathComponent, completion: { (result, data) in
                LoadingIndicatorView.hide()
                if(result == "success")
                {
                    print("video saved \(pathComponent)")
                    
                    //      self.loading.dismiss()
                    
                    var strAudioURLPath = (WEB_URL.BaseURL + sound.bgm_name!)
                    strAudioURLPath = strAudioURLPath.replacingOccurrences(of: " ", with: "%20")
                    self.playAudio(strAudioURLPath)
                    //   self.audioUrlPath = pathComponent
                    //   self.incrSecond = 0
                    
                    /*    if((self.meterTimer) != nil)
                     {
                     self.meterTimer?.invalidate()
                     }
                     if((self.animationTimer) != nil)
                     {
                     self.animationTimer?.invalidate()
                     }*/
                    
                    //   self.viewRecorder.isHidden = false
                    /*  UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                     
                     self.viewRecorderBottomLayout.constant = 0
                     self.view
                     .layoutIfNeeded()
                     
                     })
                     { (finished) -> Void in
                     self.playAudio(strDefaultUrl)
                     }*/
                }
                /* else
                 {
                 self.loading.dismiss()
                 self.showAlert(data as! String, strTitle: "Error")
                 }*/
                
                
            })
            
            //  }
        }
    }
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- UItableView Delegates And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BackgroundSoundCell") as! BackgroundSoundCell
        let data = arrImages[indexPath.row]
        let imgView = cell.viewWithTag(100) as! UIImageView
        let lblSongTitle = cell.viewWithTag(101) as! UILabel
        let lblArtistname = cell.viewWithTag(102) as! UILabel
//        let btnPlay = cell.viewWithTag(103) as! UIButton
        
        lblSongTitle.text = data.title ?? ""
        lblArtistname.text = data.artistname ?? ""
        var urlstring = WEB_URL.BaseURL + (data.thumbnail ?? "")
            urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.btnPlay.setImage(#imageLiteral(resourceName: "play").withRenderingMode(.alwaysTemplate), for: .normal)
        cell.btnPlay.imageView?.tintColor = UIColor.white
        cell.btnPlay.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sound = arrImages[indexPath.row]
        self.delegate?.didSoundTrackSelected(sound: sound)
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- serverCalls
    func serverCallForGetSoundTracks(){
        LoadingIndicatorView.show("Loading...")
        let param = ["type":"music" ]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getBackGround, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success {
                print("Get Album Response is ----->>>>>>> \(response!)")
                if let statusDic = response?["header"] as? NSDictionary{
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        if let result = response?["result"] as? NSArray {
                            for dictAlbum in result {
                                self.arrImages = [BackGroundMusic].from(jsonArray: result as! [JSON])!
                                self.tableView.reloadData()
                            }
                        }
                    }
                }else {
                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
                }
            }
        }
    }
}
