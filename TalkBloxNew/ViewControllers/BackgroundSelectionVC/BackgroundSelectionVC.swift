//
//  BackgroundSelectionVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/13/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss

protocol BackgroundSelectionDelegates {
    func didBackgroundSelected(backgroundImage: BackGroundData)
}

class BackgroundSelectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView:UICollectionView!
    
    let aryMediaImageData : NSMutableArray = NSMutableArray()
    var galleryImageData:Data?
    
    var arrImages = [BackGroundData]()
    var delegate: BackgroundSelectionDelegates?

    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.serverCallForGetBackGroundImages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let delayInSeconds = 30.0
        let delayInNanoSeconds =
            DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayInNanoSeconds,
                                      execute: {
                                        LoadingIndicatorView.hide()
                                      })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK:- UICollectionViewDelegates And DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "backgroundImageCell", for: indexPath)
        let imgBackground = cell.viewWithTag(100) as! UIImageView
        let data = arrImages[indexPath.row]
        var urlstring =  WEB_URL.BaseURL + data.bgi_name!
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
//        imgBackground.kf.setImage(with: URL.init(string:urlstring))
        if urlstring.hasSuffix(".mp4") /*|| urlstring.hasSuffix(".gif") */{
            var img: UIImage!
            DispatchQueue.global(qos: .userInitiated).async {
                img  = AppTheme().getImageFromUrl(urlstring)
                DispatchQueue.main.async {
                    imgBackground.image = img
                }
            }
        } else {
            imgBackground.kf.setImage(with: URL.init(string:urlstring), placeholder: UIImage(named: "signup_background"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = arrImages[indexPath.row]
        self.delegate?.didBackgroundSelected(backgroundImage: data)
        self.navigationController?.popViewController(animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width / 2
        return CGSize.init(width: width - 10, height: 175)
    }
    
    //MARK:- UIButton Actions
    @IBAction func btnBackAction(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- serverCalls
    func serverCallForGetBackGroundImages(){
        LoadingIndicatorView.show("Loading...")
      let param = ["type":"image" ]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.getBackGround, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success {
                print("Get Album Response is ----->>>>>>> \(response!)")
                if let statusDic = response?["header"] as? NSDictionary{
                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                        if let result = response?["result"] as? NSArray {
                            for dictAlbum in result {
                                self.arrImages = [BackGroundData].from(jsonArray: result as! [JSON])!
                                self.arrImages.shuffle()
                                self.collectionView.reloadData()
                            }
                        }
                    }
                }else {
                    Utilities.shared.showAlertwith(message: "Unable to get data", onView: self)
                }
            }
        }
    }
    

}


