//
//  ForgotPasswordVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 9/11/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ForgotPasswordVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate  {

    @IBOutlet var txtMobile:UITextField!
    @IBOutlet var txtCountryCode:UITextField!
    @IBOutlet var lblCountryCode:UILabel!
    @IBOutlet var imgFlag:UIImageView!
    var picker:UIPickerView = UIPickerView()
    var arrCountries = [Country]()
    var otp = 0
     var userID = 0
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         self.createPicker()
        
        
        let contryUSA = self.arrCountries.filter{($0.name == "United States")}
        if contryUSA.count > 0 {
            self.txtCountryCode.text = "+\(contryUSA[0].numcode ?? 0)"
            self.imgFlag.kf.setImage(with: URL.init(string: contryUSA[0].image ?? ""))
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    //MARK:- Helper Mthods
    func showOTPAlert()  {
        let alert = UIAlertController.init(title: "OTP", message: "One time Password", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
            let textField = alert.textFields![0] as UITextField
            if Int(textField.text ?? "0") == self.otp {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewPasswordVC") as! NewPasswordVC
                vc.otp = textField.text ?? ""
                vc.userID = self.userID
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
//                AppSharedData.sharedInstance.showErrorAlert(message: "Otp is incorrect.")
                self.view.makeToast("Otp is incorrect", duration: 2.0, position: .bottom)
            }
           
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter OTP"
        }
        self.present(alert, animated: true, completion: nil)
    }
    func createPicker(){
        picker.delegate = self
        picker.dataSource = self
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(pickerDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([doneButton,spaceButton], animated: false)
        toolbar.tintColor = UIColor.black
        
        txtCountryCode.inputAccessoryView = toolbar
        txtCountryCode.tintColor = .clear
        txtCountryCode.inputView = picker
    }
    //MARK:- Selector Methods
    @objc func pickerDone(){
        self.view.endEditing(true)
    }
    
    //MARK:- UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 60))
        let myImageView = UIImageView(frame: CGRect(x: 20, y: 17, width: 25, height: 25))
        let myLabel = UILabel(frame: CGRect(x: 80, y: 0, width: pickerView.bounds.width - 90, height: 60))
        myLabel.text = "+\(arrCountries[row].numcode ?? 0) \(arrCountries[row].name ?? "")"
        myImageView.kf.setImage(with: URL.init(string: arrCountries[row].image!))
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        return myView
    }
    
    //MARK:- UIPickerViewDelegate
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.lblCountryCode.text = "+\(arrCountries[row].numcode ?? 0)"
        self.txtCountryCode.text = "+\(arrCountries[row].numcode ?? 0)"
        self.imgFlag.kf.setImage(with: URL.init(string: arrCountries[row].image ?? ""))
    }
    
    //MARK:- Button Actions
    @IBAction func btnBackAction(_ sender:Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitAction(_ sender:Any) {
        if self.txtMobile.text != "" {
            self.serverCallForSendOTP()
        }else{
//             AppSharedData.sharedInstance.showErrorAlert(message: "Please enter mobile Number.")
            self.view.makeToast("Please enter mobile number.", duration: 2.0, position: .bottom)
        }
    }
    
    //MARK:- Server Calls
    func serverCallForSendOTP(){
        LoadingIndicatorView.show("Loading...")
        let param = ["phoneNo":self.txtMobile.text ?? "","phone_code":self.txtCountryCode.text ?? ""]
        NetworkManager.sharedInstance.executeServiceFor(url: WEB_URL.forgotPassword, methodType: .post, postParameters: param) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                if let tmpOtp  =  response?["otp"] as? Int {
                    self.otp = tmpOtp
                }
                
                if let tmpUserID  =  response?["user"] as? Int {
                    self.userID = tmpUserID
                }
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    print(response!)
                    self.showOTPAlert()
                }else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
    }

}
