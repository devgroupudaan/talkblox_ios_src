//
//  NewPasswordVC.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 9/16/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class NewPasswordVC: UIViewController{

    @IBOutlet var txtNewPassword:UITextField!
    @IBOutlet var txtConfirmPassword:UITextField!
    
    var otp = ""
    var userID = 0
    
    //MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    //MARK:- Helper Methods
    func isValidData() -> Bool {
        var msg = ""
        if self.txtNewPassword.text == "" {
            msg = "Please enter new password."
        }else if self.txtNewPassword.text != self.txtConfirmPassword.text {
            msg = "Pawword and confirm password must be same."
        }
        
        if msg == "" {
            return true
        }else {
//            AppSharedData.sharedInstance.showErrorAlert(message: msg)
            self.view.makeToast(msg, duration: 2.0, position: .bottom)
            return false
        }
    }
    
    
    
    
    //MARK:- UIButton Actions
    @IBAction func btnSaveAction(_ sender:Any) {
        if self.isValidData() {
             self.serverCallForUpdatePassword()
        }
    }
    @IBAction func btnBackAction(_ sender:Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Sever calls
    func serverCallForUpdatePassword()  {
        LoadingIndicatorView.show()
        let param = ["user":userID,"password":self.txtNewPassword.text ?? "", "otp":self.otp] as [String : Any]
        NetworkManager.sharedInstance.executeService(WEB_URL.updatePassword, postParameters: param as NSDictionary) { (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                     AppSharedData.sharedInstance.showErrorAlert(message: "Password successfully changed. please login with new credentials.")
                    self.view.makeToast("Password successfully changed. Please login with new credentials", duration: 2.0, position: .bottom)
                     self.navigationController?.popToRootViewController(animated: true)
                }else {
//                    let msg = statusDic["ErrorMessage"] as! String
//                    AppSharedData.sharedInstance.showErrorAlert(message: msg)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        }
        
    }
    
    
    
    

    
    
    
}
