//
//  BloxCastViewController.swift
//  TalkBloxNew
//
//  Created by Mahendra Lariya on 05/04/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UIKit
import Gloss
import Cosmos
import AVKit
import Kingfisher

struct bloxIds {
    var chat_id : Int
    var message_id : Int
}

class PopBloxcastCell: UICollectionViewCell {
    @IBOutlet weak var bloxcaster_img: DesignableImageView!
    @IBOutlet weak var bloxcaster: UILabel!
    @IBOutlet weak var blox_count: UILabel!
    @IBOutlet weak var followers: UILabel!
    
}
class BloxCastCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var user_img: DesignableImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var viewsLbl: UILabel!
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var bloxcastSubject: UILabel!
    
}
class BloxCastViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
    @IBOutlet weak var popularBlxcstr: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var myImage: DesignableImageView!
    @IBOutlet weak var myShortName: UILabel!
    @IBOutlet weak var notificationCount: DesignableLabel!
    
    var arrTopBloxcaster = [TopBloxcaster]()
    var messageID = 0
//    var arrConversations = [Conversation]()
    var arrConversations = [BloxCast]()
    var arrBloxcastComments = [BloxcastComment]()
    
    var arrInblox = [Inblox]()
    
    var inputImageBgID: String!
    var inputSoundBgID: String!
    
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    
    var inbloxData:Inblox?
    
    //----- Code for showing message from sent and recieve list ----
    var dictUserMessageData : NSMutableDictionary!
    
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var chatID = 0
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    
    var pageNO = 1
    
    var bloxRating: Double = 0.0
    
    var bloxIDsArr = [bloxIds]()
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //newInbloxCell
//        self.serverCallForGetInBlox()
//        self.serverCallForGetBloxCast()
//        self.serverCallForGetBloxCast(pageN: pageNO)
        popularBlxcstr.delegate = self
        popularBlxcstr.dataSource = self
        
        let gestureProfile1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchtoProfile(_:)))
        gestureProfile1.numberOfTapsRequired = 1
        myImage.addGestureRecognizer(gestureProfile1)
        
        let gestureProfile2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchtoProfile(_:)))
        gestureProfile2.numberOfTapsRequired = 1
        myShortName.addGestureRecognizer(gestureProfile2)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        self.serverCallForGetTopBloxcaster()
        pageNO = 1
//        self.serverCallForGetBloxCast(pageN: pageNO)
        
        let user = AppSharedData.sharedInstance.currentUser
        DispatchQueue.main.async{
        
        let myName = user?.displayName  ?? ""
        let nameArr = myName.split(separator: " ")
        if nameArr.count == 1 {
            let firstLetter = String(nameArr[0])
            self.myShortName.text = String(Array(firstLetter)[0])
        } else if nameArr.count == 2 {
            let firstLetter = String(nameArr[0])
            let secondLetter: String = String(nameArr[1])
            self.myShortName.text = String(Array(firstLetter)[0]) + String(Array(secondLetter)[0])
        } else {
            self.myShortName.text = ""
        }
       let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
       self.myImage.kf.setImage(with: URL.init(string: imgURL), placeholder:#imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    @objc func switchtoProfile(_ sender: UITapGestureRecognizer){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
        vc.isSelfProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func ImgTapped(_ tapGesture : UITapGestureRecognizer){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.user_id = tapGesture.view?.tag
        vc.isSelfProfile = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func bell_icon_tapped(_ sender: UIButton) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.pushViewController(VC!, animated: false)
    }
    
    @IBAction func editButtonClickAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloxcastSubscriptionViewController") as! BloxcastSubscriptionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func homeButtonClickAction(_ sender: Any) {
         self.popToHome()
    }

    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
//        DispatchQueue.global().async { //1
//            let asset = AVAsset(url: url) //2
//            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
//            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
//            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
//            do {
//                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
//                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
//                DispatchQueue.main.async { //8
//                    completion(thumbNailImage) //9
//                }
//            } catch {
//                print(error.localizedDescription) //10
//                DispatchQueue.main.async {
//                    completion(nil) //11
//                }
//            }
//        }
       
        DispatchQueue.global().async {
            do {
                let asset = AVAsset(url: url)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let timestamp = CMTimeMakeWithSeconds(0.1, preferredTimescale: 600)
                print("Timestemp:   \(timestamp)")
                let cgImage = try imgGenerator.copyCGImage(at: timestamp, actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                DispatchQueue.main.async {
                    completion(thumbnail)
                }
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }

    //MARK:- UICollectionView Delegates & Datasources
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        arrConversations.count
        arrTopBloxcaster.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopBloxcastCell", for: indexPath) as! PopBloxcastCell
        let conversation = arrTopBloxcaster[indexPath.row]
        
        cell.bloxcaster.text = conversation.displayName
        var urlstring = WEB_URL.BaseURL + (conversation.user_image ?? "")
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        
        cell.bloxcaster_img.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.blox_count.text = conversation.bloxcastCount!.formattedWithSeparator + " bloxcasts"
        cell.followers.text = (Int(conversation.subscribers ?? "0"))!.formattedWithSeparator + " followers"
        
        cell.bloxcaster_img.tag = conversation.userid!
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BloxCastViewController.ImgTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        cell.bloxcaster_img.addGestureRecognizer(tapGesture)
        
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = arrTopBloxcaster[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.user_id = user.userid
        vc.isSelfProfile = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    //MARK:- UITableViewDelegates And DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrConversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BloxCastCell", for: indexPath) as! BloxCastCell
        cell.tag = indexPath.row
        let conversation = arrConversations[indexPath.row]
        cell.dateLbl.text = Utilities.shared.getChangedDate(date: conversation.created_at!)
        
        var urlstring: String = WEB_URL.BaseURL + (conversation.mediaUrl ?? "")
        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
        
        if urlstring.hasSuffix(".mp4") /*|| urlstring.hasSuffix(".gif")*/ {
            var img: UIImage!
            cell.imgView.image = nil
            DispatchQueue.global(qos: .userInitiated).async {
                img  = AppTheme().getImageFromUrl(urlstring)
                DispatchQueue.main.async {
                    cell.imgView.image = img
                }
            }
            
//            var completeURL = WEB_URL.BaseURL + urlstring
//            if urlstring.hasPrefix("https://api.talkblox.info/") {
//                completeURL = urlstring
//            }
//            self.getThumbnailImageFromVideoUrl(url: URL(string: completeURL)!) { (thumbNailImage) in
//                cell.imgView.image = thumbNailImage
//            }
            
        } else {
        cell.imgView.kf.setImage(with: URL.init(string: urlstring), placeholder: #imageLiteral(resourceName: "splash_bg-a-logo"), options: nil, progressBlock: nil, completionHandler: nil)
//            let url = URL(string: urlstring)!
//            let processor = DownsamplingImageProcessor(size: cell.imgView.bounds.size)
//                |> RoundCornerImageProcessor(cornerRadius: 20)
//            cell.imgView.kf.indicatorType = .activity
//            cell.imgView.kf.setImage(
//                with: url,
//                placeholder: #imageLiteral(resourceName: "place_holder_user"),
//                options: [
//                    .processor(processor),
//                    .scaleFactor(UIScreen.main.scale),
//                    .transition(.fade(1)),
//                    .cacheOriginalImage
//                ])
//            {
//                result in
//                switch result {
//                case .success(let value):
//                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
//                case .failure(let error):
//                    print("Job failed: \(error.localizedDescription)")
//                }
//            }
        }
        var urlstring2: String = WEB_URL.BaseURL + (conversation.user_image ?? "")
        urlstring2 = urlstring2.replacingOccurrences(of: " ", with: "%20")
        cell.user_img.kf.setImage(with: URL.init(string: urlstring2), placeholder: #imageLiteral(resourceName: "place_holder_user"), options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.nameLbl.text = conversation.displayName
        cell.viewsLbl.text = (conversation.views)!.formattedWithSeparator + " views"
        cell.bloxcastSubject.text = conversation.subject
        cell.starRating.rating = conversation.rating!
        return cell
        
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let conversation = arrConversations[indexPath.row]
        self.aryMessagePreivewData.removeAllObjects()
        self.chatID = conversation.chat_id ?? 0
//        if "\(conversation.sent_by ?? 0)" == AppSharedData.sharedInstance.getUser()?.id {
//           self.isSentByMe = true
//
//        }else {
//            self.isSentByMe = false
//        }
        self.bloxSubject = conversation.subject!
        self.Blox_user_image = conversation.user_image!
        self.bloxcastUsername = conversation.displayName!
        self.BloxDate =  Utilities.shared.getChangedDate(date: conversation.created_at!)
        self.aryMessagePreivewData.removeAllObjects()
        self.serverCallForPreview(message: conversation)
        self.chatID = conversation.chat_id ?? 0
        self.userID_for_profile = conversation.userid!
        self.bloxRating = conversation.rating ?? 0.0
      
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row == arrConversations.count - 1 {
        if indexPath.row == (pageNO * 10) - 1 {
            // we are at last cell load more content
            // we need to bring more records as there are some pending records available
//            let index = arrConversations.count
            pageNO = pageNO + 1
            self.serverCallForGetBloxCast(pageN: pageNO)
        }
    }
    
    
    //MARK:- server Calls
//    func serverCallForGetInBlox(){
//
//        let mobile  = "\(USER_DEFAULT.value(forKey: NOTIF.PHONE_CODE) ?? "")\(USER_DEFAULT.value(forKey: NOTIF.MOBILE_NUMBER) ?? "")"
//        let params:NSDictionary = ["sender_id":AppSharedData.sharedInstance.currentUser?.userid ?? 0]
//
//
//        LoadingIndicatorView.show("Loading...")
//        NetworkManager.sharedInstance.executeService(WEB_URL.getMessages, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
//
////            LoadingIndicatorView.hide()
//
//            if success == true {
//                if let statusDic = response?["header"] as? NSDictionary {
//                    if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                        let inbloxData = [Inblox].from(jsonArray: response?["result"] as! [JSON])
//                        let result = inbloxData!.sorted(by: {
//                            (first: Inblox, second: Inblox) -> Bool in
//                            let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                            let dateAString = first.message_date
//                            let dateBString = second.message_date
//                            let dateA = dateFormatter.date(from: dateAString!)
//                            let dateB = dateFormatter.date(from: dateBString!)
//                            if dateA == nil || dateB == nil {
//                                return true
//                            }
//                            return dateA!.compare(dateB!) == .orderedAscending
//                        })
//                        self.arrInblox = result
//                        var msgIDs = [Int]()
//                        for inblox in result{
//                            msgIDs.append(inblox.message_id!)
//                        }
//
//                        self.messageID = msgIDs.randomElement()!
//
//                        let selectedIND = result.firstIndex(where:{ $0.message_id == self.messageID })
//                        let inblox = self.arrInblox[selectedIND!]
//                        self.inbloxData = inblox
//
//                        self.serverCallForGetConversation()
//                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
//
//
//                    }else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                    }
//                }
//            }
//        })
//    }
//    func serverCallForGetConversation(){
//        let params:NSDictionary = ["message_id":messageID]
//
//            LoadingIndicatorView.show("Loading...")
//
//        NetworkManager.sharedInstance.executeService(WEB_URL.getConversation, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
//            if success == true {
//                let statusDic = response?["header"] as! NSDictionary
//                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                    let inbloxData = [Conversation].from(jsonArray: response?["result"] as! [JSON])
//                    let result = inbloxData!.sorted(by: {
//                        (first: Conversation, second: Conversation) -> Bool in
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                        let dateAString = first.text_added
//                        let dateBString = second.text_added
//                        let dateA = dateFormatter.date(from: dateAString!)
//                        let dateB = dateFormatter.date(from: dateBString!)
//                        if dateA == nil || dateB == nil {
//                            return true
//                        }
//                        return dateA!.compare(dateB!) == .orderedAscending
//                    })
//                    self.arrConversations = result
////                    self.tblView.reloadData()
////                    if self.arrConversations.count > 0 {
////                        let indexPath = NSIndexPath(item: self.arrConversations.count - 1, section: 0)
////                        self.tblView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
////                    }
////                    let indexPath = NSIndexPath(row: self.selectedIndex, section: 0)
////                    let cell = self.tableView.cellForRow(at: indexPath as IndexPath) as! MessageCell
////                    cell.configure(with: self.arrConversations)
//
//                    self.tableView.reloadData()
////                    self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
//                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
//
//
//                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                }
//            }
//        })
//    }
    
    func serverCallForGetBloxCast(pageN: Int){
            LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "userid=%d&page=%d&limit=10", AppSharedData.sharedInstance.currentUser?.userid ?? 0,pageN )
        
        NetworkManager.sharedInstance.executeGetService(WEB_URL.seeBloxCastWithRating, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [BloxCast].from(jsonArray: response?["Result"] as! [JSON])
                    
                    if pageN == 1 {
                        self.arrConversations = inbloxData!
                    } else {
                        self.arrConversations.append(contentsOf: inbloxData!)
                    }
                    for item in self.arrConversations {
                        let IDs = bloxIds(chat_id: item.chat_id!, message_id:0 )
                        self.bloxIDsArr.append(IDs)
                    }
                    
//                    self.tableView.reloadSections([0], with: .automatic)//reloadData()
//                    self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
                    self.tableView.reloadData()
                    if pageN > 1 {
                        let currentIndex = ((pageN - 1) * 10) - 1
                        let indexPath = NSIndexPath(item: currentIndex, section: 0)
                        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: false)
                    }
                    
                    LoadingIndicatorView.hide()
//                    self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    LoadingIndicatorView.hide()
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    LoadingIndicatorView.hide()
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCallForGetTopBloxcaster(){
//        let params:NSDictionary = ["message_id":messageID]
        
            LoadingIndicatorView.show("Loading...")
        
//        NetworkManager.sharedInstance.executeService(WEB_URL.topRatngsBlox, postParameters: params, completionHandler: {
            NetworkManager.sharedInstance.executeGetService(WEB_URL.topUsersWhoseMaxFollowers, parameters: "", completionHandler: {
            (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    let inbloxData = [TopBloxcaster].from(jsonArray: response?["result"] as! [JSON])
//                    let result = inbloxData!.sorted(by: {
//                        (first: TopBloxCast, second: TopBloxCast) -> Bool in
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                        let dateAString = first.text_added
//                        let dateBString = second.text_added
//                        let dateA = dateFormatter.date(from: dateAString!)
//                        let dateB = dateFormatter.date(from: dateBString!)
//                        if dateA == nil || dateB == nil {
//                            return true
//                        }
//                        return dateA!.compare(dateB!) == .orderedAscending
//                    })
//                    self.arrConversations = result
                    
                    self.arrTopBloxcaster = inbloxData!
                    DispatchQueue.main.async{
                        self.popularBlxcstr.reloadData()
                    }
                    let when = DispatchTime.now() + 0.5
                    DispatchQueue.main.asyncAfter(deadline: when){
//                        self.serverCallForGetRecentBloxcast()
                        self.serverCallForGetBloxCast(pageN: self.pageNO)
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
//    func serverCallForGetBloxCast(){
//            LoadingIndicatorView.show("Loading...")
//        let parameter = String(format: "userid=%d", AppSharedData.sharedInstance.currentUser?.userid ?? 0)
//
//        NetworkManager.sharedInstance.executeGetService(WEB_URL.seeBloxCastWithRating, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
////        NetworkManager.sharedInstance.executeGetService(WEB_URL.seeBloxCastOfOurSubscriber, parameters: parameter, completionHandler: { (success:Bool, response:NSDictionary?) in
////        NetworkManager.sharedInstance.executeService(WEB_URL.getConversation, postParameters: nil, completionHandler: { (success:Bool, response:NSDictionary?) in
//            LoadingIndicatorView.hide()
//            if success == true {
//                let statusDic = response?["header"] as! NSDictionary
//                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
//                    let inbloxData = [Conversation].from(jsonArray: response?["result"] as! [JSON])
//                    let result = inbloxData!.sorted(by: {
//                        (first: Conversation, second: Conversation) -> Bool in
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "MMMM dd yyyy, h:mm a"
//
//                        let dateAString = first.text_added
//                        let dateBString = second.text_added
//                        let dateA = dateFormatter.date(from: dateAString!)
//                        let dateB = dateFormatter.date(from: dateBString!)
//                        if dateA == nil || dateB == nil {
//                            return true
//                        }
//                        return dateA!.compare(dateB!) == .orderedAscending
//                    })
//                    self.arrConversations = result.reversed()
////                    self.tblView.reloadData()
////                    if self.arrConversations.count > 0 {
////                        let indexPath = NSIndexPath(item: self.arrConversations.count - 1, section: 0)
////                        self.tblView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.bottom, animated: true)
////                    }
////                    let indexPath = NSIndexPath(row: self.selectedIndex, section: 0)
////                    let cell = self.tableView.cellForRow(at: indexPath as IndexPath) as! MessageCell
////                    cell.configure(with: self.arrConversations)
//
//                    self.tableView.reloadData()
////                    self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
//                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
//
//
//                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
//
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                }
//            }
//        })
//    }
    
    func serverCallForPreview(message:BloxCast)  {
        let params:NSDictionary = ["message_id":messageID,"chat_id":message.chat_id ?? 0]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.preview, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            //            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let dict : NSDictionary = response {
                        
                        print("\n\ndictionary for message all urls :", dict, "\n\n")
                        self.generateDictionaryForPreview(dict.mutableCopy() as! NSMutableDictionary )
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCallForGetBloxcastComments(chatId: Int){
        
            LoadingIndicatorView.show("Loading...")
        let parameter = String(format: "chatId=%d",chatId)
        
            NetworkManager.sharedInstance.executeGetService(WEB_URL.viewComment, parameters: parameter, completionHandler: {
            (success:Bool, response:NSDictionary?) in
            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if response?["result"] != nil {
                    let inbloxData = [BloxcastComment].from(jsonArray: response?["result"] as! [JSON])
                    let result = inbloxData!.sorted(by: {
                        (first: BloxcastComment, second: BloxcastComment) -> Bool in
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

                        let dateAString = first.created_at
                        let dateBString = second.created_at
                        let dateA = dateFormatter.date(from: dateAString!)
                        let dateB = dateFormatter.date(from: dateBString!)
                        if dateA == nil || dateB == nil {
                            return true
                        }
                        return dateA!.compare(dateB!) == .orderedAscending
                    })
                    self.arrBloxcastComments = result
                    
                    self.tableView.reloadData()
                    } else {
                        self.tableView.reloadData()
//                        AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                        self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    
    //MARK:- Helper Methods
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
        self.present(errorAlert, animated: true ,completion: nil)
    }
    func generateDictionaryForPreview(_ dictMessageData : NSMutableDictionary)
    {
//        if let imageBg = dictMessageData.value(forKey: "imageBg")
        if let imageBg = dictMessageData.value(forKey: "chatImage") // changed on 28Dec2020
        {
            inputImageBgID = imageBg as! String
        }
        if let imageBg = dictMessageData.value(forKey: "chatSound") // changed on 28Dec2020
        {
            inputSoundBgID = imageBg as! String
        }
        if let contactId = dictMessageData.value(forKey: "contact_id")
        {
            dictMessagePreivewData.setValue(contactId, forKey: "contact_id")
        }
        if let messageRecList = dictMessageData.value(forKey: "message_receiver_list")
        {
            dictMessagePreivewData.setValue(messageRecList, forKey: "message_receiver_list")
        }
        if let messageUserId = dictMessageData.value(forKey: "message_user_id")
        {
            dictMessagePreivewData.setValue(messageUserId, forKey: "message_user_id")
        }
        if let messageUserName = dictMessageData.value(forKey: "message_user_name")
        {
            dictMessagePreivewData.setValue(messageUserName, forKey: "message_user_name")
        }
        
        if(messageParentId != 0)
        {
            dictMessagePreivewData.setValue(messageParentId, forKey: "parentId")
        }
        if(strMessageUserName != "")
        {
            dictMessagePreivewData.setValue(strMessageUserName, forKey: "messageUserName")
        }
        
        dictMessagePreivewData.setValue(strMessageId, forKey: "id")
        
        dictMessagePreivewData.setValue("Inbox", forKey: "MessagePreviewType")
        
        
        //-- Code to download BG bubble image ---
        
        if(previewBubbleImageURL != "")
        {
          
            previewBubbleImageURL = previewBubbleImageURL.replacingOccurrences(of: " ", with: "%20")
            var imageData : UIImage = UIImage()
            let img : UIImage = AppTheme().getImageFromUrl(previewBubbleImageURL)
            if img != nil
            {
                imageData = img
            }
            else
            {
                previewBubbleImageURL = ""
            }
            if(imageData.size.height > 0)
            {
                bubblePreviewImage = imageData
            }
            else
            {
                previewBubbleImageURL = ""
            }
        }
        //----
        
        /////new logic for bg Music
        if var strBgURL : String = dictMessageData.value(forKey: "chatSound") as? String
        {
              strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
            //AppTheme().killFileOnPath("previewBg.caf")
            
            //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
            AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                
                if(result == "success")
                {
                    self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
                else
                {
                    self.showAlert(data as! String, strTitle: "Error")
                }
                
            })
        }
        
        
        if let aryBgMedia : NSArray = dictMessageData.value(forKey: "bgMedia") as? NSArray
        {
            if let dictBgMedia : NSDictionary = aryBgMedia.object(at: 0) as? NSDictionary
            {
                if var strBgURL : String = dictBgMedia.value(forKey: "mediaUrl") as? String
                {
                      strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
                    //AppTheme().killFileOnPath("previewBg.caf")
                    
                    //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                            if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                            {
                                if aryMedia.count > 0
                                {
                                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                                    self.processResponce()
                                }
                            }
                        }
                        else
                        {
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                    })
                }
                else
                {
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
            }
            else
            {
                if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                {
                    if aryMedia.count > 0
                    {
                        self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                        self.processResponce()
                    }
                }
            }
        }
        else
        {
            if let aryMedia : NSArray =  dictMessageData.value(forKey: "frames") as? NSArray
            {
                if aryMedia.count > 0
                {
                    let result: NSArray = aryMedia.sorted(by: {(int1, int2)  -> Bool in
                        return ((int1 as! NSDictionary).value(forKey: "frameNo") as! Int) < ((int2 as! NSDictionary).value(forKey: "frameNo") as! Int) // It sorted the values and return to the mySortedArray
                    }) as NSArray
                    self.aryMessageMedia = result.mutableCopy() as! NSMutableArray
                    self.processResponce()
                }
            }
        }
        
        
    }
    
    var aryCountIncr : NSInteger = 0
    func processResponce()
    {
        if(aryMessageMedia.count > aryCountIncr) {
            
            if let dictMedia : NSDictionary = aryMessageMedia.object(at: aryCountIncr) as? NSDictionary
            {
                //bloxDetails
                dictBloxDetails = NSMutableDictionary()
                //var strBloxType : String = String()
                if let strCaption : String = dictMedia.value(forKey: "caption") as? String
                {
                    dictBloxDetails.setValue(strCaption, forKey: "caption")
                }
                else
                {
                    dictBloxDetails.setValue("", forKey: "caption")
                }
                
                if var soundURL : String = dictMedia.value(forKey: "soundURL") as? String
                {
                        soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    dictBloxDetails.setValue(soundURL, forKey: "soundURL")
                }
                
                
                if let duration : NSNumber = dictMedia.value(forKey: "animationDuration") as? NSNumber
                {
                    dictBloxDetails.setValue(duration, forKey: "animationDuration")
                }
                else
                {
                    dictBloxDetails.setValue(1.0, forKey: "animationDuration")
                }
                
                if let strType : String = dictMedia.value(forKey: "mediaType") as? String
                {
                    
                    if(strType == GalleryType.Text as String)
                    {
                        strMessageBloxType = GalleryType.Text as String
                        
                        if let txtMessage : String = dictMedia.value(forKey: "text") as? String
                        {
                            self.txtMessage = txtMessage
                            dictBloxDetails.setValue(txtMessage, forKey: "message")
                        }
                        if let textColor : String = dictMedia.value(forKey: "textColor") as? String
                        {
                            self.txtColor = textColor
                            dictBloxDetails.setValue(textColor, forKey: "textColor")
                        }
                        if let txtSize : String = dictMedia.value(forKey: "textSize") as? String
                        {
                            guard let n = NumberFormatter().number(from: txtSize) else { return }
                            self.txtSize = CGFloat(n)
                            dictBloxDetails.setValue(txtSize, forKey: "fontSize")
                        }
                        if let txtStyle : String = dictMedia.value(forKey: "textStyle") as? String
                        {
                            self.txtStyle = txtStyle
                            dictBloxDetails.setValue(txtStyle, forKey: "fontName")
                        }
                        if let textBgMediaId : NSInteger = dictMedia.value(forKey: "textBgMediaId") as? NSInteger
                        {
                            dictBloxDetails.setValue(textBgMediaId, forKey: "textBgMediaId")
                        }
                        loadTextMessage()
                        
                    }
                    
                    
                    if(strType == GalleryType.Image as String)
                    {
                        strMessageBloxType = GalleryType.Image as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                    else if(strType == GalleryType.Video as String)
                    {
                        strMessageBloxType = GalleryType.Video as String
                        /*
                         if let strURL : String = dictMedia.valueForKey("thumb_url") as? String
                         {
                         if let img : UIImage = AppTheme().getImageFromUrl(strURL)
                         {
                         let data : NSData = UIImagePNGRepresentation(img)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         }
                         else
                         {
                         let data : NSData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         */
                        if var strVideoURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                             strVideoURL = strVideoURL.replacingOccurrences(of: " ", with: "%20")
                            //AppTheme().killFileOnPath("previewFile\(i).mp4")
                            //dictBloxDetails.setValue("previewFile\(i).mp4", forKey: "video")
                            var strImageThumbURL : String = String()
                            if var strURL : String = dictMedia.value(forKey: "thumbUrl") as? String {
                                   strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                                strImageThumbURL = strURL
                            }
                            loadVideoDataForURL(strVideoURL, fileName: "previewFile\(aryCountIncr).mp4", strURL: strImageThumbURL)
                            /*
                             AppTheme().callGetServiceForDownload(strVideoURL, fileNameToBeSaved: "previewFile\(i).mp4", completion: { (result, data) in
                             if(result == "success")
                             {
                             self.dictBloxDetails.setValue(data, forKey: "video")
                             }
                             })
                             */
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                        
                    }else if(strType == GalleryType.Sound as String){
                        strMessageBloxType = GalleryType.Sound as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
//                            self.playAudio(strURL)
//                            if var strMp3URL : String = dictMedia.value(forKey: "soundURL") as? String{
//                                strMp3URL = strMp3URL.replacingOccurrences(of: " ", with: "%20")
//                                self.playAudio(strMp3URL)
//                            }
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    } else if(strType == GalleryType.Gif as String){
                        strMessageBloxType = GalleryType.Gif as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            dictBloxDetails.setValue(strURL, forKey: "image")
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
                            
                            aryMessagePreivewData.add(dictTemp)
                            aryCountIncr += 1
                            processResponce()
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                }
                
            }
        }
        else
        {
            aryCountIncr = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
            vc.aryUserMessageData = self.aryMessagePreivewData
            vc.dictUserMessageData = self.dictUserMessageData
            vc.inbloxData = self.inbloxData
            vc.chatId = self.chatID
            vc.previewKind = "Bloxcast"
            vc.isSentByMe = self.isSentByMe
            vc.inputImageBgID = self.inputImageBgID
//            vc.bgImageName = self.bgImageName
            vc.strBgAudioPath = self.inputSoundBgID
            vc.bloxSubject = self.bloxSubject
            vc.user_name = self.bloxcastUsername
            vc.user_image = self.Blox_user_image
            vc.bloxDate = self.BloxDate
            vc.ratingFromBlox = self.bloxRating
            vc.userID_for_profile = self.userID_for_profile
            //    vc.imgViewBackground.image = self.imgBackground.image
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            //self.perform(#selector(MessageConversationVC.allSetUpWithPreviewData), with: nil, afterDelay: 1.0)
        }
        
    }
    
    
    
    func loadTextMessage() {
        
        dictBloxDetails.setValue(UIColor.white, forKey: "backgroundColor")
        let imageData : Data = screenShot().jpegData(compressionQuality: 0.8)!//UIImageJPEGRepresentation(screenShot(), 0.8)!
        
        dictBloxDetails.setValue(imageData, forKey: "image")
        
        let dictTemp : NSMutableDictionary = NSMutableDictionary()
        dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
        dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
        
        aryMessagePreivewData.add(dictTemp)
        aryCountIncr += 1
        processResponce()
    }
    
    func loadImageFromURL(_ strURL:String)
    {
        var imageData : Data = Data()
        
        let fileName = URL(fileURLWithPath: strURL).pathExtension
        if fileName == "gif"{
            let urlNew:String = WEB_URL.BaseURL + strURL//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let imageData1 = try? Data(contentsOf: URL(string: urlNew)!)
            {
                let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
                print("isAnimated: \(isImageAnimated)")
                imageData = imageData1
            } else {
                
            }
        } else {
            let img : UIImage = AppTheme().getImageFromUrl(WEB_URL.BaseURL + strURL)
            //        if let img : UIImage = AppTheme().getImageFromUrl("http://103.76.248.170:3001/" + strURL)
            if img != nil
            {
                imageData = img.pngData()! // UIImagePNGRepresentation(img)! //
            } else {
                imageData = UIImage(named: "SingleBlox")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)! //
            }
        }
        let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
        print("isAnimated: \(isImageAnimated)")
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
            
            aryMessagePreivewData.add(dictTemp)
            aryCountIncr += 1
            processResponce()
        }
    }
    
    func loadVideoDataForURL(_ strVideoURL:String, fileName: String, strURL : String)
    {
        var imageData : Data = Data()
//        if let img : UIImage = AppTheme().getImageFromUrl(strURL)  //commented on 25Jan
        let img : UIImage = AppTheme().getImageFromUrl(strVideoURL)
        if img != nil
        {
            imageData = img.pngData()! // UIImagePNGRepresentation(img)!//
        }
        else
        {
//            imageData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!  //commented on 25Jan
            imageData = UIImage(named: "signup_background")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "signup_background")!)!//
        }
        
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
        }
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
            {
                // do some task
                let url = WEB_URL.BaseURL + strVideoURL//"http://103.76.248.170:3001/"
//            let url = "http://103.76.248.170:3001/" + strVideoURL
                AppTheme().callGetServiceForDownload(url, fileNameToBeSaved: fileName, completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async {
                            // update some UI
                            self.dictBloxDetails.setValue(data, forKey: "video")
                            //  self.dictBloxDetails.setValue(64, forKey: "id")
                            
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(self.dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue("Video", forKey: "bloxType")
                            
                            self.aryMessagePreivewData.add(dictTemp)
                            self.aryCountIncr += 1
                            self.processResponce()
                        }
                    } else {
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
        }
    }
    func screenShot() -> UIImage {
        var DynamicView=UILabel(frame: CGRect(x:68, y:0, width:240, height:240))
        DynamicView.backgroundColor=UIColor.white
        //DynamicView.layer.cornerRadius=25
        //DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
        DynamicView.text = self.txtMessage
        DynamicView.textAlignment = NSTextAlignment.center
        DynamicView.font = UIFont(name:self.txtStyle, size: self.txtSize)
        let clr = Utilities.shared.convertHexToUIColor(hexColor: self.txtColor)
        DynamicView.textColor = clr
        //DynamicView.font = UIFont.systemFont(ofSize: self.txtSize)
        // DynamicView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DynamicView.lineBreakMode = .byWordWrapping
        DynamicView.numberOfLines = 0
        
        var frameRect : CGRect = DynamicView.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        //var textView=UITextView(frame: CGRect(x:68, y:0, width:240, height:240))
        
        
        DynamicView.layer.borderColor = UIColor.clear.cgColor
        UIGraphicsBeginImageContextWithOptions(DynamicView.bounds.size, DynamicView.isOpaque, 0.0)
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        DynamicView.layer.borderColor = AppTheme().themeColorBlue.cgColor
        // txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
    
//    func convertHexToUIColor(hexColor : String) -> UIColor {
//        
//        // define character set (include whitespace, newline character etc.)
//        let characterSet = CharacterSet.whitespacesAndNewlines as CharacterSet
//        
//        //trim unnecessary character set from string
//        var colorString : String = hexColor.trimmingCharacters(in: characterSet)
//        
//        // convert to uppercase
//        colorString = colorString.uppercased()
//        
//        //if # found at start then remove it.
//        if colorString.hasPrefix("#") {
//            let index = colorString.index(colorString.startIndex, offsetBy: 1)
//            colorString =  String(colorString[..<index])
//        }
//        
//        if colorString.count != 6 {
//            return UIColor.black
//        }
//        
//        // split R,G,B component
//        var rgbValue: UInt32 = 0
//        Scanner(string:colorString).scanHexInt32(&rgbValue)
//        let valueRed    = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
//        let valueGreen  = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
//        let valueBlue   = CGFloat(rgbValue & 0x0000FF) / 255.0
//        let valueAlpha  = CGFloat(1.0)
//        
//        // return UIColor
//        return UIColor(red: valueRed, green: valueGreen, blue: valueBlue, alpha: valueAlpha)
//    }
    
//    func resizeImage(_ image: UIImage, newSize: CGSize) -> (UIImage) {
//        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
//        let imageRef = image.cgImage
//
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
//        let context = UIGraphicsGetCurrentContext()
//
//        // Set the quality level to use when rescaling
//        context!.interpolationQuality = CGInterpolationQuality.high
//        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
//
//        context!.concatenate(flipVertical)
//        // Draw into the context; this scales the image
//        context!.draw(imageRef!, in: newRect)
//
//        let newImageRef = context!.makeImage()! as CGImage
//        let newImage = UIImage(cgImage: newImageRef)
//
//        // Get the resized image from the context and a UIImage
//        UIGraphicsEndImageContext()
//
//        return newImage
//    }
}
