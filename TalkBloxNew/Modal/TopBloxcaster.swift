//
//  TopBloxcaster.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 28/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct TopBloxcaster : JSONDecodable {
    var userid          : Int?
    var displayName       : String?
    var user_image        : String?
    var subscribers   : String?
    var bloxcastCount    : Int?
    init?(json: JSON) {
        self.userid    = "userid" <~~ json
        self.displayName          = "displayName" <~~ json
        self.user_image       = "user_image" <~~ json
        self.subscribers       = "subscribers" <~~ json
        self.bloxcastCount          = "bloxcastCount" <~~ json
    }
}
