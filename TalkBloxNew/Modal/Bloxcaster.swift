//
//  Bloxcaster.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 21/06/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct Bloxcaster : JSONDecodable {
    var userid          : Int?
    var displayName       : String?
    var user_image        : String?
    var rating   : Double?
    var chat_id    : Int?
    var ratingOutOf : Double?
    init?(json: JSON) {
        self.userid      = "userid" <~~ json
        self.displayName = "displayName" <~~ json
        self.user_image  = "user_image" <~~ json
        self.rating      = "rating" <~~ json
        self.chat_id     = "chat_id" <~~ json
        self.ratingOutOf = "ratingOutOf" <~~ json
    }
}
