//
//  GroupMember.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 01/07/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct GroupMember : JSONDecodable {
    var phoneNo          : String?
    var displayName       : String?
    var user_image       : String?
    var member_id        : Int?
    var userid        : Int?
    var member_status        : Int?
    
    init?(json: JSON) {
        self.phoneNo      = "phoneNo" <~~ json
        self.displayName = "displayName" <~~ json
        self.user_image = "user_image" <~~ json
        self.member_id = "member_id" <~~ json
        self.userid = "userid" <~~ json
        self.member_status = "member_status" <~~ json
    }
}
