//
//  Group.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 25/06/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct Group : JSONDecodable {
    var group_id          : Int?
    var group_name       : String?
    init?(json: JSON) {
        self.group_id      = "group_id" <~~ json
        self.group_name = "group_name" <~~ json
    }
}
