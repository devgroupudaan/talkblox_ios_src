//
//  UserDetail.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 8/2/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss
struct UserDetail : JSONDecodable{
    var userid          : Int?
    var displayName       : String?
    var user_email        : String?
    var description       :String?

    var phoneNo   : String?
    var user_image    : String?
    var username  :String?
    var myBloxCast_Count          : Int?
    var subscribers          : String?
    var following          : Int?
    init?(json: JSON) {
        self.userid    = "userid" <~~ json
        self.displayName = "displayName" <~~ json
        self.user_email             = "user_email" <~~ json
        self.phoneNo    = "phoneNo" <~~ json
        self.description    = "description" <~~ json
        self.user_image        = "user_image" <~~ json
        self.username        = "username" <~~ json
        self.myBloxCast_Count        = "myBloxCast_Count" <~~ json
        self.subscribers        = "subscribers" <~~ json
        self.following        = "following" <~~ json
    }
}
