//
//  Page.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 8/21/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct Page : JSONDecodable{
    var pagename          : String?
    var pagecode       : String?
    var url        : String?
   
    init?(json: JSON) {
        self.pagename    = "pagename" <~~ json
        self.pagecode = "pagecode" <~~ json
        self.url             = "url" <~~ json
        
    }
}
