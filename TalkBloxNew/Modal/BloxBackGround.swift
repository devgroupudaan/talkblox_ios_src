//
//  BloxBackGround.swift
//  TalkBloxNew
//
//  Created by Mahendra Lariya on 28/01/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss
struct BloxBackGround : JSONDecodable{
    var bgi_id          : Int?
    var bgi_name       : String?
    var bgi_added        : String?
    var bgi_status   : Int?
   
    init?(json: JSON) {
        self.bgi_id    = "bgi_id" <~~ json
        self.bgi_name = "bgi_name" <~~ json
        self.bgi_added             = "bgi_added" <~~ json
        self.bgi_status    = "bgi_status" <~~ json
       
    }
}

                                                                                             
