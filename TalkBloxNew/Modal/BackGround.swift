//
//  BackGround.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 6/20/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss
struct BackGroundData : JSONDecodable{
    var bgi_id          : Int?
    var bgi_name       : String?
    var bgi_added        : String?
    var bgi_status   : Int?
   
    init?(json: JSON) {
        self.bgi_id    = "bgi_id" <~~ json
        self.bgi_name = "bgi_name" <~~ json
        self.bgi_added             = "bgi_added" <~~ json
        self.bgi_status    = "bgi_status" <~~ json
       
    }
}
struct BackGroundMusic : JSONDecodable{
    var bgm_id         : Int?
    var artistname     : String?
    var bgm_name       : String?
    var title       : String?
    var bgm_added      : String?
    var bgm_status     : Int?
    var thumbnail     : String?
    
    init?(json: JSON) {
        self.bgm_id    = "bgm_id" <~~ json
        self.bgm_name = "bgm_name" <~~ json
        self.bgm_added             = "bgm_added" <~~ json
        self.bgm_status    = "bgm_status" <~~ json
        self.artistname             = "artistname" <~~ json
        self.title             = "title" <~~ json
        self.thumbnail    = "thumbnail" <~~ json
        
    }
}
