//
//  TopBloxCast.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 03/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct TopBloxCast : JSONDecodable {
    var userid    : Int?
    var displayName    : String?
    var user_image    : String?
    var rating          : Double?
    var chat_id       : Int?
    var ratingOutOf        : Int?
    var mediaId   : Int?
    var mediaUrl    : String?
    var subject    :String?
    var views : Int?
    var created_at    : String?
    
    init?(json: JSON) {
        self.userid         = "userid" <~~ json
        self.displayName    = "displayName" <~~ json
        self.user_image     = "user_image" <~~ json
        self.rating         = "rating" <~~ json
        self.chat_id        = "chat_id" <~~ json
        self.ratingOutOf    = "ratingOutOf" <~~ json
        self.mediaId        = "mediaId" <~~ json
        self.mediaUrl       = "mediaUrl" <~~ json
        self.subject        = "subject" <~~ json
        self.views          = "views" <~~ json
        self.created_at     = "created_at" <~~ json
    }
}
