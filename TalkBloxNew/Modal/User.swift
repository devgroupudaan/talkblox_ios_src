//
//  User.swift
//  SKTATTV
//
//  Created by abdeali on 12/23/16.
//  Copyright © 2016 haider. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    public var id : String?
    public var name : String?
    public var email : String?
    public var phone_number : String?
    public var username : String?
    public var country : String?
    public var display_name : String?
    public var address : String?
    public var city : String?
    public var age_range : String?
    public var birth_month : String?
    public var gender : String?
    public var notificationChanel : String?
    public var state : String?
    public var zip_code : String?
    public var authentication_token : String?
    
    
    init(id: String, name: String, email: String, phone_number: String, country: String, display_name: String, address: String, city: String, age_range: String, birth_month: String, gender: String, notificationChanel: String , state: String, zip_code: String, authentication_token: String,username:String) {
        
        self.id = id
        self.name = name
        self.email = email
        self.phone_number = phone_number
        self.country = country
        self.display_name = display_name
        self.address = address
        self.city = city
        self.age_range = age_range
        self.birth_month = birth_month
        self.gender = gender
        self.notificationChanel = notificationChanel
        self.state = state
        self.zip_code = zip_code
        self.username = username
        self.authentication_token = authentication_token
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        let id = aDecoder.decodeObject(forKey: "id") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let email = aDecoder.decodeObject(forKey: "email") as! String
        let phone_number = aDecoder.decodeObject(forKey: "phone_number") as! String
        let country = aDecoder.decodeObject(forKey: "country") as! String
        let display_name = aDecoder.decodeObject(forKey: "display_name") as! String
        let address = aDecoder.decodeObject(forKey: "address") as! String
        let city = aDecoder.decodeObject(forKey: "city") as! String
        let age_range = aDecoder.decodeObject(forKey: "age_range") as! String
        let birth_month = aDecoder.decodeObject(forKey: "birth_month") as! String
        let gender = aDecoder.decodeObject(forKey: "gender") as! String
        let notificationChanel = aDecoder.decodeObject(forKey: "notificationChanel") as! String
        let state = aDecoder.decodeObject(forKey: "state") as! String
        let zip_code = aDecoder.decodeObject(forKey: "zip_code") as! String
         let username = aDecoder.decodeObject(forKey: "username") as! String
        let authentication_token = aDecoder.decodeObject(forKey: "authentication_token") as! String
  
       
        self.init(id: id, name: name, email: email, phone_number: phone_number, country: country, display_name: display_name, address: address, city: city, age_range: age_range, birth_month: birth_month, gender: gender, notificationChanel: notificationChanel, state: state, zip_code: zip_code, authentication_token: authentication_token,username:username)
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone_number, forKey: "phone_number")
        aCoder.encode(country, forKey: "country")
        aCoder.encode(display_name, forKey: "display_name")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(age_range, forKey: "age_range")
        aCoder.encode(birth_month, forKey: "birth_month")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(notificationChanel, forKey: "notificationChanel")
        aCoder.encode(state, forKey: "state")
        aCoder.encode(zip_code, forKey: "zip_code")
        aCoder.encode(authentication_token, forKey: "authentication_token")
        aCoder.encode(username, forKey: "username")
        
    }
    
}
