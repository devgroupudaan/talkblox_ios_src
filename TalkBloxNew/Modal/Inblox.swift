//
//  Inblox.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 6/25/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct Inblox : JSONDecodable{
    var message_id          : Int?
    var display_image       : String?
    var display_image_2       : String?
    var display_name        : String?
    var display_name_2        : String?
    var message_type   : Int?
    var total_users    : Int?
    var message_date: String?
    var user_ids: String?
    var user_phone_1: String?
    var user_phone_2: String?
    init?(json: JSON) {
        self.message_id    = "message_id" <~~ json
        self.display_image = "display_image" <~~ json
        self.display_image_2 = "display_image_2" <~~ json
        self.display_name             = "display_name" <~~ json
        self.display_name_2             = "display_name_2" <~~ json
        self.message_type    = "message_type" <~~ json
        self.total_users        = "total_users" <~~ json
        self.message_date        = "message_date" <~~ json
        self.user_ids        = "user_ids" <~~ json
        self.user_phone_1        = "user_phone_1" <~~ json
        self.user_phone_2        = "user_phone_2" <~~ json
    }
}
