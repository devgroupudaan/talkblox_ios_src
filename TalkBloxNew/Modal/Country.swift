//
//  Country.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/8/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss
struct Country : JSONDecodable{
    var id          : String?
    var image       : String?
    var name        : String?
    var numcode   : Int?
    var sortname    : String?
    init?(json: JSON) {
        self.id    = "id" <~~ json
        self.image = "image" <~~ json
        self.name             = "name" <~~ json
        self.numcode    = "numcode" <~~ json
        self.sortname        = "sortname" <~~ json
    }
}
