//
//  Conversation.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 7/27/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss
struct Conversation : JSONDecodable{
    var text_media_id          : Int?
    var sent_by       : Int?
    var sendername        : String?
    var text_added   : String?
    var gc_path    : String?
    var setthumb   :String?
    var subject    :String?
    var chatSubject :String?
    var chat_id    :Int?
    var message_id    :Int?
    init?(json: JSON) {
        self.text_media_id    = "text_media_id" <~~ json
        self.sent_by          = "sent_by" <~~ json
        self.sendername       = "sendername" <~~ json
        self.text_added       = "media_added" <~~ json
        self.gc_path          = "gc_path" <~~ json
        self.subject          = "subject" <~~ json
        self.chatSubject          = "chatSubject" <~~ json// changed on 28Dec2020
        self.chat_id          = "chat_id" <~~ json
        self.setthumb          = "setthumb" <~~ json
        self.message_id          = "message_id" <~~ json
    }
}
