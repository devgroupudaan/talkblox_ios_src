//
//  notificationItem.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 01/06/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct notificationItem: JSONDecodable {
    var notificationId   : Int?
    var chat_id          : Int?
    var message_id       : Int?
    var senderName       : String?
    var title            : String?
    var subject          : String?
    var type             : String?
    var userid           : Int?
    var status           : Int?
    var created_at       : String?
    var receiverId       : Int?
    var user_image       : String?
    
    init?(json: JSON) {
        self.notificationId    = "notificationId" <~~ json
        self.chat_id           = "chat_id" <~~ json
        self.message_id        = "message_id" <~~ json
        self.senderName        = "senderName" <~~ json
        self.title             = "title" <~~ json
        self.subject           = "subject" <~~ json
        self.type              = "type" <~~ json
        self.userid            = "userid" <~~ json
        self.status            = "status" <~~ json
        self.created_at        = "created_at" <~~ json
        self.receiverId        = "receiverId" <~~ json
        self.user_image        = "user_image" <~~ json
    }
}

