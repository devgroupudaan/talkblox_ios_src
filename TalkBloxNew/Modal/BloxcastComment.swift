//
//  BloxcastComment.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 29/04/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct BloxcastComment : JSONDecodable {
    var userid          : Int?
    var displayName       : String?
    var user_image        : String?
    var comment   : String?
    var created_at    : String?
    init?(json: JSON) {
        self.userid    = "userid" <~~ json
        self.displayName          = "displayName" <~~ json
        self.user_image       = "user_image" <~~ json
        self.comment       = "comment" <~~ json
        self.created_at          = "created_at" <~~ json
    }
}
