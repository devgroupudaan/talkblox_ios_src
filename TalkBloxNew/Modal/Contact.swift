//
//  Contact.swift
//  TalkBox
//
//  Created by codezilla-mac1 on 16/02/18.
//  Copyright © 2018 codezilla. All rights reserved.
//

import UIKit
import Gloss

struct Contact: JSONDecodable{
    let phoneNo: String?
    let ContactUserId: String?
    let name: String?
    let id: String?

    let registered: Bool?
    
    
    init?(json: JSON) {
        self.phoneNo = "phoneNo" <~~ json
        self.ContactUserId        = "ContactUserId" <~~ json
        self.name        = "name" <~~ json
        self.id        = "id" <~~ json
        
        self.registered       = "registered" <~~ json
        
    }
}
