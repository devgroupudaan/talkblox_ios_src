//
//  RecentBloxCast.swift
//  TalkBloxNew
//
//  Created by Macbook Pro (L43) on 15/05/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct bloxFrame: JSONDecodable {
    var frame_no    : Int?
    var mediaId    : Int?
    var mediaUrl    : String?
    init?(json: JSON) {
        self.frame_no         = "frame_no" <~~ json
        self.mediaId        = "mediaId" <~~ json
        self.mediaUrl       = "mediaUrl" <~~ json
    }
}

struct RecentBloxCast : JSONDecodable {
    var chat_id    : Int?
    var displayName    : String?
    var user_image    : String?
    var imageBg    : String?
    var soundBg          : String?
    var subject       : String?
    var views    : Int?
    var userid    : Int?
    var created_at    : String?
    var rating    : Double?
    var frames        : [bloxFrame]?
    var chat_imageBg   : String?
    
//    var rating          : Double?
//    var ratingOutOf        : Int?
    
    init?(json: JSON) {
        self.chat_id        = "chat_id" <~~ json
        self.displayName    = "sendername" <~~ json
        self.user_image    = "user_image" <~~ json
        self.imageBg        = "imageBg" <~~ json
        self.soundBg       = "soundBg" <~~ json
        self.subject        = "subject" <~~ json
        self.views        = "views" <~~ json
        self.userid        = "userid" <~~ json
        self.created_at     = "created_at" <~~ json
        self.rating        = "rating" <~~ json
        self.frames        = "frames" <~~ json
        self.chat_imageBg        = "chat_imageBg" <~~ json
        
//        self.rating     = "rating" <~~ json
//        self.ratingOutOf        = "ratingOutOf" <~~ json
    }
}
