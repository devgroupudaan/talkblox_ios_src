//
//  SearchData.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 6/26/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import Gloss

struct SearchResult : JSONDecodable{
    var gc_id          : Int?
    var gal_alb_id       : Int?
    var gal_cat_id        : Int?
    var gc_name   : String?
    var gal_col_thumb    : String?
    var gal_id: Int?
    var gal_type          : Int?
    var gc_path       : String?
    var gc_added_by        : String?
   
    init?(json: JSON) {
        self.gc_id    = "gc_id" <~~ json
        self.gal_alb_id = "gal_alb_id" <~~ json
        self.gal_cat_id             = "gal_cat_id" <~~ json
        self.gc_name    = "gc_name" <~~ json
        self.gal_col_thumb        = "gal_col_thumb" <~~ json
        self.gal_id        = "gal_id" <~~ json
        self.gal_type    = "gal_type" <~~ json
        self.gc_path        = "gc_path" <~~ json
        self.gc_added_by        = "gc_added_by" <~~ json
    }
}
