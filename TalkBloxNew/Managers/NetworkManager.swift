//
//  NetworkManager.swift
//  RewinrSwift3.0
//
//  Created by abdeali on 12/5/16.
//  Copyright © 2016 demo. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    static let sharedInstance = NetworkManager()
    fileprivate override init() {} //This prevents others from using the default '()' initializer for this class.
    func executeService(_ url:NSString, postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
       
        var headers: HTTPHeaders
        headers = ["Content-Type":"application/json"]

       
        if postParameters == nil {
            
//            Alamofire.request(urlString, method: .post , headers:headers).responseJSON { response in
            AF.request(urlString, method: .post , headers:headers).responseJSON { (response) in
                    switch response.result {
                                  case .success(let value):
                                      if let json = value as? [String: Any] {
                                        print("response - \(json as NSDictionary?)")
                                        completionHandler(true,json as NSDictionary?)
                                      }
                                  case .failure(let error): break
                                      // error handling
                                    print(error)
//                                    AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                    completionHandler(false,nil)
                                  }
//                guard response.result.error == nil else {
//                    // got an error in getting the data, need to handle it
//                    print(response.result.error!)
//                    AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                    completionHandler(false,nil)
//                    return
//                }
                
                // make sure we got some JSON since that's what we expect
//                guard let json = response.result.value as? [String: Any] else {
//                    print("didn't get todo object as JSON from API")
//                    print("Error: \(response.result.error)")
//                    AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                    completionHandler(false,nil)
//                    return
//                }
//
//                print("response - \(json as NSDictionary!)")
//                completionHandler(true,json as NSDictionary?)
            }
        } else {
            
//            Alamofire.request(urlString, method: .post ,parameters: postParameters! as? Parameters, encoding: JSONEncoding.default).responseJSON { response in
//
//                guard response.result.error == nil else {
//                    // got an error in getting the data, need to handle it
//                    print(response.result.error!)
//                    print(response)
//                    AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                    completionHandler(false,nil)
//                    return
//                }
//
//                // make sure we got some JSON since that's what we expect
//                guard let json = response.result.value as? [String: Any] else {
//                    print("didn't get todo object as JSON from API")
//                    print("Error: \(response.result.error)")
//                    AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                    completionHandler(false,nil)
//                    return
//                }
//
//                print("response - \(json as NSDictionary!)")
//                completionHandler(true,json as NSDictionary?)
//            }
            
            AF.request(urlString, method: .post ,parameters: postParameters! as? Parameters, encoding: JSONEncoding.default).responseJSON { (response) in
                    switch response.result {
                                  case .success(let value):
                                      if let json = value as? [String: Any] {
                                        print("response - \(json as NSDictionary?)")
                                        completionHandler(true,json as NSDictionary?)
                                      }
                                  case .failure(let error): break
                                      // error handling
                                    print(error)
//                                    AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                    completionHandler(false,nil)
                                  }
                }
        }
    }
    
    func executeServiceWithMultipart(_ url:NSString,fileType:String,arrDataToSend:NSArray, postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
        
//        Alamofire.upload
/*        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (index,dataToSend) in arrDataToSend.enumerated() {
           
                let data:Data = dataToSend as! Data
                
                if fileType == GalleryType.Image as String || fileType == GalleryType.Backgroundimage as String{
                    print(index)
                    multipartFormData.append(data, withName: "filename", fileName: "imagefile\(index).png", mimeType: "image/png")
                
                }else if fileType == GalleryType.Video as String{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "videofile\(index).mp4", mimeType: "video/mp4")
               
                }else if fileType == GalleryType.Sound as String{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "audiofile\(index).mp3", mimeType: "audio/mpeg")
//                    multipartFormData.append(data, withName: "file_pack\(index)", fileName: "audiofile\(index).mp3", mimeType: "audio/mpeg")

                }else if fileType == GalleryType.Gif as String{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "giffile\(index).gif", mimeType: "image/gif")
               
                }
              
                

            }
            
            
          /*  for (key, value) in postParameters! {
               
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }*/
//        }, to:urlString, method: .post, headers: nil)
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.responseJSON { response in
//                    print("STatus Code is :\(response.response?.statusCode ?? 0)")
//                    guard (response.result.value as? [String: Any]) != nil else {
//                        print("didn't get todo object as JSON from API")
//                        print("Error: \(String(describing: response.result.error))")
//                        AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                        completionHandler(false,nil)
//                        return
//                    }
//
//
//                    if let JSON = response.result.value  as? [String: Any] {
//                       print("JSON: \(JSON)")
//                       completionHandler(true,JSON as Any? as? NSDictionary)
//                    }
//                }
//
//            case .failure(let encodingError):
//
//                print(encodingError.localizedDescription)
//                completionHandler(false,nil)
//            }
//        }
            
        }, to:urlString, encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { progress in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })

                upload.responseJSON(completionHandler: { response in
                    print("STatus Code is :\(response.response?.statusCode ?? 0)")
                    guard (response.result.value as? [String: Any]) != nil else {
                        print("didn't get todo object as JSON from API")
                        print("Error: \(String(describing: response.result.error))")
                        AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
                        completionHandler(false,nil)
                        return
                    }
                    
                    
                    if let JSON = response.result.value  as? [String: Any] {
                        print("JSON: \(JSON)")
                        completionHandler(true,JSON as Any? as? NSDictionary)
                    }
                })

            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                completionHandler(false,nil)
            }
        })*/
        
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (index,dataToSend) in arrDataToSend.enumerated() {
           
                let data:Data = dataToSend as! Data
                
                if fileType == GalleryType.Image as String || fileType == GalleryType.Backgroundimage as String{
                    print(index)
                    multipartFormData.append(data, withName: "filename", fileName: "imagefile\(index).png", mimeType: "image/png")
                
                }else if fileType == GalleryType.Video as String{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "videofile\(index).mp4", mimeType: "video/mp4")
               
                }else if fileType == GalleryType.Sound as String{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "audiofile\(index).mp3", mimeType: "audio/mpeg")
//                    multipartFormData.append(data, withName: "file_pack\(index)", fileName: "audiofile\(index).mp3", mimeType: "audio/mpeg")

                }else if fileType == GalleryType.Gif as String{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "giffile\(index).gif", mimeType: "image/gif")
               
                }

            }
                }, to: urlString)
            .uploadProgress{progress in
                debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                        
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
//                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                completionHandler(false,nil)
            }
        }
    }
    func executeServiceWithMultipartWithImageName(_ url:NSString,fileName:String,arrDataToSend:NSArray, postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (index,dataToSend) in arrDataToSend.enumerated() {
                
                let data:Data = dataToSend as! Data
                print(index)
                multipartFormData.append(data, withName: fileName, fileName: "imagefile\(index).png", mimeType: "image/png")
                
                for (key, value) in postParameters! {
//                    if parse(parameter: value) == 
                    if key as! String == "group_member_id" {
                        if let tagsArray = value as? [String]{
                            let stringsData = NSMutableData()
                            for tag in tagsArray{
                                if let stringData = tag.data(using: String.Encoding.utf8) {
                                    stringsData.append(stringData)
                                }
                            }
                            multipartFormData.append((stringsData as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                        }
                    } else {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
                    }
                }
            }
        }, to: urlString)
        .uploadProgress{progress in
            debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
//                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                completionHandler(false,nil)
            }
        }
    }
    
    func executeServiceWithImage(_ url:NSString,postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
        
        
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//
//            for (key, value) in postParameters! {
//
//             //  multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
//            }
//        }, to:urlString)
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.responseJSON { response in
//
//                    guard (response.result.value as? [String: Any]) != nil else {
//                        print("didn't get todo object as JSON from API")
//                        print("Error: \(String(describing: response.result.error))")
//                        AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                        completionHandler(false,nil)
//                        return
//                    }
//
//
//                    if let JSON = response.result.value  as? [String: Any] {
//                        print("JSON: \(JSON)")
//                        completionHandler(true,JSON as Any? as? NSDictionary)
//                    }
//                }
//
//            case .failure(let encodingError):
//
//                print(encodingError.localizedDescription)
//                completionHandler(false,nil)
//            }
//        }
        
        AF.upload(multipartFormData: { (multipart) in
            for (key, value) in postParameters! {
                //  multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
                }, to: urlString)
            .uploadProgress{progress in
                debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                        
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
                completionHandler(false,nil)
            }
        }
    }

    func defaultGalleryServerRequest(url:String,videoData:Data, completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        
        let timeStamp = NSDate().timeIntervalSince1970
//        Alamofire.upload(multipartFormData: { multipart in
//            multipart.append(videoData, withName: "filename", fileName: "\(timeStamp).mp4", mimeType: "video/mp4")
//        }, to: "\(url)", method: .post, headers: nil) { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                upload.responseJSON { response in
//
//                    guard (response.result.value as? [String: Any]) != nil else {
//                        print("didn't get todo object as JSON from API")
//                        print("Error: \(String(describing: response.result.error))")
//                        AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                        completionHandler(false,nil)
//                        return
//                    }
//
//
//                    if let JSON = response.result.value  as? [String: Any] {
//                        print("JSON: \(JSON)")
//                        completionHandler(true,JSON as Any? as? NSDictionary)
//                    }
//                }
//            case .failure(let encodingError):
//                print("multipart upload encodingError: \(encodingError)")
//                completionHandler(false,nil)
//            }
//        }
        
        AF.upload(multipartFormData: { (multipart) in
            multipart.append(videoData, withName: "filename", fileName: "\(timeStamp).mp4", mimeType: "video/mp4")
                }, to: "\(url)", method: .post, headers: nil)
            .uploadProgress{progress in
                debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                        
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
                completionHandler(false,nil)
            }
        }
    }
    
    func executeGetService(_ url:NSString, parameters:String?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        
        print("URL " + (url as String))
        print("params - \(parameters as String?)")
        let urlString:String = (url as String) + "?" + parameters!
        
        var headers: HTTPHeaders
        headers = ["Content-Type":"application/json"]
        
// Alamofire.request(urlString, method: .get , encoding: URLEncoding.queryString , headers:headers).responseJSON { response in
//        Alamofire.request(urlString, method: .get , headers:headers).responseJSON { response in
//
//
//            guard response.result.error == nil else {
//                // got an error in getting the data, need to handle it
//                print(response.result.error!)
//                AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                completionHandler(false,nil)
//                return
//            }
//
//            // make sure we got some JSON since that's what we expect
//            guard let json = response.result.value as? [String: Any] else {
//                print("didn't get todo object as JSON from API")
//                print("Error: \(String(describing: response.result.error))")
//                AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                completionHandler(false,nil)
//                return
//            }
//
//            print("response - \(json as NSDictionary!)")
//            completionHandler(true,json as NSDictionary?)
//        }
        AF.request(urlString, method: .get, headers:headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    print("response - \(json as NSDictionary?)")
                    completionHandler(true,json as NSDictionary?)
                }
            case .failure(let error): break
                // error handling
                print(error)
//                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                completionHandler(false,nil)
            }
        }
    }
    
    func executeServiceFor(url:NSString, methodType:HTTPMethod, postParameters:[String:Any]?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        
        let urlString:String = url as String
        
//        var headers: HTTPHeaders
//        headers    = [
//                    "Content-Type": "application/x-www-form-urlencoded",
//                    "Authorization" : "Token"
//                ]
       
        AF.request(urlString, method: methodType, parameters: postParameters, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                switch response.result {
                              case .success(let value):
                                  if let json = value as? [String: Any] {
                                    print("response - \(json as NSDictionary?)")
                                    completionHandler(true,json as NSDictionary?)
                                  }
                              case .failure(let error):
                                  // error handling
                                print(error)
//                                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                completionHandler(false,nil)
                                break
                              }
            }
    }
   
//    func executeRequestForUploadImageOnServerWith( _url:NSString, postParameters:NSDictionary?, imageData: [Data]?,name:String, completionHandler:@escaping (_ success:Bool,_ errorCode:Int, _ response: NSDictionary?)->()) {
//
//        print("URL " + (_url as String))
//        print("params - \(postParameters as NSDictionary!)")
//
//        var headers = HTTPHeaders()
//
//
//         headers = [
//            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
//            "cache-control": "no-cache",
//            "Accept-Encoding": "gzip, deflate",
//            "Connection": "keep-alive"
//        ]
//
//        print("header \(headers)")
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            if imageData != nil {
//                for imgData in imageData!{
//                    let sticks = String(Date().timeIntervalSince1970)
//                    multipartFormData.append(imgData, withName: name,fileName: "file\(sticks).jpg", mimeType: "image/jpeg")
//                }
//
//            }
//
//            for (key, value) in postParameters! {
//                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
//            }
//
//
//        },to: _url as String, headers: headers)
//        { (result) in
//            switch result {
//            case .success(let upload, _,_ ):
//
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//
//                upload.responseJSON { response in
//                    //print(response.result.value!)
//                    var httpStatusCode:Int = 0
//                    if let serverCode = response.response?.statusCode{
//                        httpStatusCode = serverCode
//                    }
//                    print("response - \(String(describing: response.result.value)) server code - \(httpStatusCode)")
//
//                    if let json = response.result.value as? [String: Any]{  //Check for json
//                        print("Json response - \(json as NSDictionary!)")
//                        completionHandler(true,httpStatusCode,json as NSDictionary?)
//
//                    }else if let arrResponse:NSArray = response.result.value as? NSArray{ //Check for array
//                        if let dict = arrResponse[0] as? NSDictionary {
//                            print("Json response - \(dict as NSDictionary!)")
//                            completionHandler(true,httpStatusCode,dict as NSDictionary?)
//                        }
//
//                    }else{
//                        completionHandler(false,httpStatusCode,nil)
//                        AppSharedData.sharedInstance.showErrorAlert(message: (response.result.error?.localizedDescription)!)
//                    }
//                }
//            case .failure(let encodingError):
//                print(encodingError)
//            }
//        }
//    }
    
//    func uploadImageWith(url:String,userID:String,imageData:Data,completionHandler:@escaping (_ success:Bool)->()){
//
//        // generate boundary string using a unique per-app string
////        let boundary = UUID().uuidString
//      /*  let boundary = "Boundary-\(UUID().uuidString)"
//
//        let fieldName = "userid"
//        let fieldValue = "\(userID)"
//
//
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//        // Set the URLRequest to POST and to the specified URL
////        var urlRequest = URLRequest(url: URL(string:"http://3.23.52.49:3002/updatephoto")!)
//        var urlRequest = URLRequest(url: URL(string:"https://api.talkblox.info/api/v1/updatephoto")!)
//        urlRequest.httpMethod = "POST"
//
//        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
//        // And the boundary is also set here
//        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        var data = Data()
//
//        // Add the reqtype field and its value to the raw http request data
//
//
////        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
////        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
////        data.append("\(fieldValue)".data(using: .utf8)!)
//        data.append("--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"".data(using: .utf8)!)
//        data.append("\r\n\r\n\(fieldValue)\r\n".data(using: .utf8)!)
//
//
//        let imgData = imageData
//        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"filename\"; filename=\"ddd\"\r\n".data(using: .utf8)!)
//        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
//        data.append(imgData)
//
//
//        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
//        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
//        data.append("--\(boundary)--\r\n".data(using: .utf8)!)
//
//        // Send a POST request to the URL, with the data we created earlier
//        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
//
//            if(error != nil){
//                print("\(error!.localizedDescription)")
//                completionHandler(false)
//            }
//
//            guard let responseData = responseData else {
//                print("no response data")
//                completionHandler(false)
//                return
//            }
//
//            if let responseString = String(data: responseData, encoding: .utf8) {
//                print("uploaded to: \(responseString)")
//                completionHandler(true)
//            }
//        }).resume()*/
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//        let parameters = [
//          [
//            "key": "filename",
//            "data": imageData,
//            "type": "file"
//          ],
//          [
//            "key": "userid",
//            "value": userID,
//            "type": "text"
//          ]] as [[String : Any]]
//
//        let boundary = "Boundary-\(UUID().uuidString)"
//        var body = ""
//        var error: Error? = nil
//        for param in parameters {
//          if param["disabled"] == nil {
//            let paramName = param["key"]!
//            body += "--\(boundary)\r\n"
//            body += "Content-Disposition:form-data; name=\"\(paramName)\""
//            if param["contentType"] != nil {
//              body += "\r\nContent-Type: \(param["contentType"] as! String)"
//            }
//            let paramType = param["type"] as! String
//            if paramType == "text" {
//              let paramValue = param["value"] as! String
//              body += "\r\n\r\n\(paramValue)\r\n"
//            } else {
//              let fileData = param["data"] as! Data
////              let fileContent = String(data: fileData, encoding: .utf8)!
//              body += "; filename=\"\("image.png")\"\r\n"
//                + "Content-Type: \"content-type header\"\r\n\r\n\(fileData)\r\n"
//            }
//          }
//        }
//        body += "--\(boundary)--\r\n";
//        let postData = body.data(using: .utf8)
//
//        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
//        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        request.httpMethod = "POST"
//        request.httpBody = postData
//
//        // Send a POST request to the URL, with the data we created earlier
//        session.uploadTask(with: request, from: postData, completionHandler: { responseData, response, error in
//
//            if(error != nil){
//                print("\(error!.localizedDescription)")
//                completionHandler(false)
//            }
//
//            guard let responseData = responseData else {
//                print("no response data")
//                completionHandler(false)
//                return
//            }
//
//            if let responseString = String(data: responseData, encoding: .utf8) {
//                print("uploaded to: \(responseString)")
//                completionHandler(true)
//            }
//        }).resume()
//
////        Alamofire.upload(multipartFormData: { (multipartFormData) in
////
////                    multipartFormData.append(imageData, withName: "filename", fileName: "imagefile.png", mimeType: "image/png")
////                multipartFormData.append("\(userID)".data(using: String.Encoding.utf8)!, withName: "userid")
////        }, to:url)
////        { (result) in
////            switch result {
////            case .success(let upload, _, _):
////
////                upload.responseJSON { response in
////                    print("STatus Code is :\(response.response?.statusCode ?? 0)")
////                    guard (response.result.value as? [String: Any]) != nil else {
////                        print("Error: \(String(describing: response.result.error))")
////                        completionHandler(false)
////                        return
////                    }
////
////
////                    if let JSON = response.result.value  as? [String: Any] {
////                       print("JSON: \(JSON)")
////                       completionHandler(true)
////                    }
////                }
////
////            case .failure(let encodingError):
////
////                print(encodingError.localizedDescription)
////                completionHandler(false)
////            }
////        }
//    }
    
    func uploadImageWith(url:String,userID:String,imageData:Data,completionHandler:@escaping (_ success:Bool)->()){
        
        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString
        
        let fieldName = "userid"
        let fieldValue = "\(userID)"
        
        
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: URL(string:"https://api.talkblox.info/updatephoto")!)
        urlRequest.httpMethod = "POST"
        
        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        
        // Add the reqtype field and its value to the raw http request data
        
        
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue)".data(using: .utf8)!)
        
        
        
        let imgData = imageData
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"filename\"; filename=\"ddd\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(imgData)
        
        
        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            
            if(error != nil){
                print("\(error!.localizedDescription)")
                completionHandler(false)
            }
            
            guard let responseData = responseData else {
                print("no response data")
                completionHandler(false)
                return
            }
            
            if let responseString = String(data: responseData, encoding: .utf8) {
                print("uploaded to: \(responseString)")
                completionHandler(true)
            }
        }).resume()
    }
    
//    func executeServiceForJSONEncoding(_ url:NSString,methodType:HTTPMethod, postParameters:[String:Any]?,completionHandler:@escaping (_ success:Bool,_ httpStatusCode:Int, _ response:NSDictionary?)->())  {
//        
//        print("URL " + (url as String))
//        print("params - \(postParameters as NSDictionary!)")
//        let urlString:String = url as String
//        
//        var headers = HTTPHeaders()
//       // let accesToken = AppSharedData.sharedInstance.getAccessTokenAndRefreshToken()?.value(forKey: kAccessToken as String) as? String
//        headers = ["Content-Type":"application/json"]
//        Alamofire.request(urlString, method: methodType, parameters: postParameters!, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
//            
//            print(response.result.value)
//            
//            let httpStatusCode = response.response?.statusCode
//            if response.error != nil{
//                if httpStatusCode == HTTP_STATUS_CODE.SUCCESS{
//                    
//                    completionHandler(true,httpStatusCode!,nil)
//                }else {
//                    completionHandler(false,httpStatusCode ?? 0,nil)
//                    //                    AppSharedData.sharedInstance.showErrorAlert(message: "\(String(describing: response.error!.localizedDescription))")
//                }
//                
//                
//            }else{
//                
//                if httpStatusCode == HTTP_STATUS_CODE.SUCCESS{
//                    
//                    if let json = response.result.value as? [String: Any]{
//                        completionHandler(true,httpStatusCode!,json as NSDictionary?)
//                    }else if let jsonArr = response.result.value as? Array<Any>{
//                        let dic = ["data":jsonArr]
//                        completionHandler(true,httpStatusCode!,dic as NSDictionary?)
//                    }else{
//                        print("didn't get todo object as JSON from API")
//                        print("Error: \(String(describing: response.result.error))")
//                        let dicMsg = ["message":"didn't get todo object as JSON from API"]
//                        completionHandler(false,httpStatusCode!,dicMsg as NSDictionary)
//                    }
//                }else if httpStatusCode == HTTP_STATUS_CODE.TOKEN_EXPIRE {
//                    
//                    if url == WEB_URL.login{
//                        if let json = response.result.value as? [String: Any]{
//                            completionHandler(false,httpStatusCode!,json as NSDictionary?)
//                        }
//                    }else if httpStatusCode == HTTP_STATUS_CODE.SHOW_LOADER {
//                        
//                        completionHandler(false,httpStatusCode!,nil)
//                        
//                    }else{
//                        /*self.refreshTokenRequest(completionHandler: { (isSuccesRefresh:Bool) in
//                            if isSuccesRefresh{
//                                //                                self.executeService(urlString as NSString, postParameters: postParameters as! NSDictionary, completionHandler: { (success:Bool, response:NSDictionary?) in
//                                //                                     completionHandler(success,200,response)
//                                //                                })
//                                
//                                self.executeServiceForJSONEncoding(urlString as NSString, methodType: methodType, postParameters: postParameters, completionHandler: { (success:Bool, httpStatus:Int, response:NSDictionary?) in
//                                    completionHandler(success,httpStatus,response)
//                                })
//                            }else{
//                                completionHandler(false,httpStatusCode!,nil)
//                            }
//                        })*/
//                    }
//                }else{
//                    let json = response.result.value as? [String: Any]
//                    completionHandler(false,httpStatusCode!,json as NSDictionary?)
//                }
//            }
//        }
//    }
    
    func apiCallForUpdateProfileWith(parameters:[String:Any],completionHandler:@escaping (_ success:Bool,_ httpStatusCode:Int, _ response:NSDictionary?)->()){
        
        let headers = [
            "Content-Type": "application/json",
            ]
        
        do{
            print(parameters)
            
            let param = [
                "authentication_Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NjU2ODExOTUsImp3dGlkIjoibGRuanN3bjVsdTA4NGxoYW9yIiwiZGF0YSI6Ilt7XCJ1c2VyaWRcIjoxMyxcImJsb3hfYWNjZXNzXCI6MCxcImRpc3BsYXlOYW1lXCI6XCJ0ZXN0aW5nIDJcIixcInVzZXJfZW1haWxcIjpcIlwiLFwicGhvbmVOb1wiOlwiMjAyMDIwMjAyMFwiLFwicGhvbmVfY29kZVwiOlwiKzFcIixcInVzZXJfaW1hZ2VcIjpcIlwiLFwiY2l0eVwiOlwiXCIsXCJzdGF0ZVwiOlwiXCIsXCJjb3VudHJ5XCI6XCJcIixcImFkZHJlc3NcIjpcIlwiLFwiemlwX2NvZGVcIjowLFwiYWdlX3JhbmdlXCI6XCJcIixcImJpcnRoX21vbnRoXCI6XCJcIixcImdlbmRlclwiOlwiXCIsXCJ1c2VyX2FkZGVkX29uXCI6XCIyMDE5LTA4LTEzVDA3OjI1OjI5LjAwMFpcIixcInVzZXJfdmVyaWZpZWRcIjowLFwidXNlcl9pbnZpdGVfY29kZVwiOlwiXCIsXCJ1c2VyX3N0YXR1c1wiOjEsXCJkZXZpY2VUeXBlXCI6XCJpXCIsXCJkZXZpY2VJZFwiOlwiQkVBMjk5MDAtMDk3Ri00QzBBLTkyNjctNDM1MUY1MUIyOTY4XCIsXCJwYXNzd29yZFwiOlwiMjAyY2I5NjJhYzU5MDc1Yjk2NGIwNzE1MmQyMzRiNzBcIixcInBhc3N3b3JkX2NvbmZpcm1hdGlvblwiOlwiMjAyY2I5NjJhYzU5MDc1Yjk2NGIwNzE1MmQyMzRiNzBcIixcIm90cFwiOjAsXCJyZXNldFwiOjAsXCJhdXRoZW50aWNhdGlvbl9Ub2tlblwiOlwiXCIsXCJibG9ja19zdGF0dXNcIjowLFwiYmxvY2tfcmVhc29uXCI6XCJcIixcInVzZXJuYW1lXCI6XCJUZXN0aW5nIDJcIixcInN1YnNjcmliZXJzXCI6XCJcIn1dIiwiZXhwIjoxNTY1Njg0Nzk1fQ.uGP84dL-MA49wAWg8j5LYWWqzrlkWfM_gJjw2UqSBl4",
                "userId": "13",
                "user": [
                    "address": "Indore",
                    "age_range": "25",
                    "birth_month": "March",
                    "city": "",
                    "country": "",
                    "current_password": "",
                    "description": "this is testing accounts",
                    "display_name": "ios developer",
                    "gender": "",
                    "state": "",
                    "username": "Testing 2",
                    "zip_code": ""
                ]
                ] as [String : Any]
            
            let postData =  try JSONSerialization.data(withJSONObject: parameters, options: [])
            
//            let request = NSMutableURLRequest(url: NSURL(string: "http://3.23.52.49:3002/api/v1/profile")! as URL,
//                                              cachePolicy: .useProtocolCachePolicy,
//                                              timeoutInterval: 10.0)
            let request = NSMutableURLRequest(url: NSURL(string: "https://api.talkblox.info/api/v1/profile")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print(error)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse)
                    do{
                        let json =   try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                        completionHandler(true,(httpResponse?.statusCode)!,json)
                        print(json)
                    }catch{
                        completionHandler(false,(httpResponse?.statusCode)!,nil)
                    }
                    
                }
            })
            
            dataTask.resume()
        }catch{
            
        }
        
        
        
    }
}

