//
//  AppDelegate.swift
//  TalkBloxNew
//
//  Created by codezilla-mac3 on 02/05/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseMessaging
import GiphyUISDK
//com.TalkbloxNew.App.123

//com.nexomni.TalkBox

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {

    var window: UIWindow?
    var navigation_Controller : UINavigationController!
    
    var prefs : UserDefaults!
    var isLogedIn : ObjCBool!
    var isPushedOccured : Bool = false
    var notificationOpen: Bool = false
    var isInMessageVC : Bool = false
    var isInConversationMessageVC : Bool = false
    
    var isReplyMessage : Bool = false
    var isNewChat : Bool = false
    var isReplyAllMessage : Bool = false
    var isReplyPreviewMessage : Bool = false
    var dictReplyMessageDetails : NSMutableDictionary = NSMutableDictionary()
    var isAppRunningFirstTime : Bool = true
    
    var isPushForNewMessage : Bool = false
    var striPush : NSString = ""
    
    var isMessageSkipped : Bool = false
    
    let gcmMessageIDKey = "gcm.message_id"
    
    
    var messageID = 0
    var chatID = 0
    var bloxSubject = ""
    var bloxcastUsername = ""
    var Blox_user_image = ""
    var BloxDate = ""
    var userID_for_profile = 0
    var inputImageBgID: String!
    var inputSoundBgID: String!
    
    var aryMessagePreivewData : NSMutableArray = NSMutableArray()
    var aryMessageMedia : NSMutableArray = NSMutableArray()
    var dictBloxDetails : NSMutableDictionary = NSMutableDictionary()
    var dictMessagePreivewData : NSMutableDictionary = NSMutableDictionary()
    var strMessageBloxType : String = String()
    var strMessageId : NSInteger = 0
    var strMessageUserName : String = ""
    var messageParentId : NSInteger = 0
    
    var dictUserMessageData : NSMutableDictionary!
    
    var previewBubbleImageURL : String = ""
    var bubblePreviewImage : UIImage = UIImage()
    
    var txtMessage : String = ""
    var txtSize : CGFloat = 90
    var txtStyle : String = ""
    var txtColor : String = ""
    var isSentByMe = false
    
    var inbloxData:Inblox?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Giphy.configure(apiKey: "T7ZgBoWScnadTaALjWAMh979sXE47d9l")
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
      print("Firebase registration token: \(String(describing: fcmToken))")

      let dataDict:[String: String] = ["token": fcmToken ?? ""]
        print("FCM Token:", fcmToken)
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID1: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID2: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken.map { String(format: "%02.2hhx", $0) }.joined())")
        //        let token = InstanceID.instanceID().token()
        //        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
        
        //        device_token = InstanceID.instanceID().token()!//deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "TalkBloxNew")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func serverCallForPreview()  {
        let params:NSDictionary = ["message_id":messageID,"chat_id":chatID]
        LoadingIndicatorView.show("Loading...")
        NetworkManager.sharedInstance.executeService(WEB_URL.preview, postParameters: params, completionHandler: { (success:Bool, response:NSDictionary?) in
            //            LoadingIndicatorView.hide()
            if success == true {
                let statusDic = response?["header"] as! NSDictionary
                if statusDic["ErrorCode"] as! Int == ErroCode.Succes{
                    if let dict : NSDictionary = response {
                        
                        print("\n\ndictionary for message all urls :", dict, "\n\n")
                        self.generateDictionaryForPreview(dict.mutableCopy() as! NSMutableDictionary )
                    }
                }else if statusDic["ErrorCode"] as! Int == ErroCode.Pending{
                    
                    
                } else if statusDic["ErrorCode"] as! Int == ErroCode.Fail{
                    
//                    AppSharedData.sharedInstance.showErrorAlert(message: statusDic["ErrorMessage"] as! String)
//                    self.view.makeToast(statusDic["ErrorMessage"] as! String, duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    //MARK:- Helper Methods
    func showAlert(_ strMessage: String, strTitle: String)
    {
        let errorAlert = AppTheme().showAlert(strMessage, errorTitle: strTitle)
//        self.present(errorAlert, animated: true ,completion: nil)
        UIApplication.shared.keyWindow?.rootViewController?.present(errorAlert, animated: true, completion: nil)
    }
    func generateDictionaryForPreview(_ dictMessageData : NSMutableDictionary)
    {
//        if let imageBg = dictMessageData.value(forKey: "imageBg")
        if let imageBg = dictMessageData.value(forKey: "chatImage") // changed on 28Dec2020
        {
            inputImageBgID = imageBg as! String
        }
        if let chatSubject = dictMessageData.value(forKey: "chatSubject") // changed on 28Dec2020
        {
            bloxSubject = chatSubject as! String
        }
        if let imageBg = dictMessageData.value(forKey: "chatSound") // changed on 28Dec2020
        {
            inputSoundBgID = imageBg as! String
        }
        if let contactId = dictMessageData.value(forKey: "contact_id")
        {
            dictMessagePreivewData.setValue(contactId, forKey: "contact_id")
        }
        if let messageRecList = dictMessageData.value(forKey: "message_receiver_list")
        {
            dictMessagePreivewData.setValue(messageRecList, forKey: "message_receiver_list")
        }
        if let messageUserId = dictMessageData.value(forKey: "message_user_id")
        {
            dictMessagePreivewData.setValue(messageUserId, forKey: "message_user_id")
        }
        if let messageUserName = dictMessageData.value(forKey: "message_user_name")
        {
            dictMessagePreivewData.setValue(messageUserName, forKey: "message_user_name")
        }
        
        if(messageParentId != 0)
        {
            dictMessagePreivewData.setValue(messageParentId, forKey: "parentId")
        }
        if(strMessageUserName != "")
        {
            dictMessagePreivewData.setValue(strMessageUserName, forKey: "messageUserName")
        }
        
        dictMessagePreivewData.setValue(strMessageId, forKey: "id")
        
        dictMessagePreivewData.setValue("Inbox", forKey: "MessagePreviewType")
        
        
        //-- Code to download BG bubble image ---
        
        if(previewBubbleImageURL != "")
        {
          
            previewBubbleImageURL = previewBubbleImageURL.replacingOccurrences(of: " ", with: "%20")
            var imageData : UIImage = UIImage()
            let img : UIImage = AppTheme().getImageFromUrl(previewBubbleImageURL)
            if img != nil
            {
                imageData = img
            }
            else
            {
                previewBubbleImageURL = ""
            }
            if(imageData.size.height > 0)
            {
                bubblePreviewImage = imageData
            }
            else
            {
                previewBubbleImageURL = ""
            }
        }
        //----
        
        /////new logic for bg Music
        if var strBgURL : String = dictMessageData.value(forKey: "chatSound") as? String
        {
              strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
            //AppTheme().killFileOnPath("previewBg.caf")
            
            //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
            AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                
                if(result == "success")
                {
                    self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
                else
                {
                    self.showAlert(data as! String, strTitle: "Error")
                }
                
            })
        }
        
        
        if let aryBgMedia : NSArray = dictMessageData.value(forKey: "bgMedia") as? NSArray
        {
            if let dictBgMedia : NSDictionary = aryBgMedia.object(at: 0) as? NSDictionary
            {
                if var strBgURL : String = dictBgMedia.value(forKey: "mediaUrl") as? String
                {
                      strBgURL = strBgURL.replacingOccurrences(of: " ", with: "%20")
                    //AppTheme().killFileOnPath("previewBg.caf")
                    
                    //dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                    AppTheme().callGetServiceForDownload(strBgURL, fileNameToBeSaved: "previewBg.caf", completion: { (result, data) in
                        
                        if(result == "success")
                        {
                            self.dictMessagePreivewData.setValue("previewBg.caf", forKey: "bloxBubbleAudio")
                            if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                            {
                                if aryMedia.count > 0
                                {
                                    self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                                    self.processResponce()
                                }
                            }
                        }
                        else
                        {
                            self.showAlert(data as! String, strTitle: "Error")
                        }
                        
                    })
                }
                else
                {
                    if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                    {
                        if aryMedia.count > 0
                        {
                            self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                            self.processResponce()
                        }
                    }
                }
            }
            else
            {
                if let aryMedia : NSArray =  dictMessageData.value(forKey: "medias") as? NSArray
                {
                    if aryMedia.count > 0
                    {
                        self.aryMessageMedia = aryMedia.mutableCopy() as! NSMutableArray
                        self.processResponce()
                    }
                }
            }
        }
        else
        {
            if let aryMedia : NSArray =  dictMessageData.value(forKey: "frames") as? NSArray
            {
                if aryMedia.count > 0
                {
                    let result: NSArray = aryMedia.sorted(by: {(int1, int2)  -> Bool in
                        return ((int1 as! NSDictionary).value(forKey: "frameNo") as! Int) < ((int2 as! NSDictionary).value(forKey: "frameNo") as! Int) // It sorted the values and return to the mySortedArray
                    }) as NSArray
                    self.aryMessageMedia = result.mutableCopy() as! NSMutableArray
                    self.processResponce()
                }
            }
        }
        
        
    }
    
    var aryCountIncr : NSInteger = 0
    func processResponce()
    {
        if(aryMessageMedia.count > aryCountIncr) {
            
            if let dictMedia : NSDictionary = aryMessageMedia.object(at: aryCountIncr) as? NSDictionary
            {
                //bloxDetails
                dictBloxDetails = NSMutableDictionary()
                //var strBloxType : String = String()
                if let strCaption : String = dictMedia.value(forKey: "caption") as? String
                {
                    dictBloxDetails.setValue(strCaption, forKey: "caption")
                }
                else
                {
                    dictBloxDetails.setValue("", forKey: "caption")
                }
                
                if var soundURL : String = dictMedia.value(forKey: "soundURL") as? String
                {
                        soundURL = soundURL.replacingOccurrences(of: " ", with: "%20")
                    dictBloxDetails.setValue(soundURL, forKey: "soundURL")
                }
                
                
                if let duration : NSNumber = dictMedia.value(forKey: "animationDuration") as? NSNumber
                {
                    dictBloxDetails.setValue(duration, forKey: "animationDuration")
                }
                else
                {
                    dictBloxDetails.setValue(1.0, forKey: "animationDuration")
                }
                
                if let strType : String = dictMedia.value(forKey: "mediaType") as? String
                {
                    
                    if(strType == GalleryType.Text as String)
                    {
                        strMessageBloxType = GalleryType.Text as String
                        
                        if let txtMessage : String = dictMedia.value(forKey: "text") as? String
                        {
                            self.txtMessage = txtMessage
                            dictBloxDetails.setValue(txtMessage, forKey: "message")
                        }
                        if let textColor : String = dictMedia.value(forKey: "textColor") as? String
                        {
                            self.txtColor = textColor
                            dictBloxDetails.setValue(textColor, forKey: "textColor")
                        }
                        if let txtSize : String = dictMedia.value(forKey: "textSize") as? String
                        {
                            guard let n = NumberFormatter().number(from: txtSize) else { return }
                            self.txtSize = CGFloat(n)
                            dictBloxDetails.setValue(txtSize, forKey: "fontSize")
                        }
                        if let txtStyle : String = dictMedia.value(forKey: "textStyle") as? String
                        {
                            self.txtStyle = txtStyle
                            dictBloxDetails.setValue(txtStyle, forKey: "fontName")
                        }
                        if let textBgMediaId : NSInteger = dictMedia.value(forKey: "textBgMediaId") as? NSInteger
                        {
                            dictBloxDetails.setValue(textBgMediaId, forKey: "textBgMediaId")
                        }
                        loadTextMessage()
                        
                    }
                    
                    
                    if(strType == GalleryType.Image as String)
                    {
                        strMessageBloxType = GalleryType.Image as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                    else if(strType == GalleryType.Video as String)
                    {
                        strMessageBloxType = GalleryType.Video as String
                        /*
                         if let strURL : String = dictMedia.valueForKey("thumb_url") as? String
                         {
                         if let img : UIImage = AppTheme().getImageFromUrl(strURL)
                         {
                         let data : NSData = UIImagePNGRepresentation(img)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         }
                         else
                         {
                         let data : NSData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!
                         dictBloxDetails.setValue(data, forKey: "image")
                         }
                         */
                        if var strVideoURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                             strVideoURL = strVideoURL.replacingOccurrences(of: " ", with: "%20")
                            //AppTheme().killFileOnPath("previewFile\(i).mp4")
                            //dictBloxDetails.setValue("previewFile\(i).mp4", forKey: "video")
                            var strImageThumbURL : String = String()
                            if var strURL : String = dictMedia.value(forKey: "thumbUrl") as? String {
                                   strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                                strImageThumbURL = strURL
                            }
                            loadVideoDataForURL(strVideoURL, fileName: "previewFile\(aryCountIncr).mp4", strURL: strImageThumbURL)
                            /*
                             AppTheme().callGetServiceForDownload(strVideoURL, fileNameToBeSaved: "previewFile\(i).mp4", completion: { (result, data) in
                             if(result == "success")
                             {
                             self.dictBloxDetails.setValue(data, forKey: "video")
                             }
                             })
                             */
                        } else {
                            aryCountIncr += 1
                            processResponce()
                        }
                        
                    }else if(strType == GalleryType.Sound as String){
                        strMessageBloxType = GalleryType.Sound as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            loadImageFromURL(strURL)
//                            self.playAudio(strURL)
//                            if var strMp3URL : String = dictMedia.value(forKey: "soundURL") as? String{
//                                strMp3URL = strMp3URL.replacingOccurrences(of: " ", with: "%20")
//                                self.playAudio(strMp3URL)
//                            }
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    } else if(strType == GalleryType.Gif as String){
                        strMessageBloxType = GalleryType.Gif as String
                        if var strURL : String = dictMedia.value(forKey: "mediaUrl") as? String
                        {
                            strURL = strURL.replacingOccurrences(of: " ", with: "%20")
                            dictBloxDetails.setValue(strURL, forKey: "image")
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
                            
                            aryMessagePreivewData.add(dictTemp)
                            aryCountIncr += 1
                            processResponce()
                        }else {
                            aryCountIncr += 1
                            processResponce()
                        }
                    }
                }
            }
        }
        else
        {
            aryCountIncr = 0
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PreviewBloxVC") as! PreviewBloxVC
            vc.aryUserMessageData = self.aryMessagePreivewData
            vc.dictUserMessageData = self.dictUserMessageData
            vc.inbloxData = self.inbloxData
            vc.chatId = self.chatID
            vc.previewKind = "Bloxcast"
            vc.isSentByMe = self.isSentByMe
            vc.inputImageBgID = self.inputImageBgID
//            vc.bgImageName = self.bgImageName
            vc.strBgAudioPath = self.inputSoundBgID
            vc.bloxSubject = self.bloxSubject
            vc.user_name = self.bloxcastUsername
            vc.user_image = self.Blox_user_image
            vc.bloxDate = self.BloxDate
//            vc.ratingFromBlox = self.bloxRating
            vc.userID_for_profile = self.userID_for_profile
            //    vc.imgViewBackground.image = self.imgBackground.image
            
            let rootViewController = self.window!.rootViewController as! UINavigationController
            rootViewController.pushViewController(vc, animated: true)
            
            //self.perform(#selector(MessageConversationVC.allSetUpWithPreviewData), with: nil, afterDelay: 1.0)
        }
        
    }
    
    
    
    func loadTextMessage() {
        
        dictBloxDetails.setValue(UIColor.white, forKey: "backgroundColor")
        let imageData : Data = screenShot().jpegData(compressionQuality: 0.8)!//UIImageJPEGRepresentation(screenShot(), 0.8)!
        
        dictBloxDetails.setValue(imageData, forKey: "image")
        
        let dictTemp : NSMutableDictionary = NSMutableDictionary()
        dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
        dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
        
        aryMessagePreivewData.add(dictTemp)
        aryCountIncr += 1
        processResponce()
    }
    
    func loadImageFromURL(_ strURL:String)
    {
        var imageData : Data = Data()
        
        let fileName = URL(fileURLWithPath: strURL).pathExtension
        if fileName == "gif"{
            let urlNew:String = WEB_URL.BaseURL + strURL//.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let imageData1 = try? Data(contentsOf: URL(string: urlNew)!)
            {
                let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
                print("isAnimated: \(isImageAnimated)")
                imageData = imageData1
            } else {
                
            }
        } else {
            let img : UIImage = AppTheme().getImageFromUrl(WEB_URL.BaseURL + strURL)
            //        if let img : UIImage = AppTheme().getImageFromUrl("http://103.76.248.170:3001/" + strURL)
            if img != nil
            {
                imageData = img.pngData()! // UIImagePNGRepresentation(img)! //
            } else {
                imageData = UIImage(named: "SingleBlox")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)! //
            }
        }
        let isImageAnimated = Utilities.shared.isAnimatedImage(imageData)//isAnimatedImage(data)
        print("isAnimated: \(isImageAnimated)")
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
            let dictTemp : NSMutableDictionary = NSMutableDictionary()
            dictTemp.setValue(dictBloxDetails, forKey: "bloxDetails")
            dictTemp.setValue(strMessageBloxType, forKey: "bloxType")
            
            aryMessagePreivewData.add(dictTemp)
            aryCountIncr += 1
            processResponce()
        }
    }
    
    func loadVideoDataForURL(_ strVideoURL:String, fileName: String, strURL : String)
    {
        var imageData : Data = Data()
//        if let img : UIImage = AppTheme().getImageFromUrl(strURL)  //commented on 25Jan
        let img : UIImage = AppTheme().getImageFromUrl(strVideoURL)
        if img != nil
        {
            imageData = img.pngData()! // UIImagePNGRepresentation(img)!//
        }
        else
        {
//            imageData = UIImagePNGRepresentation(UIImage(named: "SingleBlox")!)!  //commented on 25Jan
            imageData = UIImage(named: "signup_background")!.pngData()! //UIImagePNGRepresentation(UIImage(named: "signup_background")!)!//
        }
        
        if(imageData.count > 0)
        {
            dictBloxDetails.setValue(imageData, forKey: "image")
        }
        
        let priority = DispatchQueue.GlobalQueuePriority.default
        DispatchQueue.global(priority: priority).async
            {
                // do some task
                let url = WEB_URL.BaseURL + strVideoURL//"http://103.76.248.170:3001/"
//            let url = "http://103.76.248.170:3001/" + strVideoURL
                AppTheme().callGetServiceForDownload(url, fileNameToBeSaved: fileName, completion: { (result, data) in
                    if(result == "success")
                    {
                        DispatchQueue.main.async {
                            // update some UI
                            self.dictBloxDetails.setValue(data, forKey: "video")
                            //  self.dictBloxDetails.setValue(64, forKey: "id")
                            
                            let dictTemp : NSMutableDictionary = NSMutableDictionary()
                            dictTemp.setValue(self.dictBloxDetails, forKey: "bloxDetails")
                            dictTemp.setValue("Video", forKey: "bloxType")
                            
                            self.aryMessagePreivewData.add(dictTemp)
                            self.aryCountIncr += 1
                            self.processResponce()
                        }
                    } else {
                        self.showAlert(data as! String, strTitle: "Error")
                    }
                })
        }
    }
    func screenShot() -> UIImage {
        var DynamicView=UILabel(frame: CGRect(x:68, y:0, width:240, height:240))
        DynamicView.backgroundColor=UIColor.white
        //DynamicView.layer.cornerRadius=25
        //DynamicView.layer.borderWidth=2
        //self.view.addSubview(DynamicView)
        DynamicView.text = self.txtMessage
        DynamicView.textAlignment = NSTextAlignment.center
        DynamicView.font = UIFont(name:self.txtStyle, size: self.txtSize)
        let clr = Utilities.shared.convertHexToUIColor(hexColor: self.txtColor)
        DynamicView.textColor = clr
        //DynamicView.font = UIFont.systemFont(ofSize: self.txtSize)
        // DynamicView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DynamicView.lineBreakMode = .byWordWrapping
        DynamicView.numberOfLines = 0
        
        var frameRect : CGRect = DynamicView.frame
        frameRect.origin.x += 2
        frameRect.origin.y += 2
        frameRect.size.width -= 4
        frameRect.size.height -= 4
        
        //var textView=UITextView(frame: CGRect(x:68, y:0, width:240, height:240))
        
        
        DynamicView.layer.borderColor = UIColor.clear.cgColor
        UIGraphicsBeginImageContextWithOptions(DynamicView.bounds.size, DynamicView.isOpaque, 0.0)
        //UIGraphicsBeginImageContext(viewSquareBlox.frame.size)
        DynamicView.layer.render(in: UIGraphicsGetCurrentContext()!)
        var image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        DynamicView.layer.borderColor = AppTheme().themeColorBlue.cgColor
        // txtViewMessage.tintColor = color
        
        //let imageData : NSData = UIImageJPEGRepresentation(image, 1.0)!
        //image = UIImage(data: imageData)
        
        image = Utilities.shared.resizeImage(image!, newSize: image!.size)
        
        //******
        return image!
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo

    if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID3: \(messageID)")
    }
    // With swizzling disabled you must let Messaging know about the message, for Analytics
     Messaging.messaging().appDidReceiveMessage(userInfo)

    // ...

    // Print full message.
    print(userInfo)

    // Change this to your preferred presentation option
    completionHandler([[.alert, .sound]])
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo

    // ...
    if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID4: \(messageID)")
    }

    // With swizzling disabled you must let Messaging know about the message, for Analytics
     Messaging.messaging().appDidReceiveMessage(userInfo)

    // Print full message.
    print(userInfo)
    
    if userInfo["gcm.notification.type"] != nil {
     let type = userInfo["gcm.notification.type"]! as! String
        let rootViewController = self.window!.rootViewController as! UINavigationController
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        switch type {
        case "rating":
//            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//            vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
//            vc.isSelfProfile = true
//            rootViewController.pushViewController(vc, animated: true)
            self.messageID = Int((userInfo["message_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            self.chatID = Int((userInfo["chat_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
//            self.bloxSubject = notif.subject!
            
            let user = AppSharedData.sharedInstance.currentUser
//           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
            self.Blox_user_image = user?.user_image ?? ""
            self.bloxcastUsername = user?.displayName  ?? ""
//            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = user?.userid ?? 0//notif.userid!
        case "comment":
//            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//            vc.user_id = AppSharedData.sharedInstance.currentUser?.userid ?? 0
//            vc.isSelfProfile = true
//            rootViewController.pushViewController(vc, animated: true)
        
            self.messageID = Int((userInfo["message_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            self.chatID = Int((userInfo["chat_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
//            self.bloxSubject = notif.subject!
            
            let user = AppSharedData.sharedInstance.currentUser
//           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
            self.Blox_user_image = user?.user_image ?? ""
            self.bloxcastUsername = user?.displayName  ?? ""
//            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = user?.userid ?? 0//notif.userid!
        case "follow":
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.user_id = userInfo["userid"]! as? Int
            vc.isSelfProfile = false
            rootViewController.pushViewController(vc, animated: true)
        case "sendbloxcastToUser":
//            let vc = storyboard.instantiateViewController(withIdentifier: "BloxCastViewController") as! BloxCastViewController
//            rootViewController.pushViewController(vc, animated: true)
        
            self.messageID = Int((userInfo["message_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            self.chatID = Int((userInfo["chat_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
//            self.bloxSubject = notif.subject!
            
//            let user = AppSharedData.sharedInstance.currentUser
////           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
//            self.Blox_user_image = user?.user_image ?? ""
//            self.bloxcastUsername = user?.displayName  ?? ""
//            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = Int((userInfo["userid"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0 //user?.userid ?? 0//notif.userid!
        case "sendbloxcastToSubscriber":
//            let vc = storyboard.instantiateViewController(withIdentifier: "BloxCastViewController") as! BloxCastViewController
//            rootViewController.pushViewController(vc, animated: true)
        
            self.messageID = Int((userInfo["message_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
            self.chatID = Int((userInfo["chat_id"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0
//            self.bloxSubject = notif.subject!
            
//            let user = AppSharedData.sharedInstance.currentUser
////           let imgURL = WEB_URL.BaseURL + (user?.user_image ?? "")
//            self.Blox_user_image = user?.user_image ?? ""
//            self.bloxcastUsername = user?.displayName  ?? ""
//            self.BloxDate =  Utilities.shared.getChangedDate(date: notif.created_at!)
            self.aryMessagePreivewData.removeAllObjects()
            self.serverCallForPreview()
            self.userID_for_profile = Int((userInfo["userid"] as! String).trimmingCharacters(in: .whitespacesAndNewlines)) ?? 0//(userInfo["userid"]! as? Int)!//user?.userid ?? 0//notif.userid!
        default:
            let vc = storyboard.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
            rootViewController.pushViewController(vc, animated: true)
        }
    }
    
    

    completionHandler()
  }
}
