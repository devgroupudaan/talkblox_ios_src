//
//  viewControllerExtention.swift
//  TalkBloxNew
//
//  Created by Codezilla-7 on 5/8/19.
//  Copyright © 2019 iOSDeveloper. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func popToHome()  {
        let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        if navController != nil{
            for controller in navController!.viewControllers as Array {
//                if controller.isKind(of: HomeVC.classForCoder()) {
//                    navController!.popToViewController(controller, animated: true)
//                    break
//                }   ///commented on 12feb
                ///added on 12feb
//                if controller.isKind(of: BloxHomeVC.classForCoder()) {
//                    navController!.popToViewController(controller, animated: true)
//                    break
//                }
                
                if controller.isKind(of: TabViewController.classForCoder()) {
                    navController!.popToViewController(controller, animated: true)
                    break
                }
                /////////////////////////////
            }
        }
    }
}

