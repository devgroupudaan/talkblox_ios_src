//
//  UITextField+placeholder.swift
//  CCWD
//
//  Created by Codezilla-7 on 11/12/18.
//  Copyright © 2018 Codezilla-7. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func setplaceHolderColor(color:UIColor)  {
       self.attributedPlaceholder =  NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
        }
    }

