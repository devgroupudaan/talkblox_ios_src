//
//  NotificationService.swift
//  TalkBloxNewNotificationService
//
//  Created by Macbook Pro (L43) on 14/06/21.
//  Copyright © 2021 iOSDeveloper. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        print("called")
//        if let bestAttemptContent = bestAttemptContent {
//            // Modify the notification content here...
//            bestAttemptContent.title = "\(bestAttemptContent.title) [mahendra]"
//
//            contentHandler(bestAttemptContent)
//        }
        

               defer {
                   contentHandler(bestAttemptContent ?? request.content)
               }

               guard let attachment = request.attachment else { return }

               bestAttemptContent?.attachments = [attachment]
        
        
//        if let bestAttemptContent = bestAttemptContent {
//            if let urlString = request.content.userInfo["image"] as? String, let fileUrl = URL(string: urlString) {
//                 // Modify the notification content here...
//                bestAttemptContent.title = "Topic:\(bestAttemptContent.title)"
//                bestAttemptContent.subtitle = "Short:\(bestAttemptContent.subtitle)"
//                bestAttemptContent.body = "Description:\(bestAttemptContent.body)"
//
//                URLSession.shared.downloadTask(with: fileUrl) { (location, response, error) in
//                    if let location = location {
//                        // Move temporary file to remove .tmp extension
//                            let tmpDirectory = NSTemporaryDirectory()
//                        let tmpFile = "file:".appending(tmpDirectory).appending(fileUrl.lastPathComponent)
//                        let tmpUrl = URL(string: tmpFile)!
//                        try? FileManager.default.moveItem(at: location, to: tmpUrl)
//                        // Add the attachment to the notification content
//                        if let attachment = try? UNNotificationAttachment(identifier: fileUrl.lastPathComponent, url: tmpUrl) {
//                            self.bestAttemptContent?.attachments = [attachment]
//                        }}
//                    // Serve the notification content
//                    contentHandler(self.bestAttemptContent!)
//                    }.resume()
//            }
//        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}


extension UNNotificationRequest {
    var attachment: UNNotificationAttachment? {
        guard let attachmentURL = content.userInfo["image"] as? String, let imageData = try? Data(contentsOf: URL(string: attachmentURL)!) else {
            return nil
        }
        print("attachmentURL",attachmentURL)
        var extensionStr = ""
        if attachmentURL.contains(".gif"){
            extensionStr = ".gif"
        } else if attachmentURL.contains(".mp4"){
            extensionStr = ".mp4"
        } else {
            extensionStr = ".jpg"
        }
        return try? UNNotificationAttachment(data: imageData, options: nil, extStr: extensionStr)
    }
}

//extension UNNotificationAttachment {
//
//    convenience init(data: Data, options: [NSObject: AnyObject]?) throws {
//        let fileManager = FileManager.default
//        let temporaryFolderName = ProcessInfo.processInfo.globallyUniqueString
//        let temporaryFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(temporaryFolderName, isDirectory: true)
//
//        try fileManager.createDirectory(at: temporaryFolderURL, withIntermediateDirectories: true, attributes: nil)
//        let imageFileIdentifier = UUID().uuidString + ".mp4"
//        let fileURL = temporaryFolderURL.appendingPathComponent(imageFileIdentifier)
//        try data.write(to: fileURL)
//        try self.init(identifier: imageFileIdentifier, url: fileURL, options: options)
//    }
//}

extension UNNotificationAttachment {
    convenience init(data: Data, options: [NSObject: AnyObject]?, extStr: String) throws {
    let fileManager = FileManager.default
    let temporaryFolderName = ProcessInfo.processInfo.globallyUniqueString
    let temporaryFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(temporaryFolderName, isDirectory: true)
    try fileManager.createDirectory(at: temporaryFolderURL, withIntermediateDirectories: true, attributes: nil)
    let imageFileIdentifier = UUID().uuidString + extStr//".mp4"
    let fileURL = temporaryFolderURL.appendingPathComponent(imageFileIdentifier)
    try data.write(to: fileURL, options: .completeFileProtectionUntilFirstUserAuthentication)
    try self.init(identifier: imageFileIdentifier, url: fileURL, options: options)
  }
}
